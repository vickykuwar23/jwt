<?php
/*** files needed to connect to database and class***/
define("ROOT_PATH", realpath(__DIR__ . '/../../..'));
include_once ROOT_PATH.'/constant.php';

include_once CONFIG_PATH.'functions.php';
include_once CONFIG_PATH.'database.php';
include_once RESOURCE_PATH.'user/user.php';
include_once RESOURCE_PATH.'logs/logs.php';
include_once RESOURCE_PATH.'auth/auth.php';

include_once LIB_PATH.'eCos/eCos.php';

// Required headers
requireHeaders();
 
/*** get database connection ***/
$database 	= new Database();
$db 		= $database->getConnection();
$mdb 		= $database->getMongoConnection();
 
// instantiate product object
$user  = new User($db);
$logs  = new Logs($mdb);
$auth  = new Auth();

// Get Request Method 
$request_method		= get_request_method();

switch($request_method)
{
	case 'POST':
		
		// get posted data
		$data 	= get_request_body();
		
		if($data['status'] == true){

		// set user property values
		$user->FirstName = isset($data['requestData']['FirstName']) ? $data['requestData']['FirstName'] : "";
		$user->LastName  = isset($data['requestData']['LastName']) ? $data['requestData']['LastName'] : "";
		$user->Password  = isset($data['requestData']['Password']) ? $data['requestData']['Password'] : "";	
		$user->EmailAddress = isset($data['requestData']['EmailAddress']) ? $data['requestData']['EmailAddress'] : "";	
			
		$error = array();
		
			if($user->FirstName == ""){
				
				$error[] = "FirstName is required.";
				
			} 
			if($user->LastName == ""){
				
				$error[]= "LastName is required.";
				
			} 
			if($user->EmailAddress == ""){
				
				$error[]= "EmailAddress is required.";
				
			} else if(!filter_var($user->EmailAddress, FILTER_VALIDATE_EMAIL)){
				
				$error[]= "Please enter valid EmailAddress.";
				
			}
			if($user->Password == ""){
				
				$error[] = "Password is required.";
				
			}		
					
			if(count($error) > 0){
				
					$errorMsg = implode(", ", $error);
					
					// tell the user login failed
					$resArr = array("status" => "0", "message" => $errorMsg);
					
					 //Add log data
					 $logs->Module 		= "User";
					 $logs->Action		= "create";
					 $logs->Request		= json_encode($data);
					 $logs->Response	= json_encode($resArr);
					 $logs->UserId		= "";
					 $logs->UserType	= "User";
					 $logs->AddLog();
						 
					// tell the user login failed
					 response($resArr, 400);
				
			} else {				
				
				// set user property values
				$email_exists 		= 	$user->emailExists();
			
				if(!$email_exists){	// Email Check if not exist success
					
					// Add user
						if($user->create()){
							
							// display message: user was created
							$resArr = array("status" => "1", "message" => "User created successfully.");
							
							//Add log data
							 $logs->Module 		= "User";
							 $logs->Action		= "create";
							 $logs->Request		= json_encode($data);
							 $logs->Response	= json_encode($resArr);
							 $logs->UserId		= "";
							 $logs->UserType	= "User";
							 $logs->AddLog();
							
							// display message: user was created
							 response($resArr, 200);
							
						} else { // message if unable to create user
						
							// display message: unable to create user
							$resArr = array("status" => "0", "message" => "Unable to create user.");
							
							//Add log data
							 $logs->Module 		= "User";
							 $logs->Action		= "create";
							 $logs->Request		= json_encode($data);
							 $logs->Response	= json_encode($resArr);
							 $logs->UserId		= "";
							 $logs->UserType	= "User";
							 $logs->AddLog();
							
							 // display message: unable to create user
							 response($resArr, 400);
							
						}
					
				} else { // Email Already exist error
			 
						// tell the user login failed
						$resArr = array("status" => "0", "message" => "User already exist.");
						 
						 //Add log data	
						 $logs->Module 		= "User";
						 $logs->Action		= "create";
						 $logs->Request		= json_encode($data);
						 $logs->Response	= json_encode($resArr);
						 $logs->UserId		= "";
						 $logs->UserType	= "User";
						 $logs->AddLog();
						 
						 // tell the user login failed
						 response($resArr, 401);
				}	
				
			}
			
		} else {
			
			// display message: unable to create user
			$resArr = array("status" => "0", "message" => $data["requestData"]);
			
			//Add log data	
			 $logs->Module 		= "User";
			 $logs->Action		= "create";
			 $logs->Request		= "";
			 $logs->Response	= json_encode($resArr);
			 $logs->UserId		= "";
			 $logs->UserType	= "";
			 $logs->AddLog();
				
			response($resArr, 400);
			
		}
		
		break;
		
	case 'PUT':		
		
		$bearerToken = getBearerToken();
		
		$access_token = isset($bearerToken) ? $bearerToken : "";
		
		// if jwt is not empty
		if($access_token){			
			
			// decode jwt
			$decoded = $auth->validateToken($access_token);
			
			$data 	= array();
			$getArr = array();
			$reqArr = array();			
			
			// if decode succeed, show user details			
			if($decoded['status'] == true){	

				$updateUser 	= false;
				$userID 		= $decoded['TokenData']->data->UserId; // Decode ID
				$UserName 		= $decoded['TokenData']->data->UserName; // Decode UserName
				$is_admin 		= isset($decoded['TokenData']->data->admin) ? $decoded['TokenData']->data->admin : false;
				
				if($is_admin == true){ //If admin user	
					
					$tokenArr = array("UserId" => (int)$userID, "UserName" => $UserName, "admin" => true);	
					$logs->UserType = "Admin";
					
				} else { // Else regular user
					
					$tokenArr = array("UserId" => (int)$userID, "UserName" => $UserName);
					$logs->UserType = "User";
					
				}
				
				$getArr = get_url_data();
					
				//Get ID From URL		
				$user->UserId = isset($getArr['id']) ? intval($getArr['id']) : "";
				
				
				if($user->UserId == ""){
							
					$error[] = "UserId is required.";
					
				}
				
				// get posted data
				$data = get_request_body();
				
				//print_r($data);exit;
				
				if($data["status"] == true){
					
					$error = array();
					
					// set user property values				
					$user->FirstName 		= isset($data['requestData']['FirstName']) ? $data['requestData']['FirstName'] : "";
					$user->LastName 		= isset($data['requestData']['LastName']) ? $data['requestData']['LastName'] : "";
					$user->Password 		= isset($data['requestData']['Password']) ? $data['requestData']['Password'] : "";		
					$user->IsActive 		= isset($data['requestData']['IsActive']) ? $data['requestData']['IsActive'] : "";
					$user->imgName 			= isset($data['requestData']['imgName']) ? $data['requestData']['imgName'] : "";
					$user->ProfileImage 	= isset($data['requestData']['ProfileImage']) ? $data['requestData']['ProfileImage'] : "";
					$user->AttachFilename 	= isset($data['requestData']['AttachFilename']) ? $data['requestData']['AttachFilename'] : "";
					$user->Attachment		= isset($data['requestData']['Attachment']) ? $data['requestData']['Attachment'] : "";
					
					
					
					$updateStatus = "";
					
					if(isset($data['requestData']['IsActive'])){
							
							$updateStatus 		= "UpdateStatus";
							$user->IsActive 	= $data['requestData']['IsActive'];
							
							if($user->IsActive == ""){
						
									$error[] = "IsActive is required.";
									
							} 
						
					} else {
							
							$updateStatus = "UpdateProfile";
							
							if($user->FirstName == ""){
						
								$error[] = "FirstName is required.";
						
							} 
							if($user->LastName == ""){
								
								$error[]= "LastName is required.";
								
							} 
					}
					
					// File Attachment/Upload Code
					if($user->Attachment!= "" && $user->AttachFilename!=""){						
									
						$AttachFilename = $user->AttachFilename;
						$file_ext 		= explode('.',$AttachFilename);
						$extension 		= strtolower( end($file_ext));						
						
						$allowedExts 	= array("pdf", "docx");

						// for create physical file.
						$encoded_file 	= $user->Attachment;
						
						$replaceStr  	= str_replace(" ", "+", $encoded_file);
						
						$decoded_file1 	= base64_decode($replaceStr);
						
						// Min size for resume size: 1 KB
						// Max size for resume size: upto 2 MB	
						
						$size 		= strlen($decoded_file1);
						$size_kb 	= floor($size / 1024);	
						
						if(($extension != "pdf" || $extension!= "PDF") && ($extension != "docx" || $extension != "DOCX") && !in_array($extension, $allowedExts)){
							
							$error[] = "Please attch file with .pdf, .docx format.";
							
						} else if($size_kb < ATTACHMENT_MIN_SIZE){
							
							$error[] = "Upload document size must be more than 1kb.";
							
						} else if($size_kb > ATTACHMENT_MAX_SIZE){
							
							$error[] = "Upload document size must be less than equal to 2mb.";
							
						} else {
							
							$AttachFilename = 'myAttachment-'.time().'.'.$extension;
							$user->AttachFilename = $AttachFilename;
							
							//file_put_contents(UPLOAD_FILE.$file_ext[0].'.'.$file_ext[1], $decoded_file1);
							
							//$filePath = UPLOAD_FILE.$AttachFilename;
							
							//$fileResponse = convertBase64ToFile($filePath, $decoded_file1);
							
							//data:image/png;base64,
							// code to upload doc to eCos
							$ecos = new eCos();
							$ecos->getToken();
							
							$container_name = CONTAINER_NAME;
							$folder_name 	= ATTACHMENT_FOLDER;
							$file_path 		= $replaceStr;							
							$file_name 		= $AttachFilename;
							$fileType		= getContentType($extension);
							$file_content	= 'data:'.$fileType.';base64,'.$file_path;
							
							$fileResponse 	= $ecos->uploadObject($container_name, $folder_name, $file_content, $file_name);
							// eof code to upload doc to eCos
							
							if($fileResponse == false){
								
								$error[] = "Attachement not uploaded.";
								
							}
							
							/*$pdf = fopen($filePath, 'w');
							if($pdf){
								$content = fwrite($pdf, $decoded_file1);
								
								if(!$content){
									$error[] = "Attachement not uploaded.";
								} 								 
							} else {
								$error[] = "Attachement not uploaded.";
							}
							fclose ($pdf);*/
						}	

												
					} // Attachment end
					
					// Profile Upload Code
					if($user->imgName!= "" && $user->ProfileImage!=""){
						
						$allowedExts = array("jpeg", "jpg", "png");
						$imgName 	 = $user->imgName;
						$img_ext 	 = explode('.', $imgName );
						$extension   = strtolower( end($img_ext));
						$fileType	 = getContentType($extension);
						// for create physical file.
						$encoded_file = $user->ProfileImage;
						
						//$encoded_file = str_replace('data:image/png;base64,', '', $encoded_file);
						$encoded_file = str_replace(" ", "+", $encoded_file);
						//$encoded_file = str_replace('data:image/jpeg;base64,', '', $encoded_file);
						//$encoded_file = str_replace('data:image/gif;base64,', '', $encoded_file);	

					   // Min size for Image size : 5 KB ,Max size for Image size : 500 					
						$decoded_file1 = base64_decode($encoded_file);
						
						$size = strlen($decoded_file1);
						$size_kb = floor($size / 1024);
							
					if(($extension != "jpg" || $extension!= "JPG") && ($extension != "jpeg" || $extension != "JPEG") && ($extension != "png" || $extension != "PNG") && !in_array($extension, $allowedExts)){		
							
							$error[] = "Please upload file with .jpg, .jpeg, .png format.";
							
						} else if($size_kb < PROFILE_MIN_SIZE){
							
							$error[] = "Upload Image size must be more than 5kb.";
							
						} else if($size_kb > PROFILE_MAX_SIZE){
							
							$error[] = "Upload Image size must be less than equal to 500kb.";
							
						} else {
							
							$imgName = 'myImage-'.time().'.'.$extension;
							$user->imgName = $imgName;
							
							//Now you can copy the uploaded file to your server.					
							//file_put_contents(UPLOAD_IMG.$img_ext[0].'.'.$img_ext[1], $decoded_file1);
						
							// Image Destination 
							//$path 	= UPLOAD_IMG;
							
							//$imageResponse = convertBase64ToImage($path, $imgName, $decoded_file1);
							
							// code to upload doc to eCos
							$ecos = new eCos();
							$ecos->getToken();
							
							$container_name = CONTAINER_NAME;
							$folder_name 	= PROFILEIMG_FOLDER;							
							$file_name 		= $imgName;
							$fileType		= getContentType($extension);
							$file_path 		= 'data:'.$fileType.';base64,'.$encoded_file;
							
							$imageResponse 	= $ecos->uploadObject($container_name, $folder_name, $file_path, $file_name);
							// eof code to upload doc to eCos
							
							if($imageResponse == false){
								
								$error[] = "Profile Image not uploaded.";
								
							}
							
						} // extension end
						
					} // profile Image end	
							
				
					if(count($error) > 0){
				
							$errorMsg = implode(", ", $error);
							
							// tell the user login failed
							$resArr = array("status" => "0", "message" => $errorMsg);
							
							// Merge Array Post And GET Values
							$reqArr = array_merge($data, $getArr);	
							 
							 //Add log data
							 $logs->Module 		= "User";
							 $logs->Action		= "update";
							 $logs->Request		= json_encode($reqArr);
							 $logs->Response	= json_encode($resArr);
							 $logs->UserId		= (int)$userID;
							 $logs->AddLog();
							 
							response($resArr, 400);
							
					} else {
						
						if($is_admin == true || ($userID == $user->UserId)){ //If admin user
							
							$userExist = $user->userExists($user->UserId);
							
							if($userExist == true){
								
								$updateUser = $user->update($updateStatus);
								
							} else {
									
									// generate jwt
									$access_token = $auth->generateToken($tokenArr);
									
									// response in json format
									$resArr = array(
													"status" 		=> "1", 
													"message" 		=> "User does not exist.",
													"access_token" 	=> $access_token
												);		
										
									// Merge Array Post And GET Values
									$reqArr = array_merge($data, $getArr);		
												
										//Add log data
										 $logs->Module 		= "User";
										 $logs->Action		= "update";
										 $logs->Request		= json_encode($reqArr);
										 $logs->Response	= json_encode($resArr);
										 $logs->UserId		= (int)$userID;						 
										 $logs->AddLog();		
									 
									response($resArr, 200);
								
							}
							
						/*elseif($userID == $user->UserId): // Else regular user
							$userList = $user->getAllUser($id);*/
							
						}
						
						// create the product
						if($updateUser){
							
							// generate jwt
							$access_token = $auth->generateToken($tokenArr);
							 
							// response in json format
							$resArr = array(
											"status" 		=> "1", 
											"message" 		=> "User was successfully updated.",
											"access_token" 	=> $access_token
										);		
								
							// Merge Array Post And GET Values
							$reqArr = array_merge($data, $getArr);		
										
								//Add log data
								 $logs->Module 		= "User";
								 $logs->Action		= "update";
								 $logs->Request		= json_encode($reqArr);
								 $logs->Response	= json_encode($resArr);
								 $logs->UserId		= (int)$userID;						 
								 $logs->AddLog();		
							 
							response($resArr, 200);
						}
						 
						// message if unable to update user
						else {
							
							// generate jwt
							$access_token = $auth->generateToken($tokenArr);
						 
							// show error message
							$resArr = array("status" => "1", 
											"message" => "Unable to update user.",
											"access_token" 	=> $access_token);
							 
							 // Merge Array Post And GET Values
							$reqArr = array_merge($data, $getArr);
							 
							 //Add log data
							 $logs->Module 		= "User";
							 $logs->Action		= "update";
							 $logs->Request		= json_encode($reqArr);
							 $logs->Response	= json_encode($resArr);
							 $logs->UserId		= (int)$userID;						 
							 $logs->AddLog();
							 
							response($resArr, 401);
						}
						
					}
					
				} else {
					
					// show error message
					$resArr = array("status" => "0", "message" => $data["requestData"]);
				
					// Merge Array Post And GET Values
					$reqArr = array_merge($data, $getArr);	
				
					//Add log data
					 $logs->Module 		= "User";
					 $logs->Action		= "update";
					 $logs->Request		= json_encode($reqArr);
					 $logs->Response	= json_encode($resArr);
					 $logs->UserId		= "";
					 $logs->UserType    = ""; 	
					 $logs->AddLog();
				
					response($resArr, 400);
					
				}				
				
			} else {
				
				// show error message
				$resArr = array(
					"status" 	=> "0", 
					"message" 	=> "Invalid Authorization. Error : ". $decoded["TokenData"]
				);
				
				// Merge Array Post And GET Values
				$reqArr = array_merge($data, $getArr);
				
				//Add log data
				 $logs->Module 		= "User";
				 $logs->Action		= "update";
				 $logs->Request		= json_encode($reqArr);
				 $logs->Response	= json_encode($resArr);
				 $logs->UserId		= "";
				 $logs->UserType    = "";	
				 $logs->AddLog();
				
				response($resArr, 401);
			}
			
		}  else { // show error message if jwt is empty
		 
			// tell the user access denied
			$resArr = array("status" => "0", "message" => "Access denied.");
			
			//Add log data
			 $logs->Module 		= "User";
			 $logs->Action		= "update";
			 $logs->Request		= "";
			 $logs->Response	= json_encode($resArr);
			 $logs->UserId		= "";
			 $logs->UserType	= "";			 
			 $logs->AddLog();
			
			// display message: unable to create user
			response($resArr, 401);
		}		
	
		break;
		
	case 'GET':	
	
		$bearerToken = getBearerToken();
		
		$access_token = isset($bearerToken) ? $bearerToken : "";
		
		  /*$userList = $user->get("183");
			
			$attachment = $userList[0]['Attachment'];
		
			// Attachment Destination 
			$path 	= UPLOAD_FILE.$attachment;
			
			$fileType = explode(".", $attachment);
			//print_r($fileType);exit;
			
			$attachFile = file_get_contents($path);
			
			$finfo = finfo_open();
			$mime_type = finfo_buffer($finfo, $attachFile, FILEINFO_MIME_TYPE);
			header("Content-Type: " . $mime_type);			
			header('Content-Disposition: attachment; filename="'.$attachment.'"');	// force file download
			
			// alternatively specify an URL, if PHP settings allow
			echo $attachFile;exit;*/
		
		
		// if jwt is not empty
		if($access_token){

			// decode jwt		
			$decoded  = $auth->validateToken($access_token);
			
			$getArr = array();
			
			// if decode succeed, show user details
			if($decoded["status"] == true) {
				
				$userID   = $decoded["TokenData"]->data->UserId;
				$UserName = $decoded["TokenData"]->data->UserName;
				$is_admin = isset($decoded["TokenData"]->data->admin) ? $decoded["TokenData"]->data->admin : false;
				
				$getArr = get_url_data();					
				
				$id 	= isset($getArr['id']) ? intval($getArr['id']) : 0;
				$type   = isset($getArr['type']) ? $getArr['type'] : "";
				
				if($is_admin == true){ //If admin user	
									
					$logs->UserType = "Admin";
					$tokenArr = array("UserId" => (int)$userID, "UserName" => $UserName, "admin" => true);
					
				} else { // Else regular user
					
					$logs->UserType = "User";
					$tokenArr = array("UserId" => (int)$userID, "UserName" => $UserName);
					
				}	
				
				if($id > 0 && $type == "profileimg")
				{
					
					$userExist = $user->userExists($id);
					
					if($userExist == true)
					{
						
						$userList = $user->get($id);
								
						$file_name = $userList[0]['ProfileImage'];
						
						if($file_name!= ""){
							
							// Image Destination 
							/*$path 	= UPLOAD_IMG.$file_name;						
							$profilePic = file_get_contents($path);
							$finfo = finfo_open();
							$mime_type = finfo_buffer($finfo, $profilePic, FILEINFO_MIME_TYPE);*/
							
							// code to download doc from eCos
							$ecos = new eCos();
							$ecos->getToken();
							
							$container_name = CONTAINER_NAME;
							$folder_name 	= PROFILEIMG_FOLDER;
							
							$downloadObject_resp = $ecos->downloadObject($container_name, $folder_name, $file_name);
							// eof code to download doc from eCos
							
							// get file content type
							 $finfo = finfo_open();
							 $mime_type = finfo_buffer($finfo, $downloadObject_resp, FILEINFO_MIME_TYPE);
							 
							 /*$profileArr = array(
									"profileImage" => array(
										"name" => $file_name,
										"type" => $mime_type,
										"data" => base64_encode($downloadObject_resp)
									));*/
							 
							if(strlen($downloadObject_resp) > 0)
							{
								$profileArr = array(
									"profileImage" => array(
										"name" => $file_name,
										"type" => $mime_type,
										"data" => base64_encode($downloadObject_resp)
									));
									
							} else {
								
								$profileArr = array(
									"profileImage" => array(
										"name" => $file_name,
										"type" => $mime_type,
										"data" => 'No Profile Picture found.'
									));
							}
						
							// generate jwt
							$access_token = $auth->generateToken($tokenArr);			
								
							$resArr = array(
									"status" 		=> "1", 
									"message" 		=> "User Profile Image get successfully.",
									"access_token" 	=> $access_token,
									"data" 			=> $profileArr
								);

							 $logs->Module 		= "User";
							 $logs->Action		= "get";
							 $logs->Request		= json_encode($getArr);
							 $logs->Response	= json_encode($resArr);
							 $logs->UserId		= (int)$userID;
							 $logs->AddLog();		
							
							response($resArr, 200);
							
						} else {
							
							// generate jwt
							$access_token = $auth->generateToken($tokenArr);
							
							$resArr = array(
									"status" 		=> "1", 
									"message" 		=> "No Profile Picture found.",
									"access_token" 	=> $access_token
								);
								
							 $logs->Module 		= "User";
							 $logs->Action		= "get";
							 $logs->Request		= json_encode($getArr);
							 $logs->Response	= json_encode($resArr);
							 $logs->UserId		= (int)$userID;
							 $logs->AddLog();
							 
							response($resArr, 200);
						}
					

					
					} else {
						
						// generate jwt
							$access_token = $auth->generateToken($tokenArr);
							
							$resArr = array(
									"status" 		=> "1", 
									"message" 		=> "User does not exist.",
									"access_token" 	=> $access_token
								);
								
							 $logs->Module 		= "User";
							 $logs->Action		= "get";
							 $logs->Request		= json_encode($getArr);
							 $logs->Response	= json_encode($resArr);
							 $logs->UserId		= (int)$userID;
							 $logs->AddLog();
							 
							response($resArr, 200);
						
					}
					
					
					
				
				} else if($id > 0 && $type == "attachment"){ 
				
					$userExist = $user->userExists($id);
					
					if($userExist == true)
					{
					
					$userList = $user->get($id);
					
					$file_name = $userList[0]['Attachment'];
					
					if($file_name!= ""){
						
						// Attachment Destination 
						//$path 	= UPLOAD_FILE.$file_name;
						
						//$fileType = explode(".", $file_name);	
						
						//$attachFile = file_get_contents($path);
				
						//$finfo = finfo_open();
						//$mime_type = finfo_buffer($finfo, $attachFile, FILEINFO_MIME_TYPE);
						
						
						// code to download doc from eCos
						$ecos = new eCos();
						$ecos->getToken();
						
						$container_name = CONTAINER_NAME;
						$folder_name 	= ATTACHMENT_FOLDER;
						
						$downloadObject_resp = $ecos->downloadObject($container_name, $folder_name, $file_name);
						// eof code to download doc from eCos
						
						// get file content type
						$finfo = finfo_open();
						$mime_type = finfo_buffer($finfo, $downloadObject_resp, FILEINFO_MIME_TYPE);
							
						if(strlen($downloadObject_resp) > 0)
						{
							$attachArr = array(
								"attachment" => array(
									"name" => $file_name,
									"type" => $mime_type,
									"data" => base64_encode($downloadObject_resp)
								)
							);
								
						} else {
							
							$attachArr = array(
								"attachment" => array(
									"name" => $file_name,
									"type" => $mime_type,
									"data" => 'No Attachement found.'
								)
							);
						}
					
						// generate jwt
						$access_token = $auth->generateToken($tokenArr);	
							
						$resArr = array(
								"status" 		=> "1", 
								"message" 		=> "User Attachment get successfully.",
								"access_token" 	=> $access_token,
								"data" 			=> $attachArr
							);

						 $logs->Module 		= "User";
						 $logs->Action		= "get";
						 $logs->Request		= json_encode($getArr);
						 $logs->Response	= json_encode($resArr);
						 $logs->UserId		= (int)$userID;
						 $logs->AddLog();		
							
						response($resArr, 200);
						
					} else {
						
							// generate jwt
							$access_token = $auth->generateToken($tokenArr);
							
							$resArr = array(
									"status" 		=> "1", 
									"message" 		=> "No Attachement found.",
									"access_token" 	=> $access_token
								);
								
							 $logs->Module 		= "User";
							 $logs->Action		= "get";
							 $logs->Request		= json_encode($getArr);
							 $logs->Response	= json_encode($resArr);
							 $logs->UserId		= (int)$userID;
							 $logs->AddLog();	
								
							response($resArr, 200);
						}					
					
					} else {
						
						// generate jwt
						$access_token = $auth->generateToken($tokenArr);
						
						$resArr = array(
								"status" 		=> "1", 
								"message" 		=> "User does not exist.",
								"access_token" 	=> $access_token
							);
							
						 $logs->Module 		= "User";
						 $logs->Action		= "get";
						 $logs->Request		= json_encode($getArr);
						 $logs->Response	= json_encode($resArr);
						 $logs->UserId		= (int)$userID;
						 $logs->AddLog();	
							
						response($resArr, 200);
					}
				
				} // Attachment End
				
				$userList 	 = false;
				$userListing = false;
				
				if($is_admin == true && $id == 0){ //If admin user
				
					$userListing = $user->get();
				}
				 
				 if($id > 0){
				 
					if($is_admin == true){ //If admin user
					
						$userExist = $user->userExists($id);
						
						if($userExist == true){
							
							$userListing = $user->get($id);
							
						} else {
							
							// generate jwt
							$access_token = $auth->generateToken($tokenArr);
							
							$resArr = array(
									"status" 		=> "1", 
									"message" 		=> "User does not exist.",
									"access_token" 	=> $access_token
								);
								
							 $logs->Module 		= "User";
							 $logs->Action		= "get";
							 $logs->Request		= json_encode($getArr);
							 $logs->Response	= json_encode($resArr);
							 $logs->UserId		= (int)$userID;
							 $logs->AddLog();	
								
							response($resArr, 200);
						}
						
						
					} else if($userID == $id){ // Else regular user
					
						$userExist = $user->userExists($id);
						
						if($userExist == true){
					
							$userListing = $user->get($id);
							
						} else {
							
							// generate jwt
							$access_token = $auth->generateToken($tokenArr);
							
							$resArr = array(
									"status" 		=> "1", 
									"message" 		=> "User does not exist.",
									"access_token" 	=> $access_token
								);
								
							 $logs->Module 		= "User";
							 $logs->Action		= "get";
							 $logs->Request		= json_encode($getArr);
							 $logs->Response	= json_encode($resArr);
							 $logs->UserId		= (int)$userID;
							 $logs->AddLog();	
								
							response($resArr, 200);
						}
						
					}
					
				}				 
				 
				if($userListing){
					
					foreach($userListing as $userVal){
							
						$profileImage = $userVal['ProfileImage'];
						$attachFile   = $userVal['Attachment'];
						
						if($profileImage != ""){
							$userVal['ProfileImage'] = "/users/".$userVal['UserId']."/profileimg";
						}
						
						if($attachFile != ""){
							$userVal['Attachment'] = "/users/".$userVal['UserId']."/attachment";
						}
						
						$userList[] = $userVal;
							
					}
					
				}				
					
				 
				if($userList){
					
					// generate jwt
					$access_token = $auth->generateToken($tokenArr);
					
					// set response code
					$resArr = array(
								"status" 		=> "1", 
								"message" 		=> "User data found.",
								"access_token" 	=> $access_token,
								"data" 			=> $userList
							);
						
							
						//Add log data
						 $logs->Module 		= "User";
						 $logs->Action		= "get";
						 $logs->Request		= json_encode($getArr);
						 $logs->Response	= json_encode($resArr);
						 $logs->UserId		= (int)$userID;
						 $logs->AddLog();
							
						response($resArr, 200);
						
				} else { // message if unable to create user

					// generate jwt
					$access_token = $auth->generateToken($tokenArr);
										
					$resArr = array(
								"status" 		=> "1", 
								"message" 		=> "No data found.",
								"access_token" 	=> $access_token
							);
								
					//Add log data
					 $logs->Module 		= "User";
					 $logs->Action		= "get";
					 $logs->Request		= json_encode($getArr);
					 $logs->Response	= json_encode($resArr);
					 $logs->UserId		= (int)$userID;	
					 $logs->AddLog();	
							
					response($resArr, 200);
					
				}	
				
			} else {			 
				
				// show error message
				$resArr =array(
					"status" 	=> "0", 
					"message" 	=> "Invalid Authorization. Error : ". $decoded["TokenData"]
				);
				
				//Add log data
				 $logs->Module 		= "User";
				 $logs->Action		= "get";
				 $logs->Request		= "";
				 $logs->Response	= json_encode($resArr);
				 $logs->UserId		= "";
				 $logs->UserType	= "";	
				 $logs->AddLog();
				
				response($resArr, 401);
			}
			
		}  else { // show error message if jwt is empty		 
			
			$resArr = array("status" => "0", "message" => "Access denied.");
			
			//Add log data
			 $logs->Module 		= "User";
			 $logs->Action		= "get";
			 $logs->Request		= "";
			 $logs->Response	= json_encode($resArr);
			 $logs->UserId		= "";
			 $logs->UserType	= "";	
			 $logs->AddLog();
			
			response($resArr, 401);
		}
		
		break;
	
	case 'DELETE':

		$bearerToken = getBearerToken();
		
		$access_token = isset($bearerToken) ? $bearerToken : "";	
		
		// if jwt is not empty
		if($access_token){
			
			// decode jwt
			$decoded = $auth->validateToken($access_token);
			
			$getArr = array();
			
			// if decode succeed, show user details
			if($decoded["status"] == true) {
				
			// Token data decode	
			$userID 		= $decoded["TokenData"]->data->UserId; // Decode ID
			$UserName		= $decoded["TokenData"]->data->UserName; // Decode UserName
			$is_admin 		= isset($decoded["TokenData"]->data->admin) ? $decoded["TokenData"]->data->admin : false;
			
					$getArr = get_url_data();
					
					// set user property values	
					$user->UserId = isset($getArr['id']) ? intval($getArr['id']) : "";
					
					$error = array();
					
					if($user->UserId == ""){
						
						$error[] = "UserId is required.";					
						
					} 
					
					if(count($error) > 0){
				
						$errorMsg = implode(", ", $error);
							 
						// tell the user login failed
						$resArr = array("status" => "0", "message" => $errorMsg);						
				
						//Add log data
						 $logs->Module 		= "User";
						 $logs->Action		= "delete";
						 $logs->Request		= json_encode($getArr);
						 $logs->Response	= json_encode($resArr);
						 $logs->UserId		= (int)$userID;
						 $logs->UserType	= "Admin";
						 $logs->AddLog();
				
						response($resArr, 200);
						 
					 } else {
							
							$userExist = $user->userExists($user->UserId);
							
							if($userExist == true){
								
								$deleteUser = false;
						 
								$deleteUser = $user->delete($user->UserId);
								
								if($deleteUser > 0){
							 
									 $tokenArr = array("UserId" => (int)$userID, "UserName" => $UserName, "admin" => true);
									 
									// generate jwt
									$access_token = $auth->generateToken($tokenArr);
										 
									$resArr = array(
													"status" 		=> "1", 
													"message" 		=> "User was successfully deleted.",
													"access_token" 	=> $access_token
												);			
									
									//Add log data
									 $logs->Module 		= "User";
									 $logs->Action		= "delete";
									 $logs->Request		= json_encode($getArr);
									 $logs->Response	= json_encode($resArr);
									 $logs->UserId		= (int)$userID;
									 $logs->UserType	= "Admin";
									 $logs->AddLog();
						
									response($resArr, 200);
									
								 } else {
							 
									$tokenArr = array("UserId" => (int)$userID, "UserName" => $UserName, "admin" => true);

										// generate jwt
										$access_token = $auth->generateToken($tokenArr);
										 
										 $resArr = array(
														"status" 		=> "1", 
														"message" 		=> "Unable to delete user.",
														"access_token" 	=> $access_token
													);
													
										//Add log data
										 $logs->Module 		= "User";
										 $logs->Action		= "delete";
										 $logs->Request		= json_encode($getArr);
										 $logs->Response	= json_encode($resArr);
										 $logs->UserId		= (int)$userID;
										 $logs->UserType	= "Admin";
										 $logs->AddLog();		
											 
										response($resArr, 401);
										 
									 }		
								
							} else {
								
								 $tokenArr = array("UserId" => (int)$userID, "UserName" => $UserName, "admin" => true);
									// generate jwt
									$access_token = $auth->generateToken($tokenArr);
										 
									$resArr = array(
													"status" 		=> "1", 
													"message" 		=> "User does not exist.",
													"access_token" 	=> $access_token
												);			
									
									//Add log data
									 $logs->Module 		= "User";
									 $logs->Action		= "delete";
									 $logs->Request		= json_encode($getArr);
									 $logs->Response	= json_encode($resArr);
									 $logs->UserId		= (int)$userID;
									 $logs->UserType	= "Admin";
									 $logs->AddLog();
						
									response($resArr, 200);
								
							}
							
					 }
				
			} else {
			 
				// set response code				
				$resArr = array(
							"status" 	=> "0", 
							"message" 	=> "Invalid Authorization. Error : ". $decoded["TokenData"]
						);
					
				//Add log data
				 $logs->Module 		= "User";
				 $logs->Action		= "delete";
				 $logs->Request		= json_encode($getArr);
				 $logs->Response	= json_encode($resArr);
				 $logs->UserId		= "";
				 $logs->UserType	= "Admin";
				 $logs->AddLog();			
								 
				response($resArr, 401);
			}
		}
		 
		// show error message if jwt is empty
		else{
		 
			// tell the user access denied
			$resArr = array("status" => "0", "message" => "Access denied.");
			
			//Add log data
			 $logs->Module 		= "User";
			 $logs->Action		= "delete";
			 $logs->Request		= "";
			 $logs->Response	= json_encode($resArr);
			 $logs->UserId		= "";
			 $logs->UserType	= "";
			 $logs->AddLog();
						 
			response($resArr, 401);
			
		}		
	
		break;
		
	case "OPTIONS": //Handle Preflighted requests for CORS issue
		
		// set response code		
		header('Content-Type: text/plain');
		
		response("", 200);
		
		break;
		
	default:
	
		// Invalid Request Method
		$resArr = array("status" => "0", "message" => "Method Not Allowed.");						
		// display message: unable to create user
		response($resArr, 405);	
		
		break;
}


?>