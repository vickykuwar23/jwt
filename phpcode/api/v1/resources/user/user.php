<?php
/*** 'user' object ***/
class User{
 
    /*** database connection and table name ***/
    private $conn;
    private $table_name = 'public."Users"';
 
    /*** object properties ***/
    public $UserId;
    public $FirstName;
    public $LastName;
    public $EmailAddress;
	public $Password;
	public $IsActive;
	public $imgName;
	public $ProfileImage;
	public $Attachment;
	public $AttachFilename;
	//public $currentDateTime = date("Y-m-d H:i:s");
 
    /*** constructor ***/
    public function __construct($db){
        $this->conn = $db;
		$this->currentDateTime = date("Y-m-d H:i:s");
    }
 
	/*** create new user record ***/
	function create(){
		
		/*** insert query	***/	
		$query = 'INSERT INTO '.$this->table_name.' ("FirstName", "LastName", "EmailAddress", "Password", "IsActive", "CreatedOn") VALUES (:FirstName, :LastName, :EmailAddress, :Password, :IsActive, :CreatedOn)';
		
		/*** prepare the query ***/
		$stmt = $this->conn->prepare($query);
	 
		/*** sanitize ***/
		$this->Password		=	md5($this->Password);
		//$this->EmailAddress	=	htmlspecialchars(strip_tags($this->EmailAddress));
		$IsActive 			= 	'1';
		$CreatedOn			=	$this->currentDateTime;
	     
		/*** bind the values ***/
		$stmt->bindParam(':FirstName', $this->FirstName);
		$stmt->bindParam(':LastName', $this->LastName);
		$stmt->bindParam(':EmailAddress', $this->EmailAddress);
		$stmt->bindParam(':Password', $this->Password);
		$stmt->bindParam(':IsActive', $IsActive);
		$stmt->bindParam(':CreatedOn', $CreatedOn);
	 
		/*** execute the query, also check if query was successful ***/
		if($stmt->execute()){
			
			return true;
		}
		
		return false;
		
	}
 
	/*** check if given email exist in the database ***/
	function emailExists(){
	 
		/*** query to check if email exists ***/
		$query = 'SELECT "UserId", "EmailAddress"
					FROM '.$this->table_name.'
					WHERE "EmailAddress" = ?
					LIMIT 1';
					
		/*** prepare the query ***/
		$stmt = $this->conn->prepare( $query );
	 
		/*** bind given email value ***/
		$stmt->bindParam(1, $this->EmailAddress);
	 
		/*** execute the query ***/
		$stmt->execute();
	 
		/*** get number of rows ***/
		$num = $stmt->rowCount();
	 
		/*** if email exists, assign values to object properties for easy access and use for php sessions ***/
		if($num > 0){
	 
			/*** return true because email exists in the database ***/
			return true;
		}
	 
		/*** return false if email does not exist in the database ***/
		return false;
	}
	
	
	/*** check if given user exist in the database ***/
	function userExists($UserId){
	 
		/*** query to check if email exists ***/
		$query = 'SELECT "UserId"
					FROM '.$this->table_name.'
					WHERE "UserId" = ? AND "IsDeleted" = ?
					LIMIT 1';
					
		/*** prepare the query ***/
		$stmt = $this->conn->prepare( $query );
	 
		/*** bind given UserId value ***/
		$stmt->bindParam(1, $UserId);
		$stmt->bindValue(2, false, PDO::PARAM_BOOL);
		/*** execute the query ***/
		$stmt->execute();
	 
		/*** get number of rows ***/
		$num = $stmt->rowCount();
	 
		/*** if UserId exists, assign values to object properties for easy access and use for php sessions ***/
		if($num > 0){
	 
			/*** return true because UserId exists in the database ***/
			return true;
		}
	 
		/*** return false if UserId does not exist in the database ***/
		return false;
	}
	
	 
	/*** update a user record ***/
	public function update($updateStatus){		
		
		/*** if no posted password, do not update the password ***/
		$query = 'UPDATE ' . $this->table_name . ' SET ';
		
		if($updateStatus == "UpdateProfile"){
				
			$query .= '"FirstName" 	= :FirstName, "LastName" 	= :LastName,';
						
			if($this->Password!= ""){
				
				$query .= ' "Password" = :Password,';
				
			} 			
			
			if($this->imgName != "" && $this->ProfileImage != ""){
				
				$query .= ' "ProfileImage" 	= :ProfileImage,';
				
			} 

			if($this->Attachment != "" && $this->AttachFilename != ""){
				
				$query .= ' "Attachment" = :Attachment,';
				
			} 
				
			$query .= ' "UpdatedOn" = :UpdatedOn';
			
			$query .= ' WHERE "UserId" 	= :UserId';	
			
		} else {
		
			// Only Status Update
			$query .= '"IsActive" = :IsActive, "UpdatedOn" = :UpdatedOn WHERE "UserId" = :UserId';
			
		}
		
		/*** prepare the query ***/
		$stmt = $this->conn->prepare($query);
		
		$UpdatedOn			=	$this->currentDateTime;
		$this->IsActive		=	$this->IsActive;

		if($this->Password!= ""){
			
			$this->Password		=	md5($this->Password);
			$stmt->bindParam(':Password', $this->Password);
		}	
		
		/*** bind the values from the form ***/
		if($this->FirstName!= "" && $this->LastName!= ""){
			
			if($this->imgName != "" && $this->ProfileImage != "" ){
				
				$stmt->bindParam(':ProfileImage', $this->imgName);
				
			} 
			
			 if($this->Attachment != "" && $this->AttachFilename != ""){
				
				$stmt->bindParam(':Attachment', $this->AttachFilename);
				
			} 
				
			$stmt->bindParam(':FirstName', $this->FirstName);
			$stmt->bindParam(':LastName', $this->LastName);
			
			
		} else {		
			
			$stmt->bindParam(':IsActive', $this->IsActive);
			
		}
	 
		/*** unique ID of record to be edited ***/
		$stmt->bindParam(':UpdatedOn', $UpdatedOn);
		
		$stmt->bindParam(':UserId', $this->UserId);
	 
		/*** execute the query ***/
		if($stmt->execute()){
			//return true;
			return $stmt->rowCount();
		}
	 
		return false;
	}
	
	
	function get($id = 0){
		
		/*** query to check if email exists ***/
							
		if($id > 0){
			
			$query = 'SELECT "UserId", "FirstName", "LastName", "EmailAddress", 
						"ProfileImage", "Attachment", "IsActive", "CreatedOn", "UpdatedOn"
					  FROM '.$this->table_name.' 
					  WHERE "UserId" = ? AND "IsDeleted" = ? ORDER BY "UserId" ASC';
			
		} else {
			
			$query = 'SELECT "UserId", "FirstName", "LastName", "EmailAddress", 
					 "ProfileImage", "Attachment", "IsActive", "CreatedOn", "UpdatedOn"
					 FROM '.$this->table_name.' 
					 WHERE "IsDeleted" = ? ORDER BY "UserId" ASC';
		}		
	 
		/*** prepare the query ***/
		$stmt = $this->conn->prepare( $query );		
	 
		if($id > 0){
			/*** bind given email value ***/
			$stmt->bindValue(1, $id);
			$stmt->bindValue(2, false, PDO::PARAM_BOOL);
		} else {
			$stmt->bindValue(1, false, PDO::PARAM_BOOL);
		}
		
		/*** execute the query ***/
		$stmt->execute();
	 
		/*** get number of rows ***/
		$num = $stmt->rowCount();
	 
		/*** if email exists, assign values to object properties for easy access and use for php sessions ***/
		if($num>0){
	 
			/*** get record details / values ***/			
			while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
					$userData[] = $row;
			}
	 
			/*** return true because email exists in the database ***/
			return $userData;
		}
	 
		/*** return false if email does not exist in the database ***/
		return false;
		
	}
	
	/*************** Delete (Deactivate) User ****************/
	function delete($ID){
		
		$query = 'UPDATE ' . $this->table_name . '
					  SET
						"IsDeleted"	 	= :IsDeleted,
						"IsDeletedOn"	= :IsDeletedOn,
						"DeletedByUser" = :DeletedByUser
					  WHERE "UserId" 	= :UserId';
		$stmt = $this->conn->prepare($query);	
		
		$this->IsDeleted	=	'1';
		$this->IsDeletedOn  =	$this->currentDateTime;
		$stmt->bindParam(':IsDeleted', $this->IsDeleted);
		$stmt->bindParam(':IsDeletedOn', $this->IsDeletedOn);
		$stmt->bindParam(':DeletedByUser', $ID);		
		$stmt->bindParam(':UserId', $this->UserId);
			
		if($stmt->execute()){
			
			return $stmt->rowCount();
			//return true;
			
		} else {
			
			return false;
			
		}
			
	}
	
}