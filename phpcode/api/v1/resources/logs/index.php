<?php
// Files needed to connect to database and class
define("ROOT_PATH", realpath(__DIR__ . '/../../..'));
include_once ROOT_PATH.'/constant.php';

include_once CONFIG_PATH.'functions.php';
include_once CONFIG_PATH.'database.php';
include_once RESOURCE_PATH.'logs/logs.php';
include_once RESOURCE_PATH.'auth/auth.php';
include_once RESOURCE_PATH.'user/user.php';

// Required headers
requireHeaders();
 
// Get database connection
$database = new Database();
$db = $database->getConnection();
$mdb = $database->getMongoConnection();
 
// Instantiate logs object
$logs = new Logs($mdb);
$auth  = new Auth();
$user  = new User($db);

// Get Request Method
$request_method	= get_request_method();

switch($request_method)
{		
	case 'GET':
				
		$bearerToken = getBearerToken();
		
		$access_token = isset($bearerToken) ? $bearerToken : "";
		
		// Check if jwt is not empty
		if($access_token){
			
			// decode jwt		
			$decoded 	= $auth->validateToken($access_token);
		 
			// Check if token is valid
			if($decoded["status"] == true) {
				
				$LoggedInUserId = isset($decoded["TokenData"]->data->UserId) ? $decoded["TokenData"]->data->UserId : "";
				$UserName 	   = isset($decoded["TokenData"]->data->UserName) ? $decoded["TokenData"]->data->UserName : "";
				$is_admin 	   = isset($decoded["TokenData"]->data->admin) ? $decoded["TokenData"]->data->admin : false;
			
				if($is_admin == true){ //If admin user	
									
					$logs->UserType = "Admin";
					$tokenArr = array("UserId" => $LoggedInUserId, "UserName" => $UserName, "admin" => true);
					
				} else { // Else regular user
					
					$logs->UserType = "User";
					$tokenArr = array("UserId" => $LoggedInUserId, "UserName" => $UserName);
					
				}
				
				$reqArr = get_url_data();
				
				$UserId = isset($reqArr['id']) ? intval($reqArr['id']) : 0;
				
				$LogsList = false;
				
				if($is_admin == true && $UserId == 0){ // If admin user
				
					$LogsList = $logs->GetLogs();
				}
				 
				if($UserId > 0){
					
					$userExist = $user->userExists($UserId);
					
					if($userExist == true){
				
						if($is_admin == true){ // Check if logged in user is Admin User					

							$LogsList = $logs->GetLogs($UserId);
							
						} else if($LoggedInUserId == $UserId){ // Check if logged in user is Normal User		
							
							$LogsList = $logs->GetLogs($UserId);
							
						}
						
					}  else {
							
						// generate jwt
						$access_token = $auth->generateToken($tokenArr);
						
						// response in json format
						$resArr = array(
										"status" 		=> "1", 
										"message" 		=> "User does not exist.",
										"access_token" 	=> $access_token
									);
									
							//Add log data
							 $logs->Module 		= "Logs";
							 $logs->Action		= "get";
							 $logs->Request		= json_encode($reqArr);
							 $logs->Response	= json_encode($resArr);
							 $logs->UserId		= (int)$LoggedInUserId;
							 $logs->AddLog();
						 
						response($resArr, 200);
					}
					
				}
				 
				if($LogsList){
					
					// code to get username by id
					foreach($LogsList as $userVal){
						$userVal->UserName = "";
						if(isset($userVal->UserId) && $userVal->UserId)
						{
							$userData = $user->get($userVal->UserId);
							if(!empty($userData))
							{
								foreach($userData as $userRow){
									$userVal->UserName = $userRow['FirstName'] . " " . $userRow['LastName'];
								}
							}
						}
						$LogsList[] = $userVal;
					}
					// eof code to get username by id
					
					// generate jwt
					$access_token = $auth->generateToken($tokenArr);	
					
					$resArr = array(
								"status" 		=> "1", 
								"message" 		=> "Logs get successfully.",
								"access_token" 	=> $access_token,
								"data" 			=> $LogsList
							);
					
					// Add log data
					 $logs->Module 		= "Logs";
					 $logs->Action		= "get";
					 $logs->Request		= json_encode($reqArr);
					 $logs->Response	= json_encode($resArr);
					 $logs->UserId		= (int)$LoggedInUserId;
					 $logs->AddLog();
					 
					 //$logs->ListenLogs();	
					
					response($resArr, 200);
						
				} else {
					
					// generate jwt
					$access_token = $auth->generateToken($tokenArr);
				 
					// Display message: No data found
					$resArr = array("status" => "1", 
									"message" => "No data found.",
									"access_token" 	=> $access_token);
					
					// Add log data
					 $logs->Module 		= "Logs";
					 $logs->Action		= "get";
					 $logs->Request		= json_encode($reqArr);
					 $logs->Response	= json_encode($resArr);
					 $logs->UserId		= (int)$LoggedInUserId;
					 $logs->AddLog();
					
					response($resArr, 200);
					
				}
				
			} else {	// If jwt is invalid
			 
				// Show error message
				$resArr = array(
					"status" => "0", 
					"message" => "Invalid Authorization. Error : ". $decoded["TokenData"]
				);
				
				// Add log data
				 $logs->Module 		= "Logs";
				 $logs->Action		= "get";
				 $logs->Request		= "";
				 $logs->Response	= json_encode($resArr);
				 $logs->UserId		= "";
				 $logs->UserType	= "";
				 $logs->AddLog();	
					 
				response($resArr, 401);
			}
			
		} else {
		 
			// Show error message as access denied
			$resArr = array("status" => "0", "message" => "Access denied.");
			
			 // Add log data
			 $logs->Module 		= "Logs";
			 $logs->Action		= "get";
			 $logs->Request		= "";
			 $logs->Response	= json_encode($resArr);
			 $logs->UserId		= "";
			 $logs->UserType	= "";
			 $logs->AddLog();	
			response($resArr, 401);
		}
		
		break;
	
	case "OPTIONS": //Handle Preflighted requests for CORS issue
		
		// set response code		
		header('Content-Type: text/plain');
		
		response("", 200);
		
		break;
		
	default:
	
		// Invalid Request Method
		$resArr = array("status" => "0", "message" => "Method Not Allowed.");						
		// display message: unable to create user
		response($resArr, 405);	
		
		break;
}
?>