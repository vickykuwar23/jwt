<?php
class Login{
	
	/*** database connection and table name ***/
    private $conn;
    private $table_Users 		= 'public."Users"';
	private $table_AdminUsers   = 'public."AdminUsers"';
 
    /*** object properties ***/
    public $UserName;
	public $Password;
	public $UserRole;
	public $CaptchaCode;
 
    /*** constructor ***/
    public function __construct($db){
        $this->conn = $db;
    }	
		
	// check if email exists and if password is correct
	function validateLogin(){
		
		/**********  Get User Password From DB **************/		
		$getPass = $this->getDetails();
		
		if(count($getPass) > 0)
		{
			if($this->UserName && (md5($this->Password) == $getPass['Password'])){			
				
				return $getPass['UserId'];
				
			} else { // Else part
				
				return false;
			}
		} else {
			
			return false;
		}
	}	
	
	function getDetails(){
		
		$data = array();
		
		if($this->UserRole == 0){
			
			$query = 'SELECT "UserId", "EmailAddress", "Password"
					FROM '.$this->table_Users.'
					WHERE "EmailAddress" = ?
					AND "IsActive" = ?
					AND "IsDeleted" = ?	
					LIMIT 1';
					
				/*** prepare the query ***/
				$stmt = $this->conn->prepare( $query );
			 
				/*** bind given email value ***/
				
				$stmt->bindValue(1, $this->UserName);
				$stmt->bindValue(2, true, PDO::PARAM_INT);
				$stmt->bindValue(3, false, PDO::PARAM_INT);
				
				/*** execute the query ***/
				if($stmt->execute()){
					
					$num = $stmt->rowCount();
						
					if($num > 0){
						
						  $row = $stmt->fetch(PDO::FETCH_ASSOC);
					
							$data = array("Password" => $row['Password'], "EmailAddress" => $row['EmailAddress'], "UserId" => $row['UserId']);
					
						} 
						
				}
				
				
		} else {
			
			$query = 'SELECT "AdminUserId", "UserName", "Password"
					FROM '.$this->table_AdminUsers.'
					WHERE "UserName" = ?
					AND "IsActive" = ?
					AND "IsDeleted" = ?	
					LIMIT 1';
					
				/*** prepare the query ***/
				$stmt = $this->conn->prepare( $query );
			 
				/*** bind given email value ***/
				$stmt->bindValue(1, $this->UserName);
				$stmt->bindValue(2, true, PDO::PARAM_INT);
				$stmt->bindValue(3, false, PDO::PARAM_INT);
				
				/*** execute the query ***/
				
				if($stmt->execute()){
					
					$num = $stmt->rowCount();
						
					if($num > 0){
						
						  $row = $stmt->fetch(PDO::FETCH_ASSOC);
					
							$data = array("Password" => $row['Password'], "EmailAddress" => $row['UserName'], "UserId" => $row['AdminUserId']);
					
						} 
					
				}
				
		}	
		
		return $data;
		
	}
	
}
?>