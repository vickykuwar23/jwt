<?php
// Files needed to connect to database and class
define("ROOT_PATH", realpath(__DIR__ . '/../../..'));
include_once ROOT_PATH.'/constant.php';

include_once CONFIG_PATH.'functions.php';
include_once CONFIG_PATH.'database.php';
include_once RESOURCE_PATH.'login/login.php';
include_once RESOURCE_PATH.'logs/logs.php';
include_once RESOURCE_PATH.'auth/auth.php';

// Required headers
requireHeaders();
 
// Get database connection
$database 	= new Database();
$db 		= $database->getConnection();
$mdb 		= $database->getMongoConnection();

// Instantiate logs object
$login = new Login($db);
$logs  = new Logs($mdb);
$auth  = new Auth();

// Get Request Method
$request_method	= get_request_method();

switch($request_method)
{
	case 'POST':
		
		$data = get_request_body();
		
		if($data["status"] == false){	
		
			// set response code & display message
			$resArr = array("status" => "0", "message" => $data["requestData"]);
			
			// Add log data  
			 $logs->Module 		= "Login";
			 $logs->Action		= "login";
			 $logs->Request		= "";
			 $logs->Response	= json_encode($resArr);
			 $logs->UserId		= "";
			 $logs->UserType	= "";
			 $logs->AddLog();
			 
			response($resArr, 400);
			
		} else {
			
			$error = array();		
			$login->UserName 	= isset($data['requestData']['UserName']) ? $data['requestData']['UserName'] : "";
			$login->Password 	= isset($data['requestData']['Password']) ? $data['requestData']['Password'] : "";
			$login->UserRole 	= isset($data['requestData']['UserRole']) ? $data['requestData']['UserRole'] : "";
			$login->CaptchaCode = isset($data['requestData']['CaptchaCode']) ? $data['requestData']['CaptchaCode'] : "";
					
			if($login->UserRole != ""){					
				
				if($login->UserName == ""){
				
					$error[]= "UserName is required.";
					
				} 
				if($login->Password == ""){
				
					$error[] = "Password is required.";
					
				}
				if($login->UserRole == ""){
				
					$error[] = "UserRole is required.";
					
				}
		
			} else {
				
				if($login->UserName == ""){
				
					$error[]= "UserName is required.";
					
				} else if(!filter_var($login->UserName, FILTER_VALIDATE_EMAIL)){
					
					$error[]= "Please enter valid EmailAddress.";
					
				}
				if($login->Password == ""){
					
					$error[] = "Password is required.";				
				}			
			}

			/*if($login->CaptchaCode == ""){
				
				$error[] = "Captcha is required.";				
			}
			else
			{
				// Google captcha validation
				$CaptchaResponse = file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=".SECRET_KEY."&response=".$login->CaptchaCode);
				$CaptchaResponse = json_decode($CaptchaResponse, true);
				
				if($CaptchaResponse["success"] == false){
					
					$error[] = "Invalid captcha.";
					
				}
				
			}*/
			
			// Check if validation errors
			if(count($error) > 0){
				
				$errorMsg = implode(", ", $error);
				
				$resArr = array("status" => "0", "message" => $errorMsg);
				
				// Add log data
				 $logs->Module 		= "Login";
				 $logs->Action		= "login";
				 $logs->Request		= json_encode($data);
				 $logs->Response	= json_encode($resArr);
				 $logs->UserId		= "";
				 
				 if($login->UserRole == ""){
				 
					$logs->UserType		= "User";
					
				} else {
				 
					$logs->UserType		= "Admin";
					
				}
				$logs->AddLog();
				
				response($resArr, 200);
							
			} else {
				
				$userID = $login->validateLogin();
				
				if($userID){ 
					  
					  // If Normal User Login
					  if($login->UserRole == 0){				  
							
						$tokenArr 	= array("UserId" => (int)$userID, "UserName" => $login->UserName);
							
						} else {
						
							// Admin User Login
						$tokenArr 	= array("UserId" => (int)$userID, "UserName" => $login->UserName, "admin" => true);
							
						}
						
						// generate jwt
						$access_token 		= $auth->generateToken($tokenArr);
				 
						// show user details
						$resArr = array(
							"status" 		=> "1", 
							"message" 		=> "User successful login.",
							"access_token" 	=> $access_token
						);

						// Add log data
						 $logs->Module 		= "Login";
						 $logs->Action		= "login";
						 $logs->Request		= json_encode($data);
						 $logs->Response	= json_encode($resArr);
						 $logs->UserId		= (int)$userID;
						 
						 if($login->UserRole == ""){
						 
							$logs->UserType		= "User";
							
						 } else {
						 
							$logs->UserType		= "Admin";
							
						 }
						 
						$logs->AddLog();	
				
						response($resArr, 200);
						
						
				} else { // message if unable to create user
				 
					// set response code					
					$resArr = array("status" => "0", "message" => "Incorrect username or password provided.");
					
					// Add log data  
					 $logs->Module 		= "Login";
					 $logs->Action		= "login";
					 $logs->Request		= json_encode($data);
					 $logs->Response	= json_encode($resArr);
					 $logs->UserId		= "";
					 
					 if($login->UserRole == ""){
					 
						$logs->UserType		= "User";
						
					 } else {
					 
						$logs->UserType		= "Admin";
						
					 }
					$logs->AddLog();
					
					// display message: unable to create user
					response($resArr, 400);
					
				}	// end create user if
				
			} // if error end
			
		}
		
		break;
	
	case "OPTIONS": //Handle Preflighted requests for CORS issue
		
		// set response code		
		header('Content-Type: text/plain');
		
		response("", 200);
		
		break;
		
	default:
	
		// Invalid Request Method
		$resArr = array("status" => "0", "message" => "Method Not Allowed.");						
		// display message: unable to create user
		response($resArr, 405);	
		
		break;
}

?>