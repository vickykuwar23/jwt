<?php
// Required files
require_once __DIR__ . '/vendor/autoload.php';

use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

class RMQ{
	
	// object properties   
    /*private $host;
	private $port;
	private $username;
	private $password;
	private $channel_name;*/
	
	//private $host = "103.249.98.164";
	private $host = "10.20.30.9";
	private $port = "5672";
	private $username = "nijis";
	private $password = "supp0rt#N1jIS";
	private $channel_name = "nijisphp";
	
	public $connection;
	public $channel;
	
	public function __construct(){
	   	
		/*$this->host = $host;
		$this->port = $port;
		$this->username = $username;
		$this->password = $password;
		$this->channel_name = $channel_name;*/
    }
	
	public function getConnection(){
		
		$this->connection = null;
        try{
            $this->connection = new AMQPStreamConnection($this->host, $this->port, $this->username, $this->password);
			$this->channel = $this->connection->channel();
        }catch(Exception $exception){
            echo "Connection error: " . $exception->getMessage();
        }
        return $this->connection;		
	}
	
	public function closeConnection(){
		
		$this->channel->close();
		$this->connection->close();	
	}
	
	public function publish($message){
		
		$this->getConnection();
		
		$msg = new AMQPMessage($message);
		$this->channel->basic_publish($msg, '', $this->channel_name);
		
		$this->closeConnection();
	}
	
	public function consume($callback){
		
		$this->getConnection();
		
		$this->channel->basic_consume($this->channel_name, '', false, true, false, false, $callback);
		while (count($this->channel->callbacks)) {
			$this->channel->wait();
		}
		
		$this->closeConnection();
	}
}
?>