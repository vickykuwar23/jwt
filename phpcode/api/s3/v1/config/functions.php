<?php

// get request method
function get_request_method(){
	return $_SERVER['REQUEST_METHOD'];
}

// get request headers
function getAuthorizationHeader(){
	$headers = NULL;
	if (isset($_SERVER['Authorization'])) {
		$headers = trim($_SERVER["Authorization"]);
	}
	else if (isset($_SERVER['HTTP_AUTHORIZATION'])) { //Nginx or fast CGI
		$headers = trim($_SERVER["HTTP_AUTHORIZATION"]);
	} elseif (function_exists('apache_request_headers')) {
		$requestHeaders = apache_request_headers();
		// Server-side fix for bug in old Android versions (a nice side-effect of this fix means we don't care about capitalization for Authorization)
		$requestHeaders = array_combine(array_map('ucwords', array_keys($requestHeaders)), array_values($requestHeaders));
		//print_r($requestHeaders);
		if (isset($requestHeaders['Authorization'])) {
			$headers = trim($requestHeaders['Authorization']);
		}
	}
	return $headers;
}

// json encode, decode with error handling
function _handleJsonError($errno)
{
	$messages = array(
		JSON_ERROR_DEPTH => 'Maximum stack depth exceeded',
		JSON_ERROR_CTRL_CHAR => 'Unexpected control character found',
		JSON_ERROR_SYNTAX => 'Syntax error, malformed JSON'
	);
		if(isset($messages[$errno]))
		{
			
			$msg = $messages[$errno];
			
		} else {
			
			$msg = 'Unknown JSON error: ' . $errno;
		}
		
	return $msg;
}

function jsonDecode($input)
{
	
		$obj = json_decode($input);	
			
		$returnArr = array("status" => true, "requestData" => $obj);
		
		if (function_exists('json_last_error') && $errno = json_last_error()) {
			$obj = _handleJsonError($errno);
			$returnArr = array("status" => false, "requestData" => $obj);
		} else if ($obj === NULL && $input !== 'null') {
			$obj = 'Null result with non-null input';
			$returnArr = array("status" => false, "requestData" => $obj);
		} else if (empty((array)$obj)) {
			$obj = 'Null result with null input';
			$returnArr = array("status" => false, "requestData" => $obj);
		}
	
	return $returnArr;
}

function jsonEncode($input)
{
	$obj = json_encode($input);
	$returnArr = array("status" => true, "requestData" => $obj);
	if (function_exists('json_last_error') && $errno = json_last_error()) {
		$obj =  _handleJsonError($errno);
		$returnArr = array("status" => false, "requestData" => $obj);
	} else if ($json === 'null' && $input !== NULL) {
		$obj = 'Null result with non-null input';
		$returnArr = array("status" => false, "requestData" => $obj);
	}
	
	return $returnArr;
}

// get request body
function get_request_body(){
	
  $input = file_get_contents('php://input');
  
  $request = jsonDecode($input);
  
  if($request['status'] == true){
	  
	$request['requestData'] = cleanInputs($request['requestData']);
	  
  } 
  return $request;
}

// for get values for get request i.e. DELETE
function get_url_data(){
	
	$request = $_GET;
	$requestData = cleanInputs($request);
	return $requestData;
	
}

// get token from request header
function getBearerToken() {
    $headers = getAuthorizationHeader();
    // HEADER: Get the access token from the header
    if (!empty($headers)) {
        if (preg_match('/Bearer\s(\S+)/', $headers, $matches)) {
            return $matches[1];
        }
		return $headers;
    }
    return NULL;
}

// response
function response($data,$status){
	$code = ($status) ? $status : 200;
	set_headers($code);
	if(count($data) > 0):
		echo json_encode($data, JSON_UNESCAPED_SLASHES);
	endif;
	exit;
}

// set headers
function set_headers($code){
	header("HTTP/1.1 ".$code." ".get_status_message($code));
	//header("Content-Type:".$content_type);
}

// set require headers 
function requireHeaders(){
	
	header("Access-Control-Allow-Origin: *");
	header("Cache-Control: no-cache");
	header("Content-Type: application/json; charset=UTF-8");
	header("Access-Control-Allow-Methods: GET, POST, OPTIONS, DELETE, PUT");
	header('Access-Control-Max-Age: 86400');    // cache for 1 day
	header("Access-Control-Allow-Headers:  Origin, Content-Type, Accept, Authorization, X-Requested-With, content-type");
	header('Access-Control-Allow-Credentials: true');
	
}

// get mime_content_type dynamically 
function getContentType($fileType){
	
	$mime_types = array("acx" => "application/internet-property-stream",
						"ai" => "application/postscript",
						"aif" => "audio/x-aiff",
						"aifc" => "audio/x-aiff",
						"aiff" => "audio/x-aiff",
						"asf" => "video/x-ms-asf",
						"asr" => "video/x-ms-asf",
						"asx" => "video/x-ms-asf",
						"au" => "audio/basic",
						"avi" => "video/x-msvideo",
						"axs" => "application/olescript",
						"bas" => "text/plain",
						"bcpio" => "application/x-bcpio",
						"bin" => "application/octet-stream",
						"bmp" => "image/bmp",
						"c" => "text/plain",
						"cat" => "application/vnd.ms-pkiseccat",
						"cdf" => "application/x-cdf",
						"cer" => "application/x-x509-ca-cert",
						"class" => "application/octet-stream",
						"clp" => "application/x-msclip",
						"cmx" => "image/x-cmx",
						"cod" => "image/cis-cod",
						"cpio" => "application/x-cpio",
						"crd" => "application/x-mscardfile",
						"crl" => "application/pkix-crl",
						"crt" => "application/x-x509-ca-cert",
						"csh" => "application/x-csh",
						"css" => "text/css",
						"dcr" => "application/x-director",
						"der" => "application/x-x509-ca-cert",
						"dir" => "application/x-director",
						"dll" => "application/x-msdownload",
						"dms" => "application/octet-stream",
						"doc" => "application/msword",
						"docx" => "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
						"dot" => "application/msword",
						"dvi" => "application/x-dvi",
						"dxr" => "application/x-director",
						"eps" => "application/postscript",
						"etx" => "text/x-setext",
						"evy" => "application/envoy",
						"exe" => "application/octet-stream",
						"fif" => "application/fractals",
						"flr" => "x-world/x-vrml",
						"gif" => "image/gif",
						"gtar" => "application/x-gtar",
						"gz" => "application/x-gzip",
						"h" => "text/plain",
						"hdf" => "application/x-hdf",
						"hlp" => "application/winhlp",
						"hqx" => "application/mac-binhex40",
						"hta" => "application/hta",
						"htc" => "text/x-component",
						"htm" => "text/html",
						"html" => "text/html",
						"htt" => "text/webviewhtml",
						"ico" => "image/x-icon",
						"ief" => "image/ief",
						"iii" => "application/x-iphone",
						"ins" => "application/x-internet-signup",
						"isp" => "application/x-internet-signup",
						"jfif" => "image/pipeg",
						"jpe" => "image/jpeg",
						"jpeg" => "image/jpeg",
						"jpg" => "image/jpeg",
						"js" => "application/x-javascript",
						"latex" => "application/x-latex",
						"lha" => "application/octet-stream",
						"lsf" => "video/x-la-asf",
						"lsx" => "video/x-la-asf",
						"lzh" => "application/octet-stream",
						"m13" => "application/x-msmediaview",
						"m14" => "application/x-msmediaview",
						"m3u" => "audio/x-mpegurl",
						"man" => "application/x-troff-man",
						"mdb" => "application/x-msaccess",
						"me" => "application/x-troff-me",
						"mht" => "message/rfc822",
						"mhtml" => "message/rfc822",
						"mid" => "audio/mid",
						"mny" => "application/x-msmoney",
						"mov" => "video/quicktime",
						"movie" => "video/x-sgi-movie",
						"mp2" => "video/mpeg",
						"mp3" => "audio/mpeg",
						"3gp" => "video/3gpp",
						"mp4" => "video/mp4",
						"mpa" => "video/mpeg",
						"mpe" => "video/mpeg",
						"mpeg" => "video/mpeg",
						"mpg" => "video/mpeg",
						"mpp" => "application/vnd.ms-project",
						"mpv2" => "video/mpeg",
						"ms" => "application/x-troff-ms",
						"mvb" => "application/x-msmediaview",
						"nws" => "message/rfc822",
						"oda" => "application/oda",
						"p10" => "application/pkcs10",
						"p12" => "application/x-pkcs12",
						"p7b" => "application/x-pkcs7-certificates",
						"p7c" => "application/x-pkcs7-mime",
						"p7m" => "application/x-pkcs7-mime",
						"p7r" => "application/x-pkcs7-certreqresp",
						"p7s" => "application/x-pkcs7-signature",
						"pbm" => "image/x-portable-bitmap",
						"pdf" => "application/pdf",
						"pfx" => "application/x-pkcs12",
						"pgm" => "image/x-portable-graymap",
						"pko" => "application/ynd.ms-pkipko",
						"pma" => "application/x-perfmon",
						"pmc" => "application/x-perfmon",
						"pml" => "application/x-perfmon",
						"pmr" => "application/x-perfmon",
						"pmw" => "application/x-perfmon",
						"png" => "image/png",
						"pnm" => "image/x-portable-anymap",
						"pot" => "application/vnd.ms-powerpoint",
						"ppm" => "image/x-portable-pixmap",
						"pps" => "application/vnd.ms-powerpoint",
						"ppt" => "application/vnd.ms-powerpoint",
						"prf" => "application/pics-rules",
						"ps" => "application/postscript",
						"pub" => "application/x-mspublisher",
						"qt" => "video/quicktime",
						"ra" => "audio/x-pn-realaudio",
						"ram" => "audio/x-pn-realaudio",
						"ras" => "image/x-cmu-raster",
						"rgb" => "image/x-rgb",
						"rmi" => "audio/mid",
						"roff" => "application/x-troff",
						"rtf" => "application/rtf",
						"rtx" => "text/richtext",
						"scd" => "application/x-msschedule",
						"sct" => "text/scriptlet",
						"setpay" => "application/set-payment-initiation",
						"setreg" => "application/set-registration-initiation",
						"sh" => "application/x-sh",
						"shar" => "application/x-shar",
						"sit" => "application/x-stuffit",
						"snd" => "audio/basic",
						"spc" => "application/x-pkcs7-certificates",
						"spl" => "application/futuresplash",
						"src" => "application/x-wais-source",
						"sst" => "application/vnd.ms-pkicertstore",
						"stl" => "application/vnd.ms-pkistl",
						"stm" => "text/html",
						"svg" => "image/svg+xml",
						"sv4cpio" => "application/x-sv4cpio",
						"sv4crc" => "application/x-sv4crc",
						"t" => "application/x-troff",
						"tar" => "application/x-tar",
						"tcl" => "application/x-tcl",
						"tex" => "application/x-tex",
						"texi" => "application/x-texinfo",
						"texinfo" => "application/x-texinfo",
						"tgz" => "application/x-compressed",
						"tif" => "image/tiff",
						"tiff" => "image/tiff",
						"tr" => "application/x-troff",
						"trm" => "application/x-msterminal",
						"tsv" => "text/tab-separated-values",
						"txt" => "text/plain",
						"uls" => "text/iuls",
						"ustar" => "application/x-ustar",
						"vcf" => "text/x-vcard",
						"vrml" => "x-world/x-vrml",
						"wav" => "audio/x-wav",
						"wcm" => "application/vnd.ms-works",
						"wdb" => "application/vnd.ms-works",
						"wks" => "application/vnd.ms-works",
						"wmf" => "application/x-msmetafile",
						"wps" => "application/vnd.ms-works",
						"wri" => "application/x-mswrite",
						"wrl" => "x-world/x-vrml",
						"wrz" => "x-world/x-vrml",
						"xaf" => "x-world/x-vrml",
						"xbm" => "image/x-xbitmap",
						"xla" => "application/vnd.ms-excel",
						"xlc" => "application/vnd.ms-excel",
						"xlm" => "application/vnd.ms-excel",
						"xls" => "application/vnd.ms-excel",
						"xlt" => "application/vnd.ms-excel",
						"xlw" => "application/vnd.ms-excel",
						"xof" => "x-world/x-vrml",
						"xpm" => "image/x-xpixmap",
						"xwd" => "image/x-xwindowdump",
						"z" => "application/x-compress",
						"zip" => "application/zip");	
	
	return $mime_types[$fileType];
}

function get_status_message($code){
	$status = array(
				100 => 'Continue',  
				101 => 'Switching Protocols',  
				200 => 'OK',
				201 => 'Created',  
				202 => 'Accepted',  
				203 => 'Non-Authoritative Information',  
				204 => 'No Content',  
				205 => 'Reset Content',  
				206 => 'Partial Content',  
				300 => 'Multiple Choices',  
				301 => 'Moved Permanently',  
				302 => 'Found',  
				303 => 'See Other',  
				304 => 'Not Modified',  
				305 => 'Use Proxy',  
				306 => '(Unused)',  
				307 => 'Temporary Redirect',  
				400 => 'Bad Request',  
				401 => 'Unauthorized',  
				402 => 'Payment Required',  
				403 => 'Forbidden',  
				404 => 'Not Found',  
				405 => 'Method Not Allowed',  
				406 => 'Not Acceptable',  
				407 => 'Proxy Authentication Required',  
				408 => 'Request Timeout',  
				409 => 'Conflict',  
				410 => 'Gone',  
				411 => 'Length Required',  
				412 => 'Precondition Failed',  
				413 => 'Request Entity Too Large',  
				414 => 'Request-URI Too Long',  
				415 => 'Unsupported Media Type',  
				416 => 'Requested Range Not Satisfiable',  
				417 => 'Expectation Failed',  
				500 => 'Internal Server Error',  
				501 => 'Not Implemented',  
				502 => 'Bad Gateway',  
				503 => 'Service Unavailable',  
				504 => 'Gateway Timeout',  
				505 => 'HTTP Version Not Supported');
	return ($status[$code]) ? $status[$code] : $status[500];
}

function cleanInputs($data){
	
    $clean_input = array();
	
	$can_foreach = is_array($data) || is_object($data);	
	
    //if(is_array($data)){
	 if ($can_foreach) {
		 
        foreach($data as $k => $v){

           $clean_input[$k] = cleanInputs($v);

        }		

    } else {
		
        if(get_magic_quotes_gpc()){

            $data = trim(stripslashes($data));

        }

        $data = strip_tags($data);

        $clean_input = trim($data);

    }
	
    return $clean_input;

}

// Convert Base64To To Image
function convertBase64ToImage($path = NULL, $fileName = NULL, $decoded_file) {
    if (!empty($decoded_file)) {
        /*$encoded_file = str_replace('data:image/png;base64,', '', $encoded_file);
        $encoded_file = str_replace(' ', '+', $encoded_file);
        $encoded_file = str_replace('data:image/jpeg;base64,', '', $encoded_file);
        $encoded_file = str_replace('data:image/gif;base64,', '', $encoded_file);
        $decoded_file = base64_decode($encoded_file);*/
        $image = imagecreatefromstring($decoded_file);
        
        $directory = $path . $fileName;
		
		$ext = explode('.', $fileName);
		
		if($ext[1] == "jpg" || $ext[1] == "jpeg"){
			
			 $saveImage = imagejpeg($image, $directory);
		
		} else {			
			
			$saveImage = imagepng($image, $directory);
			
		} 

        //imagedestroy($image);

        if ($saveImage) {
			
            return $fileName;
			
        } else {
			
            return false; // image not saved
        }
    }
}


function convertBase64ToFile($filePath, $decoded_file1){
	
	$errArr = array();
	
	/*$decoded_file1 = explode(",", $decoded_file1);
	print_r($decoded_file1);exit;
	if(isset($decoded_file1[1])!= ""){
		
		$decodeString = $decoded_file1[1];
		
	} else {
		
		$decodeString = $decoded_file1[0];
		
	}*/
	
	$pdf = fopen($filePath, 'w');

		if($pdf){
			
			 $content = fwrite($pdf, $decoded_file1);
			 
			 if(!$content){
				 
				 return false;
				 
			 } else {
				 
				 return true;
			 }								 
			
		} else {
			
			return false;
		} 
	
}

// Email Validation 
function isValidEmail($email){
		
	return (filter_var($email, FILTER_VALIDATE_EMAIL)) ? true : false;
} 

// Validation Mobile 
function isValidMobile($mobile){
	
    return (preg_match('/^[0-9]{10}+$/', $mobile)) ? true : false;
	
}

//including http/https for valid URL otherwise not working
function isValidURL($url){
	
	return (filter_var($url, FILTER_VALIDATE_URL )) ? true : false;
}

// Alfanumeric Validation
function isAlphaNumeric($str){
	
	return (preg_match('/^[a-zA-Z]+[a-zA-Z0-9._]+$/', $str)) ? true : false;
}

function isValidDateFormat($date, $format = 'Y-m-d')
{
    $d = DateTime::createFromFormat($format, $date);
    // The Y ( 4 digits year ) returns TRUE for any integer with any number of digits so changing the comparison from == to === fixes the issue.
    return $d && $d->format($format) === $date;
}

// Only Numeric Value 
function isNumericValue($str){
		
	return (preg_match("/^[0-9]?$/", $str)) ? true : false;
}

// Validation Character allowed 
function isCharAllowed($str){
	
	return (preg_match("/^[A-Za-z]+$/", $str)) ? true : false;	
}

// Select box validation
function isSelect($str){
	
	if(isset($str) && $str == 0){
		
		return false;
	}
	
	return true;
	
}

//Check for password strength, password should be at least n characters, contain at least one number, contain at least one lowercase letter, contain at least one uppercase letter, contain at least one special character.
function password_strength($password) {
	$returnVal = true;
	//$password_length = 8;
	if ( strlen($password) < 8 ) {
		$returnVal = false;
	}
	if ( !preg_match("#[0-9]+#", $password) ) {
		$returnVal = false;
	}
	if ( !preg_match("#[a-z]+#", $password) ) {
		$returnVal = false;
	}
	if ( !preg_match("#[A-Z]+#", $password) ) {
		$returnVal = false;
	}
	if ( !preg_match("/[\'^£$%&*()}{@#~?><>,|=_+!-]/", $password) ) {
		$returnVal = false;
	}
	return $returnVal;
}

// String Limit 
function stringLimit($str, $limit){
	
	if (strlen($str) > $limit){
		
		$str = substr($str, 0, $limit) . '...';
		return $str;
	}
	return $str;
}

function isValidLatitude($latitude) {
	
	return (preg_match("/^-?([1-8]?[1-9]|[1-9]0)\.{1}\d{1,8}$/", $latitude)) ? true : false;	
	
}
 
function isValidLongitude($longitude) {
	
	return (preg_match("/^-?([1]?[1-7][1-9]|[1]?[1-8][0]|[1-9]?[0-9])\.{1}\d{0,8}$/", $longitude)) ? true : false;
	
}

// Generate UUID for attachment
// http://php.net/manual/en/function.com-create-guid.php
/**
* Returns a GUIDv4 string
*
* Uses the best cryptographically secure method 
* for all supported pltforms with fallback to an older, 
* less secure version.
*
* @param bool $trim
* @return string
*/
function GUIDv4 ($trim = true)
{
    // Windows
    if (function_exists('com_create_guid') === true) {
        if ($trim === true)
            return trim(com_create_guid(), '{}');
        else
            return com_create_guid();
    }

    // OSX/Linux
    if (function_exists('openssl_random_pseudo_bytes') === true) {
		//$data = random_bytes(16);
        $data = openssl_random_pseudo_bytes(16);
        $data[6] = chr(ord($data[6]) & 0x0f | 0x40);    // set version to 0100
        $data[8] = chr(ord($data[8]) & 0x3f | 0x80);    // set bits 6-7 to 10
        return vsprintf('%s%s-%s-%s-%s-%s%s%s', str_split(bin2hex($data), 4));
    }

    // Fallback (PHP 4.2+)
    mt_srand((double)microtime() * 10000);
    $charid = strtolower(md5(uniqid(rand(), true)));
    $hyphen = chr(45);                  // "-"
    //$lbrace = $trim ? "" : chr(123);    // "{"
    //$rbrace = $trim ? "" : chr(125);    // "}"
	$rbrace = $lbrace = "";
    $guidv4 = $lbrace.
              substr($charid,  0,  8).$hyphen.
              substr($charid,  8,  4).$hyphen.
              substr($charid, 12,  4).$hyphen.
              substr($charid, 16,  4).$hyphen.
              substr($charid, 20, 12).
              $rbrace;
    return $guidv4;
}

?>