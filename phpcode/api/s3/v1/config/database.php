<?php
class Database{
 
    // Specify your local machine database credentials
	
	// PGSQL Connection - localhost
    /*private $host = "10.10.14.80";
    private $db_name = "api_db";
    private $username = "postgres";
    private $password = "root";*/
	
	// PGSQL Connection - VM
	/*private $host = "10.10.128.253";
    private $db_name = "TAMS";
    private $username = "postgres";
    private $password = "postgres";
	public $conn;*/	
	
	// PGSQL Connection
	//private $host = "103.249.98.164";
	private $host = "10.20.30.9";
	private $db_name = "TAMS";
	private $username = "postgres";
	private $password = "supp0rt#anilr";
	public $conn;
	
	// MongoDB Connection
	//private $mhost = "103.249.98.164";
	private $mhost = "10.20.30.9";
	private $mport = "27017";
	private $mdb_name = "TAMS";
	private $musername = "";
	private $mpassword = "";
	public $mconn;
	
	// MongoDB Connection - VM
	/*private $mhost = "10.10.128.253";
	private $mport = "27017";
	private $mdb_name = "TAMS";
	private $musername = "";
	private $mpassword = "";
	public $mconn;*/
 
    // PGSQL database connection
    public function getConnection(){
		
		$this->conn = NULL;
 
        try{
            $this->conn = new PDO("pgsql:host=" . $this->host . ";dbname=" . $this->db_name, $this->username, $this->password);
			//echo "Connection done"; die();
        }catch(PDOException $exception){
            echo "Connection error: " . $exception->getMessage();
        }
 
        return $this->conn;
    }
	
	// MongoDB database connection
    public function getMongoConnection(){
		
		$this->mconn = NULL;
 
        $server = "mongodb://".$this->mhost.":".$this->mport."";
		//$server = "mongodb://".$this->musername.":".$this->mpassword."@".$this->mhost.":".$this->mport."/".$this->mdb_name."";
		//$server = "mongodb://".$this->mhost.":".$this->mport."/".$this->mdb_name."";
		
		try{
            $this->mconn = new MongoDB\Driver\Manager($server);
        }catch(MongoDB\Driver\Exception\Exception $e){
			//var_dump($e);
			echo "Exception:", $e->getMessage(), "\n";
			//die();
        }
 
        return $this->mconn;
    }
}
?>