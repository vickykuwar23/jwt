<?php
/*** files needed to connect to database and class***/
define("ROOT_PATH", realpath(__DIR__ . '/../../..'));
include_once ROOT_PATH.'/constant.php';

include_once CONFIG_PATH.'functions.php';
include_once CONFIG_PATH.'database.php';
include_once RESOURCE_PATH.'accident/accident.php';
include_once RESOURCE_PATH.'logs/logs.php';
include_once RESOURCE_PATH.'auth/auth.php';

include_once LIB_PATH.'eCos/eCos.php';

// Required headers
requireHeaders();
 
/*** get database connection ***/
$database 	= new Database();
$db 		= $database->getConnection();
$mdb 		= $database->getMongoConnection();
 
// instantiate product object
$accident  = new Accident($db);
$logs  = new Logs($mdb);
$auth  = new Auth();

// Get Request Method 
$request_method		= get_request_method();

switch($request_method)
{
	case 'POST':
		
		// get posted data
		$data 	= get_request_body();
		
		if($data['status'] == true){
			
			$error = array();
			
			// set accident property values
			$accident->ReporteeName = isset($data['requestData']['ReporteeName']) ? $data['requestData']['ReporteeName'] : "";
			$accident->ReporteeEmail  = isset($data['requestData']['ReporteeEmail']) ? $data['requestData']['ReporteeEmail'] : "";
			$accident->ReporteeMobileNo  = isset($data['requestData']['ReporteeMobileNo']) ? $data['requestData']['ReporteeMobileNo'] : "";	
			$accident->ReporteeTypeId = isset($data['requestData']['ReporteeTypeId']) ? $data['requestData']['ReporteeTypeId'] : "";	
			$accident->AccidentPlaceLat = isset($data['requestData']['AccidentPlaceLat']) ? $data['requestData']['AccidentPlaceLat'] : "";
			$accident->AccidentPlaceLong = isset($data['requestData']['AccidentPlaceLong']) ? $data['requestData']['AccidentPlaceLong'] : "";
			$accident->AccidentLandmark = isset($data['requestData']['AccidentLandmark']) ? $data['requestData']['AccidentLandmark'] : "";
			$accident->AccidentAddress = isset($data['requestData']['AccidentAddress']) ? $data['requestData']['AccidentAddress'] : "";
			$accident->CollisionTypeId = isset($data['requestData']['CollisionTypeId']) ? $data['requestData']['CollisionTypeId'] : "";
			$accident->CrashSeverityTypeId = isset($data['requestData']['CrashSeverityTypeId']) ? $data['requestData']['CrashSeverityTypeId'] : "";
			$accident->AccidentDate = isset($data['requestData']['AccidentDate']) ? $data['requestData']['AccidentDate'] : "";
			$accident->AccidentCause = isset($data['requestData']['AccidentCause']) ? $data['requestData']['AccidentCause'] : "";
			$accident->CausalityCount = isset($data['requestData']['CausalityCount']) ? $data['requestData']['CausalityCount'] : "";
			$accident->SurvivorCount = isset($data['requestData']['SurvivorCount']) ? $data['requestData']['SurvivorCount'] : "";
			$accident->AccidentDescription = isset($data['requestData']['AccidentDescription']) ? $data['requestData']['AccidentDescription'] : "";
			$accident->VidName = isset($data['requestData']['VidName']) ? $data['requestData']['VidName'] : "";
			$accident->DocName = isset($data['requestData']['DocName']) ? $data['requestData']['DocName'] : "";
			//$accident->WitnessDetails = isset($data['requestData']['WitnessDetails']) ? $data['requestData']['WitnessDetails'] : "";
			$accident->VehicleDetails = isset($data['requestData']['VehicleDetails']) ? $data['requestData']['VehicleDetails'] : "";
			
			if(trim($accident->ReporteeName) == ""){
				
				$error[] = "Please enter Reportee Name.";
				
			} 
			if($accident->ReporteeEmail == ""){
				
				$error[] = "Please enter Reportee Email.";
				
			} else if(isValidEmail($accident->ReporteeEmail) != true){
				
				$error[] = "Please enter valid Reportee Email.";
				
			}
			if($accident->ReporteeMobileNo == ""){
				
				$error[] = "Please enter Reportee Mobile No.";
				
			} else if(isValidMobile($accident->ReporteeMobileNo) == false){
				
				$error[] = "Please enter 10 digit Reportee Mobile No.";
				
			} 
			if(isSelect($accident->ReporteeTypeId) == false){
				
				$error[] = "Please select Reportee Type.";
				
			} 
			if(isSelect($accident->CollisionTypeId) == false){
				
				$error[] = "Please select Collision Type.";
				
			} 
			if(isSelect($accident->CrashSeverityTypeId) == false){
				
				$error[] = "Please select Crash Severity Type.";
				
			} 
			if($accident->AccidentDate == ""){
				
				$error[] = "Please enter Accident Date.";
				
			} else if(isValidDateFormat($accident->AccidentDate) == 0){
				
				$error[] = "Please enter Accident Date in YYYY-MM-DD format.";
				
			}
			/*if($accident->AccidentCause == ""){
				
				$error[] = "Please enter Accident Cause.";
			}*/
			if($accident->AccidentPlaceLong == ""){
				
				$error[] = "Please enter longitude.";
				
			}
			if($accident->AccidentPlaceLat == ""){
				
				$error[] = "Please enter latitude.";
				
			}
			
			
			// Images Upload Code				
				if (is_array($accident->DocName) || is_object($accident->DocName)) {
					
					if(count($accident->DocName) <= MAX_IMG){
						
						foreach($accident->DocName as $document){
							
								if(!empty($document)){
													
									$allowedExts = array("jpeg", "jpg", "png");
									
									$imgName 	 = $document['AttachName'];
									
									if(strpos($imgName, ".") === false) {
										
										$error[] = "Uploaded file ".$imgName." is invalid.";
										
									} else {
										
										$img_ext 	 = explode('.', $imgName );
										$extension   = strtolower( end($img_ext));
										
										// for create physical file.
										$encoded_file = $document['AttachCode'];
										
										//$encoded_file = str_replace('data:image/png;base64,', '', $encoded_file);
										$encoded_file = str_replace(" ", "+", $encoded_file);
										//$encoded_file = str_replace('data:image/jpeg;base64,', '', $encoded_file);
										//$encoded_file = str_replace('data:image/gif;base64,', '', $encoded_file);	
				
									   // Min size for Image size : 5 KB ,Max size for Image size :500 					
										$decoded_file1 = base64_decode($encoded_file);
										
										$size = strlen($decoded_file1);
										$size_kb = floor($size / 1024);
										
											
									if(($extension != "jpg" || $extension!= "JPG") && ($extension != "jpeg" || $extension != "JPEG") && ($extension != "png" || $extension != "PNG") && !in_array($extension, $allowedExts)){		
											
											$error[] = "Please upload picture ".$imgName." with .jpg, .jpeg, .png format.";
											
											
										} else if($size_kb < IMG_MIN_SIZE){
											
											$error[] = "Upload Picture ".$imgName." size must be more than 5kb.";
											
											
										} else if($size_kb > IMG_MAX_SIZE){
											
											$error[] = "Upload Picture ".$imgName." size must be less than equal to 500kb.";
											
											
										} else {
											
											$uniq_id = GUIDv4();
											$imgName = 'myPicure-'.$uniq_id.'.'.$extension;
											$accident->AttachName[] = $imgName;
											
											// code to upload doc to eCos
											$ecos = new eCos();
											$ecos->getToken();
											
											$container_name = CONTAINER_NAME;
											$folder_name 	= UPLOAD_PIC;							
											$file_name 		= $imgName;
											$ImageType		= getContentType($extension);
											//$file_path 		= 'data:'.$ImageType.';base64,'.$encoded_file;
											$file_path 		= $encoded_file;
											
											$imageResponse 	= $ecos->uploadObject($container_name, $folder_name, $file_path, $file_name);
											// eof code to upload doc to eCos
											
											if($imageResponse == false){
												
												$error[] = "Picture not uploaded.";
												
											}
											
										} // extension end
									} // If no extension found									
								} // Image end						
							} // end foreach
						
					} else {
						
						$error[] = "Maximum 5 pictures upload.";
						
					}

				} // Images Object End
				
					
				if (is_array($accident->VidName) || is_object($accident->VidName)) {
					
					if(count($accident->VidName) <= MAX_VIDEO){
					
						foreach($accident->VidName as $documentV){
							
								if(!empty($documentV)){
													
									$allowedExts = array("3gp", "mp4");
									
									$imgName 	 = $documentV['AttachVideo'];
									
									if(strpos($imgName, ".") === false) {
										
										$error[] = "Uploaded file ".$imgName." is invalid.";
										
									} else {
									
										$img_ext 	 = explode('.', $imgName );
										$extension   = strtolower( end($img_ext));
										
										// for create physical file.
										$encoded_file = $documentV['VideoCode'];
										
										//$encoded_file = str_replace('data:image/png;base64,', '', $encoded_file);
										$encoded_file = str_replace(" ", "+", $encoded_file);
										//$encoded_file = str_replace('data:image/jpeg;base64,', '', $encoded_file);
										//$encoded_file = str_replace('data:image/gif;base64,', '', $encoded_file);	
				
									   //Min size for Video : 1KB, Max size for Video :2048KB 					
										$decoded_file1 = base64_decode($encoded_file);
										
										$size = strlen($decoded_file1);
										$size_kb = floor($size / 1024);
										
										
										if(($extension != "3gp" || $extension!= "3GP") && ($extension != "mp4" || $extension != "MP4") && !in_array($extension, $allowedExts)){		
											
											$error[] = "Please upload video ".$imgName." with .3gp, .mp4 format.";
											
										} else if($size_kb < VIDEO_MIN_SIZE){
											
											$error[] = "Upload video ".$imgName." size must be more than 1kb.";
											
										} else if($size_kb > VIDEO_MAX_SIZE){
											
											$error[] = "Upload video ".$imgName." size must be less than equal to 2048kb.";
											
										} else {
										
											$uniq_id = GUIDv4();
											$vidName = 'myVideo-'.$uniq_id.'.'.$extension;
											$accident->AttachVideo[] = $vidName;
											
											// code to upload video to eCos
											$ecos = new eCos();
											$ecos->getToken();
											
											$container_name = CONTAINER_NAME;
											$folder_name 	= VIDEO_FOLDER;							
											$file_name 		= $vidName;
											$ImageType		= getContentType($extension);
											//$file_path 		= 'data:'.$ImageType.';base64,'.$encoded_file;
											$file_path 		= $encoded_file;
											
											$imageResponse 	= $ecos->uploadObject($container_name, $folder_name, $file_path, $file_name);
											// eof code to upload doc to eCos
											
											if($imageResponse == false){
												
												$error[] = "Video not uploaded.";
												
											}
										
									} // Extension if checked
								
								} // No extension validation
									
							} // Video end
						
						} // end foreach
					
					} else {
						
						$error[] = "Maximum 2 video upload.";
					}

				} // Video Object End
			
			
			
			if(count($error) > 0){
				
					$errorMsg = implode(", ", $error);
					
					// tell the user login failed
					$resArr = array("status" => "0", "message" => $errorMsg);
					
					 //Add log data
					 $logs->Module 		= "Accident";
					 $logs->Action		= "create";
					 $logs->Request		= json_encode($data);
					 $logs->Response	= json_encode($resArr);
					 $logs->UserId		= "";
					 $logs->UserType	= "";
					 $logs->AddLog();
						 
					// tell the user login failed
					 response($resArr, 400);
				
			} else {
				
					$insertId = $accident->create();
			
					if($insertId > 0){
						
						$action	=	"Add";
						
						$accident->insertId = $insertId;
						
						$uploadFile 	= $accident->addDocuments($action);
						
						$uploadVideos 	= $accident->addVideos($action);
			
						$addVehicleData = $accident->addVehicleData($action);

						//$addWitnessData = $accident->addWitnessData($add);	
						
						// display message: accident was created
						$resArr = array("status" => "1", "message" => "Accident details created successfully.");
						
						 $logs->Module 		= "Accident";
						 $logs->Action		= "create";
						 $logs->Request		= json_encode($data);
						 $logs->Response	= json_encode($resArr);
						 $logs->UserId		= "";
						 $logs->UserType	= "";
						 $logs->AddLog();
						
						// display message: accident was created
						 response($resArr, 200);
						
					} else { // message if unable to create accident
					
						// display message: unable to create accident
						$resArr = array("status" => "0", "message" => "Unable to create data.");
						
						 $logs->Module 		= "Accident";
						 $logs->Action		= "create";
						 $logs->Request		= json_encode($data);
						 $logs->Response	= json_encode($resArr);
						 $logs->UserId		= "";
						 $logs->UserType	= "";
						 $logs->AddLog();
						
						 // display message: unable to create accident
						 response($resArr, 400);
						
					}
				
			} // Error End				
			
		} else {
			
			// display message: unable to create accident
			$resArr = array("status" => "0", "message" => $data["requestData"]);
			
			 $logs->Module 		= "Accident";
			 $logs->Action		= "create";
			 $logs->Request		= "";
			 $logs->Response	= json_encode($resArr);
			 $logs->UserId		= "";
			 $logs->UserType	= "";
			 $logs->AddLog();
			 
			response($resArr, 400);
		}
		
		break;
		
	case 'PUT':	
	
		$bearerToken = getBearerToken();
		
		$access_token = isset($bearerToken) ? $bearerToken : "";
		
		$access_token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9uaWppcy5jb20iLCJhdWQiOiJodHRwOlwvXC9uaWppcy5jb20iLCJpYXQiOjE1NDg5MzI4NDQsIm5iZiI6MTU0ODkzMjg0NCwiZXhwIjoxNjA4OTMyODQ0LCJqdGkiOiI1YzUyZDZlY2QyZjIwIiwiZGF0YSI6eyJVc2VySWQiOjMsIlVzZXJOYW1lIjoidGVzdCIsIlVzZXJSb2xlIjoiSW52ZXN0aWdhdGlvbiBPZmZpY2VyIn19.8HPMopaREF94dQ4ZTUdCFHtL6-nmeFnHspr5argMyY8";
	
		if($access_token){
			
			$data 	= array();
			$getArr = array();
			$reqArr = array();
			
			// decode jwt
			$decoded = $auth->validateToken($access_token);
			
			if($decoded['status'] == true){
				
				$userID 		= $decoded['TokenData']->data->UserId; // Decode ID
				$UserName		= $decoded['TokenData']->data->UserName; // Decode UserName
				$UserRole 		= $decoded['TokenData']->data->UserRole; // Decode UserRole			
				
				$tokenArr = array("UserId" => (int)$userID, "UserName" => $UserName, "UserRole" => $UserRole);

				// If UserRole is Police Officer execute following code
				if($UserRole == "PO"){
					
					// generate jwt
					 $access_token = $auth->generateToken($tokenArr);
					
					 // response in json format
					 $resArr = array(
									"status" 		=> "0", 
									"message" 		=> "You are not authorized to perform this operation.",
									"access_token" 	=> $access_token
								);		
						
					 // Merge Array Post And GET Values
					 $reqArr = array_merge($data, $getArr);		
								
					 //Add log data
					 $logs->Module 		= "Accident";
					 $logs->Action		= "update";
					 $logs->Request		= json_encode($reqArr);
					 $logs->Response	= json_encode($resArr);
					 $logs->UserId		= (int)$userID;
					 $logs->UserType 	= $UserRole;	
					 $logs->AddLog();		
					 
					 response($resArr, 403);

				} // End Authorized Role End	
				
				// Get URL Data	
				$getArr = get_url_data();
				
				// Get ID From URL		
				$accident->insertId = isset($getArr['id']) ? intval($getArr['id']) : "";
				
				// get posted data
				$data = get_request_body();
				
				if($data["status"] == true){					
					
					// set accident property values
					$accident->ReporteeName = isset($data['requestData']['ReporteeName']) ? $data['requestData']['ReporteeName'] : "";
					$accident->ReporteeEmail  = isset($data['requestData']['ReporteeEmail']) ? $data['requestData']['ReporteeEmail'] : "";
					$accident->ReporteeMobileNo  = isset($data['requestData']['ReporteeMobileNo']) ? $data['requestData']['ReporteeMobileNo'] : "";	
					$accident->ReporteeTypeId = isset($data['requestData']['ReporteeTypeId']) ? $data['requestData']['ReporteeTypeId'] : "";	
					$accident->AccidentPlaceLat = isset($data['requestData']['AccidentPlaceLat']) ? $data['requestData']['AccidentPlaceLat'] : "";
					$accident->AccidentPlaceLong = isset($data['requestData']['AccidentPlaceLong']) ? $data['requestData']['AccidentPlaceLong'] : "";
					$accident->AccidentLandmark = isset($data['requestData']['AccidentLandmark']) ? $data['requestData']['AccidentLandmark'] : "";
					$accident->AccidentAddress = isset($data['requestData']['AccidentAddress']) ? $data['requestData']['AccidentAddress'] : "";
					$accident->CollisionTypeId = isset($data['requestData']['CollisionTypeId']) ? $data['requestData']['CollisionTypeId'] : "";
					$accident->CrashSeverityTypeId = isset($data['requestData']['CrashSeverityTypeId']) ? $data['requestData']['CrashSeverityTypeId'] : "";
					$accident->AccidentDate = isset($data['requestData']['AccidentDate']) ? $data['requestData']['AccidentDate'] : "";
					$accident->AccidentCause = isset($data['requestData']['AccidentCause']) ? $data['requestData']['AccidentCause'] : "";
					$accident->CausalityCount = isset($data['requestData']['CausalityCount']) ? $data['requestData']['CausalityCount'] : "";
					$accident->SurvivorCount = isset($data['requestData']['SurvivorCount']) ? $data['requestData']['SurvivorCount'] : "";
					$accident->AccidentDescription = isset($data['requestData']['AccidentDescription']) ? $data['requestData']['AccidentDescription'] : "";
					$accident->AccidentStatus = isset($data['requestData']['AccidentStatus']) ? $data['requestData']['AccidentStatus'] : "";
					$accident->VidName = isset($data['requestData']['VidName']) ? $data['requestData']['VidName'] : "";
					$accident->DocName = isset($data['requestData']['DocName']) ? $data['requestData']['DocName'] : "";
					$accident->WitnessDetails = isset($data['requestData']['WitnessDetails']) ? $data['requestData']['WitnessDetails'] : "";
					$accident->VehicleDetails = isset($data['requestData']['VehicleDetails']) ? $data['requestData']['VehicleDetails'] : "";
					
					$error = array();
					
					if($accident->insertId == ""){
						
						$error[] = "Accident ID is required.";
						
					} else {
						
						$UpdateDuplicate = "";
					
						if(isset($data['requestData']['IsDuplicate'])){
								
							$UpdateDuplicate = "UpdateDuplicate";
							$UpdateMsg		 = "Accident mark as duplicate successfully.";
							$accident->IsDuplicate = $data['requestData']['IsDuplicate'];
							
							if($accident->IsDuplicate == ""){
						
									$error[] = "IsDuplicate is required.";
									
							} 
							
						} else {
								
								$UpdateDuplicate = "UpdateProfile";
								$UpdateMsg	= "Accident details was successfully updated.";
								
								if(trim($accident->ReporteeName) == ""){
				
									$error[] = "Please enter Reportee Name.";
									
								} 
								if($accident->ReporteeEmail == ""){
									
									$error[] = "Please enter Reportee Email.";
									
								} else if(isValidEmail($accident->ReporteeEmail) != true){
									
									$error[] = "Please enter valid Reportee Email.";
									
								}
								if($accident->ReporteeMobileNo == ""){
									
									$error[] = "Please enter Reportee Mobile No.";
									
								} else if(isValidMobile($accident->ReporteeMobileNo) == false){
									
									$error[] = "Please enter 10 digit Reportee Mobile No.";
									
								} 
								if(isSelect($accident->ReporteeTypeId) == false){
									
									$error[] = "Please select Reportee Type.";
									
								} 
								if(isSelect($accident->CollisionTypeId) == false){
									
									$error[] = "Please select Collision Type.";
									
								} 
								if(isSelect($accident->CrashSeverityTypeId) == false){
									
									$error[] = "Please select Crash Severity Type.";
									
								} 
								if($accident->AccidentDate == ""){
									
									$error[] = "Please enter Accident Date.";
									
								} else if(isValidDateFormat($accident->AccidentDate) == 0){
									
									$error[] = "Please enter Accident Date in YYYY-MM-DD format.";
									
								}
								/*if($accident->AccidentCause == ""){
									
									$error[] = "Please enter Accident Cause.";
								}*/
								if($accident->AccidentPlaceLong == ""){
									
									$error[] = "Please enter longitude.";
									
								}
								if($accident->AccidentPlaceLat == ""){
									
									$error[] = "Please enter latitude.";
									
								}
						}						
						
						
						$accidentExists = $accident->checkAccidentDetails($accident->insertId);
					
						if($accidentExists == false){
							
							// generate jwt
							 $access_token = $auth->generateToken($tokenArr);
							
							 // response in json format
							 $resArr = array(
											"status" 		=> "1", 
											"message" 		=> "Accident details does not exist.",
											"access_token" 	=> $access_token
										);		
								
							 // Merge Array Post And GET Values
							 $reqArr = array_merge($data, $getArr);		
										
							 //Add log data
							 $logs->Module 		= "Accident";
							 $logs->Action		= "update";
							 $logs->Request		= json_encode($reqArr);
							 $logs->Response	= json_encode($resArr);
							 $logs->UserId		= (int)$userID;
							 $logs->UserType 	= $UserRole;	
							 $logs->AddLog();		
							 
							 response($resArr, 200);
						}
						
						// Check assign officer for case
						$getAccidentData = $accident->checkAssignedOfficer();
						
						$AccidentDetailsId = $getAccidentData[0]['AccidentDetailsId'];
						$assignUserID 	  = $getAccidentData[0]['AssignedUserId'];
						$updateAccident   = false;
						
						if($userID != $assignUserID){
							
							// generate jwt
							 $access_token = $auth->generateToken($tokenArr);
							
							 $resArr = array(
									"status" 		=> "1", 
									"message" 		=> "An accident details not assign to you.",
									"access_token" 	=> $access_token
								);
							
							 // Merge Array Post And GET Values
							 $reqArr = array_merge($data, $getArr);
							
							 $logs->Module 		= "Accident";
							 $logs->Action		= "update";
							 $logs->Request		= json_encode($reqArr);
							 $logs->Response	= json_encode($resArr);
							 $logs->UserType 	= $UserRole;
							 $logs->UserId		= (int)$userID;
							 $logs->AddLog();	
								
							 response($resArr, 200);
						}
							
						
						if($accident->insertId != ""){
							
							// Profile Upload Code
							if (is_array($accident->DocName) || is_object($accident->DocName)) {
								
								if(count($accident->DocName) <= MAX_IMG){
							
									foreach($accident->DocName as $document){
											
											if(!empty($document)){
																
												$allowedExts = array("jpeg", "jpg", "png");
												
												$imgName 	 = $document['AttachName'];
												
												if(strpos($imgName, ".") === false) {
										
													$error[] = "Uploaded file ".$imgName." is invalid.";
													
												} else {
												
												$img_ext 	 = explode('.', $imgName );
												$extension   = strtolower( end($img_ext));
												
												// for create physical file.
												$encoded_file = $document['AttachCode'];
												
												//$encoded_file = str_replace('data:image/png;base64,', '', $encoded_file);
												$encoded_file = str_replace(" ", "+", $encoded_file);
												//$encoded_file = str_replace('data:image/jpeg;base64,', '', $encoded_file);
												//$encoded_file = str_replace('data:image/gif;base64,', '', $encoded_file);	
						
											   // Min size for Image : 5 KB ,Max size for Image :500 					
												$decoded_file1 = base64_decode($encoded_file);
												
												$size = strlen($decoded_file1);
												$size_kb = floor($size / 1024);
												
													
											if(($extension != "jpg" || $extension!= "JPG") && ($extension != "jpeg" || $extension != "JPEG") && ($extension != "png" || $extension != "PNG") && !in_array($extension, $allowedExts)){		
													
													$error[] = "Please upload picture ".$imgName." with .jpg, .jpeg, .png format.";
													
												} else if($size_kb < IMG_MIN_SIZE){
													
													$error[] = "Upload Picture ".$imgName." size must be more than 5kb.";
													
												} else if($size_kb > IMG_MAX_SIZE){
													
													$error[] = "Upload Picture ".$imgName." size must be less than equal to 500kb.";
													
												} else {
													
													$uniq_id = GUIDv4();
													$imgName = 'myPicure-'.$uniq_id.'.'.$extension;
													$accident->AttachName[] = $imgName;
													
													// code to upload doc to eCos
													$ecos = new eCos();
													$ecos->getToken();
													
													$container_name = CONTAINER_NAME;
													$folder_name 	= UPLOAD_PIC;							
													$file_name 		= $imgName;
													$ImageType		= getContentType($extension);
													//$file_path 		= 'data:'.$ImageType.';base64,'.$encoded_file;
													$file_path 		= $encoded_file;
													
													$imageResponse 	= $ecos->uploadObject($container_name, $folder_name, $file_path, $file_name);
													// eof code to upload doc to eCos
													
													if($imageResponse == false){
														
														$error[] = "Picture not uploaded.";
														
													}
													
												} // extension end
												
											} // Image end
											
										} // End without extension file
									
									} // end foreach
									
								} else {
						
									$error[] = "Maximum 5 pictures upload.";
									
								} // Image Count End
								
							} // Images Object End
							
							if (is_array($accident->VidName) || is_object($accident->VidName)) {
								
								if(count($accident->VidName) <= MAX_VIDEO){
									
									foreach($accident->VidName as $documentV){
											
											if(!empty($documentV)){
																
												$allowedExts = array("3gp", "mp4");
												
												$imgName 	 = $documentV['AttachVideo'];
												
												if(strpos($imgName, ".") === false) {
										
													$error[] = "Uploaded file ".$imgName." is invalid.";
													
												} else {
													
													$img_ext 	 = explode('.', $imgName );
													$extension   = strtolower( end($img_ext));
													
													// for create physical file.
													$encoded_file = $documentV['VideoCode'];
													
													//$encoded_file = str_replace('data:image/png;base64,', '', $encoded_file);
													$encoded_file = str_replace(" ", "+", $encoded_file);
													//$encoded_file = str_replace('data:image/jpeg;base64,', '', $encoded_file);
													//$encoded_file = str_replace('data:image/gif;base64,', '', $encoded_file);	
							
													//Min size for Video : 1KB, Max size for Video 2048KB  					
													$decoded_file1 = base64_decode($encoded_file);
													
													  $size = strlen($decoded_file1);
													$size_kb = floor($size / 1024);
													
													
													if(($extension != "3gp" || $extension!= "3GP") && ($extension != "mp4" || $extension != "MP4") && !in_array($extension, $allowedExts)){		
													
														$error[] = "Please upload video ".$imgName." with .3gp, .mp4 format.";
														
													} else if($size_kb < VIDEO_MIN_SIZE){
														
														$error[] = "Upload video ".$imgName." size must be more than 1kb.";
														
													} else if($size_kb > VIDEO_MAX_SIZE){
														
														$error[] = "Upload video ".$imgName." size must be less than equal to 2048kb.";
														
													} else {
													
														$uniq_id = GUIDv4();
														$vidName = 'myVideo-'.$uniq_id.'.'.$extension;
														$accident->AttachVideo[] = $vidName;
														
														// code to upload video to eCos
														$ecos = new eCos();
														$ecos->getToken();
														
														$container_name = CONTAINER_NAME;
														$folder_name 	= VIDEO_FOLDER;							
														$file_name 		= $vidName;
														$ImageType		= getContentType($extension);
														//$file_path 		= 'data:'.$ImageType.';base64,'.$encoded_file;
														$file_path 		= $encoded_file;
														
														$imageResponse 	= $ecos->uploadObject($container_name, $folder_name, $file_path, $file_name);
														// eof code to upload doc to eCos
														
														if($imageResponse == false){
															
															$error[] = "Video not uploaded.";
															
														}
													
												} // Validation end
													
											} // Video end
										
										} // End without extension end
									
									} // end foreach
								
								} else {
						
									$error[] = "Maximum 2 video upload.";
								} // Video Count End

							} // Video Object End
						
						} // End If Get ID Is Null
						
					} // End If
					
					// Error Count Check to validate records
					if(count($error) > 0){
						
						$errorMsg = implode(", ", $error);
					
						// tell the user login failed
						$resArr = array("status" => "0", "message" => $errorMsg);
						
						 //Add log data
						 $logs->Module 		= "Accident";
						 $logs->Action		= "update";
						 $logs->Request		= json_encode($data);
						 $logs->Response	= json_encode($resArr);
						 $logs->UserId		= (int)$userID;
						 $logs->UserType 	= $UserRole;
						 $logs->AddLog();
							 
						// tell the user login failed
						 response($resArr, 400);
						
					} else {
							
							$accident->UpdatedByUserId = $userID;
																	
							$updateAccident = $accident->update($UpdateDuplicate);

							// If Success
							if($updateAccident){						
							
								$action	=	"Update";
										
								$uploadFile 	= $accident->addDocuments($action);
				
								$uploadVideos 	= $accident->addVideos($action);
					
								$addVehicleData = $accident->addVehicleData($action);

								$addWitnessData = $accident->addWitnessData();	
								
								 // generate jwt
								 $access_token = $auth->generateToken($tokenArr);
								 
								 
								 // response in json format
								 $resArr = array(
												"status" 		=> "1", 
												"message" 		=> $UpdateMsg,
												"access_token" 	=> $access_token
											);		
									
								 // Merge Array Post And GET Values
								 $reqArr = array_merge($data, $getArr);		
											
								 //Add log data
								 $logs->Module 		= "Accident";
								 $logs->Action		= "update";
								 $logs->Request		= json_encode($reqArr);
								 $logs->Response	= json_encode($resArr);
								 $logs->UserId		= (int)$userID;
								 $logs->UserType 	= $UserRole; 
								 $logs->AddLog();		
								 
								 response($resArr, 200);
							}
							 
							// message if unable to update data
							else {
								
								 // generate jwt
								 $access_token = $auth->generateToken($tokenArr);
							 
								 // show error message
								 $resArr = array("status" => "1", 
												"message" => "Unable to update data.",
												"access_token" 	=> $access_token);
								 
								 // Merge Array Post And GET Values
								 $reqArr = array_merge($data, $getArr);
								 
								 //Add log data
								 $logs->Module 		= "Accident";
								 $logs->Action		= "update";
								 $logs->Request		= json_encode($reqArr);
								 $logs->Response	= json_encode($resArr);
								 $logs->UserId		= (int)$userID;	
								 $logs->UserType 	= $UserRole;		
								 $logs->AddLog();
								 
								 response($resArr, 401);
							}	
						
						
					} // End Error Count Check to validate records
				
				} else {
					
					// show error message
					$resArr = array("status" => "0", "message" => $data["requestData"]);
				
					// Merge Array Post And GET Values
					$reqArr = array_merge($data, $getArr);	
				
					//Add log data
					 $logs->Module 		= "Accident";
					 $logs->Action		= "update";
					 $logs->Request		= json_encode($reqArr);
					 $logs->Response	= json_encode($resArr);
					 $logs->UserId		= "";
					 $logs->UserType    = ""; 	
					 $logs->AddLog();
				
					response($resArr, 400);
					
				}	
			
			} else {
				
				// show error message
				$resArr = array(
					"status" 	=> "0", 
					"message" 	=> "Invalid Authorization. Error : ". $decoded["TokenData"]
				);
				
				// Merge Array Post And GET Values
				$reqArr = array_merge($data, $getArr);
				
				//Add log data
				 $logs->Module 		= "Accident";
				 $logs->Action		= "update";
				 $logs->Request		= json_encode($reqArr);
				 $logs->Response	= json_encode($resArr);
				 $logs->UserId		= "";
				 $logs->UserType    = "";	
				 $logs->AddLog();
				
				response($resArr, 401);
			}
			
			
		}  else { // show error message if jwt is empty		 
			
			$resArr = array("status" => "0", "message" => "Access denied.");
			
			//Add log data
			 $logs->Module 		= "Accident";
			 $logs->Action		= "update";
			 $logs->Request		= "";
			 $logs->Response	= json_encode($resArr);
			 $logs->UserId		= "";
			 $logs->UserType	= "";	
			 $logs->AddLog();
			
			response($resArr, 401);
		}
	
		break;	
		
	case 'GET':	
		
		$bearerToken = getBearerToken();
		
		$access_token = isset($bearerToken) ? $bearerToken : "";
		
		if($access_token){

			// decode jwt		
			$decoded  = $auth->validateToken($access_token);
			
			$getArr = array();
			
			if($decoded["status"] == true) {
				
				$userID   = $decoded["TokenData"]->data->UserId;
				$UserName = $decoded['TokenData']->data->UserName; 
				$UserRole = $decoded["TokenData"]->data->UserRole;
				
				
				$getArr = get_url_data();					
				
				$id 	= isset($getArr['id']) ? intval($getArr['id']) : 0;
				$type   = isset($getArr['type']) ? $getArr['type'] : "";
				
				$tokenArr = array("UserId" => (int)$userID, "UserName" => $UserName, "UserRole" => $UserRole);
								
				$accidentListing = false;
				
				if($id > 0 && $type == "accidentimages")
				{				
					
					$accident->insertId = $id;
					
					$accidentRecord = $accident->checkAccidentAlert();
					
					if($accidentRecord == true)
					{
						
						$showImages = false;
						
						if($UserRole == "PO"){
							
							$showImages = true;
							
						} else if($UserRole == "IO"){
							
							// Check assign officer for case
							$getAccidentData = $accident->checkAssignedOfficer();
							
							$AccidentDetailsId = $getAccidentData[0]['AccidentDetailsId'];
							$assignUserID 	  = $getAccidentData[0]['AssignedUserId'];
							
							if($assignUserID == $userID){
								
								$showImages = true;
								
							} else {
								
								// generate jwt
								$access_token = $auth->generateToken($tokenArr);
								
								$resArr = array(
										"status" 		=> "1", 
										"message" 		=> "An accident details not assign to you.",
										"access_token" 	=> $access_token
									);
									
								 $logs->Module 		= "Accident";
								 $logs->Action		= "get";
								 $logs->Request		= json_encode($getArr);
								 $logs->Response	= json_encode($resArr);
								 $logs->UserId		= (int)$userID;
								 $logs->UserType 	= $UserRole;
								 $logs->AddLog();	
									
								response($resArr, 200);
							}
						
						}
						
						if($showImages){
						
							$accidentList = $accident->getAccidentPictures($id);
							
							if (is_array($accidentList) || is_object($accidentList)){
								
								$profileArr 	= array();
								
								foreach($accidentList as $accidentImg){
									
									$file_name = $accidentImg['DocName'];
									
									// code to download doc from eCos
									$ecos = new eCos();
									$ecos->getToken();
									
									$container_name = CONTAINER_NAME;
									$folder_name 	= UPLOAD_PIC;
									
									$downloadObject_resp = $ecos->downloadObject($container_name, $folder_name, $file_name);
									// eof code to download doc from eCos
									
									// get file content type
									$finfo = finfo_open();
									$mime_type = finfo_buffer($finfo, $downloadObject_resp, FILEINFO_MIME_TYPE);
									
									if(strlen($downloadObject_resp) > 0)
									{
										$profileArr["accidentImage"][] = array(
												"name" => $file_name,
												"type" => $mime_type,
												"data" => base64_encode($downloadObject_resp)
											);
											
									} 
									
								}
								
								// generate jwt
								$access_token = $auth->generateToken($tokenArr);			
									
								$resArr = array(
										"status" 		=> "1", 
										"message" 		=> "Accident Images get successfully.",
										"access_token" 	=> $access_token,
										"data" 			=> $profileArr
									);
									
								 $logs->Module 		= "Accident";
								 $logs->Action		= "get";
								 $logs->Request		= json_encode($getArr);
								 $logs->Response	= json_encode($resArr);
								 $logs->UserId		= (int)$userID;
								 $logs->UserType 	= $UserRole;
								 $logs->AddLog();		
								
								response($resArr, 200);
								
							} else {
								
								// generate jwt
								$access_token = $auth->generateToken($tokenArr);
								
								$resArr = array(
										"status" 		=> "1", 
										"message" 		=> "No accident Images found.",
										"access_token" 	=> $access_token
									);
									
								 $logs->Module 		= "Accident";
								 $logs->Action		= "get";
								 $logs->Request		= json_encode($getArr);
								 $logs->Response	= json_encode($resArr);
								 $logs->UserId		= (int)$userID;
								 $logs->UserType 	= $UserRole;
								 $logs->AddLog();
								 
								response($resArr, 200);
							}
						
						}
					
					} else {
						
						// generate jwt
							$access_token = $auth->generateToken($tokenArr);
							
							$resArr = array(
									"status" 		=> "1", 
									"message" 		=> "Accident details does not exist.",
									"access_token" 	=> $access_token
								);
								
							 $logs->Module 		= "Accident";
							 $logs->Action		= "get";
							 $logs->Request		= json_encode($getArr);
							 $logs->Response	= json_encode($resArr);
							 $logs->UserId		= (int)$userID;
							 $logs->UserType 	= $UserRole;
							 $logs->AddLog();
							 
							response($resArr, 200);
						
					}
					
				
				} else if($id > 0 && $type == "accidentvideos"){
					
					$accident->insertId = $id;
					
					$accidentRecord = $accident->checkAccidentAlert();
					
					if($accidentRecord == true)
					{
						
						$showVideo = false;
						
						if($UserRole == "PO"){
							
							$showVideo = true;
							
						} else if($UserRole == "IO"){
							
							// Check assign officer for case
							$getAccidentData = $accident->checkAssignedOfficer();
							
							$AccidentDetailsId = $getAccidentData[0]['AccidentDetailsId'];
							$assignUserID 	  = $getAccidentData[0]['AssignedUserId'];
						
							if($assignUserID == $userID){
								
								$showVideo = true;
								
							} else {
								
								// generate jwt
								$access_token = $auth->generateToken($tokenArr);
								
								$resArr = array(
										"status" 		=> "1", 
										"message" 		=> "An accident details not assign to you.",
										"access_token" 	=> $access_token
									);
									
								 $logs->Module 		= "Accident";
								 $logs->Action		= "get";
								 $logs->Request		= json_encode($getArr);
								 $logs->Response	= json_encode($resArr);
								 $logs->UserId		= (int)$userID;
								 $logs->UserType 	= $UserRole;
								 $logs->AddLog();	
									
								response($resArr, 200);
							}
						
						}
						
						if($showVideo){
						
							$accidentList = $accident->getAccidentVideos($id);
							
							if (is_array($accidentList) || is_object($accidentList)){
								
								$videoArr 	= array();
								
								foreach($accidentList as $accidentVideo){
									
									$file_name = $accidentVideo['DocName'];
									
									// code to download doc from eCos
									$ecos = new eCos();
									$ecos->getToken();
									
									$container_name = CONTAINER_NAME;
									$folder_name 	= VIDEO_FOLDER;
									
									$downloadObject_resp = $ecos->downloadObject($container_name, $folder_name, $file_name);
									// eof code to download doc from eCos
									
									// get file content type
									$finfo = finfo_open();
									$mime_type = finfo_buffer($finfo, $downloadObject_resp, FILEINFO_MIME_TYPE);
									
									if(strlen($downloadObject_resp) > 0)
									{
										$videoArr["accidentVideo"][] = array(
												"name" => $file_name,
												"type" => $mime_type,
												"data" => base64_encode($downloadObject_resp)
											);
											
									} 
									
								}
								
								// generate jwt
								$access_token = $auth->generateToken($tokenArr);			
									
								$resArr = array(
										"status" 		=> "1", 
										"message" 		=> "Accident video get successfully.",
										"access_token" 	=> $access_token,
										"data" 			=> $videoArr
									);

								 $logs->Module 		= "Accident";
								 $logs->Action		= "get";
								 $logs->Request		= json_encode($getArr);
								 $logs->Response	= json_encode($resArr);
								 $logs->UserId		= (int)$userID;
								 $logs->UserType 	= $UserRole;
								 $logs->AddLog();		
								
								response($resArr, 200);
								
							} else {
								
								// generate jwt
								$access_token = $auth->generateToken($tokenArr);
								
								$resArr = array(
										"status" 		=> "1", 
										"message" 		=> "No accident video found.",
										"access_token" 	=> $access_token
									);
									
								 $logs->Module 		= "Accident";
								 $logs->Action		= "get";
								 $logs->Request		= json_encode($getArr);
								 $logs->Response	= json_encode($resArr);
								 $logs->UserId		= (int)$userID;
								 $logs->UserType 	= $UserRole;
								 $logs->AddLog();
								 
								response($resArr, 200);
							}
						
						}
						
					} else {
						
						// generate jwt
							$access_token = $auth->generateToken($tokenArr);
							
							$resArr = array(
									"status" 		=> "1", 
									"message" 		=> "Accident details does not exist.",
									"access_token" 	=> $access_token
								);
								
							 $logs->Module 		= "Accident";
							 $logs->Action		= "get";
							 $logs->Request		= json_encode($getArr);
							 $logs->Response	= json_encode($resArr);
							 $logs->UserId		= (int)$userID;
							 $logs->UserType 	= $UserRole;
							 $logs->AddLog();
							 
							response($resArr, 200);
						
					}
					
				}  else if($id > 0 && $type == "vehicleDetails"){
					
					$accident->insertId = $id;
					
					$accidentRecord = $accident->checkAccidentAlert();
					
					if($accidentRecord == true)
					{
						
						$showVehicle = false;
						
						if($UserRole == "PO"){
							
							$showVehicle = true;
							
						} else if($UserRole == "IO"){
							
							// Check assign officer for case
							$getAccidentData = $accident->checkAssignedOfficer();
							
							$AccidentDetailsId = $getAccidentData[0]['AccidentDetailsId'];
							$assignUserID 	   = $getAccidentData[0]['AssignedUserId'];
						
							if($assignUserID == $userID){
								
								$showVehicle = true;
								
							} else {
								
								// generate jwt
								$access_token = $auth->generateToken($tokenArr);
								
								$resArr = array(
										"status" 		=> "1", 
										"message" 		=> "An accident details not assign to you.",
										"access_token" 	=> $access_token
									);
									
								 $logs->Module 		= "Accident";
								 $logs->Action		= "get";
								 $logs->Request		= json_encode($getArr);
								 $logs->Response	= json_encode($resArr);
								 $logs->UserId		= (int)$userID;
								 $logs->UserType 	= $UserRole;
								 $logs->AddLog();	
									
								response($resArr, 200);
							}
						
						}
						
						if($showVehicle){
						
							$vehicleList = $accident->getVehicleData($id);
							
							if($vehicleList){
								
								$vehicleArr = array();
								foreach($vehicleList as $vehicleinfo){
									
									$vehicleArr[] = $vehicleinfo;
									
								}
								
								// generate jwt
								$access_token = $auth->generateToken($tokenArr);			
									
								$resArr = array(
										"status" 		=> "1", 
										"message" 		=> "Vehicle details get successfully.",
										"access_token" 	=> $access_token,
										"data" 			=> $vehicleArr
									);

								 $logs->Module 		= "Accident";
								 $logs->Action		= "get";
								 $logs->Request		= json_encode($getArr);
								 $logs->Response	= json_encode($resArr);
								 $logs->UserId		= (int)$userID;
								 $logs->UserType 	= $UserRole;
								 $logs->AddLog();	
								
								response($resArr, 200);
								
							} else {
								
								// generate jwt
								$access_token = $auth->generateToken($tokenArr);
								
								$resArr = array(
										"status" 		=> "1", 
										"message" 		=> "No vehicle details found.",
										"access_token" 	=> $access_token
									);
									
								 $logs->Module 		= "Accident";
								 $logs->Action		= "get";
								 $logs->Request		= json_encode($getArr);
								 $logs->Response	= json_encode($resArr);
								 $logs->UserId		= (int)$userID;
								 $logs->UserType 	= $UserRole;
								 $logs->AddLog();
								 
								response($resArr, 200);
							}
						
						}
					
					} else {
						
						// generate jwt
							$access_token = $auth->generateToken($tokenArr);
							
							$resArr = array(
									"status" 		=> "1", 
									"message" 		=> "Vehicle details does not exist.",
									"access_token" 	=> $access_token
								);
								
							 $logs->Module 		= "Accident";
							 $logs->Action		= "get";
							 $logs->Request		= json_encode($getArr);
							 $logs->Response	= json_encode($resArr);
							 $logs->UserId		= (int)$userID;
							 $logs->UserType 	= $UserRole;
							 $logs->AddLog();
							 
							response($resArr, 200);
						
					}
					
				} else if($id > 0 && $type == "witnessDetails"){
					
					$accident->insertId = $id;
					
					$accidentRecord = $accident->checkAccidentAlert();
					
					if($accidentRecord == true)
					{
						
						$showWitness = false;
						
						if($UserRole == "PO"){
							
							$showWitness = true;
							
						} else if($UserRole == "IO"){
							
							// Check assign officer for case
							$getAccidentData = $accident->checkAssignedOfficer();
							
							$AccidentDetailsId = $getAccidentData[0]['AccidentDetailsId'];
							$assignUserID 	  = $getAccidentData[0]['AssignedUserId'];
						
							if($assignUserID == $userID){
								
								$showWitness = true;
								
							} else {
								
								// generate jwt
								$access_token = $auth->generateToken($tokenArr);
								
								$resArr = array(
										"status" 		=> "1", 
										"message" 		=> "An accident details not assign to you.",
										"access_token" 	=> $access_token
									);
									
								 $logs->Module 		= "Accident";
								 $logs->Action		= "get";
								 $logs->Request		= json_encode($getArr);
								 $logs->Response	= json_encode($resArr);
								 $logs->UserId		= (int)$userID;
								 $logs->UserType 	= $UserRole;
								 $logs->AddLog();	
									
								response($resArr, 200);
							}
						
						}
						
						if($showWitness){
						
							$witnessList = $accident->getWitnessData($id);
							
							if($witnessList){
								
								$witnessArr = array();
								foreach($witnessList as $witnessinfo){
									
									$witnessArr[] = $witnessinfo;
									
								}
								
								// generate jwt
								$access_token = $auth->generateToken($tokenArr);			
									
								$resArr = array(
										"status" 		=> "1", 
										"message" 		=> "Witness details get successfully.",
										"access_token" 	=> $access_token,
										"data" 			=> $witnessArr
									);

								 $logs->Module 		= "Accident";
								 $logs->Action		= "get";
								 $logs->Request		= json_encode($getArr);
								 $logs->Response	= json_encode($resArr);
								 $logs->UserId		= (int)$userID;
								 $logs->UserType 	= $UserRole;
								 $logs->AddLog();		
								
								response($resArr, 200);
								
							} else {
								
								// generate jwt
								$access_token = $auth->generateToken($tokenArr);
								
								$resArr = array(
										"status" 		=> "1", 
										"message" 		=> "No witness details found.",
										"access_token" 	=> $access_token
									);
									
								 $logs->Module 		= "Accident";
								 $logs->Action		= "get";
								 $logs->Request		= json_encode($getArr);
								 $logs->Response	= json_encode($resArr);
								 $logs->UserId		= (int)$userID;
								 $logs->UserType 	= $UserRole;
								 $logs->AddLog();
								 
								response($resArr, 200);
							}
						
						}
					
					} else {
						
						// generate jwt
							$access_token = $auth->generateToken($tokenArr);
							
							$resArr = array(
									"status" 		=> "1", 
									"message" 		=> "Witness details does not exist.",
									"access_token" 	=> $access_token
								);
								
							 $logs->Module 		= "Accident";
							 $logs->Action		= "get";
							 $logs->Request		= json_encode($getArr);
							 $logs->Response	= json_encode($resArr);
							 $logs->UserId		= (int)$userID;
							 $logs->UserType 	= $UserRole;
							 $logs->AddLog();
							 
							response($resArr, 200);
						
					}
					
				} else if($id > 0){
					
					
					$accident->insertId = $id;					
					
					if($UserRole == "PO"){
						
						$accidentDetails = $accident->checkAccidentAlert();
					
						if($accidentDetails == true){
					
							$accidentListing = $accident->get($id);
							
						} else {
							
							// generate jwt
							$access_token = $auth->generateToken($tokenArr);
							
							$resArr = array(
									"status" 		=> "1", 
									"message" 		=> "Accident details does not exist.",
									"access_token" 	=> $access_token
								);
								
							 $logs->Module 		= "Accident";
							 $logs->Action		= "get";
							 $logs->Request		= json_encode($getArr);
							 $logs->Response	= json_encode($resArr);
							 $logs->UserId		= (int)$userID;
							 $logs->UserType 	= $UserRole;
							 $logs->AddLog();	
								
							response($resArr, 200);
						}
						
					} else if($UserRole == "IO"){
						
						$accidentDetails = $accident->checkAccidentDetails();
						
						if($accidentDetails){							
							
							$getAssignUserID = $accident->getOfficerDetails();
							
							$getUserID	= $getAssignUserID[0]['AssignedUserId'];
							
							if($getUserID == $userID){
								
								$accidentListing = $accident->getRecords($id);
								
							} else {
								
								// generate jwt
								$access_token = $auth->generateToken($tokenArr);
								
								$resArr = array(
										"status" 		=> "1", 
										"message" 		=> "An accident details not assign to you.",
										"access_token" 	=> $access_token
									);
									
								 $logs->Module 		= "Accident";
								 $logs->Action		= "get";
								 $logs->Request		= json_encode($getArr);
								 $logs->Response	= json_encode($resArr);
								 $logs->UserId		= (int)$userID;
								 $logs->UserType 	= $UserRole;
								 $logs->AddLog();	
									
								response($resArr, 200);
								
							}
							
							
						} else {
							
							// generate jwt
							$access_token = $auth->generateToken($tokenArr);
							
							$resArr = array(
									"status" 		=> "1", 
									"message" 		=> "Accident details does not exist.",
									"access_token" 	=> $access_token
								);
								
							 $logs->Module 		= "Accident";
							 $logs->Action		= "get";
							 $logs->Request		= json_encode($getArr);
							 $logs->Response	= json_encode($resArr);
							 $logs->UserId		= (int)$userID;
							 $logs->UserType 	= $UserRole;
							 $logs->AddLog();	
								
							response($resArr, 200);
						}
						
					}
					
				}
				
				if($id == 0 && $UserRole == "PO"){ //If admin Police Officer
					
					$accidentListing = $accident->get();
					
				} else if($id == 0 && $UserRole == "IO"){
					
					$accidentListing = $accident->getAccidentDetails($userID);
					
				}
				
				if($accidentListing){
					
					// generate jwt
					$access_token = $auth->generateToken($tokenArr);
						
						// set response code
						$resArr = array(
									"status" 		=> "1", 
									"message" 		=> "Accident data found.",
									"access_token" 	=> $access_token,
									"data" 			=> $accidentListing
								);
							
								
							//Add log data
							 $logs->Module 		= "Accident";
							 $logs->Action		= "get";
							 $logs->Request		= json_encode($getArr);
							 $logs->Response	= json_encode($resArr);
							 $logs->UserId		= (int)$userID;
							 $logs->UserType 	= $UserRole;
							 $logs->AddLog();
								
							response($resArr, 200);
							
					} else { // message if unable to get accident

						// generate jwt
						$access_token = $auth->generateToken($tokenArr);
											
						$resArr = array(
									"status" 		=> "1", 
									"message" 		=> "No data found.",
									"access_token" 	=> $access_token
								);
									
						//Add log data
						 $logs->Module 		= "Accident";
						 $logs->Action		= "get";
						 $logs->Request		= json_encode($getArr);
						 $logs->Response	= json_encode($resArr);
						 $logs->UserId		= (int)$userID;	
						 $logs->UserType 	= $UserRole;
						 $logs->AddLog();	
								
						response($resArr, 200);
						
					}			
			
			} else {			 
				
				// show error message
				$resArr =array(
					"status" 	=> "0", 
					"message" 	=> "Invalid Authorization. Error : ". $decoded["TokenData"]
				);
				
				//Add log data
				 $logs->Module 		= "Accident";
				 $logs->Action		= "get";
				 $logs->Request		= "";
				 $logs->Response	= json_encode($resArr);
				 $logs->UserId		= "";
				 $logs->UserType	= "";	
				 $logs->AddLog();
				
				response($resArr, 401);
			}
		
		}  else { // show error message if jwt is empty		 
			
			$resArr = array("status" => "0", "message" => "Access denied.");
			
			//Add log data
			 $logs->Module 		= "Accident";
			 $logs->Action		= "get";
			 $logs->Request		= "";
			 $logs->Response	= json_encode($resArr);
			 $logs->UserId		= "";
			 $logs->UserType	= "";	
			 $logs->AddLog();
			
			response($resArr, 401);
		}
	
		break;
	
	case "OPTIONS": //Handle Preflighted requests for CORS issue
		
		// set response code		
		header('Content-Type: text/plain');
		
		response("", 200);
		
		break;
		
	default:
	
		// Invalid Request Method
		$resArr = array("status" => "0", "message" => "Method Not Allowed.");						
		// display message: unable to create user
		response($resArr, 405);	
		
		break;
}


?>