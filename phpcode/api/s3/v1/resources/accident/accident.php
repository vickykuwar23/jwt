<?php
/*** 'accident' object ***/
class Accident{
 
    /*** database connection and table name ***/
    private $conn;
    private $table_name = 'public."AccidentAlerts"';
	private $table_name_doc = 'public."AccidentDocDetails"';
	private $table_name_vehicle = 'public."AccidentVehicleDetails"';
	private $table_name_witness = 'public."AccidentWitnessDetails"';
	private $table_name_details = 'public."AccidentDetails"';
	
    /*** object properties ***/
    public $ReporteeName;
    public $ReporteeEmail;
    public $ReporteeMobileNo;
	public $ReporteeTypeId;
	public $AccidentPlaceLat;
	public $AccidentPlaceLong;
	public $AccidentLandmark;
	public $AccidentAddress;
	public $CollisionTypeId;
	public $CrashSeverityTypeId;
	public $AccidentDate;
	public $AccidentCause;
	public $CausalityCount;
	public $SurvivorCount;
	public $AccidentDescription;	
	public $DocName;
	public $AttachVideo;
	public $AccidentStatus;
	public $VehicleDetails;
	public $WitnessDetails;
	public $AttachName;
	public $IsDuplicate;
	public $insertId;
	public $UpdatedByUserId;
	
    /*** constructor ***/
    public function __construct($db){
        $this->conn = $db;
		$this->CreatedOn = date("Y-m-d H:i:s");
		$this->GetAutoIncrementID = 'public."AccidentAlerts_AccidentAlertId_seq"';
    }
 
	/*** create new user record ***/
	function create(){
		
		$query = 'INSERT INTO '.$this->table_name.' ("ReporteeName", "ReporteeEmail", "ReporteeMobileNo", "ReporteeTypeId", "AccidentPlaceLat", "AccidentPlaceLong", "AccidentLandmark", "AccidentAddress", "CollisionTypeId", "CrashSeverityTypeId", "AccidentDate", "AccidentCause", "CausalityCount", "SurvivorCount", "AccidentDescription", "IsValid", "IsActive", "CreatedOn", "CreatedByUserId", "IsDeleted") VALUES (:ReporteeName, :ReporteeEmail, :ReporteeMobileNo, :ReporteeTypeId, :AccidentPlaceLat, :AccidentPlaceLong, :AccidentLandmark, :AccidentAddress, :CollisionTypeId, :CrashSeverityTypeId, :AccidentDate, :AccidentCause, :CausalityCount, :SurvivorCount, :AccidentDescription, :IsValid, :IsActive, :CreatedOn, :CreatedByUserId, :IsDeleted)';		
		
		/*** prepare the query ***/
		$stmt = $this->conn->prepare($query);		
	 
		/*** sanitize ***/
		$this->AccidentAlertNo		=  '0';
		$this->AssignedUserId		=  '0';
		$this->AssignedOn			=  '0000-00-00 00:00:00';		
		$this->IsValid  			=  '0';
		$this->IsActive				=  '1';
		$this->IsDeleted			=  '0';
	    $this->CreatedByUserId		=  '0';
		
		// Document Values		
		$this->DocName   		= 	$this->DocName;
		
		$stmt->bindParam(':ReporteeName', $this->ReporteeName);
		$stmt->bindParam(':ReporteeEmail', $this->ReporteeEmail);
		$stmt->bindParam(':ReporteeMobileNo', $this->ReporteeMobileNo);
		$stmt->bindParam(':ReporteeTypeId', $this->ReporteeTypeId);
		$stmt->bindParam(':AccidentPlaceLat', $this->AccidentPlaceLat);
		$stmt->bindParam(':AccidentPlaceLong', $this->AccidentPlaceLong);
		$stmt->bindParam(':AccidentLandmark', $this->AccidentLandmark);
		$stmt->bindParam(':AccidentAddress', $this->AccidentAddress);
		$stmt->bindParam(':CollisionTypeId', $this->CollisionTypeId);
		$stmt->bindParam(':CrashSeverityTypeId', $this->CrashSeverityTypeId);
		$stmt->bindParam(':AccidentDate', $this->AccidentDate);
		$stmt->bindParam(':AccidentCause', $this->AccidentCause);
		$stmt->bindParam(':CausalityCount', $this->CausalityCount);
		$stmt->bindParam(':SurvivorCount', $this->SurvivorCount);
		$stmt->bindParam(':AccidentDescription', $this->AccidentDescription);
		$stmt->bindParam(':IsValid', $this->IsValid);
		$stmt->bindParam(':IsActive', $this->IsActive);
		$stmt->bindParam(':IsDeleted', $this->IsDeleted);
		$stmt->bindParam(':CreatedOn', $this->CreatedOn);
		$stmt->bindParam(':CreatedByUserId', $this->CreatedByUserId);
	 
		/*** execute the query, also check if query was successful ***/		
		if($stmt->execute()){
			
			$id = $this->conn->lastinsertId($this->GetAutoIncrementID);
			
			return $id;
		}
		
		return false;
		
	} // Function End
	
	// Upload pictures
	function addDocuments($action){
			
			if (is_array($this->AttachName) || is_object($this->AttachName)) {
				
				foreach($this->AttachName as $document){
					
					//RefID 	= AccidentDetails Table PK i.e. AccidentDetailsId					
					//RefType 	= Alert table 0 and details table when updated is 1
					
					if(!empty($document)){
					
					$query_doc = 'INSERT INTO '.$this->table_name_doc.' ("RefId", "RefType", "DocType", "DocName", "IsActive", "CreatedOn", "CreatedByUserId", "IsDeleted") VALUES (:RefId, :RefType, :DocType, :DocName, :IsActive, :CreatedOn, :CreatedByUserId, :IsDeleted)';
				
					$stmt_doc = $this->conn->prepare($query_doc);
					
					$this->IsDeleted	=  '0';
					$this->DocType		=  'pic';
					$this->IsActive		=  '1';
					$this->CreatedByUserId = '0';
					
					if($action == "Add"): 
						$this->RefType		=  '0';
					else:
						$this->RefType		=  '1';
					endif;
					
					$stmt_doc->bindParam(':RefId', $this->insertId);
					$stmt_doc->bindParam(':RefType', $this->RefType);
					$stmt_doc->bindParam(':DocType', $this->DocType);
					$stmt_doc->bindParam(':DocName', $document);
					$stmt_doc->bindParam(':IsActive', $this->IsActive);
					$stmt_doc->bindParam(':IsDeleted', $this->IsDeleted);
					$stmt_doc->bindParam(':CreatedOn', $this->CreatedOn);
					$stmt_doc->bindParam(':CreatedByUserId', $this->CreatedByUserId);
					
					$stmt_doc->execute();
					
					} // If End
					
				} // Foreach end
				
			} // Picture/Attachment Count IF End
		
	} // Function End
	
	// Upload Videos	
	function addVideos($action){		
		
		if (is_array($this->AttachVideo) || is_object($this->AttachVideo)) {
							
				foreach($this->AttachVideo as $documentVideo){
					
					//RefID 	= AccidentDetails Table PK i.e. AccidentDetailsId					
					//RefType 	= Alert table 0 and details table when updated is 1
					
					if(!empty($documentVideo)){
					
					$query_vid = 'INSERT INTO '.$this->table_name_doc.' ("RefId", "RefType", "DocType", "DocName", "IsActive", "CreatedOn", "CreatedByUserId", "IsDeleted") VALUES (:RefId, :RefType, :DocType, :DocName, :IsActive, :CreatedOn, :CreatedByUserId, :IsDeleted)';
				
					$stmt_vid = $this->conn->prepare($query_vid);
					
					$this->IsDeleted	=  '0';
					$this->DocType		=  'video';
					$this->IsActive		=  '1';
					$this->CreatedByUserId = '0';
					
					if($action == "Add"): 
						$this->RefType		=  '0';
					else:
						$this->RefType		=  '1';
					endif;				
					
					
					$stmt_vid->bindParam(':RefId', $this->insertId);
					$stmt_vid->bindParam(':RefType', $this->RefType);
					$stmt_vid->bindParam(':DocType', $this->DocType);
					$stmt_vid->bindParam(':DocName', $documentVideo);
					$stmt_vid->bindParam(':IsActive', $this->IsActive);
					$stmt_vid->bindParam(':IsDeleted', $this->IsDeleted);
					$stmt_vid->bindParam(':CreatedOn', $this->CreatedOn);
					$stmt_vid->bindParam(':CreatedByUserId', $this->CreatedByUserId);
					
					$stmt_vid->execute();
					
					
					} // if end
				} // Foreach end
			} // Video Count IF End
		
	} // Function End
	
	function addVehicleData($action){
		
			$this->delete($this->table_name_vehicle);
			
			if (is_array($this->VehicleDetails) || is_object($this->VehicleDetails)) {
				
				/*if(count($this->VehicleDetails) > 0){
					
					if(isset($this->VehicleDetails[0]) && !empty($this->VehicleDetails[0])){
						
						$this->delete($this->table_name_vehicle);
						
					}
					
				}*/
				
				for($i=0;$i<count($this->VehicleDetails);$i++){
					
					//RefID = AccidentDetails Table PK i.e. AccidentDetailsId				
					$query_vehicle = 'INSERT INTO '.$this->table_name_vehicle.' ("RefId", "RefType", "VehicleNo", "VehicleTypeId", "VehicleColor", "IsActive", "CreatedOn", "CreatedByUserId", "IsDeleted") VALUES (:RefId, :RefType, :VehicleNo, :VehicleTypeId, :VehicleColor, :IsActive, :CreatedOn, :CreatedByUserId, :IsDeleted)';
					
					$stmt_vehicle = $this->conn->prepare($query_vehicle);
					
					$this->IsDeleted	=  '0';
					$this->IsActive		=  '1';
					$this->CreatedByUserId = '0';
					
					if($action == "Add"): 
						$this->RefType		=  '0';
					else:
						$this->RefType		=  '1';
					endif;					
					
					$stmt_vehicle->bindParam(':RefId', $this->insertId);
					$stmt_vehicle->bindParam(':RefType', $this->RefType);
					$stmt_vehicle->bindParam(':VehicleNo', $this->VehicleDetails[$i]['VehicleNo']);
					$stmt_vehicle->bindParam(':VehicleTypeId', $this->VehicleDetails[$i]['VehicleTypeId']);
					$stmt_vehicle->bindParam(':VehicleColor', $this->VehicleDetails[$i]['VehicleColor']);
					$stmt_vehicle->bindParam(':IsActive', $this->IsActive);
					$stmt_vehicle->bindParam(':IsDeleted', $this->IsDeleted);
					$stmt_vehicle->bindParam(':CreatedOn', $this->CreatedOn);
					$stmt_vehicle->bindParam(':CreatedByUserId', $this->CreatedByUserId);
					
					$stmt_vehicle->execute();
					
				}
				
			}
		
	} // Function End
	
	
	// add witness data 
	function addWitnessData(){
		
		//count($this->WitnessDetails);exit;
		
			// delete existing details
			$this->delete($this->table_name_witness);
			
			if (is_array($this->WitnessDetails) || is_object($this->WitnessDetails)) {
				
				/*if(count($this->WitnessDetails) > 0){
					
					if(isset($this->WitnessDetails[0]) && !empty($this->WitnessDetails[0])){
						
						$this->delete($this->table_name_witness);
						
					}
					
				}*/
				
				foreach($this->WitnessDetails as $witnessInfo){
					
					if(!empty($witnessInfo)){
					
						//RefID 	= AccidentDetails Table PK i.e. AccidentDetailsId					
						//RefType 	= Alert table 0 and details table when updated is 1
						
						$query_witness = 'INSERT INTO '.$this->table_name_witness.' ("RefId", "RefType", "WitnessName", "WitnessEmail", "WitnessMobileNo", "WitnessNotes", "IsActive", "CreatedOn", "CreatedByUserId", "IsDeleted") VALUES (:RefId, :RefType, :WitnessName, :WitnessEmail, :WitnessMobileNo, :WitnessNotes, :IsActive, :CreatedOn, :CreatedByUserId, :IsDeleted)';
					
						$stmt_witness = $this->conn->prepare($query_witness);
						
						$this->IsDeleted	=  '0';					
						$this->RefType		=  '1';
						$this->IsActive		=  '1';
						$this->CreatedByUserId = '0';
						
						$stmt_witness->bindParam(':RefId', $this->insertId);
						$stmt_witness->bindParam(':RefType', $this->RefType);
						$stmt_witness->bindParam(':WitnessName', $witnessInfo['WitnessName']);
						$stmt_witness->bindParam(':WitnessEmail', $witnessInfo['WitnessEmail']);
						$stmt_witness->bindParam(':WitnessMobileNo', $witnessInfo['WitnessMobileNo']);
						$stmt_witness->bindParam(':WitnessNotes', $witnessInfo['WitnessNotes']);
						$stmt_witness->bindParam(':IsActive', $this->IsActive);
						$stmt_witness->bindParam(':IsDeleted', $this->IsDeleted);
						$stmt_witness->bindParam(':CreatedOn', $this->CreatedOn);
						$stmt_witness->bindParam(':CreatedByUserId', $this->CreatedByUserId);
						
						$stmt_witness->execute();
					}
				}			
			}
			
	} // Function End
	
	function delete($table){
		
		$query = 'UPDATE ' . $table . '
					  SET
						"IsDeleted"	 	= :IsDeleted,
						"DeletedOn"	= :DeletedOn,
						"DeletedByUserId" = :DeletedByUserId
					  WHERE "RefId" 	= :RefId  AND "RefType" = :RefType';
		$stmt = $this->conn->prepare($query);	
		
		$this->IsDeleted	=	'1';
		$this->RefType		=	'1';
		$this->DeletedOn  	=	$this->CreatedOn;
		
		$stmt->bindParam(':IsDeleted', $this->IsDeleted);
		$stmt->bindParam(':DeletedOn', $this->DeletedOn);
		$stmt->bindParam(':DeletedByUserId', $this->UpdatedByUserId);
		
		$stmt->bindParam(':RefId', $this->insertId);
		$stmt->bindParam(':RefType', $this->RefType);
		
		$stmt->execute();
			
	}
	
	/*** check if given accident exist in the database ***/
	function checkAccidentAlert(){
		
		/*** query to check if data exists ***/
		$query = 'SELECT "AccidentAlertId"
					FROM '.$this->table_name.'
					WHERE "AccidentAlertId" = ? AND "IsDeleted" = ?
					LIMIT 1';
					
		/*** prepare the query ***/
		$stmt = $this->conn->prepare( $query );
		
		/*** bind given data value ***/
		$stmt->bindParam(1, $this->insertId);
		$stmt->bindValue(2, false, PDO::PARAM_BOOL);
		/*** execute the query ***/
		if($stmt->execute()){
			
			/*** get number of rows ***/
			$num = $stmt->rowCount();
			
			/*** if data exists, assign values to object properties for easy access and use for php sessions ***/
			if($num > 0){
		 
				/*** return true because data exists in the database ***/
				return true;
			}
		}
		/*** return false if data does not exist in the database ***/
		return false;
		
	} // Function End
	
	
	/*** check if given accident exist in the database ***/
	function checkAccidentDetails(){
	 
		/*** query to check if data exists ***/
		$query = 'SELECT "AccidentDetailsId"
					FROM '.$this->table_name_details.'
					WHERE "AccidentDetailsId" = ? AND "IsDeleted" = ?
					LIMIT 1';
					
		/*** prepare the query ***/
		$stmt = $this->conn->prepare( $query );
		
		/*** bind given data value ***/
		$stmt->bindParam(1, $this->insertId);
		$stmt->bindValue(2, false, PDO::PARAM_BOOL);
		/*** execute the query ***/
		if($stmt->execute()){
			
			/*** get number of rows ***/
			$num = $stmt->rowCount();
			
			/*** if data exists, assign values to object properties for easy access and use for php sessions ***/
			if($num > 0){
		 
				/*** return true because data exists in the database ***/
				return true;
			}
		}
		/*** return false if data does not exist in the database ***/
		return false;
		
	} // Function End
	
	/*** check if given accident exist in the database ***/
	function checkAssignedOfficer(){
	
		/*** query to check if data exists ***/
		$query = 'SELECT "AccidentDetailsId", "AssignedUserId"
					FROM '.$this->table_name_details.'
					WHERE "AccidentDetailsId" = ? AND "IsDeleted" = ?
					LIMIT 1';
					
		/*** prepare the query ***/
		$stmt = $this->conn->prepare( $query );
		
		/*** bind given accident ID and IsDeleted value ***/
		$stmt->bindParam(1, $this->insertId);
		$stmt->bindValue(2, false, PDO::PARAM_BOOL);
		/*** execute the query ***/
		if($stmt->execute()){
			
			/*** get number of rows ***/
			$num = $stmt->rowCount();
			
			if($num > 0){
		 
				while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
						$accidentData[] = $row;
				}
				
				return $accidentData;
				//return true;
			}
		}		
		return false;
		
	} // Function End
	
	function getOfficerDetails(){
	
		/*** query to check if data exists ***/
		$query = 'SELECT "AccidentDetailsId", "AssignedUserId"
					FROM '.$this->table_name_details.'
					WHERE "AccidentDetailsId" = ? AND "IsDeleted" = ?
					LIMIT 1';
					
		/*** prepare the query ***/
		$stmt = $this->conn->prepare( $query );
		
		/*** bind given Accident ID and Is Deleted value ***/
		$stmt->bindParam(1, $this->insertId);
		$stmt->bindValue(2, false, PDO::PARAM_BOOL);
		
		/*** execute the query ***/
		if($stmt->execute()){
			
			/*** get number of rows ***/
			$num = $stmt->rowCount();
			
			if($num > 0){
		 
				while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
						$accidentData[] = $row;
				}
				
				return $accidentData;				
			}
		}		
		return false;
		
	} // Function End
	
	 
	/*** update record ***/
	public function update($UpdateDuplicate){
		
		/*** SQL QUERY ***/
		$query = 'UPDATE ' . $this->table_name_details . ' SET ';
		
		if($UpdateDuplicate == 'UpdateProfile'){
			
			$query .= '"ReporteeName" = :ReporteeName, "ReporteeEmail" = :ReporteeEmail, "ReporteeMobileNo" = :ReporteeMobileNo, "ReporteeTypeId" = :ReporteeTypeId, "AccidentPlaceLat" = :AccidentPlaceLat, "AccidentPlaceLong" = :AccidentPlaceLong, "AccidentLandmark" = :AccidentLandmark, "AccidentAddress" = :AccidentAddress, "CollisionTypeId" = :CollisionTypeId, "CrashSeverityTypeId" = :CrashSeverityTypeId, "AccidentDate" = :AccidentDate, "AccidentCause" = :AccidentCause, "CausalityCount" = :CausalityCount, "SurvivorCount" = :SurvivorCount, "AccidentDescription" = :AccidentDescription, "AccidentStatus" = :AccidentStatus, "UpdatedOn" = :UpdatedOn, "UpdatedByUserId" = :UpdatedByUserId';
			
		} else {
			
			$query .= '"IsDuplicate" = :IsDuplicate, "UpdatedByUserId" = :UpdatedByUserId, "UpdatedOn" = :UpdatedOn';
		}
			
			$query .= ' WHERE "AccidentDetailsId" = :AccidentDetailsId';
		
		/*** prepare the query ***/
		$stmt = $this->conn->prepare($query);
		
		$this->UpdatedOn	=	$this->CreatedOn;
		 
		/*** bind the values from the form ***/	
		if($UpdateDuplicate == 'UpdateProfile'){
			
			$stmt->bindParam(':ReporteeName', $this->ReporteeName);
			$stmt->bindParam(':ReporteeEmail', $this->ReporteeEmail);
			$stmt->bindParam(':ReporteeMobileNo', $this->ReporteeMobileNo);
			$stmt->bindParam(':ReporteeTypeId', $this->ReporteeTypeId);
			$stmt->bindParam(':AccidentPlaceLat', $this->AccidentPlaceLat);
			$stmt->bindParam(':AccidentPlaceLong', $this->AccidentPlaceLong);
			$stmt->bindParam(':AccidentLandmark', $this->AccidentLandmark);
			$stmt->bindParam(':AccidentAddress', $this->AccidentAddress);
			$stmt->bindParam(':CollisionTypeId', $this->CollisionTypeId);
			$stmt->bindParam(':CrashSeverityTypeId', $this->CrashSeverityTypeId);
			$stmt->bindParam(':AccidentDate', $this->AccidentDate);
			$stmt->bindParam(':AccidentCause', $this->AccidentCause);
			$stmt->bindParam(':CausalityCount', $this->CausalityCount);
			$stmt->bindParam(':SurvivorCount', $this->SurvivorCount);
			$stmt->bindParam(':AccidentDescription', $this->AccidentDescription);					
			$stmt->bindParam(':AccidentStatus', $this->AccidentStatus);	//0=Pending,1=In-progress,2=Hold,3=Closed
			
		} else {
			
			$stmt->bindParam(':IsDuplicate', $this->IsDuplicate);	
		}
			$stmt->bindParam(':UpdatedByUserId', $this->UpdatedByUserId);
			$stmt->bindParam(':UpdatedOn', $this->UpdatedOn);		
		
		/*** unique ID of record to be edited ***/
		$stmt->bindParam(':AccidentDetailsId', $this->insertId);
	 
		/*** execute the query ***/
		if($stmt->execute()){
			
			return $stmt->rowCount();
		}
	 
		return false;
		
	} // Function End
	
	// Only For Police Officer call
	function get($id = 0){
		
		/*** query to check if condition ***/					
		if($id > 0){
			
			$query = 'SELECT "AccidentAlertId", "AccidentAlertNo", "AssignedUserId", "AssignedOn", 
						"ReporteeName", "ReporteeEmail", "ReporteeTypeId", "AccidentPlaceLat", "AccidentPlaceLong", "AccidentLandmark", "AccidentAddress", "CollisionTypeId", "CrashSeverityTypeId", "AccidentDate", "AccidentCause", "CausalityCount", "SurvivorCount", "AccidentDescription", "IsValid", "IsActive", "CreatedOn", "CreatedByUserId" FROM '.$this->table_name.' 
					  WHERE "AccidentAlertId" = ? AND "IsDeleted" = ? ORDER BY "AccidentAlertId" ASC';
			
		} else {
			
			$query = 'SELECT "AccidentAlertId", "AccidentAlertNo", "AssignedUserId", "AssignedOn", 
						"ReporteeName", "ReporteeEmail", "ReporteeTypeId", "AccidentPlaceLat", "AccidentPlaceLong", "AccidentLandmark", "AccidentAddress", "CollisionTypeId", "CrashSeverityTypeId", "AccidentDate", "AccidentCause", "CausalityCount", "SurvivorCount", "AccidentDescription", "IsValid", "IsActive", "CreatedOn", "CreatedByUserId"
					 FROM '.$this->table_name.' 
					 WHERE "IsDeleted" = ? ORDER BY "AccidentAlertId" ASC';
		}		
	 
		/*** prepare the query ***/
		$stmt = $this->conn->prepare( $query );		
	 
		if($id > 0){
			/*** bind given value ***/
			$stmt->bindValue(1, $id);
			$stmt->bindValue(2, false, PDO::PARAM_BOOL);
		} else {
			$stmt->bindValue(1, false, PDO::PARAM_BOOL);
		}
		
		/*** execute the query ***/
		$stmt->execute();
	 
		/*** get number of rows ***/
		$num = $stmt->rowCount();
	 
		if($num > 0){
	 
			/*** get record details / values ***/			
			while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
					$userData[] = $row;
			}
	 
			/*** return true because details exists in the database ***/
			return $userData;
		}
	 
		/*** return false if details does not exist in the database ***/
		return false;
		
	} // Function End
	
	// Investigation Officers Single Records by AccidentDetailsId 
	function getRecords($id = 0){
		
		/*** query to check if condition ***/
			
			$query = 'SELECT "AccidentDetailsId", "AccidentAlertId", "AssignedUserId", "AssignedOn", "ReporteeName", "ReporteeEmail", "ReporteeTypeId", "AccidentPlaceLat", "AccidentPlaceLong", "AccidentLandmark", "AccidentAddress", "CollisionTypeId", "CrashSeverityTypeId", "AccidentDate", "AccidentCause", "CausalityCount", "SurvivorCount", "AccidentDescription", "InvestigatorNotes", "AccidentStatus", "ReporteeMobileNo" FROM '.$this->table_name_details.' 
			WHERE "AccidentDetailsId" = ? AND "IsDeleted" = ? ORDER BY "AccidentDetailsId" ASC';
	 
		/*** prepare the query ***/
		$stmt = $this->conn->prepare( $query );	
		
		/*** bind given value ***/
		$stmt->bindValue(1, $id);
		$stmt->bindValue(2, false, PDO::PARAM_BOOL);
		
		/*** execute the query ***/
		$stmt->execute();
	 
		/*** get number of rows ***/
		$num = $stmt->rowCount();
	 
		if($num > 0){
	 
			/*** get record details / values ***/			
			while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
					$userData[] = $row;
			}
	 
			/*** return true because details exists in the database ***/
			return $userData;
		}
	 
		/*** return false if details does not exist in the database ***/
		return false;
		
	} // Function End
	
	
	// Investigation Officers data All records for particular Investigation officers by AssignedUserId
	function getAccidentDetails($userID){
		
		$query = 'SELECT "AccidentDetailsId", "AccidentAlertId", "AssignedUserId", "AssignedOn", "ReporteeName", "ReporteeEmail", "ReporteeTypeId", "AccidentPlaceLat", "AccidentPlaceLong", "AccidentLandmark", "AccidentAddress", "CollisionTypeId", "CrashSeverityTypeId", "AccidentDate", "AccidentCause", "CausalityCount", "SurvivorCount", "AccidentDescription", "InvestigatorNotes", "AccidentStatus", "IsActive", "OnholdDate", "ClosedDate", "CreatedOn", "CreatedByUserId", "AccidentCaseNo", "ReporteeMobileNo"
		FROM '.$this->table_name_details.' 
		WHERE "AssignedUserId" = ? AND "IsDeleted" = ? ORDER BY "AccidentDetailsId" ASC';
					 
			/*** prepare the query ***/
			$stmt = $this->conn->prepare( $query );		
		 
			
			/*** bind given email value ***/
			$stmt->bindValue(1, $userID);
			$stmt->bindValue(2, false, PDO::PARAM_BOOL);
			
			
			/*** execute the query ***/
			$stmt->execute();
		 
			/*** get number of rows ***/
			$num = $stmt->rowCount();
		 
			if($num > 0){
		 
				/*** get record details / values ***/			
				while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
						$userData[] = $row;
				}
		 
				/*** return true because details exists in the database ***/
				return $userData;
			}
		 
			/*** return false if details does not exist in the database ***/
			return false;
		
	} // Function End
	
	// Accident Pictures 
	function getAccidentPictures($id = 0){
		
		$query = 'SELECT "AccidentDocDetailsId", "RefId", "RefType", "DocType", "DocName", "IsActive", "CreatedOn", "CreatedByUserId", "UpdatedOn", "UpdatedByUserId" FROM '.$this->table_name_doc.' 
				WHERE "RefId" = ? AND "IsDeleted" = ? AND "IsActive" = ? AND "DocType" = ? ORDER BY "AccidentDocDetailsId" ASC';
	 
		/*** prepare the query ***/
		$stmt = $this->conn->prepare( $query );		
	    $pic  = 'pic';
		
		if($id > 0){ 
			/*** bind given value ***/
			$stmt->bindValue(1, $id);
			$stmt->bindValue(2, false, PDO::PARAM_BOOL);
			$stmt->bindValue(3, true, PDO::PARAM_BOOL);
			$stmt->bindValue(4, $pic);
		} 
		
		/*** execute the query ***/
		$stmt->execute();
	 
		/*** get number of rows ***/
		$num = $stmt->rowCount();
	 
		if($num > 0){
	 
			/*** get record details / values ***/			
			while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
					$userData[] = $row;
			}
	 
			/*** return true because details exists in the database ***/
			return $userData;
		}
	 
		/*** return false if details does not exist in the database ***/
		return false;
		
	} // Function End
	
	// Accident Videos 
	function getAccidentVideos($id = 0){
		
		$query = 'SELECT "AccidentDocDetailsId", "RefId", "RefType", "DocType", "DocName", "IsActive", "CreatedOn", "CreatedByUserId", "UpdatedOn", "UpdatedByUserId" FROM '.$this->table_name_doc.' 
				WHERE "RefId" = ? AND "IsDeleted" = ? AND "IsActive" = ? AND "DocType" = ? ORDER BY "AccidentDocDetailsId" ASC';
	 
		/*** prepare the query ***/
		$stmt = $this->conn->prepare( $query );		
	    $video  = 'video';
		
		if($id > 0){ 
			/*** bind given email value ***/
			$stmt->bindValue(1, $id);
			$stmt->bindValue(2, false, PDO::PARAM_BOOL);
			$stmt->bindValue(3, true, PDO::PARAM_BOOL);
			$stmt->bindValue(4, $video);
		} 
		
		/*** execute the query ***/
		$stmt->execute();
	 
		/*** get number of rows ***/
		$num = $stmt->rowCount();
	 
		if($num > 0){
	 
			/*** get record details / values ***/			
			while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
					$userData[] = $row;
			}
	 
			/*** return true because details exists in the database ***/
			return $userData;
		}
	 
		/*** return false if details does not exist in the database ***/
		return false;
		
	} // Function End
	
	
	// get Vehicle Data 
	function getVehicleData($id = 0){
		
		$query = 'SELECT "AccidentVehicleDetailsId", "RefId", "RefType", "VehicleNo", 
				"VehicleTypeId", "VehicleColor", "IsActive", "CreatedOn", "CreatedByUserId", "UpdatedOn", "UpdatedByUserId"
				  FROM '.$this->table_name_vehicle.' 
				  WHERE "RefId" = ? AND "IsDeleted" = ? AND "IsActive" = ? ORDER BY "AccidentVehicleDetailsId" ASC';
	 
		/*** prepare the query ***/
		$stmt = $this->conn->prepare( $query );	
		
		if($id > 0){ 
			/*** bind given value ***/
			$stmt->bindValue(1, $id);
			$stmt->bindValue(2, false, PDO::PARAM_BOOL);
			$stmt->bindValue(3, true, PDO::PARAM_BOOL);
		} 
		
		/*** execute the query ***/
		$stmt->execute();
	 
		/*** get number of rows ***/
		$num = $stmt->rowCount();
	 
		if($num > 0){
	 
			/*** get record details / values ***/			
			while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
					$vehicleData[] = $row;
			}
	 
			/*** return true because details exists in the database ***/
			return $vehicleData;
		}
	 
		/*** return false if details does not exist in the database ***/
		return false;
		
	} // Function End
	
	
	// get Witness Data 
	function getWitnessData($id = 0){
		
		$query = 'SELECT "AccidentwitnessdetailsId", "RefId", "RefType", "WitnessName", 
				  "WitnessEmail", "WitnessMobileNo", "WitnessNotes", "CreatedOn", "CreatedByUserId", 
				   "UpdatedOn", "UpdatedByUserId"
				  FROM '.$this->table_name_witness.' 
				  WHERE "RefId" = ? AND "IsDeleted" = ? AND "IsActive" = ? ORDER BY "AccidentwitnessdetailsId" ASC';
	 
		/*** prepare the query ***/
		$stmt = $this->conn->prepare( $query );	
		
		if($id > 0){ 
			/*** bind given value ***/
			$stmt->bindValue(1, $id);
			$stmt->bindValue(2, false, PDO::PARAM_BOOL);
			$stmt->bindValue(3, true, PDO::PARAM_BOOL);
		} 
		
		/*** execute the query ***/
		$stmt->execute();
	 
		/*** get number of rows ***/
		$num = $stmt->rowCount();
		
		if($num > 0){
	 
			/*** get record details / values ***/			
			while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
					$witnessData[] = $row;
			}
	 
			/*** return true because details exists in the database ***/
			return $witnessData;
		}
	 
		/*** return false if details does not exist in the database ***/
		return false;
		
	} // Function End
	
		
} // End Class