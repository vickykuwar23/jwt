<?php
// Files needed to connect to database and class
define("ROOT_PATH", realpath(__DIR__ . '/../../..'));
include_once ROOT_PATH.'/constant.php';

include_once CONFIG_PATH.'functions.php';
include_once CONFIG_PATH.'database.php';
include_once RESOURCE_PATH.'logs/logs.php';
include_once RESOURCE_PATH.'auth/auth.php';

// Required headers
requireHeaders();
 
// Get database connection
$database = new Database();
$db = $database->getMongoConnection();
 
// Instantiate logs object
$logs = new Logs($db);
$auth  = new Auth();

// Get Request Method
$request_method	= get_request_method();

switch($request_method)
{		
	case 'GET':
				
		$bearerToken = getBearerToken();
		
		$access_token = isset($bearerToken) ? $bearerToken : "";
		
		// Check if jwt is not empty
		if($access_token){
			
			// decode jwt		
			$decoded 	= $auth->validateToken($access_token);
		 
			// Check if token is valid
			if($decoded["status"] == true) {
				
				$userID 		= $decoded['TokenData']->data->UserId; // Decode ID
				$UserName		= $decoded['TokenData']->data->UserName; // Decode UserName
				$UserRole 		= $decoded['TokenData']->data->UserRole; // Decode UserRole
			
				$tokenArr = array("UserId" => (int)$userID, "UserName" => $UserName, "UserRole" => $UserRole);
				
				$reqArr = get_url_data();
				
				$id = isset($reqArr['id']) ? intval($reqArr['id']) : 0;
				
				$LogsList = false;
				
				if($UserRole == "Police Officer" && $id == 0){ // If Police Officer
				
					$LogsList = $logs->GetLogs();
				}
				 
				if($id > 0){
				
					if($UserRole == "Police Officer"){ // Check if logged in user is Police Officer
						
						$LogsList = $logs->GetLogs($id);
						
					} else if($userID == $id){ // Check if logged in user is Investigator		
						
						$LogsList = $logs->GetLogs($id);
						
					}
					
				}
				 
				if($LogsList){	
					
					// generate jwt
					$access_token = $auth->generateToken($tokenArr);	
					
					$resArr = array(
								"status" 		=> "1", 
								"message" 		=> "Logs get successfully.",
								"access_token" 	=> $access_token,
								"data" 			=> $LogsList
							);
					
					// Add log data
					 $logs->Module 		= "Logs";
					 $logs->Action		= "get";
					 $logs->Request		= json_encode($reqArr);
					 $logs->Response	= json_encode($resArr);
					 $logs->UserId		= (int)$userID;
					 $logs->AddLog();
					 
					 //$logs->ListenLogs();	
					
					response($resArr, 200);
						
				} else {
					
					// generate jwt
					$access_token = $auth->generateToken($tokenArr);
				 
					// Display message: No data found
					$resArr = array("status" => "1", 
									"message" => "No data found.",
									"access_token" 	=> $access_token);
					
					// Add log data
					 $logs->Module 		= "Logs";
					 $logs->Action		= "get";
					 $logs->Request		= json_encode($reqArr);
					 $logs->Response	= json_encode($resArr);
					 $logs->UserId		= (int)$userID;
					 $logs->AddLog();
					
					response($resArr, 200);
					
				}
				
			} else {	// If jwt is invalid
			 
				// Show error message
				$resArr = array(
					"status" => "0", 
					"message" => "Invalid Authorization. Error : ". $decoded["TokenData"]
				);
				
				// Add log data
				 $logs->Module 		= "Logs";
				 $logs->Action		= "get";
				 $logs->Request		= "";
				 $logs->Response	= json_encode($resArr);
				 $logs->UserId		= "";
				 $logs->UserType	= "";
				 $logs->AddLog();	
					 
				response($resArr, 401);
			}
			
		} else {
		 
			// Show error message as access denied
			$resArr = array("status" => "0", "message" => "Access denied.");
			
			 // Add log data
			 $logs->Module 		= "Logs";
			 $logs->Action		= "get";
			 $logs->Request		= "";
			 $logs->Response	= json_encode($resArr);
			 $logs->UserId		= "";
			 $logs->UserType	= "";
			 $logs->AddLog();	
			response($resArr, 401);
		}
		
		break;
	
	case "OPTIONS": //Handle Preflighted requests for CORS issue
		
		// set response code		
		header('Content-Type: text/plain');
		
		response("", 200);
		
		break;
		
	default:
	
		// Invalid Request Method
		$resArr = array("status" => "0", "message" => "Method Not Allowed.");						
		// display message: unable to create user
		response($resArr, 405);	
		
		break;
}
?>