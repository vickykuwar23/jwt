<?php 

/*** files needed to connect to database and class***/
define("ROOT_PATH", realpath(__DIR__ . '/../../..'));
include_once ROOT_PATH.'/constant.php';

include_once CONFIG_PATH.'functions.php';
include_once CONFIG_PATH.'database.php';
include_once RESOURCE_PATH.'master/master.php';
include_once RESOURCE_PATH.'logs/logs.php';
//include_once RESOURCE_PATH.'auth/auth.php';

// Required headers
requireHeaders();
 
/*** get database connection ***/
$database 	= new Database();
$db 		= $database->getConnection();
$mdb 		= $database->getMongoConnection();
 
// instantiate product object
$master  = new Master($db);
$logs  = new Logs($mdb);
//$auth  = new Auth();

// Get Request Method 
$request_method		= get_request_method();

switch($request_method)
{
			
	case 'GET':
			
			$getArr = array();
			
				$getArr = get_url_data();					
				
				//$id 	= isset($getArr['id']) ? intval($getArr['id']) : 0;
				$type   = isset($getArr['type']) ? $getArr['type'] : "";
				
				if($type == "reporteeTypes")
				{
					
						$getList = $master->getReporteeType();
						
						if($getList){
							
							$resArr = array(
								"status" 		=> "1", 
								"message" 		=> "Reportee data get successfully.",
								"data" 			=> $getList
							);

							 $logs->Module 		= "Master";
							 $logs->Action		= "get";
							 $logs->Request		= json_encode($getArr);
							 $logs->Response	= json_encode($resArr);
							 $logs->UserId		= "";
							 $logs->UserType    = ""; 	
							 $logs->AddLog();		
							
							response($resArr, 200);
							
						} else {
							
							$resArr = array(
								"status" 		=> "1", 
								"message" 		=> "No data found."
							);

							 $logs->Module 		= "Master";
							 $logs->Action		= "get";
							 $logs->Request		= json_encode($getArr);
							 $logs->Response	= json_encode($resArr);
							 $logs->UserId		= "";
							 $logs->UserType    = ""; 	
							 $logs->AddLog();		
							
							response($resArr, 200);
							
						}
					
				
				} else if($type == "collisionType"){ 
				
						$getList = $master->getCollisionType();	
						
						if($getList){
							
							$resArr = array(
								"status" 		=> "1", 
								"message" 		=> "Collision data get successfully.",
								"data" 			=> $getList
							);
							
							   $logs->Module 		= "Master";
							   $logs->Action		= "get";
							   $logs->Request		= json_encode($getArr);
							   $logs->Response		= json_encode($resArr);
							   $logs->UserId		= "";
							   $logs->UserType    = ""; 	
							   $logs->AddLog();		
								
							response($resArr, 200);
							
						} else {
							
							$resArr = array(
								"status" 		=> "1", 
								"message" 		=> "No data found."
							);

							 $logs->Module 		= "Master";
							 $logs->Action		= "get";
							 $logs->Request		= json_encode($getArr);
							 $logs->Response	= json_encode($resArr);
							 $logs->UserId		= "";
							 $logs->UserType    = ""; 	
							 $logs->AddLog();		
							
							response($resArr, 200);
							
						}
					
				
				} else if($type == "crashSeverityTypes"){ 
				
						$getList = $master->getCrashSeverityTypes();
						
						if($getList){
							
							$resArr = array(
								"status" 		=> "1", 
								"message" 		=> "Crash Saverity data get successfully.",
								"data" 			=> $getList
							);
							
							 $logs->Module 		= "Master";
							 $logs->Action		= "get";
							 $logs->Request		= json_encode($getArr);
							 $logs->Response	= json_encode($resArr);
							 $logs->UserId		= "";
							 $logs->UserType    = ""; 	
							 $logs->AddLog();			
								
							response($resArr, 200);
							
						} else {
							
							$resArr = array(
								"status" 		=> "1", 
								"message" 		=> "No data found."
							);

							 $logs->Module 		= "Master";
							 $logs->Action		= "get";
							 $logs->Request		= json_encode($getArr);
							 $logs->Response	= json_encode($resArr);
							 $logs->UserId		= "";
							 $logs->UserType    = ""; 	
							 $logs->AddLog();		
							
							response($resArr, 200);
							
						}
				
				} else if($type == "vehicleTypes"){ 
				
						$getList = $master->getVehicleTypes();								
						
						if($getList){
							
							$resArr = array(
								"status" 		=> "1", 
								"message" 		=> "Vehicle data get successfully.",
								"data" 			=> $getList
							);
							
							 $logs->Module 		= "Master";
							 $logs->Action		= "get";
							 $logs->Request		= json_encode($getArr);
							 $logs->Response	= json_encode($resArr);
							 $logs->UserId		= "";
							 $logs->UserType    = ""; 	
							 $logs->AddLog();		
								
							response($resArr, 200);
							
						} else {
							
							$resArr = array(
								"status" 		=> "1", 
								"message" 		=> "No data found."
							);

							 $logs->Module 		= "Master";
							 $logs->Action		= "get";
							 $logs->Request		= json_encode($getArr);
							 $logs->Response	= json_encode($resArr);
							 $logs->UserId		= "";
							 $logs->UserType    = ""; 	
							 $logs->AddLog();		
							
							response($resArr, 200);
							
						}
				
				} else {
					
					$resArr = array(
								"status" 		=> "0", 
								"message" 		=> "Master type parameter required."
							);

					 $logs->Module 		= "Master";
					 $logs->Action		= "get";
					 $logs->Request		= json_encode($getArr);
					 $logs->Response	= json_encode($resArr);
					 $logs->UserId		= "";
					 $logs->UserType    = ""; 	
					 $logs->AddLog();		
					
					response($resArr, 200);
					
				} 
		
		break;
		
	case "OPTIONS": //Handle Preflighted requests for CORS issue
		
		// set response code		
		header('Content-Type: text/plain');
		
		response("", 200);
		
		break;
		
	default:
	
		// Invalid Request Method
		$resArr = array("status" => "0", "message" => "Method Not Allowed.");						
		// display message: unable to create user
		response($resArr, 405);	
		
		break;
}

?>