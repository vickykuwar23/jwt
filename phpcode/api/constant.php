<?php
// show error reporting
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
 
// set your default time-zone
date_default_timezone_set('Asia/Kolkata');

define("BASE_PATH", $_SERVER["DOCUMENT_ROOT"]."/api/v1/");
define("CONFIG_PATH", BASE_PATH."config/");
define("UPLOAD_IMG", BASE_PATH."profileImage/");
define("UPLOAD_FILE", BASE_PATH."attachment/");
define("RESOURCE_PATH", BASE_PATH."resources/");
define("LIB_PATH", BASE_PATH."libs/");

//Image and Attachment Size define
define("ATTACHMENT_MAX_SIZE", "2048");	// in KB
define("ATTACHMENT_MIN_SIZE", "1");	// in KB
define("PROFILE_MIN_SIZE", "5");	// in KB
define("PROFILE_MAX_SIZE", "500");	// in KB

//eCos Variables define
define("CONTAINER_NAME", "nijisphp");
define("ATTACHMENT_FOLDER", "attachment");
define("PROFILEIMG_FOLDER", "profileImage");

// Google Captcha Key
define("SECRET_KEY", "6LcROTgUAAAAAIBoqaLaQwxvEruJccFSh812zIvp");
?>