<?php
// show error reporting
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
//error_reporting(0);
 
// set your default time-zone
//date_default_timezone_set('Asia/Kolkata');

define("BASE_PATH", $_SERVER["DOCUMENT_ROOT"]."/api/s4/uat/v1/");
define("CONFIG_PATH", BASE_PATH."config/");
define("UPLOAD_IMG", BASE_PATH."accidentImages/");
define("UPLOAD_FILE", BASE_PATH."accidentVideos/");
define("RESOURCE_PATH", BASE_PATH."resources/");
define("RESOURCE_MASTERPATH", BASE_PATH."resources/master/");
define("LIB_PATH", BASE_PATH."libs/");

//Image and Attachment Size define
define("IMG_MIN_SIZE", "5");		// in KB
define("IMG_MAX_SIZE", "500");		// in KB
define("VIDEO_MIN_SIZE", "1");		// in KB
define("VIDEO_MAX_SIZE", "2048");	// in KB
define("MAX_IMG", "5"); // max 5 Image
define("MAX_VIDEO", "2"); // Max 2 Video

//eCos Variables define
define("CONTAINER_NAME", "nijisphp");
define("VIDEO_FOLDER", "accidentVideos");
define("UPLOAD_PIC", "accidentImages");

// Google Captcha Key
define("SECRET_KEY", "6LcROTgUAAAAAIBoqaLaQwxvEruJccFSh812zIvp");
?>