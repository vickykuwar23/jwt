<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInit9f4d3aaac8bd4cd5ccc556a353af674a
{
    public static $prefixLengthsPsr4 = array (
        'P' => 
        array (
            'PhpAmqpLib\\' => 11,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'PhpAmqpLib\\' => 
        array (
            0 => __DIR__ . '/..' . '/php-amqplib/php-amqplib/PhpAmqpLib',
        ),
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInit9f4d3aaac8bd4cd5ccc556a353af674a::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInit9f4d3aaac8bd4cd5ccc556a353af674a::$prefixDirsPsr4;

        }, null, ClassLoader::class);
    }
}
