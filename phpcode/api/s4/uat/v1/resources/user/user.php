<?php
/*** 'user' object ***/
class User{
 
    /*** database connection and table name ***/
    private $aconn;
    private $table_name = 'public."AspNetUsers"';
	private $table_role = 'public."AspNetUserRoles"';
	private $table_role_detail = 'public."AspNetRoles"';
	
    /*** object properties ***/
    public $user_id;
    public $role_id;
    public $username;
    public $password;
	public $phonenumber;
	public $firstname;
	public $email;
    public $is_active;	
 
    /*** constructor ***/
    public function __construct($db){
        $this->aconn = $db;
		$this->createdOn = date("Y-m-d H:i:s");
    }
 
	/*** create new user record ***/
	function create(){
		
		/*** insert query	***/	
		$query = 'INSERT INTO '.$this->table_name.' ("role_id", "username", "password", "display_name", "email", "is_active", "is_deleted", "created_on") VALUES (:role_id, :username, :password, :display_name, :email, :is_active, :is_deleted, :created_on)  RETURNING users_id';
		
		/*** prepare the query ***/
		$stmt = $this->aconn->prepare($query);
	 
		/*** sanitize ***/
		$this->password		=	md5($this->password);
		//$this->EmailAddress	=	htmlspecialchars(strip_tags($this->EmailAddress));
		$this->is_active 		= 	'1';
		$this->is_deleted 		= 	'0';
		$this->created_on		=	$this->createdOn;
		//$this->updated_on			=	null;
		//$this->deleted_on			=	null;	
	     
		/*** bind the values ***/
		$stmt->bindParam(':username', $this->username);
		$stmt->bindParam(':role_id', $this->role_id);
		$stmt->bindParam(':display_name', $this->display_name);
		$stmt->bindParam(':email', $this->email);
		$stmt->bindParam(':password', $this->password);
		$stmt->bindParam(':is_active', $this->is_active);
		$stmt->bindParam(':is_deleted', $this->is_deleted);
		$stmt->bindParam(':created_on', $this->created_on);
		//$stmt->bindParam(':updated_on', $this->updated_on);
		//$stmt->bindParam(':deleted_on', $this->deleted_on);
	 
		/*** execute the query, also check if query was successful ***/
		if($stmt->execute()){
			
			$result = $stmt->fetch(PDO::FETCH_ASSOC);
						
			//return $result["users_id"];
			return true;
		} 
		
		return false;
		
	}
 
	/*** check if given email exist in the database ***/
	function emailExists(){
	 
		/*** query to check if email exists ***/
		$query = 'SELECT "users_id", "email"
					FROM '.$this->table_name.'
					WHERE "email" = ?
					LIMIT 1';
					
		/*** prepare the query ***/
		$stmt = $this->aconn->prepare( $query );
	 
		/*** bind given email value ***/
		$stmt->bindParam(1, $this->email);
	 
		/*** execute the query ***/
		$stmt->execute();
	 
		/*** get number of rows ***/
		$num = $stmt->rowCount();
	 
		/*** if email exists, assign values to object properties for easy access and use for php sessions ***/
		if($num > 0){
	 
			/*** return true because email exists in the database ***/
			return true;
		}
	 
		/*** return false if email does not exist in the database ***/
		return false;
	}
	
	
	/*** check if given user exist in the database ***/
	function userExists($UserId){
	 
		/*** query to check if email exists ***/
		$query = 'SELECT "Id"
					FROM '.$this->table_name.'
					WHERE "Id" = ? AND "is_deleted" = ?
					LIMIT 1';
					
		/*** prepare the query ***/
		$stmt = $this->aconn->prepare( $query );
	 
		/*** bind given UserId value ***/
		$stmt->bindParam(1, $UserId);
		$stmt->bindValue(2, false, PDO::PARAM_BOOL);
		/*** execute the query ***/
		$stmt->execute();
	 
		/*** get number of rows ***/
		$num = $stmt->rowCount();
	 
		/*** if UserId exists, assign values to object properties for easy access and use for php sessions ***/
		if($num > 0){
	 
			/*** return true because UserId exists in the database ***/
			return true;
		}
	 
		/*** return false if UserId does not exist in the database ***/
		return false;
	}
	
	 
	/*** update a user record ***/
	public function update($updateStatus){		
		
		/*** if no posted password, do not update the password ***/
		$query = 'UPDATE ' . $this->table_name . ' SET ';
		
		if($updateStatus == "UpdateProfile"){
				
			$query .= '"FirstName" 	= :FirstName, ';
			
			if($this->email!= ""){
				
				$query .= '"Email" = :Email, '; 
			
			}
			
			if($this->phonenumber!= ""){
				
				$query .= '"PhoneNumber" = :PhoneNumber, '; 
				
			}

			
			$query .= ' "updated_on" = :updated_on';
			
			$query .= ' WHERE "Id" 	= :Id';	
			
		} else {
		
			// Only Status Update
			$query .= '"is_active" = :is_active, "updated_on" = :updated_on WHERE "Id" = :Id';
			
		}
		//echo $query;exit;
		/*** prepare the query ***/
		$stmt = $this->aconn->prepare($query);
		
		$this->updated_on		=	$this->createdOn;
		
		/*if($this->password!= ""){
			
			$this->password		=	md5($this->password);
			$stmt->bindParam(':password', $this->password);
		}	
		
		if($this->role_id!= ""){
			$stmt->bindParam(':role_id', $this->role_id);
		}*/
		
		/*** bind the values from the form ***/
		if($updateStatus == "UpdateProfile"){
			
			
			$stmt->bindParam(':FirstName', $this->firstname);
			
			if($this->email!= ""){
				
				$stmt->bindParam(':Email', $this->email);
				
			}
			
			if($this->phonenumber!= ""){
				
				$stmt->bindParam(':PhoneNumber', $this->phonenumber);
				
			}
			
		} else {		
			
			$stmt->bindParam(':is_active', $this->is_active);
			
		}
	 
		
		/*** unique ID of record to be edited ***/
		$stmt->bindParam(':updated_on', $this->updated_on);
		
		$stmt->bindParam(':Id', $this->user_id);
	
		/*** execute the query ***/
		if($stmt->execute()){
			
			if($this->role_id!= ""){
				$q = 'UPDATE ' . $this->table_role . ' SET "RoleId" = :RoleId WHERE "UserId" = :UserId';
				$s = $this->aconn->prepare($q);			
				$s->bindParam(':RoleId', $this->role_id);
				$s->bindParam(':UserId', $this->user_id);
				$s->execute();
			}
			
			return true;
			
		} 
	 
		return false;
	}
	
	
	function get($id = 0){
		
		/*** query to check if email exists ***/
							
		if($id > 0){
			
			$query = 'SELECT U."Id", U."UserName", R."RoleId", RN."Name", U."Email", U."PhoneNumber", U."FirstName", U."is_active", U."created_on"  
					FROM '.$this->table_name.' AS U JOIN
					'.$this->table_role.' AS R ON U."Id" = R."UserId" 
					 JOIN '.$this->table_role_detail.' AS RN ON R."RoleId" = RN."Id"
					  WHERE U."Id" = ? AND U."is_deleted" = ? ORDER BY U."Id" ASC';
			
		} else {
			
			$query = 'SELECT U."Id", U."UserName", R."RoleId", RN."Name", U."Email", U."PhoneNumber", U."FirstName", U."is_active", U."created_on"  
					FROM '.$this->table_name.' AS U JOIN
					'.$this->table_role.' AS R ON U."Id" = R."UserId" 
					JOIN '.$this->table_role_detail.' AS RN ON R."RoleId" = RN."Id"
					 WHERE U."is_deleted" = ? ORDER BY U."Id" ASC';
		}		
	 
		/*** prepare the query ***/
		$stmt = $this->aconn->prepare( $query );		
	 
		if($id > 0){
			/*** bind given email value ***/
			$stmt->bindValue(1, $id);
			$stmt->bindValue(2, false, PDO::PARAM_BOOL);
		} else {
			$stmt->bindValue(1, false, PDO::PARAM_BOOL);
		}
		
		/*** execute the query ***/
		$stmt->execute();
	 
		/*** get number of rows ***/
		$num = $stmt->rowCount();
	 
		/*** if email exists, assign values to object properties for easy access and use for php sessions ***/
		if($num>0){
	 
			/*** get record details / values ***/			
			while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
				
					$dataArr = array(
					"user_id" 		=> $row['Id'],
					"user_name" 	=> $row['UserName'],
					"role_id" 		=> $row['RoleId'],
					"role_name" 	=> $row['Name'],
					"email" 		=> $row['Email'],
					"first_name" 	=> $row['FirstName'],
					"is_active" 	=> $row['is_active'],
					"created_on" 	=> $row['created_on']
					);
					$userData[] = $dataArr;
			}
	 
			/*** return true because email exists in the database ***/
			return $userData;
		}
	 
		/*** return false if email does not exist in the database ***/
		return false;
		
	}
	
	/*************** Delete (Deactivate) User ****************/
	function delete(){
		
		 $query = 'UPDATE ' . $this->table_name . ' SET
					"is_deleted"	= :is_deleted,
					"deleted_on"	= :deleted_on
					 WHERE "Id" 	= :Id';
		$stmt = $this->aconn->prepare($query);	
		
		$this->is_deleted	=	'1';
		$this->deleted_on  =	$this->createdOn;
		$stmt->bindParam(':is_deleted', $this->is_deleted);
		$stmt->bindParam(':deleted_on', $this->deleted_on);
		$stmt->bindParam(':Id', $this->user_id);
			
		if($stmt->execute()){
			
			//return $stmt->rowCount();
			return true;
			
		} else {
			
			return false;
			
		}
			
	}
	
}