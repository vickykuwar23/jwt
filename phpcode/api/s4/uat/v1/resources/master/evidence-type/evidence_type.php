<?php
/*** 'EvidenceType' object ***/
class EvidenceType{
 
    /*** database connection and table name ***/
    private $conn;
    private $table_name = 'public."evidence_type"';
	
    /*** object properties ***/ 
	public $id;
    public $evidence_type_name;
	public $is_active;
 
    /*** constructor ***/
    public function __construct($db){
        $this->conn = $db;
		$this->createdOn = date("Y-m-d H:i:s");
    }
	
	
	/*** check if given Evidence Type ID exist in the database ***/
	function evidence_type_id_exist($id){
	 
		/*** query to check if Evidence Type ID exists ***/
		$query = 'SELECT "evidence_type_id"
					FROM '.$this->table_name.'
					WHERE "evidence_type_id" = ? AND "is_deleted" = ?
					LIMIT 1';
					
		/*** prepare the query ***/
		$stmt = $this->conn->prepare( $query );
	 
		/*** bind given Evidence Type ID  value ***/
		$stmt->bindParam(1, $id);
		$stmt->bindValue(2, false, PDO::PARAM_BOOL);
		/*** execute the query ***/
		$stmt->execute();
	 
		/*** get number of rows ***/
		$num = $stmt->rowCount();
	 
		/*** if Evidence Type ID  exists, assign values to object properties for easy access and use for php sessions ***/
		if($num > 0){
	 
			/*** return true because Evidence Type ID exists in the database ***/
			return true;
		}
	 
		/*** return false if Evidence Type ID does not exist in the database ***/
		return false;
	}
	
	
	function getlisting($id = 0){
		
		/*** query to check if email exists ***/
		if($id == 0){
			
			$query = 'SELECT "evidence_type_id", "evidence_type_name", "is_active", "created_on" 
					FROM '.$this->table_name.' 
					WHERE "is_deleted" = ? ORDER BY "evidence_type_id" ASC';	
					
			/*** prepare the query ***/
			$stmt = $this->conn->prepare( $query );		
		 
			$stmt->bindValue(1, false, PDO::PARAM_BOOL);
			
		} else {
			
			$query = 'SELECT "evidence_type_id", "evidence_type_name", "is_active", "created_on" 
					FROM '.$this->table_name.' 
					WHERE "is_deleted" = ? AND "evidence_type_id" = ? ORDER BY "evidence_type_id" ASC';
			
			/*** prepare the query ***/
			$stmt = $this->conn->prepare( $query );		
		 
			$stmt->bindValue(1, false, PDO::PARAM_BOOL);
			$stmt->bindValue(2, $id);
						
		}
		
		/*** execute the query ***/
		$stmt->execute();
	 
		/*** get number of rows ***/
		$num = $stmt->rowCount();
	 
		/*** if EvidenceType exists, assign values to object properties for easy access and use for php sessions ***/
		if($num>0){
	 
			/*** get record details / values ***/			
			while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
					$getData[] = $row;
			}
	 
			/*** return true because EvidenceType exists in the database ***/
			return $getData;
		}
	 
		/*** return false if EvidenceType does not exist in the database ***/
		return false;
		
	}
	
	function create(){
		
		$query = 'INSERT INTO '.$this->table_name.' ("evidence_type_name", "is_active", "is_deleted", "created_on") VALUES (:evidence_type_name, :is_active, :is_deleted, :created_on) RETURNING evidence_type_id';		
		
		/*** prepare the query ***/
		$stmt = $this->conn->prepare($query);		
	 
		/*** sanitize ***/
		$this->is_active			=  '1';
		$this->is_deleted			=  '0';
		$this->created_on			=  $this->createdOn;
		
		$stmt->bindParam(':evidence_type_name', $this->evidence_type_name);
		$stmt->bindParam(':is_active', $this->is_active);
		$stmt->bindParam(':is_deleted', $this->is_deleted);
		$stmt->bindParam(':created_on', $this->created_on);		
	 
		/*** execute the query, also check if query was successful ***/		
		if($stmt->execute()){
			
			$result = $stmt->fetch(PDO::FETCH_ASSOC);
						
			return $result["evidence_type_id"];
			
		} 
		
		return false;
		
	}
	
	
	/*** update record ***/
	public function update($updateStatus){
		
		/*** SQL QUERY ***/
		$query = 'UPDATE ' . $this->table_name . ' SET ';
		
		
		if($updateStatus == 'UpdateField'){
			
			$query .= '"evidence_type_name" = :evidence_type_name, ';
			
		} else {
			
			$query .= '"is_active" = :is_active, ';
		}
		
		$query .='"updated_on" = :updated_on WHERE "evidence_type_id" = :evidence_type_id';
		
		/*** prepare the query ***/
		$stmt = $this->conn->prepare($query);
		
		$this->updated_on			=  $this->createdOn;
				 
		
		if($this->evidence_type_name!= ""){
			
			/*** bind the values from the form ***/		
			$stmt->bindParam(':evidence_type_name', $this->evidence_type_name);
		} 
		
		if($this->is_active!= ""){
			
			$stmt->bindParam(':is_active', $this->is_active);
		} 
		
		$stmt->bindParam(':updated_on', $this->updated_on);		
		
		/*** unique ID of record to be edited ***/
		$stmt->bindParam(':evidence_type_id', $this->id);
	 
		/*** execute the query ***/
		if($stmt->execute()){
			
			return true;
		}
	 
		return false;
		
	} // Function End
	
	
	function delete(){
		
		$query = 'UPDATE ' . $this->table_name . '
					  SET
						"deleted_on"	 	= :deleted_on,
						"is_deleted"	= :is_deleted
					  WHERE "evidence_type_id" 	= :evidence_type_id';
					  
		$stmt = $this->conn->prepare($query);	
		
		$this->is_deleted	=	'1';
		$this->deleted_on  	=	date("Y-m-d H:i:s");
		
		$stmt->bindParam(':is_deleted', $this->is_deleted);
		$stmt->bindParam(':deleted_on', $this->deleted_on);
		
		$stmt->bindParam(':evidence_type_id', $this->id);
		
		if($stmt->execute()){
			
			return true;
		}
	 
		return false;
			
	}
	
}