<?php
/*** 'State' object ***/
class States{
 
    /*** database connection and table name ***/
    private $conn;
    private $table_name 		= 'public."states"';
	private $table_countries 	= 'public."countries"';
	private $table_city 		= 'public."city"';
	
    /*** object properties ***/ 
	public $id;
    public $state_name;
	public $country_id;
	public $is_active;
 
    /*** constructor ***/
    public function __construct($db){
        $this->conn = $db;
		$this->createdOn = date("Y-m-d H:i:s");
    }
	
	/*** check if given State ID exist in the database ***/
	function state_id_exist($id){
	 
		/*** query to check if State ID exists ***/
		$query = 'SELECT "state_id"
					FROM '.$this->table_name.'
					WHERE "state_id" = ? AND "is_deleted" = ?
					LIMIT 1';
					
		/*** prepare the query ***/
		$stmt = $this->conn->prepare( $query );
	 
		/*** bind given State ID  value ***/
		$stmt->bindParam(1, $id);
		$stmt->bindValue(2, false, PDO::PARAM_BOOL);
		/*** execute the query ***/
		$stmt->execute();
	 
		/*** get number of rows ***/
		$num = $stmt->rowCount();
	 
		/*** if State ID  exists, assign values to object properties for easy access and use for php sessions ***/
		if($num > 0){
	 
			/*** return true because State ID exists in the database ***/
			return true;
		}
	 
		/*** return false if State ID does not exist in the database ***/
		return false;
	}
	
	function getlisting($id = 0, $type = false){
		
		/*** query to check if State exists ***/
		if($id == 0){
			
			$country_id = 101;	// for India country only

			$query = 'SELECT S."state_id", S."state_name", S."country_id", C."country_name", S."is_active", S."created_on" 
					FROM '.$this->table_name.' AS S JOIN 
					'.$this->table_countries.' AS C ON
					C."country_id" = S."country_id"
					WHERE S."is_deleted" = ? AND C."country_id" = ? ORDER BY S."state_id" ASC';		
					
			/*** prepare the query ***/
			$stmt = $this->conn->prepare( $query );		
		 
			$stmt->bindValue(1, false, PDO::PARAM_BOOL);
			$stmt->bindValue(2, $country_id);
			
		} else {
				
			if($type == "states"){
				
				$query = 'SELECT S."state_id", S."state_name", S."country_id", C."country_name", S."is_active", S."created_on" 
					FROM '.$this->table_name.' AS S JOIN 
					'.$this->table_countries.' AS C ON
					C."country_id" = S."country_id"
					WHERE S."is_deleted" = ? AND C."country_id" = ? ORDER BY S."state_id" ASC';	
				
			} else {
				
			$query = 'SELECT S."state_id", S."state_name", S."country_id", C."country_name", S."is_active", S."created_on" 
				FROM '.$this->table_name.' AS S JOIN 
				'.$this->table_countries.' AS C ON
				C."country_id" = S."country_id"
				WHERE S."is_deleted" = ? AND S."state_id" = ? ORDER BY S."state_id" ASC';
				
			}
				
			/*** prepare the query ***/
			$stmt = $this->conn->prepare( $query );		
		 
			$stmt->bindValue(1, false, PDO::PARAM_BOOL);
			$stmt->bindValue(2, $id);	
						
		}
		
		/*** execute the query ***/
		$stmt->execute();
	 
		/*** get number of rows ***/
		$num = $stmt->rowCount();
	 
		/*** if State exists, assign values to object properties for easy access and use for php sessions ***/
		if($num>0){
	 
			/*** get record details / values ***/			
			while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
					$getData[] = $row;
			}
	 
			/*** return true because State exists in the database ***/
			return $getData;
		}
	 
		/*** return false if State does not exist in the database ***/
		return false;
		
	}
	
	function create(){
		
		$query = 'INSERT INTO '.$this->table_name.' ("state_name", "country_id", "is_active", "is_deleted", "created_on") VALUES (:state_name, :country_id, :is_active, :is_deleted, :created_on) RETURNING state_id';		
		
		/*** prepare the query ***/
		$stmt = $this->conn->prepare($query);		
	
		/*** sanitize ***/
		$this->is_active			=  '1';
		$this->is_deleted			=  '0';
		$this->created_on			=  $this->createdOn;
		
		$stmt->bindParam(':state_name', $this->state_name);
		$stmt->bindParam(':country_id', $this->country_id);
		$stmt->bindParam(':is_active', $this->is_active);
		$stmt->bindParam(':is_deleted', $this->is_deleted);
		$stmt->bindParam(':created_on', $this->created_on);		
	 
		/*** execute the query, also check if query was successful ***/		
		if($stmt->execute()){
			
			$result = $stmt->fetch(PDO::FETCH_ASSOC);
						
			return $result["state_id"];
			
		} 
		
		return false;
		
	}
	
	
	/*** update record ***/
	public function update($updateStatus){
		
		/*** SQL QUERY ***/
		$query = 'UPDATE ' . $this->table_name . ' SET ';
		
		  
		if($updateStatus == 'UpdateField'){
			
			$query .= '"state_name" = :state_name, "country_id" = :country_id, ';
			
		} else {
			
			$query .= '"is_active" = :is_active, ';
		}	
		
		$query .= '"updated_on" = :updated_on WHERE "state_id" = :state_id';
		
		/*** prepare the query ***/
		$stmt = $this->conn->prepare($query);
		
		$this->updated_on			=  $this->createdOn;
				 
		
		
		if($this->state_name!= "" && $this->country_id!= ""){
			
			/*** bind the values from the form ***/		
			$stmt->bindParam(':state_name', $this->state_name);
			$stmt->bindParam(':country_id', $this->country_id);
		} 	
		
		if($this->is_active!= ""){
			
			$stmt->bindParam(':is_active', $this->is_active);
		}	
		
		
		$stmt->bindParam(':updated_on', $this->updated_on);		
		
		/*** unique ID of record to be edited ***/
		$stmt->bindParam(':state_id', $this->id);
	 
		/*** execute the query ***/
		if($stmt->execute()){
			
			return true;
		}
	 
		return false;
		
	} // Function End
	
	
	function delete(){
		
		$query = 'UPDATE ' . $this->table_name . ' SET
						"deleted_on"	 	= :deleted_on,
						"is_deleted"	= :is_deleted
					  WHERE "state_id" 	= :state_id';
					  
		$stmt = $this->conn->prepare($query);	
		
		$this->is_deleted	=	'1';
		$this->deleted_on  	=	date("Y-m-d H:i:s");
		
		$stmt->bindParam(':is_deleted', $this->is_deleted);
		$stmt->bindParam(':deleted_on', $this->deleted_on);
		
		$stmt->bindParam(':state_id', $this->id);
		
		if($stmt->execute()){
			
			return true;
		}
	 
		return false;
			
	}
	
}