<?php

/*** files needed to connect to database and class***/
define("ROOT_PATH", realpath(__DIR__ . '/../../../..'));
include_once ROOT_PATH.'/constant.php';

include_once CONFIG_PATH.'functions.php';
include_once CONFIG_PATH.'database.php';
include_once RESOURCE_MASTERPATH.'countries/countries.php';
include_once RESOURCE_PATH.'logs/logs.php';
include_once RESOURCE_PATH.'auth/auth.php';

// Required headers
requireHeaders();
 
/*** get database connection ***/
$database 	= new Database();
$db 		= $database->getConnection();
$mdb 		= $database->getMongoConnection();
 
// instantiate countries object
$country  = new Countries($db);
$logs  = new Logs($mdb);
$auth  = new Auth();

// Get Request Method 
$request_method		= get_request_method();

switch($request_method)
{
	
	case "POST":
	
		$bearerToken = getBearerToken();
		
		$access_token = isset($bearerToken) ? $bearerToken : "";

		// if jwt is not empty
		if($access_token){	

			// decode jwt
			$decoded = $auth->validateToken($access_token);
			
			$data 	= array();
			$getArr = array();
			$reqArr = array();
			
			// if decode succeed, show user details			
			if($decoded['status'] == true){	

				$user_id 		= $decoded['TokenData']->data->user_id; // Decode ID
				$username 		= $decoded['TokenData']->data->username; // Username
				$role_id 		= $decoded['TokenData']->data->role_id; // Role ID
				$tokenArr 		= array("user_id" => (int)$user_id, 
										"username" => $username, 
										"role_id" => (int)$role_id
										);	

					// get posted data
					$data 	= get_request_body();
						
						if($data['status'] == true){
							
							$error = array();
							
							// set country property
							$country->country_code = isset($data['requestData']['country_code']) ? $data['requestData']['country_code'] : "";
							$country->country_name = isset($data['requestData']['country_name']) ? $data['requestData']['country_name'] : "";
							$country->phone_code = isset($data['requestData']['phone_code']) ? $data['requestData']['phone_code'] : "";
							
							if($country->country_code == ""){
								
								$error[] = "Please enter country code.";
								
							} 			
							if($country->country_name == ""){
								
								$error[] = "Please enter country name.";
								
							} else if (!preg_match('/^[\p{L} ]+$/u', $country->country_name)){
								
							  $error[] = 'Country Name must contain letters and spaces only!';
							  
							}
							if($country->phone_code == ""){
								
								$error[] = "Please enter phone code.";
								
							} 
							
							if(count($error) > 0){
								
									$errorMsg = implode(", ", $error);
									
									// tell the user login failed
									$resArr = array("status" => "0", "message" => $errorMsg);
									
									 //Add log data
									 $logs->Module 		= "Countries Master";
									 $logs->Action		= "create";
									 $logs->Request		= json_encode($data);
									 $logs->Response	= json_encode($resArr);
									 $logs->UserId		= (int)$user_id;
									 $logs->UserType    = ""; 
									 $logs->AddLog();
										 
									// tell the user login failed
									 response($resArr, 400);
								
							} else {
								
								$return_id = $country->create();
								
								if($return_id > 0){
									
									// generate jwt
									//$access_token = $auth->generateToken($tokenArr);
									
									$resArr = array("status" => "1", 
													"message" => "Country created successfully.",
													"access_token" 	=> $access_token);
											
									 $logs->Module 		= "Country Master";
									 $logs->Action		= "create";
									 $logs->Request		= json_encode($data);
									 $logs->Response	= json_encode($resArr);
									 $logs->UserId		= (int)$user_id;
									 $logs->UserType    = ""; 
									 $logs->AddLog();
									
									// display message: Country Master was created
									 response($resArr, 200);
								 
								 } else { // message if unable to create Country Master
								 
										// generate jwt
										//$access_token = $auth->generateToken($tokenArr);
									
										// display message: unable to create Crime Category
										$resArr = array("status" => "0",
														"message" => "Unable to create data.",
														"access_token" 	=> $access_token);
										
										 $logs->Module 		= "Country Master";
										 $logs->Action		= "create";
										 $logs->Request		= json_encode($data);
										 $logs->Response	= json_encode($resArr);
										 $logs->UserId		= (int)$user_id;
										 $logs->UserType    = ""; 
										 $logs->AddLog();
										
										 // display message: unable to create Country Master
										 response($resArr, 400);
										
									} // Insert ID Check
									
							}
						
						} else {
							
							// display message: unable to create countries 
							$resArr = array("status" => "0", "message" => $data["requestData"]);
							
							// Merge Array Post And GET Values
							$reqArr = array_merge($data, $getArr);
							
							 $logs->Module 		= "Country Master";
							 $logs->Action		= "create";
							 $logs->Request		= json_encode($reqArr);
							 $logs->Response	= json_encode($resArr);
							 $logs->UserId		= (int)$user_id;
							 $logs->UserType    = ""; 
							 $logs->AddLog();
							 
							response($resArr, 400);
						}
			 } else {
				
					// show error message
					$resArr = array(
						"status" 	=> "0", 
						"message" 	=> "Invalid Authorization. Error : ". $decoded["TokenData"]
					);
					
					// Merge Array Post And GET Values
					$reqArr = array_merge($data, $getArr);
					
					//Add log data
					 $logs->Module 		= "Country Master";
					 $logs->Action		= "create";
					 $logs->Request		= json_encode($reqArr);
					 $logs->Response	= json_encode($resArr);
					 $logs->UserId		= "";
					 $logs->UserType    = "";	
					 $logs->AddLog();
					
					response($resArr, 401);
					
			} // Invalid Authorization error		
					
		
		}  else { // show error message if jwt is empty
		 
			// tell the user access denied
			$resArr = array("status" => "0", "message" => "Access denied.");
			
			 $logs->Module 		= "Country Master";
			 $logs->Action		= "create";
			 $logs->Request		= "";
			 $logs->Response	= json_encode($resArr);
			 $logs->UserId		= "";
			 $logs->UserType	= "";
			 $logs->AddLog();
			
			// display message: unable to create user
			response($resArr, 401);
		}	
	
	break;
		
	case "PUT":

	   $bearerToken = getBearerToken();
		
		$access_token = isset($bearerToken) ? $bearerToken : "";
	
		// if jwt is not empty
		if($access_token){	

			// decode jwt
			$decoded = $auth->validateToken($access_token);
			
			$data 	= array();
			$getArr = array();
			$reqArr = array();
			
			if($decoded['status'] == true){	

				$user_id 		= $decoded['TokenData']->data->user_id; // Decode ID
				$username 		= $decoded['TokenData']->data->username; // Username
				$role_id 		= $decoded['TokenData']->data->role_id; // Role ID
				$tokenArr 		= array("user_id" => (int)$user_id, 
										"username" => $username, 
										"role_id" => (int)$role_id
										);	 
		
							// Get URL Data	
							$getArr = get_url_data();
							
							$error = array();
							
							// Get ID From URL		
							$country->id = isset($getArr['id']) ? intval($getArr['id']) : "";
							
							if($country->id == ""){
											
								$error[] = "Country ID is required.";
								
							}
							
							if(count($error) > 0){
									
									$errorMsg = implode(", ", $error);
									
									// tell the Country Master error occur
									$resArr = array("status" => "0", "message" => $errorMsg);
									
									// Merge Array Post And GET Values
									$reqArr = array_merge($data, $getArr);
									
									 //Add log data
									 $logs->Module 		= "Country Master";
									 $logs->Action		= "update";
									 $logs->Request		= json_encode($reqArr);
									 $logs->Response	= json_encode($resArr);
									 $logs->UserId		= (int)$user_id;
									 $logs->UserType    = ""; 
									 $logs->AddLog();
										 
									// tell the Country Master error occur
									 response($resArr, 400);
								
							} 
							
							// get posted data
							$data = get_request_body();
							
							if($data['status'] == true){
								
								
								
								// set country property
								$country->country_code = isset($data['requestData']['country_code']) ? $data['requestData']['country_code'] : "";
								$country->country_name = isset($data['requestData']['country_name']) ? $data['requestData']['country_name'] : "";
								$country->phone_code = isset($data['requestData']['phone_code']) ? $data['requestData']['phone_code'] : "";
								$country->is_active = isset($data['requestData']['is_active']) ? $data['requestData']['is_active'] : "";
								
								if(isset($data['requestData']['is_active'])){
										
										$updateStatus 		= "UpdateStatus";
										$UpdateMsg			= "Country status successfully updated.";
										$country->is_active 	= $data['requestData']['is_active'];
										
										if($country->is_active == ""){
									
												$error[] = "Is Active is required.";
												
										} 
											
								} else {

									$updateStatus = "UpdateField";
									$UpdateMsg		= "Country updated successfully.";
											
											
									if($country->country_code == ""){
									
										$error[] = "Please enter country code.";
										
									} 			
									if($country->country_name == ""){
										
										$error[] = "Please enter country name.";
										
									} else if (!preg_match('/^[\p{L} ]+$/u', $country->country_name)){
										
									  $error[] = 'Country Name must contain letters and spaces only!';
									  
									}
									if($country->phone_code == ""){
										
										$error[] = "Please enter phone code.";
										
									} 
											
								} // URL ID validation required	


								
								
								if(count($error) > 0){
									
										$errorMsg = implode(", ", $error);
										
										// tell the Country Master error occur
										$resArr = array("status" => "0", "message" => $errorMsg);
										
										// Merge Array Post And GET Values
										$reqArr = array_merge($data, $getArr);
										
										 //Add log data
										 $logs->Module 		= "Country Master";
										 $logs->Action		= "update";
										 $logs->Request		= json_encode($reqArr);
										 $logs->Response	= json_encode($resArr);
										 $logs->UserId		= (int)$user_id;
										 $logs->UserType    = ""; 
										 $logs->AddLog();
											 
										// tell the Country Master error occur
										 response($resArr, 400);
									
								} else {
									
									$country_id_exist = $country->country_id_exist($country->id);
										
									if($country_id_exist == true){
									
											$get_id = $country->update($updateStatus);
											
											if($get_id == true){
												
												// generate jwt
												//$access_token = $auth->generateToken($tokenArr);
												
											$resArr = array("status" => "1", 
											"message" => $UpdateMsg,
											"access_token" 	=> $access_token);
											
											// Merge Array Post And GET Values
											$reqArr = array_merge($data, $getArr);	
														
												 $logs->Module 		= "Country Master";
												 $logs->Action		= "update";
												 $logs->Request		= json_encode($reqArr);
												 $logs->Response	= json_encode($resArr);
												 $logs->UserId		= (int)$user_id;
												 $logs->UserType    = ""; 
												 $logs->AddLog();
												
												// display message: Country Master was updated
												 response($resArr, 200);
											 
											 } else { 
												
												// generate jwt
												//$access_token = $auth->generateToken($tokenArr);
												
													// display message: unable to update Country Master
													$resArr = array("status" => "0", "message" => "Unable to update data.",
													"access_token" 	=> $access_token);
													
													// Merge Array Post And GET Values
													$reqArr = array_merge($data, $getArr);
													
													 $logs->Module 		= "Country Master";
													 $logs->Action		= "update";
													 $logs->Request		= json_encode($reqArr);
													 $logs->Response	= json_encode($resArr);
													 $logs->UserId		= (int)$user_id;
													 $logs->UserType    = ""; 
													 $logs->AddLog();
													
													 // display message: unable to update countries
													 response($resArr, 400);
													
												} // Insert ID Check
										
										
										} else {
											
										// generate jwt
										//$access_token = $auth->generateToken($tokenArr);	
										
										$resArr = array(
												"status" 		=> "0", 
												"message" 		=> "Country ID does not exist.",
												"access_token" 	=> $access_token
											);
											
										// Merge Array Post And GET Values
										$reqArr = array_merge($data, $getArr);	
											
										 $logs->Module 		= "Country Master";
										 $logs->Action		= "update";
										 $logs->Request		= json_encode($reqArr);
										 $logs->Response	= json_encode($resArr);
										 $logs->UserId		= (int)$user_id;
										 $logs->UserType    = "";  
										 $logs->AddLog();	
											
										response($resArr, 200);
										
									}
										
								}
							
							} else {
								
								// display message: unable to update countries
								$resArr = array("status" => "0", "message" => $data["requestData"]);
								
								// Merge Array Post And GET Values
								$reqArr = array_merge($data, $getArr);
								
								 $logs->Module 		= "Country Master";
								 $logs->Action		= "update";
								 $logs->Request		= json_encode($reqArr);
								 $logs->Response	= json_encode($resArr);
								 $logs->UserId		= (int)$user_id;
								 $logs->UserType    = ""; 
								 $logs->AddLog();
								 
								response($resArr, 400);
							}
	
				} else {
				
						// show error message
						$resArr = array(
							"status" 	=> "0", 
							"message" 	=> "Invalid Authorization. Error : ". $decoded["TokenData"]
						);
						
						// Merge Array Post And GET Values
						$reqArr = array_merge($data, $getArr);
						
						//Add log data
						 $logs->Module 		= "Country Master";
						 $logs->Action		= "update";
						 $logs->Request		= json_encode($reqArr);
						 $logs->Response	= json_encode($resArr);
						 $logs->UserId		= "";
						 $logs->UserType    = "";	
						 $logs->AddLog();
						
						response($resArr, 401);
				}
		
			}  else { // show error message if jwt is empty
		 
			// tell the user access denied
			$resArr = array("status" => "0", "message" => "Access denied.");
			
			//Add log data
			 $logs->Module 		= "Country Master";
			 $logs->Action		= "update";
			 $logs->Request		= "";
			 $logs->Response	= json_encode($resArr);
			 $logs->UserId		= "";
			 $logs->UserType	= "";			 
			 $logs->AddLog();
			
			// display message: unable to update act master
			response($resArr, 401);
		}
	
	
	break;	
		
	case 'GET':	
		
		
		$bearerToken = getBearerToken();
		
		$access_token = isset($bearerToken) ? $bearerToken : "";
			
		// if jwt is not empty
		if($access_token){
			
			// decode jwt		
			$decoded  = $auth->validateToken($access_token);	
			
			$getArr = array();
			
			 // if decode succeed, show user details
			if($decoded["status"] == true) {
				
				$user_id 		= $decoded['TokenData']->data->user_id; // Decode ID
				$username 		= $decoded['TokenData']->data->username; // Username
				$role_id 		= $decoded['TokenData']->data->role_id; // Role ID
				$tokenArr 		= array("user_id" => (int)$user_id, 
										"username" => $username, 
										"role_id" => (int)$role_id
										);			
				
			
				$getArr = get_url_data();					
				
				$id 	= isset($getArr['id']) ? intval($getArr['id']) : 0;
				//$type 	= isset($getArr['type']) ? $getArr['type'] : "";
				
				$getList = false;
					
					if($id == ""){ 
				
						$getList = $country->getlisting();
						$resMsg = "Country data get successfully.";
					}
					
					if($id > 0){
						
						$country_id_exist = $country->country_id_exist($id);
						
						if($country_id_exist == true){
							
							$getList = $country->getlisting($id);
							$resMsg = "Country data get successfully.";
							
						} else {
							
							// generate jwt
							//$access_token = $auth->generateToken($tokenArr);
							
							$resArr = array(
									"status" 		=> "0", 
									"message" 		=> "Country ID does not exist.",
									"access_token" 	=> $access_token
								);
								
							 $logs->Module 		= "Country Master";
							 $logs->Action		= "get";
							 $logs->Request		= json_encode($getArr);
							 $logs->Response	= json_encode($resArr);
							 $logs->UserId		= (int)$user_id;
							 $logs->UserType    = "";  
							 $logs->AddLog();	
								
							response($resArr, 200);
						}
						
					}
					
					
				
				if($getList){
					
					// generate jwt
					//$access_token = $auth->generateToken($tokenArr);
							
					$resArr = array(
						"status" 		=> "1", 
						"message" 		=> $resMsg,
						"access_token" 	=> $access_token,
						"data" 			=> $getList
					);

					 $logs->Module 		= "Country Master";
					 $logs->Action		= "get";
					 $logs->Request		= json_encode($getArr);
					 $logs->Response	= json_encode($resArr);
					 $logs->UserId		= (int)$user_id;
					 $logs->UserType    = "";  	
					 $logs->AddLog();		
					
					response($resArr, 200);
					
				} else {
					
					$getList = array();
					
					// generate jwt
					//$access_token = $auth->generateToken($tokenArr);
					
					$resArr = array(
						"status" 		=> "0", 
						"message" 		=> "No data found.",
						"access_token" 	=> $access_token,
						"data" 			=> $getList
					);

					 $logs->Module 		= "Country Master";
					 $logs->Action		= "get";
					 $logs->Request		= json_encode($getArr);
					 $logs->Response	= json_encode($resArr);
					 $logs->UserId		= (int)$user_id;
					 $logs->UserType    = "";  	
					 $logs->AddLog();		
					
					response($resArr, 200);
					
				}
				
		} else {			 
				
				// show error message
				$resArr =array(
					"status" 	=> "0", 
					"message" 	=> "Invalid Authorization. Error : ". $decoded["TokenData"]
				);
				
				//Add log data
				 $logs->Module 		= "Country Master";
				 $logs->Action		= "get";
				 $logs->Request		= "";
				 $logs->Response	= json_encode($resArr);
				 $logs->UserId		= "";
				 $logs->UserType	= "";	
				 $logs->AddLog();
				
				response($resArr, 401);
			}			
							
		}  else { // show error message if jwt is empty		 
			
			$resArr = array("status" => "0", "message" => "Access denied.");
			
			//Add log data
			 $logs->Module 		= "Country Master";
			 $logs->Action		= "get";
			 $logs->Request		= "";
			 $logs->Response	= json_encode($resArr);
			 $logs->UserId		= "";
			 $logs->UserType	= "";	
			 $logs->AddLog();
			
			response($resArr, 401);
		}			
		
		break;
		
		
	case 'DELETE':	
	
		$bearerToken = getBearerToken();
		
		$access_token = isset($bearerToken) ? $bearerToken : "";
	
		// if jwt is not empty
		if($access_token)
		{
			
			// decode jwt
			$decoded = $auth->validateToken($access_token);
	
			$getArr = array();
			
			// if decode succeed, show user details
			if($decoded["status"] == true) {
					
				$user_id 		= $decoded['TokenData']->data->user_id; // Decode ID
				$username 		= $decoded['TokenData']->data->username; // Username
				$role_id 		= $decoded['TokenData']->data->role_id; // Role ID
				$tokenArr 		= array("user_id" => (int)$user_id, 
										"username" => $username, 
										"role_id" => (int)$role_id
										);	
			
						$getArr = get_url_data();
									
						// set role property values	
						$country->id = isset($getArr['id']) ? intval($getArr['id']) : "";
						
						$error = array();
						
						if($country->id == ""){
										
							$error[] = "Country ID is required.";
							
						} 
						
						if(!is_numeric($country->id)){
							
							$error[] = "Country ID is must contain number.";
							
						}
						
						if(count($error) > 0){
								
						$errorMsg = implode(", ", $error);
							 
						// tell the user login failed
						$resArr = array("status" => "0", "message" => $errorMsg);						

						//Add log data
						 $logs->Module 		= "Country Master";
						 $logs->Action		= "delete";
						 $logs->Request		= json_encode($getArr);
						 $logs->Response	= json_encode($resArr);
						 $logs->UserId		= (int)$user_id;
						 $logs->UserType    = ""; 
						 $logs->AddLog();

						response($resArr, 200);
						 
					 } else {
						 
						 $country_id_exist = $country->country_id_exist($country->id);
								
						if($country_id_exist == true){	
							
							$deletecountry = $country->delete();
											
							if($deletecountry == true){	

								// generate jwt
								//$access_token = $auth->generateToken($tokenArr);
									 
								$resArr = array(
												"status" 		=> "1", 
												"message" 		=> "Country was successfully deleted.",
												"access_token" 	=> $access_token
											);			
								
								//Add log data
								 $logs->Module 		= "Country Master";
								 $logs->Action		= "delete";
								 $logs->Request		= json_encode($getArr);
								 $logs->Response	= json_encode($resArr);
								 $logs->UserId		= (int)$user_id;
								 $logs->UserType    = ""; 
								 $logs->AddLog();
					
								response($resArr, 200);
								
							 } else {
								 
								  // generate jwt
								// $access_token = $auth->generateToken($tokenArr);
								 
								 $resArr = array(
												"status" 		=> "1", 
												"message" 		=> "Unable to delete country.",
												"access_token" 	=> $access_token
											);
											
								//Add log data
								 $logs->Module 		= "Country Master";
								 $logs->Action		= "delete";
								 $logs->Request		= json_encode($getArr);
								 $logs->Response	= json_encode($resArr);
								 $logs->UserId		= (int)$user_id;
								 $logs->UserType    = ""; 
								 $logs->AddLog();		
									 
								response($resArr, 401);
								 
							 }	// delete condition end	
							 
							 
						 } else {
								
								// generate jwt
								//$access_token = $auth->generateToken($tokenArr);		
											 
								$resArr = array(
									"status" 		=> "0", 
									"message" 		=> "Country Master ID does not exist.",
									"access_token" 	=> $access_token
								);
								
								 $logs->Module 		= "Country Master";
								 $logs->Action		= "delete";
								 $logs->Request		= json_encode($getArr);
								 $logs->Response	= json_encode($resArr);
								 $logs->UserId		= (int)$user_id;
								 $logs->UserType    = "";  
								 $logs->AddLog();	
					
								response($resArr, 200);
							
						}
						 
					 }
					 
			} else {
			 
				// set response code				
				$resArr = array(
							"status" 	=> "0", 
							"message" 	=> "Invalid Authorization. Error : ". $decoded["TokenData"]
						);
					
				//Add log data
				 $logs->Module 		= "Country Master";
				 $logs->Action		= "delete";
				 $logs->Request		= json_encode($getArr);
				 $logs->Response	= json_encode($resArr);
				 $logs->UserId		= "";
				 $logs->UserType	= "";
				 $logs->AddLog();			
								 
				response($resArr, 401);
			}
			 
		}  else { // show error message if jwt is empty
			 
				// tell the City access denied
				$resArr = array("status" => "0", "message" => "Access denied.");
				
				//Add log data
				 $logs->Module 		= "Country Master";
				 $logs->Action		= "delete";
				 $logs->Request		= "";
				 $logs->Response	= json_encode($resArr);
				 $logs->UserId		= "";
				 $logs->UserType	= "";
				 $logs->AddLog();
							 
				response($resArr, 401);
				
		}
	
	
		break;
		
	case "OPTIONS": //Handle Preflighted requests for CORS issue
		
		// set response code		
		header('Content-Type: text/plain');
		
		response("", 200);
		
		break;
		
	default:
	
		// Invalid Request Method
		$resArr = array("status" => "0", "message" => "Method Not Allowed.");						
		// display message: unable to create user
		response($resArr, 405);	
		
		break;
}

?>