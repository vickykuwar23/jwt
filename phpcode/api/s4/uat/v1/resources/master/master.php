<?php
/*** 'user' object ***/
class Master{
 
    /*** database connection and table name ***/
    private $conn;
    private $table_name_1 = 'public."ReporteeType"';
	private $table_name_2 = 'public."CollisionType"';
	private $table_name_3 = 'public."CrashSeverityTypes"';
	private $table_name_4 = 'public."VehicleTypes"';
	
    /*** object properties ***/
    /*public $UserId;
    public $FirstName;
    public $LastName;
    public $EmailAddress;
	public $Password;
	public $IsActive;
	public $imgName;
	public $ProfileImage;
	public $Attachment;
	public $AttachFilename;*/
	//public $currentDateTime = date("Y-m-d H:i:s");
 
    /*** constructor ***/
    public function __construct($db){
        $this->conn = $db;
    }
	
	function getReporteeType(){
		
		/*** query to check if email exists ***/
							
		$query = 'SELECT "ReporteeTypeId", "ReporteeType", "CreatedOn" 
					FROM '.$this->table_name_1.' 
					WHERE "IsActive" = ? AND "IsDeleted" = ? ORDER BY "ReporteeTypeId" ASC';	
	 
		/*** prepare the query ***/
		$stmt = $this->conn->prepare( $query );		
	 
		$stmt->bindValue(1, true, PDO::PARAM_BOOL);
		$stmt->bindValue(2, false, PDO::PARAM_BOOL);
		
		/*** execute the query ***/
		$stmt->execute();
	 
		/*** get number of rows ***/
		$num = $stmt->rowCount();
	 
		/*** if email exists, assign values to object properties for easy access and use for php sessions ***/
		if($num>0){
	 
			/*** get record details / values ***/			
			while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
					$getData[] = $row;
			}
	 
			/*** return true because email exists in the database ***/
			return $getData;
		}
	 
		/*** return false if email does not exist in the database ***/
		return false;
		
	}
	
	
	function getCollisionType(){
		
		/*** query to check if email exists ***/
							
		$query = 'SELECT "CollisionTypeId", "CollisionType", "CreatedOn" 
					FROM '.$this->table_name_2.' 
					WHERE "IsActive" = ? AND "IsDeleted" = ? ORDER BY "CollisionTypeId" ASC';	
	 
		/*** prepare the query ***/
		$stmt = $this->conn->prepare( $query );		
	 
		$stmt->bindValue(1, true, PDO::PARAM_BOOL);
		$stmt->bindValue(2, false, PDO::PARAM_BOOL);
		
		/*** execute the query ***/
		$stmt->execute();
	 
		/*** get number of rows ***/
		$num = $stmt->rowCount();
	 
		/*** if email exists, assign values to object properties for easy access and use for php sessions ***/
		if($num>0){
	 
			/*** get record details / values ***/			
			while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
					$getData[] = $row;
			}
	 
			/*** return true because email exists in the database ***/
			return $getData;
		}
	 
		/*** return false if email does not exist in the database ***/
		return false;
		
	}
	
	function getCrashSeverityTypes(){
		
		/*** query to check if email exists ***/
							
		$query = 'SELECT "CrashSeverityTypeId", "CrashSeverityType", "CreatedOn" 
					FROM '.$this->table_name_3.' 
					WHERE "IsActive" = ? AND "IsDeleted" = ? ORDER BY "CrashSeverityTypeId" ASC';	
	 
		/*** prepare the query ***/
		$stmt = $this->conn->prepare( $query );		
	 
		$stmt->bindValue(1, true, PDO::PARAM_BOOL);
		$stmt->bindValue(2, false, PDO::PARAM_BOOL);
		
		/*** execute the query ***/
		$stmt->execute();
	 
		/*** get number of rows ***/
		$num = $stmt->rowCount();
	 
		/*** if email exists, assign values to object properties for easy access and use for php sessions ***/
		if($num>0){
	 
			/*** get record details / values ***/			
			while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
					$getData[] = $row;
			}
	 
			/*** return true because email exists in the database ***/
			return $getData;
		}
	 
		/*** return false if email does not exist in the database ***/
		return false;
		
	}
	
	function getVehicleTypes(){
		
		/*** query to check if email exists ***/
							
		$query = 'SELECT "VehicleTypeId", "VehicleType", "CreatedOn" 
					FROM '.$this->table_name_4.' 
					WHERE "IsActive" = ? AND "IsDeleted" = ? ORDER BY "VehicleTypeId" ASC';	
	 
		/*** prepare the query ***/
		$stmt = $this->conn->prepare( $query );		
	 
		$stmt->bindValue(1, true, PDO::PARAM_BOOL);
		$stmt->bindValue(2, false, PDO::PARAM_BOOL);
		
		/*** execute the query ***/
		$stmt->execute();
	 
		/*** get number of rows ***/
		$num = $stmt->rowCount();
	 
		/*** if email exists, assign values to object properties for easy access and use for php sessions ***/
		if($num>0){
	 
			/*** get record details / values ***/			
			while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
					$getData[] = $row;
			}
	 
			/*** return true because email exists in the database ***/
			return $getData;
		}
	 
		/*** return false if email does not exist in the database ***/
		return false;
		
	}
	
}