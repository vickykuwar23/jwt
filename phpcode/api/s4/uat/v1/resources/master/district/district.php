<?php
/*** 'District' object ***/
class District{
 
    /*** database connection and table name ***/
    private $conn;
    private $table_name = 'public."district_master"';
	
    /*** object properties ***/ 
	public $id;
    public $district_name;
	public $city_id;
 
    /*** constructor ***/
    public function __construct($db){
        $this->conn = $db;
		$this->createdOn = date("Y-m-d H:i:s");
    }
	
	function getlisting(){
		
		/*** query to check if State exists ***/
		if($this->id == 0 || $this->id == ""){
			
			$query = 'SELECT "district_id", "district_name", "city_id", "is_active", "created_on" 
					FROM '.$this->table_name.' 
					WHERE "is_deleted" = ? ORDER BY "district_id" ASC';	
					
			/*** prepare the query ***/
			$stmt = $this->conn->prepare( $query );		
		 
			$stmt->bindValue(1, false, PDO::PARAM_BOOL);
			
		} else {
			
			$query = 'SELECT "district_id", "district_name", "city_id", "is_active", "created_on" 
					FROM '.$this->table_name.' 
					WHERE "is_deleted" = ? AND "district_id" = ? ORDER BY "district_id" ASC';
			
			/*** prepare the query ***/
			$stmt = $this->conn->prepare( $query );		
		 
			$stmt->bindValue(1, false, PDO::PARAM_BOOL);
			$stmt->bindValue(2, $this->id);
						
		}
		
		/*** execute the query ***/
		$stmt->execute();
	 
		/*** get number of rows ***/
		$num = $stmt->rowCount();
	 
		/*** if State exists, assign values to object properties for easy access and use for php sessions ***/
		if($num>0){
	 
			/*** get record details / values ***/			
			while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
					$getData[] = $row;
			}
	 
			/*** return true because State exists in the database ***/
			return $getData;
		}
	 
		/*** return false if State does not exist in the database ***/
		return false;
		
	}
	
	function create(){
		
		$query = 'INSERT INTO '.$this->table_name.' ("district_name", "city_id", "is_active", "is_deleted", "created_on") VALUES (:district_name, :city_id, :is_active, :is_deleted, :created_on) RETURNING district_id';		
		
		/*** prepare the query ***/
		$stmt = $this->conn->prepare($query);		
	 
		/*** sanitize ***/
		$this->is_active			=  '1';
		$this->is_deleted			=  '0';
		$this->created_on			=  $this->createdOn;
		
		$stmt->bindParam(':district_name', $this->district_name);
		$stmt->bindParam(':city_id', $this->city_id);
		$stmt->bindParam(':is_active', $this->is_active);
		$stmt->bindParam(':is_deleted', $this->is_deleted);
		$stmt->bindParam(':created_on', $this->created_on);		
	 
		/*** execute the query, also check if query was successful ***/		
		if($stmt->execute()){
			
			$result = $stmt->fetch(PDO::FETCH_ASSOC);
						
			return $result["district_id"];
			
		} 
		
		return false;
		
	}
	
	
	/*** update record ***/
	public function update(){
		
		/*** SQL QUERY ***/
		$query = 'UPDATE ' . $this->table_name . ' SET "district_name" = :district_name,  "city_id" = :city_id, "updated_on" = :updated_on WHERE "district_id" = :district_id RETURNING district_id';
		
		/*** prepare the query ***/
		$stmt = $this->conn->prepare($query);
		
		$this->updated_on			=  $this->createdOn;
				 
		/*** bind the values from the form ***/		
		$stmt->bindParam(':district_name', $this->district_name);
		$stmt->bindParam(':city_id', $this->city_id);
		$stmt->bindParam(':updated_on', $this->updated_on);		
		
		/*** unique ID of record to be edited ***/
		$stmt->bindParam(':district_id', $this->id);
	 
		/*** execute the query ***/
		if($stmt->execute()){
			
			$result = $stmt->fetch(PDO::FETCH_ASSOC);
						
			return $result["district_id"];
		}
	 
		return false;
		
	} // Function End
	
	
	function delete(){
		
		$query = 'UPDATE ' . $this->table_name . ' SET
						"deleted_on"	 	= :deleted_on,
						"is_deleted"	= :is_deleted
					  WHERE "district_id" 	= :district_id RETURNING district_id';
					  
		$stmt = $this->conn->prepare($query);	
		
		$this->is_deleted	=	'1';
		$this->deleted_on  	=	date("Y-m-d H:i:s");
		
		$stmt->bindParam(':is_deleted', $this->is_deleted);
		$stmt->bindParam(':deleted_on', $this->deleted_on);
		
		$stmt->bindParam(':district_id', $this->id);
		
		if($stmt->execute()){
			
			$result = $stmt->fetch(PDO::FETCH_ASSOC);
						
			return $result["district_id"];
		}
	 
		return false;
			
	}
	
}