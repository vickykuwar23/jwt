<?php

/*** files needed to connect to database and class***/
define("ROOT_PATH", realpath(__DIR__ . '/../../../..'));
include_once ROOT_PATH.'/constant.php';

include_once CONFIG_PATH.'functions.php';
include_once CONFIG_PATH.'database.php';
include_once RESOURCE_MASTERPATH.'cities/cities.php';
include_once RESOURCE_PATH.'logs/logs.php';
include_once RESOURCE_PATH.'auth/auth.php';

// Required headers
requireHeaders();
 
/*** get database connection ***/
$database 	= new Database();
$db 		= $database->getConnection();
$mdb 		= $database->getMongoConnection();
 
// instantiate product object
$city  = new City($db);
$logs  = new Logs($mdb);
$auth  = new Auth();

// Get Request Method 
$request_method		= get_request_method();

switch($request_method)
{
	
	case "POST":
	
		$bearerToken = getBearerToken();
		
		$access_token = isset($bearerToken) ? $bearerToken : "";
		
		// if jwt is not empty
		if($access_token){	

			// decode jwt
			$decoded = $auth->validateToken($access_token);
			
			$data 	= array();
			$getArr = array();
			$reqArr = array();
			
			// if decode succeed, show user details			
			if($decoded['status'] == true){	

				$user_id 		= $decoded['TokenData']->data->user_id; // Decode ID
				$username 		= $decoded['TokenData']->data->username; // Username
				$role_id 		= $decoded['TokenData']->data->role_id; // Role ID
				$tokenArr 		= array("user_id" => (int)$user_id, 
										"username" => $username, 
										"role_id" => (int)$role_id
										);		
				

				// get posted data
				$data 	= get_request_body();
				
				if($data['status'] == true){
					
					$error = array();
					
					// set city property
					$city->city_name = isset($data['requestData']['city_name']) ? $data['requestData']['city_name'] : "";
					$city->state_id = isset($data['requestData']['state_id']) ? $data['requestData']['state_id'] : "";
					
					
					if($city->state_id == ""){
						
						$error[] = "Please select state id.";
						
					} 			
					if($city->city_name == ""){
						
						$error[] = "Please enter city name.";
						
					} else if (!preg_match('/^[\p{L} ]+$/u', $city->city_name)){
						
					  $error[] = 'City Name must contain letters and spaces only!';
					  
					}
					
					if(count($error) > 0){
						
							$errorMsg = implode(", ", $error);
							
							// tell the user login failed
							$resArr = array("status" => "0", "message" => $errorMsg);
							
							 //Add log data
							 $logs->Module 		= "City Master";
							 $logs->Action		= "create";
							 $logs->Request		= json_encode($data);
							 $logs->Response	= json_encode($resArr);
							 $logs->UserId		= (int)$user_id;
							 $logs->UserType    = ""; 
							 $logs->AddLog();
								 
							// tell the user login failed
							 response($resArr, 400);
						
					} else {
						
						$city_id = $city->create();
						
						if($city_id > 0){
							
							// generate jwt
							//$access_token = $auth->generateToken($tokenArr);
							
							$resArr = array("status" => "1", 
											"message" => "City created successfully.",
											"access_token" 	=> $access_token);
									
							 $logs->Module 		= "City Master";
							 $logs->Action		= "create";
							 $logs->Request		= json_encode($data);
							 $logs->Response	= json_encode($resArr);
							 $logs->UserId		= (int)$user_id;
							 $logs->UserType    = ""; 
							 $logs->AddLog();
							
							// display message: City Master was created
							 response($resArr, 200);
						 
						 } else { // message if unable to create City Master
						 
								// generate jwt
								//$access_token = $auth->generateToken($tokenArr);
							
								// display message: unable to create City 
								$resArr = array("status" => "0", 
												"message" => "Unable to create data.",
												"access_token" 	=> $access_token);
								
								 $logs->Module 		= "City Master";
								 $logs->Action		= "create";
								 $logs->Request		= json_encode($data);
								 $logs->Response	= json_encode($resArr);
								 $logs->UserId		= (int)$user_id;
								 $logs->UserType    = ""; 
								 $logs->AddLog();
								
								 // display message: unable to create City Master
								 response($resArr, 400);
								
							} // Insert ID Check
							
					}
				
				} else {
					
					// display message: unable to create City Category
					$resArr = array("status" => "0", "message" => $data["requestData"]);
					
					 $logs->Module 		= "City Master";
					 $logs->Action		= "create";
					 $logs->Request		= json_encode($data);
					 $logs->Response	= json_encode($resArr);
					 $logs->UserId		= (int)$user_id;
					 $logs->UserType    = ""; 
					 $logs->AddLog();
					 
					response($resArr, 400);
				}
				
				
			} else {
				
					// show error message
					$resArr = array(
						"status" 	=> "0", 
						"message" 	=> "Invalid Authorization. Error : ". $decoded["TokenData"]
					);
					
					// Merge Array Post And GET Values
					$reqArr = array_merge($data, $getArr);
					
					//Add log data
					 $logs->Module 		= "City Master";
					 $logs->Action		= "create";
					 $logs->Request		= json_encode($reqArr);
					 $logs->Response	= json_encode($resArr);
					 $logs->UserId		= "";
					 $logs->UserType    = "";	
					 $logs->AddLog();
					
					response($resArr, 401);
					
			} // Invalid Authorization error	
		
		
		}  else { // show error message if jwt is empty
		 
			// tell the user access denied
			$resArr = array("status" => "0", "message" => "Access denied.");
			
			 $logs->Module 		= "City Master";
			 $logs->Action		= "create";
			 $logs->Request		= "";
			 $logs->Response	= json_encode($resArr);
			 $logs->UserId		= "";
			 $logs->UserType	= "";
			 $logs->AddLog();
			
			// display message: unable to create user
			response($resArr, 401);
		}		
		
		break;
		
	case "PUT":

		$bearerToken = getBearerToken();
		
		$access_token = isset($bearerToken) ? $bearerToken : "";
		
		// if jwt is not empty
		if($access_token){	

			// decode jwt
			$decoded = $auth->validateToken($access_token);
			
			$data 	= array();
			$getArr = array();
			$reqArr = array();	
	    
			if($decoded['status'] == true){	

				$user_id 		= $decoded['TokenData']->data->user_id; // Decode ID
				$username 		= $decoded['TokenData']->data->username; // Username
				$role_id 		= $decoded['TokenData']->data->role_id; // Role ID
				$tokenArr 		= array("user_id" => (int)$user_id, 
										"username" => $username, 
										"role_id" => (int)$role_id
										);		
			
				// Get URL Data	
				$getArr = get_url_data();
				
				// Get ID From URL		
				$city->id = isset($getArr['id']) ? intval($getArr['id']) : "";
				
				$error = array();
				
				if($city->id == ""){
								
					$error[] = "City ID is required.";
					
				}				
				
				
				if(count($error) > 0){
						
					$errorMsg = implode(", ", $error);
							
					// tell the city Master error occur
					$resArr = array("status" => "0", "message" => $errorMsg);
					
					// Merge Array Post And GET Values
					$reqArr = array_merge($data, $getArr);
					
					 //Add log data
					 $logs->Module 		= "City Master";
					 $logs->Action		= "update";
					 $logs->Request		= json_encode($reqArr);
					 $logs->Response	= json_encode($resArr);
					 $logs->UserId		= (int)$user_id;
					 $logs->UserType    = ""; 
					 $logs->AddLog();
						 
					// tell the City Master error occur
					 response($resArr, 400);
					
				}
				
				// get posted data
				$data = get_request_body();
				
				if($data['status'] == true){
					
					//$error = array();
					
					// set city property
					$city->city_name = isset($data['requestData']['city_name']) ? $data['requestData']['city_name'] : "";
					$city->state_id = isset($data['requestData']['state_id']) ? $data['requestData']['state_id'] : "";
					$city->is_active = isset($data['requestData']['is_active']) ? $data['requestData']['is_active'] : "";
					
					
					if(isset($data['requestData']['is_active'])){
								
								$updateStatus 		= "UpdateStatus";
								$UpdateMsg			= "City status successfully updated.";
								$city->is_active 	= $data['requestData']['is_active'];
								
								if($city->is_active == ""){
							
										$error[] = "Is Active is required.";
										
								} 
							
						} else {
								
								$updateStatus   = "UpdateField";
								$UpdateMsg		= "City updated successfully.";
								
								if($city->state_id == ""){
						
									$error[] = "Please select state id.";
									
								} 			
								if($city->city_name == ""){
									
									$error[] = "Please enter city name.";
									
								} else if (!preg_match('/^[\p{L} ]+$/u', $city->city_name)){
									
								  $error[] = 'City Name must contain letters and spaces only!';
								  
								}
						}						
					
					
					if(count($error) > 0){
						
							$errorMsg = implode(", ", $error);
							
							// tell the city Master error occur
							$resArr = array("status" => "0", "message" => $errorMsg);
							
							// Merge Array Post And GET Values
							$reqArr = array_merge($data, $getArr);
							
							 //Add log data
							 $logs->Module 		= "City Master";
							 $logs->Action		= "update";
							 $logs->Request		= json_encode($reqArr);
							 $logs->Response	= json_encode($resArr);
							 $logs->UserId		= (int)$user_id;
							 $logs->UserType    = ""; 
							 $logs->AddLog();
								 
							// tell the City Master error occur
							 response($resArr, 400);
						
					} else {
						
						
						$city_exist = $city->city_exist($city->id);
							
						if($city_exist == true){
							
							$getId = $city->update($updateStatus);
						
							if($getId == true){
								
								// generate jwt
								//$access_token = $auth->generateToken($tokenArr);
								
								$resArr = array("status" => "1", 
												"message" => $UpdateMsg,
												"access_token" 	=> $access_token);
												
								// Merge Array Post And GET Values
								$reqArr = array_merge($data, $getArr);				
										
								 $logs->Module 		= "City Master";
								 $logs->Action		= "update";
								 $logs->Request		= json_encode($reqArr);
								 $logs->Response	= json_encode($resArr);
								 $logs->UserId		= (int)$user_id;
								 $logs->UserType    = ""; 
								 $logs->AddLog();
								
								// display message: City Master was updated
								 response($resArr, 200);
							 
							 } else { // message if unable to City Master
							 
									// generate jwt
									//$access_token = $auth->generateToken($tokenArr);
								
									// display message: unable to update City Master
									$resArr = array("status" => "0", 
													"message" => "Unable to update data.",
													"access_token" 	=> $access_token);
													
									// Merge Array Post And GET Values
									$reqArr = array_merge($data, $getArr);				
									
									 $logs->Module 		= "City Master";
									 $logs->Action		= "update";
									 $logs->Request		= json_encode($reqArr);
									 $logs->Response	= json_encode($resArr);
									 $logs->UserId		= (int)$user_id;
									 $logs->UserType    = ""; 
									 $logs->AddLog();
									
									 // display message: unable to update City Master
									 response($resArr, 401);
									
								} // Insert ID Check
							
						} else {
							
							// generate jwt
							//$access_token = $auth->generateToken($tokenArr);
							
							$resArr = array(
									"status" 		=> "0", 
									"message" 		=> "City ID does not exist.",
									"access_token" 	=> $access_token
								);
								
							// Merge Array Post And GET Values
							$reqArr = array_merge($data, $getArr);		
								
							 $logs->Module 		= "City Master";
							 $logs->Action		= "update";
							 $logs->Request		= json_encode($reqArr);
							 $logs->Response	= json_encode($resArr);
							 $logs->UserId		= (int)$user_id;
							 $logs->UserType    = "";  
							 $logs->AddLog();	
								
							response($resArr, 200);
							
						}
							
					}
				
				} else {
					
					// display message: unable to update City Master
					$resArr = array("status" => "0", "message" => $data["requestData"]);
					
					// Merge Array Post And GET Values
					$reqArr = array_merge($data, $getArr);
					
					 $logs->Module 		= "City Master";
					 $logs->Action		= "update";
					 $logs->Request		= json_encode($reqArr);
					 $logs->Response	= json_encode($resArr);
					 $logs->UserId		= (int)$user_id;
					 $logs->UserType    = ""; 
					 $logs->AddLog();
					 
					response($resArr, 400);
				}		
				
				
			} else {
				
				// show error message
				$resArr = array(
					"status" 	=> "0", 
					"message" 	=> "Invalid Authorization. Error : ". $decoded["TokenData"]
				);
				
				// Merge Array Post And GET Values
				$reqArr = array_merge($data, $getArr);
				
				//Add log data
				 $logs->Module 		= "City Master";
				 $logs->Action		= "update";
				 $logs->Request		= json_encode($reqArr);
				 $logs->Response	= json_encode($resArr);
				 $logs->UserId		= "";
				 $logs->UserType    = "";	
				 $logs->AddLog();
				
				response($resArr, 401);
			}			
		
		}  else { // show error message if jwt is empty
		 
			// tell the user access denied
			$resArr = array("status" => "0", "message" => "Access denied.");
			
			//Add log data
			 $logs->Module 		= "City Master";
			 $logs->Action		= "update";
			 $logs->Request		= "";
			 $logs->Response	= json_encode($resArr);
			 $logs->UserId		= "";
			 $logs->UserType	= "";			 
			 $logs->AddLog();
			
			// display message: unable to update act master
			response($resArr, 401);
		}
		
		break;	
		
	case 'GET':
	
		$bearerToken = getBearerToken();
		
		$access_token = isset($bearerToken) ? $bearerToken : "";
		
		// if jwt is not empty
		if($access_token){
			
			// decode jwt		
			$decoded  = $auth->validateToken($access_token);	
			
			$getArr = array();
			
			 // if decode succeed, show user details
			if($decoded["status"] == true) {
				
				$user_id 		= $decoded['TokenData']->data->user_id; // Decode ID
				$username 		= $decoded['TokenData']->data->username; // Username
				$role_id 		= $decoded['TokenData']->data->role_id; // Role ID
				$tokenArr 		= array("user_id" => (int)$user_id, 
										"username" => $username, 
										"role_id" => (int)$role_id
										);			
			
						$getArr = get_url_data();					
						
						$id 	= isset($getArr['id']) ? intval($getArr['id']) : 0;
						$type 	= isset($getArr['type']) ? $getArr['type'] : "";
						
						$getList = false;
						
						if($id > 0 && $type == 'cities'){					
					
							$getList = $city->getlisting($id, $type);
							
						} else {
						
								if($id == ""){ 
								
									$getList = $city->getlisting();
								}
								
								if($id > 0){
									
									$city_exist = $city->city_exist($id);
									
									if($city_exist == true){
										
										$getList = $city->getlisting($id);
										
									} else {
										
										// generate jwt
										//$access_token = $auth->generateToken($tokenArr);
										
										$resArr = array(
												"status" 		=> "0", 
												"message" 		=> "City ID does not exist.",
												"access_token" 	=> $access_token
											);
											
										 $logs->Module 		= "City Master";
										 $logs->Action		= "get";
										 $logs->Request		= json_encode($getArr);
										 $logs->Response	= json_encode($resArr);
										 $logs->UserId		= (int)$user_id;
										 $logs->UserType    = "";  
										 $logs->AddLog();	
											
										response($resArr, 200);
									}
									
								}
							}
						
						if($getList){
							
							// generate jwt
							//$access_token = $auth->generateToken($tokenArr);
							
							// set response code
							$resArr = array(
										"status" 		=> "1", 
										"message" 		=> "City data get successfully.",
										"access_token" 	=> $access_token,
										"data" 			=> $getList
									);
								
									
								//Add log data
								 $logs->Module 		= "City Master";
								 $logs->Action		= "get";
								 $logs->Request		= json_encode($getArr);
								 $logs->Response	= json_encode($resArr);
								 $logs->UserId		= (int)$user_id;
								 $logs->UserType    = ""; 
								 $logs->AddLog();
									
								response($resArr, 200);
								
						} else { // message if unable to create City Master

							$getList = array();	
								
							// generate jwt
							//$access_token = $auth->generateToken($tokenArr);
							
							$resArr = array(
										"status" 		=> "0", 
										"message" 		=> "No data found.",
										"access_token" 	=> $access_token,
										"data"			=> $getList
									);
										
							//Add log data
							 $logs->Module 		= "City Master";
							 $logs->Action		= "get";
							 $logs->Request		= json_encode($getArr);
							 $logs->Response	= json_encode($resArr);
							 $logs->UserId		= (int)$user_id;
							 $logs->UserType    = ""; 					 
							 $logs->AddLog();	
									
							response($resArr, 200);
							
						}
				
				
				} else {			 
				
				// show error message
				$resArr =array(
					"status" 	=> "0", 
					"message" 	=> "Invalid Authorization. Error : ". $decoded["TokenData"]
				);
				
				//Add log data
				 $logs->Module 		= "City Master";
				 $logs->Action		= "get";
				 $logs->Request		= "";
				 $logs->Response	= json_encode($resArr);
				 $logs->UserId		= "";
				 $logs->UserType	= "";	
				 $logs->AddLog();
				
				response($resArr, 401);
			}			
				
				
		}  else { // show error message if jwt is empty		 
			
			$resArr = array("status" => "0", "message" => "Access denied.");
			
			//Add log data
			 $logs->Module 		= "City Master";
			 $logs->Action		= "get";
			 $logs->Request		= "";
			 $logs->Response	= json_encode($resArr);
			 $logs->UserId		= "";
			 $logs->UserType	= "";	
			 $logs->AddLog();
			
			response($resArr, 401);
		}	
		
		break;
		
		
	case 'DELETE':

		$bearerToken = getBearerToken();
		
		$access_token = isset($bearerToken) ? $bearerToken : "";
		
		// if jwt is not empty
		if($access_token)
		{
			
			// decode jwt
			$decoded = $auth->validateToken($access_token);
	
			$getArr = array();
			
			// if decode succeed, show user details
			if($decoded["status"] == true) {
					
				$user_id 		= $decoded['TokenData']->data->user_id; // Decode ID
				$username 		= $decoded['TokenData']->data->username; // Username
				$role_id 		= $decoded['TokenData']->data->role_id; // Role ID
				$tokenArr 		= array("user_id" => (int)$user_id, 
										"username" => $username, 
										"role_id" => (int)$role_id
										);		
					
					$getArr = get_url_data();
								
					// set city property values	
					$city->id = isset($getArr['id']) ? intval($getArr['id']) : "";
					
					$error = array();
					
					if($city->id == ""){
									
						$error[] = "City ID is required.";
						
					} 
					
					if(!is_numeric($city->id)){
						
						$error[] = "City ID is must contain number.";
						
					}
					
					if(count($error) > 0){
							
						$errorMsg = implode(", ", $error);
							 
						// tell the user login failed
						$resArr = array("status" => "0", "message" => $errorMsg);						

						//Add log data
						 $logs->Module 		= "City Master";
						 $logs->Action		= "delete";
						 $logs->Request		= json_encode($getArr);
						 $logs->Response	= json_encode($resArr);
						 $logs->UserId		= (int)$user_id;
						 $logs->UserType    = ""; 
						 $logs->AddLog();

						response($resArr, 200);
					 
				 } else {
					 
					  $city_exist = $city->city_exist($city->id);
							
						if($city_exist == true){					
							
							$deleteCity = false;
							 
							$deleteCity = $city->delete();
											
							if($deleteCity == true){

								// generate jwt
								//$access_token = $auth->generateToken($tokenArr);	
									 
								$resArr = array(
												"status" 		=> "1", 
												"message" 		=> "City ID was successfully deleted.",
												"access_token" 	=> $access_token
											);			
								
								//Add log data
								 $logs->Module 		= "City Master";
								 $logs->Action		= "delete";
								 $logs->Request		= json_encode($getArr);
								 $logs->Response	= json_encode($resArr);
								 $logs->UserId		= (int)$user_id;
								 $logs->UserType    = ""; 
								 $logs->AddLog();
					
								response($resArr, 200);
								
							 } else {
								 
								 // generate jwt
								 //$access_token = $auth->generateToken($tokenArr);
								 
								 $resArr = array(
												"status" 		=> "1", 
												"message" 		=> "Unable to delete City.",
												"access_token" 	=> $access_token
											);
											
								//Add log data
								 $logs->Module 		= "City Master";
								 $logs->Action		= "delete";
								 $logs->Request		= json_encode($getArr);
								 $logs->Response	= json_encode($resArr);
								 $logs->UserId		= (int)$user_id;
								 $logs->UserType    = ""; 
								 $logs->AddLog();		
									 
								response($resArr, 401);
								 
							 }	// delete condition end	
							 
							
						} else {
									 
								// generate jwt
								//$access_token = $auth->generateToken($tokenArr);
							
								$resArr = array(
									"status" 		=> "0", 
									"message" 		=> "City ID does not exist.",
									"access_token" 	=> $access_token
								);
								
								 $logs->Module 		= "City Master";
								 $logs->Action		= "delete";
								 $logs->Request		= json_encode($getArr);
								 $logs->Response	= json_encode($resArr);
								 $logs->UserId		= (int)$user_id;
								 $logs->UserType    = "";  
								 $logs->AddLog();	
					
								response($resArr, 200);
							
						}
						
				 }
				 
			} else {
			 
				// set response code				
				$resArr = array(
							"status" 	=> "0", 
							"message" 	=> "Invalid Authorization. Error : ". $decoded["TokenData"]
						);
					
				//Add log data
				 $logs->Module 		= "City Master";
				 $logs->Action		= "delete";
				 $logs->Request		= json_encode($getArr);
				 $logs->Response	= json_encode($resArr);
				 $logs->UserId		= "";
				 $logs->UserType	= "";
				 $logs->AddLog();			
								 
				response($resArr, 401);
			}
		 
		 }  else { // show error message if jwt is empty
		 
			// tell the City access denied
			$resArr = array("status" => "0", "message" => "Access denied.");
			
			//Add log data
			 $logs->Module 		= "City Master";
			 $logs->Action		= "delete";
			 $logs->Request		= "";
			 $logs->Response	= json_encode($resArr);
			 $logs->UserId		= "";
			 $logs->UserType	= "";
			 $logs->AddLog();
						 
			response($resArr, 401);
			
		}		
	
		break;
		
	case "OPTIONS": //Handle Preflighted requests for CORS issue
		
		// set response code		
		header('Content-Type: text/plain');
		
		response("", 200);
		
		break;
		
	default:
	
		// Invalid Request Method
		$resArr = array("status" => "0", "message" => "Method Not Allowed.");						
		// display message: unable to create user
		response($resArr, 405);	
		
		break;
}

?>