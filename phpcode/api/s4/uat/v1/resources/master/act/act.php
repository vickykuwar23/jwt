<?php
/*** 'act' object ***/
class Act{
 
    /*** database connection and table name ***/
    private $conn;
    private $table_name = 'public."act_master"';
	
    /*** object properties ***/   
	public $id;
    public $act_name;
	public $is_active;
 
    /*** constructor ***/
    public function __construct($db){
        $this->conn = $db;
		$this->createdOn = date("Y-m-d H:i:s");
    }
	
	/*** check if given Act ID exist in the database ***/
	function act_exist($id){
	 
		/*** query to check if Act exists ***/
		$query = 'SELECT "act_id"
					FROM '.$this->table_name.'
					WHERE "act_id" = ? AND "is_deleted" = ?
					LIMIT 1';
					
		/*** prepare the query ***/
		$stmt = $this->conn->prepare( $query );
	 
		/*** bind given Act ID value ***/
		$stmt->bindParam(1, $id);
		$stmt->bindValue(2, false, PDO::PARAM_BOOL);
		/*** execute the query ***/
		$stmt->execute();
	 
		/*** get number of rows ***/
		$num = $stmt->rowCount();
	 
		/*** if Act ID exists, assign values to object properties for easy access and use for php sessions ***/
		if($num > 0){
	 
			/*** return true because Act ID exists in the database ***/
			return true;
		}
	 
		/*** return false if Act ID does not exist in the database ***/
		return false;
	}
	
	function getlisting($id = 0){
		
		/*** query to check result ***/
		if($id == 0){
			
			$query = 'SELECT "act_id", "act_name", "is_active", "created_on" 
					FROM '.$this->table_name.' 
					WHERE "is_deleted" = ? ORDER BY "act_id" ASC';	
					
			/*** prepare the query ***/
			$stmt = $this->conn->prepare( $query );		
		 
			$stmt->bindValue(1, false, PDO::PARAM_BOOL);
			
		} else {
			
			$query = 'SELECT "act_id", "act_name", "is_active", "created_on" 
					FROM '.$this->table_name.' 
					WHERE "is_deleted" = ? AND "act_id" = ? ORDER BY "act_id" ASC';
			
			/*** prepare the query ***/
			$stmt = $this->conn->prepare( $query );		
		 
			$stmt->bindValue(1, false, PDO::PARAM_BOOL);
			$stmt->bindValue(2, $id);
						
		}
		
		/*** execute the query ***/
		$stmt->execute();
	 
		/*** get number of rows ***/
		$num = $stmt->rowCount();
	 
		/*** if data exists, assign values to object properties for easy access and use for php sessions ***/
		if($num>0){
	 
			/*** get record details / values ***/			
			while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
					$getData[] = $row;
			}
	 
			/*** return true because data exists in the database ***/
			return $getData;
		}
	 
		/*** return false if data does not exist in the database ***/
		return false;
		
	}
	
	function create(){
		
		$query = 'INSERT INTO '.$this->table_name.' ("act_name", "is_active", "is_deleted", "created_on") VALUES (:act_name, :is_active, :is_deleted, :created_on) RETURNING act_id';		
		
		/*** prepare the query ***/
		$stmt = $this->conn->prepare($query);		
	 
		/*** sanitize ***/
		$this->is_active			=  '1';
		$this->is_deleted			=  '0';
		$this->created_on			=  $this->createdOn;
		
		$stmt->bindParam(':act_name', $this->act_name);
		$stmt->bindParam(':is_active', $this->is_active);
		$stmt->bindParam(':is_deleted', $this->is_deleted);
		$stmt->bindParam(':created_on', $this->created_on);
		
	 
		/*** execute the query, also check if query was successful ***/		
		if($stmt->execute()){
			
			$result = $stmt->fetch(PDO::FETCH_ASSOC);
			
			return $result['act_id'];
			
		} 
		
		return false;
		
	}
	
	
	/*** update record ***/
	public function update($updateStatus){
		
		/*** SQL QUERY ***/
		$query = 'UPDATE ' . $this->table_name . ' SET ';
		
		if($updateStatus == "UpdateStatus"){
			
			$query .= '"is_active" = :is_active, ';
			
		} else {
			
			$query .= '"act_name" = :act_name, ';
			
		}
		
		$query .= '"updated_on" = :updated_on WHERE "act_id" = :act_id';
		
		/*** prepare the query ***/
		$stmt = $this->conn->prepare($query);
		
		$this->updated_on			=  $this->createdOn;
				 
		/*** bind the values from the form ***/	
		if($this->act_name!=""){
			
			$stmt->bindParam(':act_name', $this->act_name);
		} 
		
		if($this->is_active!=""){
			
			$stmt->bindParam(':is_active', $this->is_active);
		} 
		
		$stmt->bindParam(':updated_on', $this->updated_on);		
		
		/*** unique ID of record to be edited ***/
		$stmt->bindParam(':act_id', $this->id);
	 
		/*** execute the query ***/
		if($stmt->execute()){
			
			return true;
		}
	 
		return false;
		
	} // Function End
	
	
	function delete(){
		
		$query = 'UPDATE ' . $this->table_name . '
					  SET
						"deleted_on"	 	= :deleted_on,
						"is_deleted"	= :is_deleted
					  WHERE "act_id" 	= :act_id';
					  
		$stmt = $this->conn->prepare($query);	
		
		$this->is_deleted	=	'1';
		$this->deleted_on  	=	date("Y-m-d H:i:s");
		
		$stmt->bindParam(':is_deleted', $this->is_deleted);
		$stmt->bindParam(':deleted_on', $this->deleted_on);
		
		$stmt->bindParam(':act_id', $this->id);
				
		if($stmt->execute()){
			
			
			return true;
		}
	 
		return false;
			
	}
	
}