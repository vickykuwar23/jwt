<?php
/*** files needed to connect to database and class***/
define("ROOT_PATH", realpath(__DIR__ . '/../../..'));
include_once ROOT_PATH.'/constant.php';

include_once CONFIG_PATH.'functions.php';
include_once CONFIG_PATH.'database.php';
include_once RESOURCE_PATH.'user/user.php';
include_once RESOURCE_PATH.'logs/logs.php';
include_once RESOURCE_PATH.'auth/auth.php';

// Required headers
requireHeaders();
 
/*** get database connection ***/
$database 	= new Database();
$db 		= $database->getConnection();
$mdb 		= $database->getMongoConnection();
 
// instantiate product object
$user  = new User($db);
$logs  = new Logs($mdb);
$auth  = new Auth();

// Get Request Method 
$request_method		= get_request_method();

switch($request_method)
{
	case 'POST':
		
		// get posted data
		$data 	= get_request_body();
		
		if($data['status'] == true){

		// set user property values
		$user->FirstName = isset($data['requestData']['FirstName']) ? $data['requestData']['FirstName'] : "";
		$user->LastName  = isset($data['requestData']['LastName']) ? $data['requestData']['LastName'] : "";
		$user->Password  = isset($data['requestData']['Password']) ? $data['requestData']['Password'] : "";	
		$user->EmailAddress = isset($data['requestData']['EmailAddress']) ? $data['requestData']['EmailAddress'] : "";	
			
		$error = array();
		
			if($user->FirstName == ""){
				
				$error[] = "FirstName is required.";
				
			} 
			if($user->LastName == ""){
				
				$error[]= "LastName is required.";
				
			} 
			if($user->EmailAddress == ""){
				
				$error[]= "EmailAddress is required.";
				
			} else if(!filter_var($user->EmailAddress, FILTER_VALIDATE_EMAIL)){
				
				$error[]= "Please enter valid EmailAddress.";
				
			}
			if($user->Password == ""){
				
				$error[] = "Password is required.";
				
			}		
					
			if(count($error) > 0){
				
					$errorMsg = implode(", ", $error);
					
					// tell the user login failed
					$resArr = array("status" => "0", "message" => $errorMsg);
					
					 //Add log data
					 $logs->Module 		= "User";
					 $logs->Action		= "create";
					 $logs->Request		= json_encode($data);
					 $logs->Response	= json_encode($resArr);
					 $logs->UserId		= "";
					 $logs->UserType	= "User";
					 $logs->AddLog();
						 
					// tell the user login failed
					 response($resArr, 400);
				
			} else {				
				
				// set user property values
				$email_exists 		= 	$user->emailExists();
			
				if(!$email_exists){	// Email Check if not exist success
					
					// Add user
						if($user->create()){
							
							// display message: user was created
							$resArr = array("status" => "1", "message" => "User created successfully.");
							
							//Add log data
							 $logs->Module 		= "User";
							 $logs->Action		= "create";
							 $logs->Request		= json_encode($data);
							 $logs->Response	= json_encode($resArr);
							 $logs->UserId		= "";
							 $logs->UserType	= "User";
							 $logs->AddLog();
							
							// display message: user was created
							 response($resArr, 200);
							
						} else { // message if unable to create user
						
							// display message: unable to create user
							$resArr = array("status" => "0", "message" => "Unable to create user.");
							
							//Add log data
							 $logs->Module 		= "User";
							 $logs->Action		= "create";
							 $logs->Request		= json_encode($data);
							 $logs->Response	= json_encode($resArr);
							 $logs->UserId		= "";
							 $logs->UserType	= "User";
							 $logs->AddLog();
							
							 // display message: unable to create user
							 response($resArr, 400);
							
						}
					
				} else { // Email Already exist error
			 
						// tell the user login failed
						$resArr = array("status" => "0", "message" => "User already exist.");
						 
						 //Add log data	
						 $logs->Module 		= "User";
						 $logs->Action		= "create";
						 $logs->Request		= json_encode($data);
						 $logs->Response	= json_encode($resArr);
						 $logs->UserId		= "";
						 $logs->UserType	= "User";
						 $logs->AddLog();
						 
						 // tell the user login failed
						 response($resArr, 401);
				}	
				
			}
			
		} else {
			
			// display message: unable to create user
			$resArr = array("status" => "0", "message" => $data["requestData"]);
			
			//Add log data	
			 $logs->Module 		= "User";
			 $logs->Action		= "create";
			 $logs->Request		= "";
			 $logs->Response	= json_encode($resArr);
			 $logs->UserId		= "";
			 $logs->UserType	= "";
			 $logs->AddLog();
				
			response($resArr, 400);
			
		}
		
		break;
		
	case 'PUT':		
		
		$bearerToken = getBearerToken();
		
		$access_token = isset($bearerToken) ? $bearerToken : "";
		
		// if jwt is not empty
		if($access_token){			
			
			// decode jwt
			$decoded = $auth->validateToken($access_token);

			// if decode succeed, show user details			
			if($decoded['status'] == true){	

				$updateUser 	= false;
				$userID 		= $decoded['TokenData']->data->UserId; // Decode ID
				$UserName 		= $decoded['TokenData']->data->UserName; // Decode UserName
				$is_admin 		= isset($decoded['TokenData']->data->admin) ? $decoded['TokenData']->data->admin : false;
				
				if($is_admin == true){ //If admin user	
							
					$logs->UserType = "Admin";
					
				} else { // Else regular user
					
					$logs->UserType = "User";
					
				}
				
				// get posted data
				$data = get_request_body();
				
				if($data["status"] == true){
					
					$error = array();
					
					// set user property values				
					$user->FirstName 		= isset($data['requestData']['FirstName']) ? $data['requestData']['FirstName'] : "";
					$user->LastName 		= isset($data['requestData']['LastName']) ? $data['requestData']['LastName'] : "";
					$user->Password 		= isset($data['requestData']['Password']) ? $data['requestData']['Password'] : "";		
					$user->IsActive 		= isset($data['requestData']['IsActive']) ? $data['requestData']['IsActive'] : "";
					$user->imgName 			= isset($data['requestData']['imgName']) ? $data['requestData']['imgName'] : "";
					$user->ProfileImage 	= isset($data['requestData']['ProfileImage']) ? $data['requestData']['ProfileImage'] : "";
					$user->AttachFilename 	= isset($data['requestData']['AttachFilename']) ? $data['requestData']['AttachFilename'] : "";
					$user->Attachment	= isset($data['requestData']['Attachment']) ? $data['requestData']['Attachment'] : "";
					
					$getArr = get_url_data();
					
					//Get ID From URL		
					$user->UserId = isset($getArr['id']) ? intval($getArr['id']) : 0;
					
					
					if($user->UserId == ""){
								
						$error[] = "UserId is required.";
						
					}
					
					$updateStatus = "";
					
					if(isset($data['requestData']['IsActive'])){
							
							$updateStatus 		= "UpdateStatus";
							$user->IsActive 	= $data['requestData']['IsActive'];
							
							if($user->IsActive == ""){
						
									$error[] = "IsActive is required.";
									
							} 
						
					} else {
							
							$updateStatus = "UpdateProfile";
							
							if($user->FirstName == ""){
						
								$error[] = "FirstName is required.";
						
							} 
							if($user->LastName == ""){
								
								$error[]= "LastName is required.";
								
							} 
					}
					
					// File Attachment/Upload Code
					if($user->Attachment!= "" && $user->AttachFilename!=""){						
									
						$AttachFilename = $user->AttachFilename;
						$file_ext 	= explode('.',$AttachFilename);
						$extension 	= strtolower( end($file_ext));						
						//$extension  = $file_ext[1];
						$allowedExts = array("pdf", "docx");

						// for create physical file.
						$encoded_file = $user->Attachment;
						
						$replaceStr  = str_replace(" ", "+", $encoded_file);
						
						$decoded_file1 = base64_decode($replaceStr);
						
						// Min size for resume size: 1 KB
						// Max size for resume size: upto 2 MB	
						
						$size = strlen($decoded_file1);
						$size_kb = floor($size / 1024);	
						
						if(($extension != "pdf" || $extension!= "PDF") && ($extension != "docx" || $extension != "DOCX") && !in_array($extension, $allowedExts)){
							
							$error[] = "Please attch file with .pdf, .docx format.";
							
						} else if($size_kb < ATTACHMENT_MIN_SIZE){
							
							$error[] = "Upload document size is much more than 1MB.";
							
						} else if($size_kb > ATTACHMENT_MAX_SIZE){
							
							$error[] = "Upload document size is less than equal to 2MB.";
							
						} else {
							
							//file_put_contents(UPLOAD_FILE.$file_ext[0].'.'.$file_ext[1], $decoded_file1);
							
							$filePath = UPLOAD_FILE.$AttachFilename;
							
							 $fileResponse = convertBase64ToFile($filePath, $decoded_file1);
							
							if($fileResponse == false){
								
								$error[] = "Attachement not uploaded.";
								
							}
							
							/*$pdf = fopen($filePath, 'w');

							if($pdf){
								
								 $content = fwrite($pdf, $decoded_file1);
								 
								 if(!$content){
									 
									 $error[] = "Attachement not uploaded.";
									 
								 } 								 
								
							} else {
								
								$error[] = "Attachement not uploaded.";
							}
							
							  fclose ($pdf);*/
						}	

												
					} // Attachment end
					
					// Profile Upload Code
					if($user->imgName!= "" && $user->ProfileImage!=""){
						
						$allowedExts = array("jpeg", "jpg", "png");
						$imgName 	 = $user->imgName;
						$img_ext 	 = explode('.', $imgName );
						$extension   = strtolower( end($img_ext));
						
						// for create physical file.
						$encoded_file = $user->ProfileImage;
						
						$encoded_file = str_replace('data:image/png;base64,', '', $encoded_file);
						$encoded_file = str_replace(" ", "+", $encoded_file);
						$encoded_file = str_replace('data:image/jpeg;base64,', '', $encoded_file);
						$encoded_file = str_replace('data:image/gif;base64,', '', $encoded_file);	

					   // Min size for Image size : 5 KB ,Max size for Image size : 500 					
						$decoded_file1 = base64_decode($encoded_file);
						
						$size = strlen($decoded_file1);
						$size_kb = floor($size / 1024);
							
					if(($extension != "jpg" || $extension!= "JPG") && ($extension != "jpeg" || $extension != "JPEG") && ($extension != "png" || $extension != "PNG") && !in_array($extension, $allowedExts)){		
							
							$error[] = "Please upload file with .jpg, .jpeg, .png format.";
							
						} else if($size_kb < PROFILE_MIN_SIZE){
							
							$error[] = "Upload Image size is more than 5kb.";
							
						} else if($size_kb > PROFILE_MAX_SIZE){
							
							$error[] = "Upload Image size is less than equal to 500kb.";
							
						} else {
						
							//Now you can copy the uploaded file to your server.					
							//file_put_contents(UPLOAD_IMG.$img_ext[0].'.'.$img_ext[1], $decoded_file1);
						
							// Image Destination 
							$path 	= UPLOAD_IMG;
							
							$imageResponse = convertBase64ToImage($path, $imgName, $decoded_file1);
							
							if($imageResponse == false){
								
								$error[] = "Profile Image not uploaded.";
								
							}
							
						} // extension end
						
					} // profile Image end	
							
				
					if(count($error) > 0){
				
							$errorMsg = implode(", ", $error);
							
							// tell the user login failed
							$resArr = array("status" => "0", "message" => $errorMsg);
							
							// Merge Array Post And GET Values
							$reqArr = array_merge($data, $getArr);	
							 
							 //Add log data
							 $logs->Module 		= "User";
							 $logs->Action		= "update";
							 $logs->Request		= json_encode($reqArr);
							 $logs->Response	= json_encode($resArr);
							 $logs->UserId		= (int)$userID;
							 $logs->AddLog();
							 
							response($resArr, 400);
							
					} else {
						
						if($is_admin == true || ($userID == $user->UserId)){ //If admin user
							
							
							$updateUser = $user->update($updateStatus);
							
						/*elseif($userID == $user->UserId): // Else regular user
							$userList = $user->getAllUser($id);*/
							
						}
						
						// create the product
						if($updateUser){
							
							if($is_admin == true){ //If admin user						
										
								$tokenArr = array("UserId" => (int)$userID, "UserName" => $UserName, "admin" => true);
								
							} else { // Else regular user							
										
								$tokenArr = array("UserId" => (int)$userID, "UserName" => $UserName);
								
							}
							
							// generate jwt
							$access_token = $auth->generateToken($tokenArr);
							 
							// response in json format
							$resArr = array(
											"status" 		=> "1", 
											"message" 		=> "User was successfully updated.",
											"access_token" 	=> $access_token
										);		
								
							// Merge Array Post And GET Values
							$reqArr = array_merge($data, $getArr);		
										
								//Add log data
								 $logs->Module 		= "User";
								 $logs->Action		= "update";
								 $logs->Request		= json_encode($reqArr);
								 $logs->Response	= json_encode($resArr);
								 $logs->UserId		= (int)$userID;						 
								 $logs->AddLog();		
							 
							response($resArr, 200);
						}
						 
						// message if unable to update user
						else{
						 
							// show error message
							$resArr = array("status" => "0", "message" => "Unable to update user.");
							 
							 // Merge Array Post And GET Values
							$reqArr = array_merge($data, $getArr);
							 
							 //Add log data
							 $logs->Module 		= "User";
							 $logs->Action		= "update";
							 $logs->Request		= json_encode($reqArr);
							 $logs->Response	= json_encode($resArr);
							 $logs->UserId		= (int)$userID;						 
							 $logs->AddLog();
							 
							response($resArr, 401);
						}
						
					}
					
				} else {
					
					// show error message
					$resArr = array("status" => "0", "message" => $data["requestData"]);
				
					// Merge Array Post And GET Values
					$reqArr = array_merge($data, $getArr);	
				
					//Add log data
					 $logs->Module 		= "User";
					 $logs->Action		= "update";
					 $logs->Request		= json_encode($reqArr);
					 $logs->Response	= json_encode($resArr);
					 $logs->UserId		= "";
					 $logs->UserType    = ""; 	
					 $logs->AddLog();
				
					response($resArr, 400);
					
				}				
				
			} else {
				
				// show error message
				$resArr = array(
					"status" 	=> "0", 
					"message" 	=> "Invalid Authorization. Error : ". $decoded["TokenData"]
				);
				
				// Merge Array Post And GET Values
				$reqArr = array_merge($data, $getArr);
				
				//Add log data
				 $logs->Module 		= "User";
				 $logs->Action		= "update";
				 $logs->Request		= json_encode($reqArr);
				 $logs->Response	= json_encode($resArr);
				 $logs->UserId		= "";
				 $logs->UserType    = "";	
				 $logs->AddLog();
				
				response($resArr, 401);
			}
			
		}  else { // show error message if jwt is empty
		 
			// tell the user access denied
			$resArr = array("status" => "0", "message" => "Access denied.");
			
			//Add log data
			 $logs->Module 		= "User";
			 $logs->Action		= "update";
			 $logs->Request		= "";
			 $logs->Response	= json_encode($resArr);
			 $logs->UserId		= "";
			 $logs->UserType	= "";			 
			 $logs->AddLog();
			
			// display message: unable to create user
			response($resArr, 400);
		}		
	
		break;
		
	case 'GET':	
	
		$bearerToken = getBearerToken();
		
		$access_token = isset($bearerToken) ? $bearerToken : "";
		
		  /*$userList = $user->get("183");
			
			$attachment = $userList[0]['Attachment'];
		
			// Attachment Destination 
			$path 	= UPLOAD_FILE.$attachment;
			
			$fileType = explode(".", $attachment);
			//print_r($fileType);exit;
			
			$attachFile = file_get_contents($path);
			
			$finfo = finfo_open();
			$mime_type = finfo_buffer($finfo, $attachFile, FILEINFO_MIME_TYPE);
			header("Content-Type: " . $mime_type);			
			header('Content-Disposition: attachment; filename="'.$attachment.'"');	// force file download
			
			// alternatively specify an URL, if PHP settings allow
			echo $attachFile;exit;*/
		
		
		// if jwt is not empty
		if($access_token){

			// decode jwt		
			$decoded  = $auth->validateToken($access_token);
			
			// if decode succeed, show user details
			if($decoded["status"] == true) {
				
				$userID   = $decoded["TokenData"]->data->UserId;
				$UserName = $decoded["TokenData"]->data->UserName;
				$is_admin = isset($decoded["TokenData"]->data->admin) ? $decoded["TokenData"]->data->admin : false;
				
				$getArr = get_url_data();					
				
				$id 	= isset($getArr['id']) ? intval($getArr['id']) : 0;
				$type   = isset($getArr['type']) ? $getArr['type'] : "";
				
				if($id > 0 && $type == "profileimg")
				{
					
					$userList = $user->get($id);
								
					$profileImg = $userList[0]['ProfileImage'];
					
					if($profileImg!= ""){
						
						// Image Destination 
						$path 	= UPLOAD_IMG.$profileImg;
						
						$imgType = explode(".", $profileImg);
						
						header("content-type: image/".$imgType[1]);
						
						// alternatively specify an URL, if PHP settings allow
						echo $profilePic = file_get_contents($path);exit;
						
					} else {
						
						$resArr = array(
								"status" 		=> "1", 
								"message" 		=> "No Profile Picture Found."
							);
						response($resArr, 200);
					}
					
					
				
				} else if($id > 0 && $type == "attachment"){ 
				
					
					$userList = $user->get($id);
					
					$attachment = $userList[0]['Attachment'];
					
					if($attachment!= ""){
						
						// Attachment Destination 
						$path 	= UPLOAD_FILE.$attachment;
						
						$fileType = explode(".", $attachment);	
						
						$attachFile = file_get_contents($path);
				
						$finfo = finfo_open();
						$mime_type = finfo_buffer($finfo, $attachFile, FILEINFO_MIME_TYPE);
						header("Content-Type: " . $mime_type);			
						header('Content-Disposition: attachment; filename="'.$attachment.'"');	// force file download
						
						// alternatively specify an URL, if PHP settings allow
						echo $attachFile;exit;
						
					} else {
						
						$resArr = array(
								"status" 		=> "1", 
								"message" 		=> "No Attachement Found."
							);
							
						response($resArr, 200);
					}
					
					
				
				}
				

				if($is_admin == true){ //If admin user	
									
					$logs->UserType = "Admin";
					
				} else { // Else regular user
					
					$logs->UserType = "User";
					
				}	
				
				$userList 	 = false;
				$userListing = false;
				
				if($is_admin == true && $id == 0){ //If admin user
				
					$userListing = $user->get();
				}
				 
				 if($id > 0){
				 
					if($is_admin == true){ //If admin user
					
						$userListing = $user->get($id);
						
					} else if($userID == $id){ // Else regular user
					
						$userListing = $user->get($id);
						
					}
					
				}				 
				 
				if($userListing){
					
					foreach($userListing as $userVal){
							
						$profileImage = $userVal['ProfileImage'];
						$attachFile   = $userVal['Attachment'];
						
						if($profileImage != ""){
							$userVal['ProfileImage'] = "/users/".$userVal['UserId']."/profileimg";
						}
						
						if($attachFile != ""){
							$userVal['Attachment'] = "/users/".$userVal['UserId']."/attachment";
						}
						
						$userList[] = $userVal;
							
					}
					
				}				
					
				 
				if($userList){					
					
					if($is_admin == true){ //If admin user
					
						$tokenArr = array("UserId" => (int)$userID, "UserName" => $UserName, "admin" => true);
						  
					} else {// Else regular user
					
						$tokenArr = array("UserId" => (int)$userID, "UserName" => $UserName);
						
					}
					
					// generate jwt
					$access_token = $auth->generateToken($tokenArr);
					
					// set response code
					$resArr = array(
								"status" 		=> "1", 
								"message" 		=> "User data found.",
								"access_token" 	=> $access_token,
								"data" 			=> $userList
							);
						
							
						//Add log data
						 $logs->Module 		= "User";
						 $logs->Action		= "get";
						 $logs->Request		= json_encode($getArr);
						 $logs->Response	= json_encode($resArr);
						 $logs->UserId		= (int)$userID;
						 $logs->AddLog();
							
						response($resArr, 200);
						
				} else { // message if unable to create user				 
										
					$resArr = array(
								"status" 		=> "1", 
								"message" 		=> "No data found."
							);
								
					//Add log data
					 $logs->Module 		= "User";
					 $logs->Action		= "get";
					 $logs->Request		= json_encode($getArr);
					 $logs->Response	= json_encode($resArr);
					 $logs->UserId		= (int)$userID;	
					 $logs->AddLog();	
							
					response($resArr, 200);
					
				}	
				
			} else {			 
				
				// show error message
				$resArr =array(
					"status" 	=> "0", 
					"message" 	=> "Invalid Authorization. Error : ". $decoded["TokenData"]
				);
				
				//Add log data
				 $logs->Module 		= "User";
				 $logs->Action		= "get";
				 $logs->Request		= "";
				 $logs->Response	= json_encode($resArr);
				 $logs->UserId		= "";
				 $logs->UserType	= "";	
				 $logs->AddLog();
				
				response($resArr, 401);
			}
			
		}  else { // show error message if jwt is empty		 
			
			$resArr = array("status" => "0", "message" => "Access denied.");
			
			//Add log data
			 $logs->Module 		= "User";
			 $logs->Action		= "get";
			 $logs->Request		= "";
			 $logs->Response	= json_encode($resArr);
			 $logs->UserId		= "";
			 $logs->UserType	= "";	
			 $logs->AddLog();
			
			response($resArr, 401);
		}
		
		break;
	
	case 'DELETE':

		$bearerToken = getBearerToken();
		
		$access_token = isset($bearerToken) ? $bearerToken : "";	
		
		// if jwt is not empty
		if($access_token){
			
			// decode jwt
			$decoded = $auth->validateToken($access_token);
			
			// if decode succeed, show user details
			if($decoded["status"] == true) {
				
			// Token data decode	
			$userID 		= $decoded["TokenData"]->data->UserId; // Decode ID
			$UserName		= $decoded["TokenData"]->data->UserName; // Decode UserName
			$is_admin 		= isset($decoded["TokenData"]->data->admin) ? $decoded["TokenData"]->data->admin : false;
			
					$getArr = get_url_data();
				
					// set user property values	
					$user->UserId = isset($getArr['id']) ? intval($getArr['id']) : 0;
					
					$error = array();
					
					if($user->UserId == ""){
						
						$error[] = "UserId is required.";					
						
					} 
					
					if(count($error) > 0){
				
						$errorMsg = implode(", ", $error);
							 
						// tell the user login failed
						$resArr = array("status" => "0", "message" => $errorMsg);						
				
						//Add log data
						 $logs->Module 		= "User";
						 $logs->Action		= "delete";
						 $logs->Request		= json_encode($getArr);
						 $logs->Response	= json_encode($resArr);
						 $logs->UserId		= (int)$userID;
						 $logs->UserType	= "Admin";
						 $logs->AddLog();
				
						response($resArr, 200);
						 
					 } else {
						 
						 $deleteUser 	= false;
						 
						  $deleteUser = $user->delete($userID);
						  
						 if($deleteUser > 0){
							 
							 $tokenArr = array("UserId" => (int)$userID, "UserName" => $UserName, "admin" => true);
							// generate jwt
							$access_token = $auth->generateToken($tokenArr);
								 
							$resArr = array(
											"status" 		=> "1", 
											"message" 		=> "User was successfully deleted.",
											"access_token" 	=> $access_token
										);			
							
							//Add log data
							 $logs->Module 		= "User";
							 $logs->Action		= "delete";
							 $logs->Request		= json_encode($getArr);
							 $logs->Response	= json_encode($resArr);
							 $logs->UserId		= (int)$userID;
							 $logs->UserType	= "Admin";
							 $logs->AddLog();
				
							response($resArr, 200);
							
						 } else {
							 
							// response in json format
							$resArr = array(
											"status" 		=> "0", 
											"message" 		=> "Unable to delete user."
										);
										
							//Add log data
							 $logs->Module 		= "User";
							 $logs->Action		= "delete";
							 $logs->Request		= json_encode($getArr);
							 $logs->Response	= json_encode($resArr);
							 $logs->UserId		= (int)$userID;
							 $logs->UserType	= "Admin";
							 $logs->AddLog();		
								 
							response($resArr, 401);
								
						 }
						 
					 }
				
			} else {
			 
				// set response code				
				$resArr = array(
							"status" 	=> "0", 
							"message" 	=> "Invalid Authorization. Error : ". $decoded["TokenData"]
						);
					
				//Add log data
				 $logs->Module 		= "User";
				 $logs->Action		= "delete";
				 $logs->Request		= json_encode($getArr);
				 $logs->Response	= json_encode($resArr);
				 $logs->UserId		= "";
				 $logs->UserType	= "Admin";
				 $logs->AddLog();			
								 
				response($resArr, 401);
			}
		}
		 
		// show error message if jwt is empty
		else{
		 
			// tell the user access denied
			$resArr = array("status" => "0", "message" => "Access denied.");
			
			//Add log data
			 $logs->Module 		= "User";
			 $logs->Action		= "delete";
			 $logs->Request		= "";
			 $logs->Response	= json_encode($resArr);
			 $logs->UserId		= "";
			 $logs->UserType	= "";
			 $logs->AddLog();
						 
			response($resArr, 401);
			
		}		
	
		break;
		
	case "OPTIONS": //Handle Preflighted requests for CORS issue
		
		// set response code		
		header('Content-Type: text/plain');
		
		response("", 200);
		
		break;
		
	default:
	
		// Invalid Request Method
		$resArr = array("status" => "0", "message" => "Method Not Allowed.");						
		// display message: unable to create user
		response($resArr, 405);	
		
		break;
}


?>