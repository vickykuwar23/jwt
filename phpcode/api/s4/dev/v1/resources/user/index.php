<?php
/*** files needed to connect to database and class***/
define("ROOT_PATH", realpath(__DIR__ . '/../../..'));
include_once ROOT_PATH.'/constant.php';

include_once CONFIG_PATH.'functions.php';
include_once CONFIG_PATH.'database.php';
include_once RESOURCE_PATH.'user/user.php';
include_once RESOURCE_PATH.'logs/logs.php';
include_once RESOURCE_PATH.'auth/auth.php';

//include_once LIB_PATH.'eCos/eCos.php';

// Required headers
requireHeaders();
 
/*** get database connection ***/
$database 	= new Database();
$db 		= $database->getAuthConnection();
$mdb 		= $database->getMongoConnection();
 
// instantiate product object
$user  = new User($db);
$logs  = new Logs($mdb);
$auth  = new Auth();

// Get Request Method 
$request_method		= get_request_method();

switch($request_method)
{
	case 'POST':
		
		$bearerToken = getBearerToken();
		
		$access_token = isset($bearerToken) ? $bearerToken : "";

		// if jwt is not empty
		if($access_token){	

			// decode jwt
			$decoded = $auth->validateToken($access_token);
			
			$data 	= array();
			$getArr = array();
			$reqArr = array();

			// if decode succeed, show user details			
			if($decoded['status'] == true){	

				$user_id 		= $decoded['TokenData']->data->user_id; // Decode ID
				$username 		= $decoded['TokenData']->data->username; // Username
				$role_id 		= $decoded['TokenData']->data->role_id; // Role ID
				$tokenArr 		= array("user_id" => (int)$user_id, 
										"username" => $username, 
										"role_id" => (int)$role_id
										);		
				
						// get posted data
						$data 	= get_request_body();
						
						if($data['status'] == true){

						// set user property values
						$user->role_id = isset($data['requestData']['role_id']) ? $data['requestData']['role_id'] : "";
						$user->username  = isset($data['requestData']['username']) ? $data['requestData']['username'] : "";
						$user->password  = isset($data['requestData']['password']) ? $data['requestData']['password'] : "";	
						$user->email = isset($data['requestData']['email']) ? $data['requestData']['email'] : "";
						$user->display_name = isset($data['requestData']['display_name']) ? $data['requestData']['display_name'] : "";	
							
						$error = array();
						
							if($user->role_id == ""){
								
								$error[] = "Role ID is required.";
								
							} 
							if($user->username == ""){
								
								$error[]= "Username is required.";
								
							} 
							if($user->email == ""){
								
								$error[]= "Email Address is required.";
								
							} else if(!filter_var($user->email, FILTER_VALIDATE_EMAIL)){
								
								$error[]= "Please enter valid Email Address.";
								
							}
							if($user->password == ""){
								
								$error[] = "Password is required.";
								
							}
							if($user->display_name == ""){
								
								$error[] = "Display Name is required.";
								
							}	
									
							if(count($error) > 0){
								
									$errorMsg = implode(", ", $error);
									
									// tell the user login failed
									$resArr = array("status" => "0", "message" => $errorMsg);
									
									 //Add log data
									 $logs->Module 		= "User";
									 $logs->Action		= "create";
									 $logs->Request		= json_encode($data);
									 $logs->Response	= json_encode($resArr);
									 $logs->UserId		= (int)$user_id;
									 $logs->UserType	= "";
									 $logs->AddLog();
										 
									// tell the user login failed
									 response($resArr, 400);
								
							} else {								
								
								// set user property values
								$email_exists 		= 	$user->emailExists();
							
								if(!$email_exists){	// Email Check if not exist success
									
									// Add user
										if($user->create()){
											
											// generate jwt
										//$access_token = $auth->generateToken($tokenArr);
											
											// display message: user was created
											$resArr = array("status" => "1", 
											"message" => "User created successfully.",
											"access_token" 	=> $access_token);
											
											//Add log data
											 $logs->Module 		= "User";
											 $logs->Action		= "create";
											 $logs->Request		= json_encode($data);
											 $logs->Response	= json_encode($resArr);
											 $logs->UserId		= (int)$user_id;
											 $logs->UserType	= "";
											 $logs->AddLog();
											
											// display message: user was created
											 response($resArr, 200);
											
										} else { // message if unable to create user
										
											// generate jwt
											//$access_token = $auth->generateToken($tokenArr);
										
											// display message: unable to create user
											$resArr = array("status" => "0", 
											"message" => "Unable to create user.",
											"access_token" 	=> $access_token);
											
											//Add log data
											 $logs->Module 		= "User";
											 $logs->Action		= "create";
											 $logs->Request		= json_encode($data);
											 $logs->Response	= json_encode($resArr);
											 $logs->UserId		= (int)$user_id;
											 $logs->UserType	= "";
											 $logs->AddLog();
											
											 // display message: unable to create user
											 response($resArr, 400);
											
										}
									
								} else { // Email Already exist error
								
								// generate jwt
								//$access_token = $auth->generateToken($tokenArr);
							 
										// tell the user login failed
										$resArr = array("status" => "0", 
										"message" => "User already exist.",
										"access_token" 	=> $access_token);
										 
										 //Add log data	
										 $logs->Module 		= "User";
										 $logs->Action		= "create";
										 $logs->Request		= json_encode($data);
										 $logs->Response	= json_encode($resArr);
										 $logs->UserId		= (int)$user_id;
										 $logs->UserType	= "";
										 $logs->AddLog();
										 
										 // tell the user login failed
										 response($resArr, 401);
								}	
								
							}
							
						} else {
							
							// display message: unable to create user
							$resArr = array("status" => "0", "message" => $data["requestData"]);
							
							//Add log data	
							 $logs->Module 		= "User";
							 $logs->Action		= "create";
							 $logs->Request		= json_encode($data);
							 $logs->Response	= json_encode($resArr);
							 $logs->UserId		= (int)$user_id;
							 $logs->UserType	= "";
							 $logs->AddLog();
								
							response($resArr, 400);
							
						}
		
		} else {
				
					// show error message
					$resArr = array(
						"status" 	=> "0", 
						"message" 	=> "Invalid Authorization. Error : ". $decoded["TokenData"]
					);
					
					// Merge Array Post And GET Values
					$reqArr = array_merge($data, $getArr);
					
					//Add log data
					 $logs->Module 		= "User";
					 $logs->Action		= "create";
					 $logs->Request		= json_encode($reqArr);
					 $logs->Response	= json_encode($resArr);
					 $logs->UserId		= "";
					 $logs->UserType    = "";	
					 $logs->AddLog();
					
					response($resArr, 401);
					
			} // Invalid Authorization error
		
		
		}  else { // show error message if jwt is empty
		 
			// tell the user access denied
			$resArr = array("status" => "0", "message" => "Access denied.");
			
			 $logs->Module 		= "User";
			 $logs->Action		= "create";
			 $logs->Request		= "";
			 $logs->Response	= json_encode($resArr);
			 $logs->UserId		= "";
			 $logs->UserType	= "";
			 $logs->AddLog();
			
			// display message: unable to create user
			response($resArr, 401);
		}			
		
		break;
		
	case 'PUT':				
			
		$bearerToken = getBearerToken();
		
		$access_token = isset($bearerToken) ? $bearerToken : "";
	
		// if jwt is not empty
		if($access_token){	

			// decode jwt
			$decoded = $auth->validateToken($access_token);
			
			$data 	= array();
			$getArr = array();
			$reqArr = array();	

			if($decoded['status'] == true){	

				$user_id 		= $decoded['TokenData']->data->user_id; // Decode ID
				$username 		= $decoded['TokenData']->data->username; // Username
				$role_id 		= $decoded['TokenData']->data->role_id; // Role ID
				$tokenArr 		= array("user_id" => (int)$user_id, 
										"username" => $username, 
										"role_id" => (int)$role_id
										);		
				
						$getArr = get_url_data();
								
						//Get ID From URL		
						$user->user_id = isset($getArr['id']) ? intval($getArr['id']) : "";
							
							
						if($user->user_id == ""){
									
							$error[] = "User Id is required.";
							
						}
							
							// get posted data
							$data = get_request_body();
							
							//print_r($data);exit;
							
							if($data["status"] == true){
								
							$error = array();
								
							// set user property values
							$user->role_id 		= isset($data['requestData']['role_id']) ? $data['requestData']['role_id'] : "";							
							$user->email 		= isset($data['requestData']['email']) ? $data['requestData']['email'] : "";
							$user->phonenumber 	= isset($data['requestData']['phonenumber']) ? $data['requestData']['phonenumber'] : "";	
							$user->firstname 	= isset($data['requestData']['firstname']) ? $data['requestData']['firstname'] : "";
							$user->is_active 	= isset($data['requestData']['is_active']) ? $data['requestData']['is_active'] : "";	
								
								
								$updateStatus = "";
								
								if(isset($data['requestData']['is_active'])){
										
										$updateStatus 		= "UpdateStatus";
										$UpdateMsg			= "User profile status successfully updated.";
										$user->is_active 	= $data['requestData']['is_active'];
										
										if($user->is_active == ""){
									
												$error[] = "Is Active is required.";
												
										} 
									
								} else {
										
										$updateStatus = "UpdateProfile";
										$UpdateMsg		= "User profile successfully updated.";
										
										if($user->role_id == ""){
											
											$error[] = "Role ID is required.";
											
										}
										/*if($user->email == ""){
											
											$error[] = "Email is required.";
											
										} else if(!filter_var($user->email, FILTER_VALIDATE_EMAIL)) {
											
											$error[] = $user->email. " is not a valid email address.";
											
										}
										if($user->phonenumber == ""){
											
											$error[] = "Phone Number is required.";
											
										} else if(!preg_match('/^[0-9]{10}+$/', $user->phonenumber)){
											
											$error[] = "Phone Number is Invalid.";
											
										}*/
										if($user->firstname == ""){
											
											$error[] = "First Name is required.";
											
										}
								}	
							
								if(count($error) > 0){
							
										$errorMsg = implode(", ", $error);
										
										// tell the user login failed
										$resArr = array("status" => "0", "message" => $errorMsg);
										
										// Merge Array Post And GET Values
										$reqArr = array_merge($data, $getArr);	
										 
										 //Add log data
										 $logs->Module 		= "User";
										 $logs->Action		= "update";
										 $logs->Request		= json_encode($reqArr);
										 $logs->Response	= json_encode($resArr);
										 $logs->UserId		= (int)$user_id;
										 $logs->UserType    = ""; 
										 $logs->AddLog();
										 
										response($resArr, 400);
										
								} else {
									
								  $updateUser = $user->update($updateStatus);
									
									// create the user
									if($updateUser == true){
										
										// generate jwt
									//$access_token = $auth->generateToken($tokenArr);
										 
										// response in json format
										$resArr = array(
														"status" 	=> "1", 
														"message" 	=> $UpdateMsg,
														"access_token" 	=> $access_token
													);		
											
										// Merge Array Post And GET Values
										$reqArr = array_merge($data, $getArr);		
													
											//Add log data
											 $logs->Module 		= "User";
											 $logs->Action		= "update";
											 $logs->Request		= json_encode($reqArr);
											 $logs->Response	= json_encode($resArr);
											 $logs->UserId		= (int)$user_id;;
											 $logs->UserType    = ""; 	
											 $logs->AddLog();		
										 
										response($resArr, 200);
									}
									 
									// message if unable to update user
									else {
									 
										// generate jwt
										//$access_token = $auth->generateToken($tokenArr);
									 
										// show error message
										$resArr = array("status" => "1", 
														"message" => "Unable to update user.",
														"access_token" 	=> $access_token
														);
										 
										 // Merge Array Post And GET Values
										$reqArr = array_merge($data, $getArr);
										 
										 //Add log data
										 $logs->Module 		= "User";
										 $logs->Action		= "update";
										 $logs->Request		= json_encode($reqArr);
										 $logs->Response	= json_encode($resArr);
										 $logs->UserId		= (int)$user_id;
										 $logs->UserType    = ""; 	
										 $logs->AddLog();
										 
										response($resArr, 401);
									}
									
								}
								
							} else {
								
								
								// show error message
								$resArr = array("status" => "0", "message" => $data["requestData"]);
							
								// Merge Array Post And GET Values
								$reqArr = array_merge($data, $getArr);	
							
								//Add log data
								 $logs->Module 		= "User";
								 $logs->Action		= "update";
								 $logs->Request		= json_encode($reqArr);
								 $logs->Response	= json_encode($resArr);
								 $logs->UserId		= (int)$user_id;
								 $logs->UserType    = ""; 	
								 $logs->AddLog();
							
								response($resArr, 400);
								
							}
	
		 } else {
				
				// show error message
				$resArr = array(
					"status" 	=> "0", 
					"message" 	=> "Invalid Authorization. Error : ". $decoded["TokenData"]
				);
				
				// Merge Array Post And GET Values
				$reqArr = array_merge($data, $getArr);
				
				//Add log data
				 $logs->Module 		= "User";
				 $logs->Action		= "update";
				 $logs->Request		= json_encode($reqArr);
				 $logs->Response	= json_encode($resArr);
				 $logs->UserId		= "";
				 $logs->UserType    = "";	
				 $logs->AddLog();
				
				response($resArr, 401);
			}		
		
		}  else { // show error message if jwt is empty
		 
			// tell the user access denied
			$resArr = array("status" => "0", "message" => "Access denied.");
			
			//Add log data
			 $logs->Module 		= "User";
			 $logs->Action		= "update";
			 $logs->Request		= "";
			 $logs->Response	= json_encode($resArr);
			 $logs->UserId		= "";
			 $logs->UserType	= "";			 
			 $logs->AddLog();
			
			// display message: unable to update act master
			response($resArr, 401);
		}
	
		break;
		
	case 'GET':	
			
		$bearerToken = getBearerToken();
		
		$access_token = isset($bearerToken) ? $bearerToken : "";
		
		// if jwt is not empty
		if($access_token){
			
			// decode jwt		
			$decoded  = $auth->validateToken($access_token);
			
				$getArr = array();
				
				// if decode succeed, show user details
				if($decoded["status"] == true) {
					
				$user_id 		= $decoded['TokenData']->data->user_id; // Decode ID
				$username 		= $decoded['TokenData']->data->username; // Username
				$role_id 		= $decoded['TokenData']->data->role_id; // Role ID
				$tokenArr 		= array("user_id" => (int)$user_id, 
										"username" => $username, 
										"role_id" => (int)$role_id
										);		
				
				$getArr = get_url_data();					
				
				$id 	= isset($getArr['id']) ? intval($getArr['id']) : "";				
				
				$userListing = false;
				
				if($id == ""){ //If admin user
				
					$userListing = $user->get();
				}
				 
				 if($id > 0){
					
						$userExist = $user->userExists($id);
						
						if($userExist == true){
							
							$userListing = $user->get($id);
							
						} else {
							
							// generate jwt
							//$access_token = $auth->generateToken($tokenArr);
							
							$resArr = array(
									"status" 		=> "0", 
									"message" 		=> "User does not exist.",
									"access_token" 	=> $access_token
								);
								
							 $logs->Module 		= "User";
							 $logs->Action		= "get";
							 $logs->Request		= json_encode($getArr);
							 $logs->Response	= json_encode($resArr);
							 $logs->UserId		= (int)$user_id;
							 $logs->UserType    = "";  
							 $logs->AddLog();	
								
							response($resArr, 200);
						}
					
				}
							
					
				 
				if($userListing){
					
					// generate jwt
					//$access_token = $auth->generateToken($tokenArr);
					
					// set response code
					$resArr = array(
								"status" 		=> "1", 
								"message" 		=> "User data found.",
								"access_token" 	=> $access_token,
								"data" 			=> $userListing
							);
						
							
						//Add log data
						 $logs->Module 		= "User";
						 $logs->Action		= "get";
						 $logs->Request		= json_encode($getArr);
						 $logs->Response	= json_encode($resArr);
						 $logs->UserId		= (int)$user_id;
						 $logs->UserType    = ""; 
						 $logs->AddLog();
							
						response($resArr, 200);
						
				} else { // message if unable to create user

					$userListing = array();	
					
					// generate jwt
					//$access_token = $auth->generateToken($tokenArr);	
					
					$resArr = array(
								"status" 		=> "0", 
								"message" 		=> "No data found.",
								"access_token" 	=> $access_token,
								"data"			=> $userListing
							);
								
					//Add log data
					 $logs->Module 		= "User";
					 $logs->Action		= "get";
					 $logs->Request		= json_encode($getArr);
					 $logs->Response	= json_encode($resArr);
					 $logs->UserId		= (int)$user_id;;
					 $logs->UserType    = ""; 					 
					 $logs->AddLog();	
							
					response($resArr, 200);
					
				}
				
					} else {			 
				
				// show error message
				$resArr =array(
					"status" 	=> "0", 
					"message" 	=> "Invalid Authorization. Error : ". $decoded["TokenData"]
				);
				
				//Add log data
				 $logs->Module 		= "User";
				 $logs->Action		= "get";
				 $logs->Request		= "";
				 $logs->Response	= json_encode($resArr);
				 $logs->UserId		= "";
				 $logs->UserType	= "";	
				 $logs->AddLog();
				
				response($resArr, 401);
			}			
				
				
		}  else { // show error message if jwt is empty		 
			
			$resArr = array("status" => "0", "message" => "Access denied.");
			
			//Add log data
			 $logs->Module 		= "User";
			 $logs->Action		= "get";
			 $logs->Request		= "";
			 $logs->Response	= json_encode($resArr);
			 $logs->UserId		= "";
			 $logs->UserType	= "";	
			 $logs->AddLog();
			
			response($resArr, 401);
		}
		
		break;
	
	case 'DELETE':
			
		$bearerToken = getBearerToken();
		
		$access_token = isset($bearerToken) ? $bearerToken : "";
		
		// if jwt is not empty
		if($access_token)
		{
			
			// decode jwt
			$decoded = $auth->validateToken($access_token);
			
			$getArr = array();
					
			// if decode succeed, show user details
			if($decoded["status"] == true) {
					
				$user_id 		= $decoded['TokenData']->data->user_id; // Decode ID
				$username 		= $decoded['TokenData']->data->username; // Username
				$role_id 		= $decoded['TokenData']->data->role_id; // Role ID
				$tokenArr 		= array("user_id" => (int)$user_id, 
										"username" => $username, 
										"role_id" => (int)$role_id
										);		
			
					$getArr = get_url_data();
					
					// set user property values	
					$user->user_id = isset($getArr['id']) ? intval($getArr['id']) : "";
					
					$error = array();
					
					if($user->user_id == ""){
						
						$error[] = "User Id is required.";					
						
					} 
							
					if(count($error) > 0){
				
						$errorMsg = implode(", ", $error);
							 
						// tell the user login failed
						$resArr = array("status" => "0", "message" => $errorMsg);						
				
						//Add log data
						 $logs->Module 		= "User";
						 $logs->Action		= "delete";
						 $logs->Request		= json_encode($getArr);
						 $logs->Response	= json_encode($resArr);
						 $logs->UserId		= (int)$user_id;
						 $logs->UserType    = ""; 
						 $logs->AddLog();
				
						response($resArr, 200);
						 
					 } else {
							
							$userExist = $user->userExists($user->user_id);
							
							if($userExist == true){
								
								$deleteUser = false;
						 
								$deleteUser = $user->delete();
								
								if($deleteUser == true){
									
									// generate jwt
									//$access_token = $auth->generateToken($tokenArr);
										 
									$resArr = array(
													"status" 		=> "1", 
													"message" 		=> "User was successfully deleted.",
													"access_token" 	=> $access_token
												);			
									
									//Add log data
									 $logs->Module 		= "User";
									 $logs->Action		= "delete";
									 $logs->Request		= json_encode($getArr);
									 $logs->Response	= json_encode($resArr);
									 $logs->UserId		= (int)$user_id;
									 $logs->UserType    = ""; 
									 $logs->AddLog();
						
									response($resArr, 200);
									
								 } else {
										
										 // generate jwt
										//$access_token = $auth->generateToken($tokenArr);
							 
										 $resArr = array(
														"status" 		=> "1", 
														"message" 		=> "Unable to delete user.",
														"access_token" 	=> $access_token
													);
													
										//Add log data
										 $logs->Module 		= "User";
										 $logs->Action		= "delete";
										 $logs->Request		= json_encode($getArr);
										 $logs->Response	= json_encode($resArr);
										 $logs->UserId		= (int)$user_id;
										 $logs->UserType    = ""; 
										 $logs->AddLog();		
											 
										response($resArr, 401);
										 
									 }		
								
							} else {
								 
								 // generate jwt
								//$access_token = $auth->generateToken($tokenArr);	
										 
									$resArr = array(
													"status" 		=> "0", 
													"message" 		=> "User does not exist.",
													"access_token" 	=> $access_token
												);			
									
									//Add log data
									 $logs->Module 		= "User";
									 $logs->Action		= "delete";
									 $logs->Request		= json_encode($getArr);
									 $logs->Response	= json_encode($resArr);
									 $logs->UserId		= (int)$user_id;
									 $logs->UserType    = "";  
									 $logs->AddLog();
						
									response($resArr, 200);
								
							}
							
					 }
		} else {
			 
				// set response code				
				$resArr = array(
							"status" 	=> "0", 
							"message" 	=> "Invalid Authorization. Error : ". $decoded["TokenData"]
						);
					
				//Add log data
				 $logs->Module 		= "User";
				 $logs->Action		= "delete";
				 $logs->Request		= json_encode($getArr);
				 $logs->Response	= json_encode($resArr);
				 $logs->UserId		= "";
				 $logs->UserType	= "";
				 $logs->AddLog();			
								 
				response($resArr, 401);
			}	
		 
		 }  else { // show error message if jwt is empty
		 
			// tell the act access denied
			$resArr = array("status" => "0", "message" => "Access denied.");
			
			//Add log data
			 $logs->Module 		= "User";
			 $logs->Action		= "delete";
			 $logs->Request		= "";
			 $logs->Response	= json_encode($resArr);
			 $logs->UserId		= "";
			 $logs->UserType	= "";
			 $logs->AddLog();
						 
			response($resArr, 401);
			
		}	
	
		break;
		
	case "OPTIONS": //Handle Preflighted requests for CORS issue
		
		// set response code		
		header('Content-Type: text/plain');
		
		response("", 200);
		
		break;
		
	default:
	
		// Invalid Request Method
		$resArr = array("status" => "0", "message" => "Method Not Allowed.");						
		// display message: unable to create user
		response($resArr, 405);	
		
		break;
}


?>