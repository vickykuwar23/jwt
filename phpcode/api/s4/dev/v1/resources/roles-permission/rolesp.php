<?php
/*** 'role' object ***/
class Role{
 
    /*** database connection and table name ***/
    private $aconn;
    private $table_name = 'public."AspNetRoles"';	
	
    /*** object properties ***/    
    public $Id;
    public $Name;
    public $NormalizedName;
	public $permission;

 
    /*** constructor ***/
    public function __construct($db){
        $this->aconn = $db;
		$this->createdOn = date("Y-m-d H:i:s");
    }
	
	
	function get($id = 0){
		
		/*** query to check if email exists ***/
							
		if($id > 0){
			
			$query = 'SELECT "Id", "Name", "NormalizedName", "Permissions"
					FROM '.$this->table_name.' 
				    WHERE "Id" = ?';
			
		} else {
			
			$query = 'SELECT "Id", "Name", "NormalizedName", "Permissions"
					FROM '.$this->table_name.' 
					 ORDER BY "Id" ASC';
		}		
	 
		/*** prepare the query ***/
		$stmt = $this->aconn->prepare( $query );		
	 
		if($id > 0){
			/*** bind given email value ***/
			$stmt->bindValue(1, $id);
			//$stmt->bindValue(2, false, PDO::PARAM_BOOL);
		} 
		
		/*** execute the query ***/
		$stmt->execute();
	 
		/*** get number of rows ***/
		$num = $stmt->rowCount();
	 
		/*** if email exists, assign values to object properties for easy access and use for php sessions ***/
		if($num>0){
	 
			/*** get record details / values ***/			
			while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
					$userData[] = $row;
			}
	 
			/*** return true because email exists in the database ***/
			return $userData;
		}
	 
		/*** return false if email does not exist in the database ***/
		return false;
		
	}
	
	/*** update a user record ***/
	public function update(){		
		
		/*** if no posted password, do not update the password ***/
		$query = 'UPDATE ' . $this->table_name . ' SET "Permissions" = :Permissions, "updated_on" = :updated_on WHERE "Id" = :Id';		
				
		/*** prepare the query ***/
		$stmt = $this->aconn->prepare($query);
		
		
		if($this->permission!= ""){
			
			$stmt->bindParam(':Permissions', $this->permission);
			$stmt->bindParam(':updated_on', $this->createdOn);
		}	
		
			
		$stmt->bindParam(':Id', $this->Id);
	 
		/*** execute the query ***/
		if($stmt->execute()){
			
			return true;
			//return $stmt->rowCount();
		}
	 
		return false;
	}
	
	
}