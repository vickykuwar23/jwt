<?php
/*** files needed to connect to database and class***/
define("ROOT_PATH", realpath(__DIR__ . '/../../..'));
include_once ROOT_PATH.'/constant.php';

include_once CONFIG_PATH.'functions.php';
include_once CONFIG_PATH.'database.php';
include_once RESOURCE_PATH.'roles-permission/rolesp.php';
include_once RESOURCE_PATH.'logs/logs.php';
include_once RESOURCE_PATH.'auth/auth.php';

//include_once LIB_PATH.'eCos/eCos.php';

// Required headers
requireHeaders();
 
/*** get database connection ***/
$database 	= new Database();
$db 		= $database->getAuthConnection();
$mdb 		= $database->getMongoConnection();
 

// instantiate Roles object
$roles = new Role($db);
$logs  = new Logs($mdb);
$auth  = new Auth();



// Get Request Method 
$request_method		= get_request_method();

switch($request_method)
{
	case 'POST':
	
	$bearerToken = getBearerToken();
		
	$access_token = isset($bearerToken) ? $bearerToken : "";
	
	// if jwt is not empty
	if($access_token){	
	
		// decode jwt
		$decoded = $auth->validateToken($access_token);
		
		$data 	= array();
		$getArr = array();
		$reqArr = array();
		
		// if decode succeed, show user details			
			if($decoded['status'] == true){	

				$user_id 		= $decoded['TokenData']->data->user_id; // Decode ID
				$username 		= $decoded['TokenData']->data->username; // Username
				$role_id 		= $decoded['TokenData']->data->role_id; // Role ID

				$tokenArr 		= array("user_id" => (int)$user_id, 
										"username" => $username, 
										"role_id" => (int)$role_id
										);		
				
						
						// get posted url data
						$getArr = get_url_data();
								
						//Get Parameter From URL		
						$roles->Id 		= isset($getArr['id']) ? intval($getArr['id']) : "";
						$roles->type 	= isset($getArr['type']) ? intval($getArr['type']) : "";

						if($roles->type == "permissions"){
							
							if($roles->Id == ""){
							
								$error[] = "Role ID is required.";
								
							} 
							
						} 
						
						// get posted data
						$data 	= get_request_body();						
						
						
						if($data['status'] == true){						
												
							$error = array();
							
							$roles->permission = isset($data['requestData']['permissions']) ? $data['requestData']['permissions'] : "";	
							
							
								if($roles->permission == ""){
											
									$error[] = "Permissions is required.";
									
								}
										
								if(count($error) > 0){
									
										$errorMsg = implode(", ", $error);
										
										// tell the user login failed
										$resArr = array("status" => "0", "message" => $errorMsg);
										
										 //Add log data
										 $logs->Module 		= "Role Permission";
										 $logs->Action		= "create";
										 $logs->Request		= json_encode($data);
										 $logs->Response	= json_encode($resArr);
										 $logs->UserId		= (int)$user_id;
										 $logs->UserType	= "";
										 $logs->AddLog();
											 
										// tell the user login failed
										 response($resArr, 400);
								
								} else {
								
									// Add roles permission
									if($roles->update()){
										
										// generate jwt
										//$access_token = $auth->generateToken($tokenArr);
											
											// display message: roles permission was created
											$resArr = array("status" => "1", 
											"message" => "Roles Permission created successfully.",
											"access_token" 	=> $access_token);
											
											//Add log data
											 $logs->Module 		= "Role Permission";
											 $logs->Action		= "create";
											 $logs->Request		= json_encode($data);
											 $logs->Response	= json_encode($resArr);
											 $logs->UserId		= (int)$user_id;
											 $logs->UserType	= "";
											 $logs->AddLog();
											
											// display message: Role Permission was created
											 response($resArr, 200);
										
									} else { // message if unable to create Role Permission
									
										// generate jwt
										//$access_token = $auth->generateToken($tokenArr);
									
										// display message: unable to create Role Permission
										$resArr = array("status" => "0", 
										"message" => "Unable to create Role Permission.",
										"access_token" 	=> $access_token);
										
										//Add log data
										 $logs->Module 		= "Role Permission";
										 $logs->Action		= "create";
										 $logs->Request		= json_encode($data);
										 $logs->Response	= json_encode($resArr);
										 $logs->UserId		= (int)$user_id;
										 $logs->UserType	= "";
										 $logs->AddLog();
										
										 // display message: unable to create Role Permission
										 response($resArr, 400);
										
									}
								
							}
			
						} else {
					
						// show error message
						$resArr = array(
							"status" 	=> "0", 
							"message" 	=> "Invalid Authorization. Error : ". $decoded["TokenData"]
						);
						
						// Merge Array Post And GET Values
						$reqArr = array_merge($data, $getArr);
						
						//Add log data
						 $logs->Module 		= "Role Permission";
						 $logs->Action		= "create";
						 $logs->Request		= json_encode($reqArr);
						 $logs->Response	= json_encode($resArr);
						 $logs->UserId		= "";
						 $logs->UserType    = "";	
						 $logs->AddLog();
						
						response($resArr, 401);
						
				} // Invalid Authorization error
						
			} else {
				
				// show error message
				$resArr = array(
					"status" 	=> "0", 
					"message" 	=> "Invalid Authorization. Error : ". $decoded["TokenData"]
				);
				
				// Merge Array Post And GET Values
				$reqArr = array_merge($data, $getArr);
				
				//Add log data
				 $logs->Module 		= "Role Permission";
				 $logs->Action		= "create";
				 $logs->Request		= json_encode($reqArr);
				 $logs->Response	= json_encode($resArr);
				 $logs->UserId		= "";
				 $logs->UserType    = "";	
				 $logs->AddLog();
				
				response($resArr, 401);
			}					
	
	}  else { // show error message if jwt is empty
		 
			// tell the user access denied
			$resArr = array("status" => "0", "message" => "Access denied.");
			
			 $logs->Module 		= "Role Permission";
			 $logs->Action		= "create";
			 $logs->Request		= "";
			 $logs->Response	= json_encode($resArr);
			 $logs->UserId		= "";
			 $logs->UserType	= "";
			 $logs->AddLog();
			
			// display message: unable to create user
			response($resArr, 401);
	}	

	break;
	
	case 'PUT':
	
	$bearerToken = getBearerToken();
		
	$access_token = isset($bearerToken) ? $bearerToken : "";
	
	// if jwt is not empty
	if($access_token){	
	
		// decode jwt
		$decoded = $auth->validateToken($access_token);
		
		$data 	= array();
		$getArr = array();
		$reqArr = array();
		
		// if decode succeed, show user details			
			if($decoded['status'] == true){	

				$user_id 		= $decoded['TokenData']->data->user_id; // Decode ID
				$username 		= $decoded['TokenData']->data->username; // Username
				$role_id 		= $decoded['TokenData']->data->role_id; // Role ID

				$tokenArr 		= array("user_id" => (int)$user_id, 
										"username" => $username, 
										"role_id" => (int)$role_id
										);		
				
						
						// get posted url data
						$getArr = get_url_data();
								
						//Get Parameter From URL		
						$roles->Id 		= isset($getArr['id']) ? intval($getArr['id']) : "";
						$roles->type 	= isset($getArr['type']) ? intval($getArr['type']) : "";

						if($roles->type == "permission"){
							
							if($roles->Id == ""){
							
								$error[] = "Role ID is required.";
								
							} 
							
						} 
						
						// get posted data
						$data 	= get_request_body();						
						
						
						if($data['status'] == true){						
												
							$error = array();
							
							$roles->permission = isset($data['requestData']['permissions']) ? $data['requestData']['permissions'] : "";	
							
								if($roles->permission == ""){
											
									$error[] = "Permission is required.";
									
								}
										
								if(count($error) > 0){
									
										$errorMsg = implode(", ", $error);
										
										// tell the user login failed
										$resArr = array("status" => "0", "message" => $errorMsg);
										
										 //Add log data
										 $logs->Module 		= "Role Permission";
										 $logs->Action		= "update";
										 $logs->Request		= json_encode($data);
										 $logs->Response	= json_encode($resArr);
										 $logs->UserId		= (int)$user_id;
										 $logs->UserType	= "";
										 $logs->AddLog();
											 
										// tell the user login failed
										 response($resArr, 400);
								
								} else {
								
									// Add roles permission
									if($roles->update()){
										
										// generate jwt
										//$access_token = $auth->generateToken($tokenArr);
											
											// display message: roles permission was updated
											$resArr = array("status" => "1", 
											"message" => "Roles Permission updated successfully.",
											"access_token" 	=> $access_token);
											
											//Add log data
											 $logs->Module 		= "Role Permission";
											 $logs->Action		= "update";
											 $logs->Request		= json_encode($data);
											 $logs->Response	= json_encode($resArr);
											 $logs->UserId		= (int)$user_id;
											 $logs->UserType	= "";
											 $logs->AddLog();
											
											// display message: Role Permission was updated
											 response($resArr, 200);
										
									} else { // message if unable to create Role Permission
									
										// generate jwt
										//$access_token = $auth->generateToken($tokenArr);
									
										// display message: unable to create Role Permission
										$resArr = array("status" => "0", 
										"message" => "Unable to update Role Permission.",
										"access_token" 	=> $access_token);
										
										//Add log data
										 $logs->Module 		= "Role Permission";
										 $logs->Action		= "update";
										 $logs->Request		= json_encode($data);
										 $logs->Response	= json_encode($resArr);
										 $logs->UserId		= (int)$user_id;
										 $logs->UserType	= "";
										 $logs->AddLog();
										
										 // display message: unable to create Role Permission
										 response($resArr, 400);
										
									}
								
							}
			
						} else {
					
						// show error message
						$resArr = array(
							"status" 	=> "0", 
							"message" 	=> "Invalid Authorization. Error : ". $decoded["TokenData"]
						);
						
						// Merge Array Post And GET Values
						$reqArr = array_merge($data, $getArr);
						
						//Add log data
						 $logs->Module 		= "Role Permission";
						 $logs->Action		= "update";
						 $logs->Request		= json_encode($reqArr);
						 $logs->Response	= json_encode($resArr);
						 $logs->UserId		= "";
						 $logs->UserType    = "";	
						 $logs->AddLog();
						
						response($resArr, 401);
						
				} // Invalid Authorization error
						
			} else {
				
				// show error message
				$resArr = array(
					"status" 	=> "0", 
					"message" 	=> "Invalid Authorization. Error : ". $decoded["TokenData"]
				);
				
				// Merge Array Post And GET Values
				$reqArr = array_merge($data, $getArr);
				
				//Add log data
				 $logs->Module 		= "Role Permission";
				 $logs->Action		= "update";
				 $logs->Request		= json_encode($reqArr);
				 $logs->Response	= json_encode($resArr);
				 $logs->UserId		= "";
				 $logs->UserType    = "";	
				 $logs->AddLog();
				
				response($resArr, 401);
			}					
	
	}  else { // show error message if jwt is empty
		 
			// tell the user access denied
			$resArr = array("status" => "0", "message" => "Access denied.");
			
			 $logs->Module 		= "Role Permission";
			 $logs->Action		= "update";
			 $logs->Request		= "";
			 $logs->Response	= json_encode($resArr);
			 $logs->UserId		= "";
			 $logs->UserType	= "";
			 $logs->AddLog();
			
			// display message: unable to update user
			response($resArr, 401);
	}	

	break;
	
	case 'GET':	
			
		$bearerToken = getBearerToken();
		
		$access_token = isset($bearerToken) ? $bearerToken : "";
		
		
		
		// if jwt is not empty
		if($access_token){
			
			// decode jwt		
			$decoded  = $auth->validateToken($access_token);
			
				$getArr = array();
				
				// if decode succeed, show user details
				if($decoded["status"] == true) {
					
				$user_id 		= $decoded['TokenData']->data->user_id; // Decode ID
				$username 		= $decoded['TokenData']->data->username; // Username
				$role_id 		= $decoded['TokenData']->data->role_id; // Role ID
				$tokenArr 		= array("user_id" => (int)$user_id, 
										"username" => $username, 
										"role_id" => (int)$role_id
										);		
				
				$getArr = get_url_data();					
				
				$id 	= isset($getArr['id']) ? intval($getArr['id']) : "";				
				
				$roleListing = false;
				
				if($id == ""){ //If roles
				
					$roleListing = $roles->get();
					
				} else {
					
					$roleListing = $roles->get($id);
				}				
				 
				if($roleListing){
					
					// generate jwt
					//$access_token = $auth->generateToken($tokenArr);
					
					// set response code
					$resArr = array(
								"status" 		=> "1", 
								"message" 		=> "Role data found.",
								"access_token" 	=> $access_token,
								"data" 			=> $roleListing
							);
						
							
						//Add log data
						 $logs->Module 		= "Roles";
						 $logs->Action		= "get";
						 $logs->Request		= json_encode($getArr);
						 $logs->Response	= json_encode($resArr);
						 $logs->UserId		= (int)$user_id;
						 $logs->UserType    = ""; 
						 $logs->AddLog();
							
						response($resArr, 200);
						
				} else { // message if unable to create Roles

					$roleListing = array();	
					
					// generate jwt
					//$access_token = $auth->generateToken($tokenArr);	
					
					$resArr = array(
								"status" 		=> "0", 
								"message" 		=> "No data found.",
								"access_token" 	=> $access_token,
								"data"			=> $roleListing
							);
								
					//Add log data
					 $logs->Module 		= "Roles";
					 $logs->Action		= "get";
					 $logs->Request		= json_encode($getArr);
					 $logs->Response	= json_encode($resArr);
					 $logs->UserId		= (int)$user_id;;
					 $logs->UserType    = ""; 					 
					 $logs->AddLog();	
							
					response($resArr, 200);
					
				}
				
					} else {			 
				
				// show error message
				$resArr =array(
					"status" 	=> "0", 
					"message" 	=> "Invalid Authorization. Error : ". $decoded["TokenData"]
				);
				
				//Add log data
				 $logs->Module 		= "Roles";
				 $logs->Action		= "get";
				 $logs->Request		= "";
				 $logs->Response	= json_encode($resArr);
				 $logs->UserId		= "";
				 $logs->UserType	= "";	
				 $logs->AddLog();
				
				response($resArr, 401);
			}			
				
				
		}  else { // show error message if jwt is empty		 
			
			$resArr = array("status" => "0", "message" => "Access denied.");
			
			//Add log data
			 $logs->Module 		= "Roles";
			 $logs->Action		= "get";
			 $logs->Request		= "";
			 $logs->Response	= json_encode($resArr);
			 $logs->UserId		= "";
			 $logs->UserType	= "";	
			 $logs->AddLog();
			
			response($resArr, 401);
		}
		
		break;
		
	case "OPTIONS": //Handle Preflighted requests for CORS issue
		
		// set response code		
		header('Content-Type: text/plain');
		
		response("", 200);
		
		break;
		
	default:
	
		// Invalid Request Method
		$resArr = array("status" => "0", "message" => "Method Not Allowed.");						
		// display message: unable to create user
		response($resArr, 405);	
		
		break;
}


?>