<?php
// Files needed to connect to database and class
define("ROOT_PATH", realpath(__DIR__ . '/../../..'));
include_once ROOT_PATH.'/constant.php';

include_once CONFIG_PATH.'functions.php';
include_once CONFIG_PATH.'database.php';
include_once RESOURCE_PATH.'fir/fir.php';
include_once RESOURCE_PATH.'logs/logs.php';
//include_once RESOURCE_PATH.'auth/auth.php';

// Required headers
requireHeaders();
 
// Get database connection
$database 	= new Database();
$db 		= $database->getConnection();
$mdb 		= $database->getMongoConnection();

// Instantiate logs object
$fir 	= new Fir($db);
$logs   = new Logs($mdb);
//$auth   = new Auth();

//$logs->ListenLogs();

// Get Request Method
$request_method	= get_request_method();

switch($request_method)
{
	case 'POST':
		
			$data = get_request_body();
			
			if($data["status"] == false){	
			
				// set response code & display message
				$resArr = array("status" => "0", "message" => $data["requestData"]);
				
				// Add log data  
				$logs->Module 		= "FIR";
				$logs->Action		= "Search";
				$logs->Request		= "";
				$logs->Response	= json_encode($resArr);
				$logs->UserId		= "";
				$logs->UserType	= "";
				$logs->AddLog();
				 
				response($resArr, 400);
				
			} else {
				
				$error = array();		
				$fir->fir_id 	= isset($data['requestData']['fir_id']) ? $data['requestData']['fir_id'] : "";
				$fir->applicant_national_id 	= isset($data['requestData']['applicant_national_id']) ? $data['requestData']['applicant_national_id'] : "";
				$fir->applicant_mobile 	= isset($data['requestData']['applicant_mobile']) ? $data['requestData']['applicant_mobile'] : "";
				$fir->captcha_code = isset($data['requestData']['captcha_code']) ? $data['requestData']['captcha_code'] : "";	
					
				if($fir->fir_id == ""){
				
					$error[]= "FIR No. is required.";
					
				}
				if($fir->applicant_national_id == ""){
				
					$error[] = "Complaint National ID is required.";
					
				}
				if($fir->applicant_mobile == ""){
				
					$error[] = "Mobile No is required.";
					
				} else if(isValidMobile($fir->applicant_mobile) == false){
					
					$error[] = "Please enter 10 digit Mobile No.";
					
				} 
				/*if($fir->captcha_code == ""){
					
					$error[] = "Captcha is required.";				
				}
				else
				{
					// Google captcha validation
					$captcha_response = file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=".SECRET_KEY."&response=".$fir->captcha_code);
					$captcha_response = json_decode($captcha_response, true);
					
					if($captcha_response["success"] == false){
						
						$error[] = "Invalid captcha.";
						
					}
					
				}*/
				
				// Check if validation errors
				if(count($error) > 0){
					
					$error_msg = implode(", ", $error);
					
					$resArr = array("status" => "0", "message" => $error_msg);
					
					// Add log data
					$logs->Module 		= "FIR";
					$logs->Action		= "Search";
					$logs->Request		= json_encode($data);
					$logs->Response		= json_encode($resArr);
					$logs->UserId		= "";
					$logs->UserType		= "";
					$logs->AddLog();
					
					response($resArr, 200);
								
				} else {
					
					$get_fir_details = $fir->get_details();
					
					if($get_fir_details){
					 
							// show Fir details
							$resArr = array(
								"status" 		=> "1", 
								"message" 		=> "FIR details get successfully.",
								"data" 			=> $get_fir_details
							);

							// Add log data
							 $logs->Module 		= "FIR";
							 $logs->Action		= "Search";
							 $logs->Request		= json_encode($data);
							 $logs->Response	= json_encode($resArr);
							 $logs->UserId		= "";
							 $logs->UserType	= "";
							 $logs->AddLog();	
					
							response($resArr, 200);
							
							
					} else { // message if unable to create Fir
					
					 
						// set response code					
						$resArr = array("status" => "0", 
						"message" => "Provide details are incorrenct.");
						
						// Add log data  
						 $logs->Module 		= "FIR";
						 $logs->Action		= "Search";
						 $logs->Request		= json_encode($data);
						 $logs->Response	= json_encode($resArr);
						 $logs->UserId		= "";
						 $logs->UserType	= "";
						 $logs->AddLog();
						
						// display message: unable to create Fir
						response($resArr, 400);
						
					}	// end create Fir if
					
				} // if error end
				
			}	
		
		break;
	
	case "OPTIONS": //Handle Preflighted requests for CORS issue
		
		// set response code		
		header('Content-Type: text/plain');
		
		response("", 200);
		
		break;
		
	default:
	
		// Invalid Request Method
		$resArr = array("status" => "0", "message" => "Method Not Allowed.");						
		// display message: unable to create Fir
		response($resArr, 405);	
		
		break;
}

?>