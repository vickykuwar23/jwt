<?php
class Fir{
	
	/*** database connection and table name ***/
    private $conn;
    private $table 	= 'public."firs"';	
 
    /*** object properties ***/
    public $fir_id;
	public $applicant_national_id;
	public $applicant_mobile;
	public $captcha_code;
 
    /*** constructor ***/
    public function __construct($db){
        $this->conn = $db;
    }	
	
	function get_details(){
		
		$data = array();
					
			 $query = 'SELECT "police_case_status"
						FROM '.$this->table.'
						WHERE "police_case_no" = ?
						AND "applicant_national_id" = ?
						AND "applicant_mobile" = ?
						AND "is_active" = ?
						AND "is_deleted" = ?	
						LIMIT 1';
					
				/*** prepare the query ***/
				$stmt = $this->conn->prepare( $query );
			 
				/*** bind given values ***/
				
				$stmt->bindValue(1, $this->fir_id);
				$stmt->bindValue(2, $this->applicant_national_id);
				$stmt->bindValue(3, $this->applicant_mobile);
				$stmt->bindValue(4, true, PDO::PARAM_INT);
				$stmt->bindValue(5, false, PDO::PARAM_INT);
				
				/*** execute the query ***/
				if($stmt->execute()){
					
					$num = $stmt->rowCount();
						
					if($num > 0){
						
						  $row = $stmt->fetch(PDO::FETCH_ASSOC);
					
							$data = array("fir_status" => $row['police_case_status']);
					
						} 
						
				}
		
		return $data;
		
	}
	
}
?>