<?php
// Files needed to connect to database and class
define("ROOT_PATH", realpath(__DIR__ . '/../../..'));
include_once ROOT_PATH.'/constant.php';

include_once CONFIG_PATH.'functions.php';
include_once CONFIG_PATH.'database.php';
include_once RESOURCE_PATH.'login/login.php';
include_once RESOURCE_PATH.'logs/logs.php';
include_once RESOURCE_PATH.'auth/auth.php';

// Required headers
requireHeaders();
 
// Get database connection
$database 	= new Database();
$db 		= $database->getConnection();
$mdb 		= $database->getMongoConnection();

// Instantiate logs object
$login = new Login($db);
$logs  = new Logs($mdb);
$auth  = new Auth();

//$logs->ListenLogs();

// Get Request Method
$request_method	= get_request_method();

switch($request_method)
{
	case 'POST':
		
		$data = get_request_body();
		
		if($data["status"] == false){	
		
			// set response code & display message
			$resArr = array("status" => "0", "message" => $data["requestData"]);
			
			// Add log data  
			$logs->Module 		= "Login";
			$logs->Action		= "login";
			$logs->Request		= "";
			$logs->Response		= json_encode($resArr);
			$logs->UserId		= "";
			$logs->UserType		= "";
			$logs->AddLog();
			 
			response($resArr, 400);
			
		} else {
			
			$error = array();		
			$login->username 	= isset($data['requestData']['username']) ? $data['requestData']['username'] : "";
			$login->password 	= isset($data['requestData']['password']) ? $data['requestData']['password'] : "";
			$login->captcha_code = isset($data['requestData']['captcha_code']) ? $data['requestData']['captcha_code'] : "";	
			$login->role_id = 4;
			
			
			if($login->username == ""){
			
				$error[]= "UserName is required.";
				
			}
			if($login->password == ""){
			
				$error[] = "Password is required.";
				
			}
			/*if($login->captcha_code == ""){
				
				$error[] = "Captcha is required.";				
			}
			else
			{
				// Google captcha validation
				$captcha_response = file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=".SECRET_KEY."&response=".$login->captcha_code);
				$captcha_response = json_decode($captcha_response, true);
				
				if($captcha_response["success"] == false){
					
					$error[] = "Invalid captcha.";
					
				}
				
			}*/
			
			// Check if validation errors
			if(count($error) > 0){
				
				$errorMsg = implode(", ", $error);
				
				$resArr = array("status" => "0", "message" => $errorMsg);
				
				// Add log data
				$logs->Module 		= "Login";
				$logs->Action		= "login";
				$logs->Request		= json_encode($data);
				$logs->Response		= json_encode($resArr);
				$logs->UserId		= "";
				$logs->UserType		= "";
				$logs->AddLog();
				
				response($resArr, 200);
							
			} else {
				
				$userData = $login->validateLogin();
				
				if($userData){ 
						
					$tokenArr 	= array("user_id" => (int)$userData['user_id'], 
										"username" => $userData['username'], 
										"role_id" => (int)$userData['role_id']
									);
						
						// generate jwt
						$access_token 		= $auth->generateToken($tokenArr);
				 
						// show user details
						$resArr = array(
							"status" 		=> "1", 
							"message" 		=> "User successful login.",
							"access_token" 	=> $access_token
						);

						// Add log data
						 $logs->Module 		= "Login";
						 $logs->Action		= "login";
						 $logs->Request		= json_encode($data);
						 $logs->Response	= json_encode($resArr);
						 $logs->UserId		= (int)$userData['user_id'];
						 $logs->UserType	= "";
						 $logs->AddLog();	
				
						response($resArr, 200);
						
						
				} else { // message if unable to create user
				 
					// set response code					
					$resArr = array("status" => "0", "message" => "Incorrect username or password provided.");
					
					// Add log data  
					 $logs->Module 		= "Login";
					 $logs->Action		= "login";
					 $logs->Request		= json_encode($data);
					 $logs->Response	= json_encode($resArr);
					 $logs->UserId		= "";
					 $logs->UserType	= "";
					 $logs->AddLog();
					
					// display message: unable to create user
					response($resArr, 400);
					
				}	// end create user if
				
			} // if error end
			
		}
		
		break;
	
	case "OPTIONS": //Handle Preflighted requests for CORS issue
		
		// set response code		
		header('Content-Type: text/plain');
		
		response("", 200);
		
		break;
		
	default:
	
		// Invalid Request Method
		$resArr = array("status" => "0", "message" => "Method Not Allowed.");						
		// display message: unable to create user
		response($resArr, 405);	
		
		break;
}

?>