<?php
class Login{
	
	/*** database connection and table name ***/
    private $conn;
    private $table_name = 'public."users"';
    private $table_role = 'public."roles"';	
 
    /*** object properties ***/
    public $username;
	public $password;
	public $role_id;
	public $captcha_code;
 
    /*** constructor ***/
    public function __construct($db){
        $this->conn = $db;
    }	
		
	// check if username exists and if password is correct
	function validateLogin(){
		
		/**********  Get User password From DB **************/		
		$getPass = $this->getDetails();
		
		if(count($getPass) > 0)
		{
			if($this->username && (md5($this->password) == $getPass['password'])){
				
				return array(	"user_id" => $getPass['user_id'], 
								"username" => $getPass['username'], 
								"role_id" => $getPass['role_id']
							);				
				
			} else { // Else part
				
				return false;
			}
		} else {
			
			return false;
		}
	}	
	
	function getDetails(){
		
		$data = array();
					
			$query = 'SELECT "users_id", "role_id", "password", "username"
					FROM '.$this->table_name.'
					WHERE "username" = ?
					AND "role_id" = ?
					AND "is_active" = ?
					AND "is_deleted" = ?	
					LIMIT 1';
					
				/*** prepare the query ***/
				$stmt = $this->conn->prepare( $query );
			 
				/*** bind given email value ***/			
				
				$stmt->bindValue(1, $this->username);
				$stmt->bindValue(2, $this->role_id);
				$stmt->bindValue(3, true, PDO::PARAM_INT);
				$stmt->bindValue(4, false, PDO::PARAM_INT);
				
				/*** execute the query ***/
				if($stmt->execute()){
					
					$num = $stmt->rowCount();
						
					if($num > 0){
						
						  $row = $stmt->fetch(PDO::FETCH_ASSOC);
					
							$data = array("user_id" => $row['users_id'], "username" => $row['username'], "role_id" => $row['role_id'], "password" => $row['password']);
					
						} 
						
				}
		
		return $data;
		
	}
	
}
?>