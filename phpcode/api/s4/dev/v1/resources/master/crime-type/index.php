<?php

/*** files needed to connect to database and class***/
define("ROOT_PATH", realpath(__DIR__ . '/../../../..'));
include_once ROOT_PATH.'/constant.php';

include_once CONFIG_PATH.'functions.php';
include_once CONFIG_PATH.'database.php';
include_once RESOURCE_MASTERPATH.'crime-type/crime_type.php';
include_once RESOURCE_PATH.'logs/logs.php';
include_once RESOURCE_PATH.'auth/auth.php';

// Required headers
requireHeaders();
 
/*** get database connection ***/
$database 	= new Database();
$db 		= $database->getConnection();
$mdb 		= $database->getMongoConnection();
 
// instantiate CrimeType object
$crimeType  = new CrimeType($db);
$logs  = new Logs($mdb);
$auth  = new Auth();

// Get Request Method 
$request_method		= get_request_method();

switch($request_method)
{
	
	case "POST":
	
	$bearerToken = getBearerToken();
		
		$access_token = isset($bearerToken) ? $bearerToken : "";

		// if jwt is not empty
		if($access_token){	

			// decode jwt
			$decoded = $auth->validateToken($access_token);
			
			$data 	= array();
			$getArr = array();
			$reqArr = array();
			
			// if decode succeed, show user details			
			if($decoded['status'] == true){	

				$user_id 		= $decoded['TokenData']->data->user_id; // Decode ID
				$username 		= $decoded['TokenData']->data->username; // Username
				$role_id 		= $decoded['TokenData']->data->role_id; // Role ID
				$tokenArr 		= array("user_id" => (int)$user_id, 
										"username" => $username, 
										"role_id" => (int)$role_id
										);	

						// get posted data
						$data 	= get_request_body();
						
						if($data['status'] == true){
							
							$error = array();
							
							// set crime Type property
							$crimeType->category_type_name = isset($data['requestData']['category_type_name']) ? $data['requestData']['category_type_name'] : "";
							
							if($crimeType->category_type_name == ""){
								
								$error[] = "Please enter Crime Type.";
								
							} 
							if (!preg_match('/^[\p{L} ]+$/u', $crimeType->category_type_name)){
								
							  $error[] = 'Crime Type must contain letters and spaces only!';
							  
							}
							
							
							if(count($error) > 0){
								
									$errorMsg = implode(", ", $error);
									
									// tell the user login failed
									$resArr = array("status" => "0", "message" => $errorMsg);
									
									 //Add log data
									 $logs->Module 		= "Crime Type";
									 $logs->Action		= "create";
									 $logs->Request		= json_encode($data);
									 $logs->Response	= json_encode($resArr);
									 $logs->UserId		= (int)$user_id;
									 $logs->UserType    = ""; 
									 $logs->AddLog();
										 
									// tell the user login failed
									 response($resArr, 400);
								
							} else {
								
								$crime_type_id = $crimeType->create();
								
								if($crime_type_id > 0){
									
									// generate jwt
									//$access_token = $auth->generateToken($tokenArr);
									
									$resArr = array("status" => "1", 
									"message" => "Crime Type created successfully.",
									"access_token" 	=> $access_token);
											
									 $logs->Module 		= "Crime Type";
									 $logs->Action		= "create";
									 $logs->Request		= json_encode($data);
									 $logs->Response	= json_encode($resArr);
									 $logs->UserId		= (int)$user_id;
									 $logs->UserType    = ""; 
									 $logs->AddLog();
									
									// display message: Crime Type was created
									 response($resArr, 200);
								 
								 } else { // message if unable to create Crime Type
								 
								 // generate jwt
									//$access_token = $auth->generateToken($tokenArr);
									
										// display message: unable to create Crime Type
										$resArr = array("status" => "0", 
										"message" => "Unable to create data.",
										"access_token" 	=> $access_token);
										
										 $logs->Module 		= "Crime Type";
										 $logs->Action		= "create";
										 $logs->Request		= json_encode($data);
										 $logs->Response	= json_encode($resArr);
										 $logs->UserId		= (int)$user_id;
										 $logs->UserType    = ""; 
										 $logs->AddLog();
										
										 // display message: unable to create Crime Type
										 response($resArr, 400);
										
									} // Insert ID Check
									
							}
						
						} else {
							
							// display message: unable to create Crime Type
							$resArr = array("status" => "0", "message" => $data["requestData"]);
							
							// Merge Array Post And GET Values
							$reqArr = array_merge($data, $getArr);
							
							 $logs->Module 		= "Crime Type";
							 $logs->Action		= "create";
							 $logs->Request		= json_encode($reqArr);
							 $logs->Response	= json_encode($resArr);
							 $logs->UserId		= (int)$user_id;
							 $logs->UserType    = ""; 
							 $logs->AddLog();
							 
							response($resArr, 400);
						}
						
						
		 } else {
				
					// show error message
					$resArr = array(
						"status" 	=> "0", 
						"message" 	=> "Invalid Authorization. Error : ". $decoded["TokenData"]
					);
					
					// Merge Array Post And GET Values
					$reqArr = array_merge($data, $getArr);
					
					//Add log data
					 $logs->Module 		= "Crime Type";
					 $logs->Action		= "create";
					 $logs->Request		= json_encode($reqArr);
					 $logs->Response	= json_encode($resArr);
					 $logs->UserId		= "";
					 $logs->UserType    = "";	
					 $logs->AddLog();
					
					response($resArr, 401);
					
			} // Invalid Authorization error		
					
		
		}  else { // show error message if jwt is empty
		 
			// tell the user access denied
			$resArr = array("status" => "0", "message" => "Access denied.");
			
			 $logs->Module 		= "Crime Type";
			 $logs->Action		= "create";
			 $logs->Request		= "";
			 $logs->Response	= json_encode($resArr);
			 $logs->UserId		= "";
			 $logs->UserType	= "";
			 $logs->AddLog();
			
			// display message: unable to create user
			response($resArr, 401);
		}	
		
		break;
		
	case "PUT":

		$bearerToken = getBearerToken();
		
		$access_token = isset($bearerToken) ? $bearerToken : "";
	
		// if jwt is not empty
		if($access_token){	

			// decode jwt
			$decoded = $auth->validateToken($access_token);
			
			$data 	= array();
			$getArr = array();
			$reqArr = array();
			
			if($decoded['status'] == true){	

				$user_id 		= $decoded['TokenData']->data->user_id; // Decode ID
				$username 		= $decoded['TokenData']->data->username; // Username
				$role_id 		= $decoded['TokenData']->data->role_id; // Role ID
				$tokenArr 		= array("user_id" => (int)$user_id, 
										"username" => $username, 
										"role_id" => (int)$role_id
										);	
	    
							// Get URL Data	
							$getArr = get_url_data();
							
							// Get ID From URL		
							$crimeType->id = isset($getArr['id']) ? intval($getArr['id']) : "";
							
							$error = array();
							
							if($crimeType->id == ""){
											
									$error[] = "Crime Type ID is required.";
									
								}
								
							if(count($error) > 0){
									
								$errorMsg = implode(", ", $error);
								
								// tell the Type error occur
								$resArr = array("status" => "0", "message" => $errorMsg);
								
								// Merge Array Post And GET Values
								$reqArr = array_merge($data, $getArr);
								
								 //Add log data
								 $logs->Module 		= "Crime Type";
								 $logs->Action		= "update";
								 $logs->Request		= json_encode($reqArr);
								 $logs->Response	= json_encode($resArr);
								 $logs->UserId		= (int)$user_id;
								 $logs->UserType    = ""; 
								 $logs->AddLog();
									 
								// tell the Crime Type error occur
								 response($resArr, 400);
							
						}	
							
							// get posted data
							$data = get_request_body();
							
							if($data['status'] == true){
								
								
								
								// set crimeType property
								$crimeType->category_type_name = isset($data['requestData']['category_type_name']) ? $data['requestData']['category_type_name'] : "";
								
								$crimeType->is_active = isset($data['requestData']['is_active']) ? $data['requestData']['is_active'] : "";
								

								
							if(isset($data['requestData']['is_active'])){
									
								$updateStatus 		= "UpdateStatus";
								$UpdateMsg			= "Crime Type status successfully updated.";
								$crimeType->is_active 	= $data['requestData']['is_active'];
								
								if($crimeType->is_active == ""){
							
										$error[] = "Is Active is required.";
										
								} 
										
							} else {			
										
									
								$updateStatus 		= "UpdateField";
								$UpdateMsg			= "Crime Type updated successfully.";
									
									if($crimeType->category_type_name == ""){
										
										$error[] = "Please enter Crime Type.";
										
									} 
									if (!preg_match('/^[\p{L} ]+$/u', $crimeType->category_type_name)){
										
										$error[] = 'Crime Type Name must contain letters and spaces only!';
									  
									}
									
							} // URL ID validation required		
								
								if(count($error) > 0){
									
										$errorMsg = implode(", ", $error);
										
										// tell the Type error occur
										$resArr = array("status" => "0", "message" => $errorMsg);
										
										// Merge Array Post And GET Values
										$reqArr = array_merge($data, $getArr);
										
										 //Add log data
										 $logs->Module 		= "Crime Type";
										 $logs->Action		= "update";
										 $logs->Request		= json_encode($reqArr);
										 $logs->Response	= json_encode($resArr);
										 $logs->UserId		= (int)$user_id;
										 $logs->UserType    = ""; 
										 $logs->AddLog();
											 
										// tell the Crime Type error occur
										 response($resArr, 400);
									
								} else {
									
									$crime_type_exist = $crimeType->crime_type_exist($crimeType->id);
										
									if($crime_type_exist == true){
									
											$type_id = $crimeType->update($updateStatus);
											
											if($type_id == true){
												
												// generate jwt
										//$access_token = $auth->generateToken($tokenArr);
												
												$resArr = array("status" => "1", 
												"message" => $UpdateMsg,
												"access_token" 	=> $access_token);
												
												// Merge Array Post And GET Values
										$reqArr = array_merge($data, $getArr);
														
												 $logs->Module 		= "Crime Type";
												 $logs->Action		= "update";
												 $logs->Request		= json_encode($reqArr);
												 $logs->Response	= json_encode($resArr);
												 $logs->UserId		= (int)$user_id;
												 $logs->UserType    = ""; 
												 $logs->AddLog();
												
												// display message: Crime Type was updated
												 response($resArr, 200);
											 
											 } else { // message if unable to Crime Type
											 
											  // generate jwt
										//$access_token = $auth->generateToken($tokenArr);
												
													// display message: unable to update Crime Type
													$resArr = array("status" => "0", "message" => "Unable to update data.",
													"access_token" 	=> $access_token);
													
													// Merge Array Post And GET Values
											$reqArr = array_merge($data, $getArr);
													
													 $logs->Module 		= "Crime Type";
													 $logs->Action		= "update";
													 $logs->Request		= json_encode($reqArr);
													 $logs->Response	= json_encode($resArr);
													 $logs->UserId		= (int)$user_id;
													 $logs->UserType    = ""; 
													 $logs->AddLog();
													
													 // display message: unable to update Crime Type
													 response($resArr, 400);
													
												} // Insert ID Check
										
										} else {
											
											// generate jwt
								//$access_token = $auth->generateToken($tokenArr);
										
										$resArr = array(
												"status" 		=> "0", 
												"message" 		=> "Crime Type ID does not exist.",
												"access_token" 	=> $access_token
											);
											
											// Merge Array Post And GET Values
								$reqArr = array_merge($data, $getArr);	
											
										 $logs->Module 		= "Crime Type";
										 $logs->Action		= "update";
										 $logs->Request		= json_encode($reqArr);
										 $logs->Response	= json_encode($resArr);
										 $logs->UserId		= (int)$user_id;
										 $logs->UserType    = ""; 
										 $logs->AddLog();	
											
										response($resArr, 200);
										
									}
										
								}
							
							} else {
								
								// Merge Array Post And GET Values
								$reqArr = array_merge($data, $getArr);
								
								// display message: unable to update Crime Type
								$resArr = array("status" => "0", "message" => $data["requestData"]);
								
								 $logs->Module 		= "Crime Type";
								 $logs->Action		= "update";
								 $logs->Request		= json_encode($reqArr);
								 $logs->Response	= json_encode($resArr);
								 $logs->UserId		= (int)$user_id;
								 $logs->UserType    = ""; 
								 $logs->AddLog();
								 
								response($resArr, 400);
							}
							
		} else {
				
						// show error message
						$resArr = array(
							"status" 	=> "0", 
							"message" 	=> "Invalid Authorization. Error : ". $decoded["TokenData"]
						);
						
						// Merge Array Post And GET Values
						$reqArr = array_merge($data, $getArr);
						
						//Add log data
						 $logs->Module 		= "Crime Sub Type";
						 $logs->Action		= "update";
						 $logs->Request		= json_encode($reqArr);
						 $logs->Response	= json_encode($resArr);
						 $logs->UserId		= "";
						 $logs->UserType    = "";	
						 $logs->AddLog();
						
						response($resArr, 401);
				}
		
			}  else { // show error message if jwt is empty
		 
			// tell the user access denied
			$resArr = array("status" => "0", "message" => "Access denied.");
			
			//Add log data
			 $logs->Module 		= "Crime Sub Type";
			 $logs->Action		= "update";
			 $logs->Request		= "";
			 $logs->Response	= json_encode($resArr);
			 $logs->UserId		= "";
			 $logs->UserType	= "";			 
			 $logs->AddLog();
			
			// display message: unable to update Category master
			response($resArr, 401);
		}		
		
		break;	
		
	case 'GET':
			
		$bearerToken = getBearerToken();
		
		$access_token = isset($bearerToken) ? $bearerToken : "";
			
		// if jwt is not empty
		if($access_token){
			
			// decode jwt		
			$decoded  = $auth->validateToken($access_token);	
			
			$getArr = array();
			
			 // if decode succeed, show user details
			if($decoded["status"] == true) {
				
				$user_id 		= $decoded['TokenData']->data->user_id; // Decode ID
				$username 		= $decoded['TokenData']->data->username; // Username
				$role_id 		= $decoded['TokenData']->data->role_id; // Role ID
				$tokenArr 		= array("user_id" => (int)$user_id, 
										"username" => $username, 
										"role_id" => (int)$role_id
										);	
			
				$getArr = get_url_data();					
				
				$id 	= isset($getArr['id']) ? intval($getArr['id']) : 0;
				
				$getList = false;
					
					if($id == ""){ 
					
						$getList = $crimeType->getlisting();
						$resMsg = "Crime Type data get successfully.";
					}
					
					if($id > 0){
						
						$crime_type_exist = $crimeType->crime_type_exist($id);
					
						if($crime_type_exist == true){
							
							$getList = $crimeType->getlisting($id);
							$resMsg = "Crime Type data get successfully.";
							
						} else {
							
							// generate jwt
							//$access_token = $auth->generateToken($tokenArr);
							
							$resArr = array(
									"status" 		=> "0", 
									"message" 		=> "Crime Type ID does not exist.",
									"access_token" 	=> $access_token
								);
								
							 $logs->Module 		= "Crime Type";
							 $logs->Action		= "get";
							 $logs->Request		= json_encode($getArr);
							 $logs->Response	= json_encode($resArr);
							 $logs->UserId		= (int)$user_id;
							 $logs->UserType    = ""; 
							 $logs->AddLog();	
								
							response($resArr, 200);
						}
						
					}
					
				
					
					if($getList){
						
						// generate jwt
					//$access_token = $auth->generateToken($tokenArr);
							
						$resArr = array(
							"status" 		=> "1", 
							"message" 		=> $resMsg,
							"access_token" 	=> $access_token,
							"data" 			=> $getList
						);

						 $logs->Module 		= "Crime Type";
						 $logs->Action		= "get";
						 $logs->Request		= json_encode($getArr);
						 $logs->Response	= json_encode($resArr);
						 $logs->UserId		= (int)$user_id;
						 $logs->UserType    = ""; 		
						 $logs->AddLog();		
						
						response($resArr, 200);
						
					} else {
						
						$getList = array();
						
						// generate jwt
					//$access_token = $auth->generateToken($tokenArr);
						
						$resArr = array(
							"status" 		=> "0", 
							"message" 		=> "No data found.",
							"access_token" 	=> $access_token,
							"data" 			=> $getList
						);

						 $logs->Module 		= "Crime Type";
						 $logs->Action		= "get";
						 $logs->Request		= json_encode($getArr);
						 $logs->Response	= json_encode($resArr);
						 $logs->UserId		= (int)$user_id;
						 $logs->UserType    = ""; 
						 $logs->AddLog();		
						
						response($resArr, 200);
						
					}	
					
		} else {			 
				
				// show error message
				$resArr =array(
					"status" 	=> "0", 
					"message" 	=> "Invalid Authorization. Error : ". $decoded["TokenData"]
				);
				
				//Add log data
				 $logs->Module 		= "Crime Type";
				 $logs->Action		= "get";
				 $logs->Request		= "";
				 $logs->Response	= json_encode($resArr);
				 $logs->UserId		= "";
				 $logs->UserType	= "";	
				 $logs->AddLog();
				
				response($resArr, 401);
			}			
							
		}  else { // show error message if jwt is empty		 
			
			$resArr = array("status" => "0", "message" => "Access denied.");
			
			//Add log data
			 $logs->Module 		= "Crime Type";
			 $logs->Action		= "get";
			 $logs->Request		= "";
			 $logs->Response	= json_encode($resArr);
			 $logs->UserId		= "";
			 $logs->UserType	= "";	
			 $logs->AddLog();
			
			response($resArr, 401);
		}	

			
		
		break;
		
		
	case 'DELETE':	
	
		$bearerToken = getBearerToken();
		
		$access_token = isset($bearerToken) ? $bearerToken : "";
	
		// if jwt is not empty
		if($access_token)
		{
			
			// decode jwt
			$decoded = $auth->validateToken($access_token);
	
			$getArr = array();
			
			// if decode succeed, show user details
			if($decoded["status"] == true) {
					
				$user_id 		= $decoded['TokenData']->data->user_id; // Decode ID
				$username 		= $decoded['TokenData']->data->username; // Username
				$role_id 		= $decoded['TokenData']->data->role_id; // Role ID
				$tokenArr 		= array("user_id" => (int)$user_id, 
										"username" => $username, 
										"role_id" => (int)$role_id
										);	
			
					$getArr = get_url_data();
								
					// set CrimeType property values	
					$crimeType->id = isset($getArr['id']) ? intval($getArr['id']) : "";
					
					$error = array();
					
					if($crimeType->id == ""){
									
						$error[] = "Crime Type ID is required.";
						
					} 
					
					if(!is_numeric($crimeType->id)){
						
						$error[] = "Crime Type ID is must contain number.";
						
					}
					
					if(count($error) > 0){
							
					$errorMsg = implode(", ", $error);
						 
					// tell the user login failed
					$resArr = array("status" => "0", "message" => $errorMsg);						

					//Add log data
					 $logs->Module 		= "Crime Type";
					 $logs->Action		= "delete";
					 $logs->Request		= json_encode($getArr);
					 $logs->Response	= json_encode($resArr);
					 $logs->UserId		= (int)$user_id;
					 $logs->UserType    = ""; 
					 $logs->AddLog();

					response($resArr, 200);
					 
				 } else {
					 
					 $crime_type_exist = $crimeType->crime_type_exist($crimeType->id);
							
					if($crime_type_exist == true){
					 
					  $type_id = $crimeType->delete();
										
						if($type_id == true){

							// generate jwt
							//	$access_token = $auth->generateToken($tokenArr);
								 
							$resArr = array(
											"status" 		=> "1", 
											"message" 		=> "Crime Type was successfully deleted.",
											"access_token" 	=> $access_token
										);			
							
							//Add log data
							 $logs->Module 		= "Crime Type";
							 $logs->Action		= "delete";
							 $logs->Request		= json_encode($getArr);
							 $logs->Response	= json_encode($resArr);
							 $logs->UserId		= (int)$user_id;
							 $logs->UserType    = ""; 
							 $logs->AddLog();
				
							response($resArr, 200);
							
						 } else {
							 
							  // generate jwt
								// $access_token = $auth->generateToken($tokenArr);
							 
							 $resArr = array(
											"status" 		=> "1", 
											"message" 		=> "Unable to delete crime type.",
											"access_token" 	=> $access_token
										);
										
							//Add log data
							 $logs->Module 		= "Crime Type";
							 $logs->Action		= "delete";
							 $logs->Request		= json_encode($getArr);
							 $logs->Response	= json_encode($resArr);
							 $logs->UserId		= (int)$user_id;
							 $logs->UserType    = ""; 
							 $logs->AddLog();		
								 
							response($resArr, 401);
							 
						 }	// delete condition end	
						 
					} else {
								 
						// generate jwt
								//$access_token = $auth->generateToken($tokenArr);				 
							$resArr = array(
								"status" 		=> "0", 
								"message" 		=> "Crime Type ID does not exist.",
								"access_token" 	=> $access_token
							);
							
							 $logs->Module 		= "Crime Type";
							 $logs->Action		= "get";
							 $logs->Request		= json_encode($getArr);
							 $logs->Response	= json_encode($resArr);
							 $logs->UserId		= (int)$user_id;
							 $logs->UserType    = ""; 
							 $logs->AddLog();	
				
							response($resArr, 200);
						
					}	 	 
						 
					 
				 }
				 
			 } else {
					 
						// set response code				
						$resArr = array(
									"status" 	=> "0", 
									"message" 	=> "Invalid Authorization. Error : ". $decoded["TokenData"]
								);
							
						//Add log data
						 $logs->Module 		= "Crime Type";
						 $logs->Action		= "delete";
						 $logs->Request		= json_encode($getArr);
						 $logs->Response	= json_encode($resArr);
						 $logs->UserId		= "";
						 $logs->UserType	= "";
						 $logs->AddLog();			
										 
						response($resArr, 401);
					}
			 
		}  else { // show error message if jwt is empty
			 
				// tell the City access denied
				$resArr = array("status" => "0", "message" => "Access denied.");
				
				//Add log data
				 $logs->Module 		= "Crime Type";
				 $logs->Action		= "delete";
				 $logs->Request		= "";
				 $logs->Response	= json_encode($resArr);
				 $logs->UserId		= "";
				 $logs->UserType	= "";
				 $logs->AddLog();
							 
				response($resArr, 401);
				
		}
	
		break;
		
	case "OPTIONS": //Handle Preflighted requests for CORS issue
		
		// set response code		
		header('Content-Type: text/plain');
		
		response("", 200);
		
		break;
		
	default:
	
		// Invalid Request Method
		$resArr = array("status" => "0", "message" => "Method Not Allowed.");						
		// display message: unable to create user
		response($resArr, 405);	
		
		break;
}

?>