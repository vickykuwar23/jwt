<?php

/*** files needed to connect to database and class***/
define("ROOT_PATH", realpath(__DIR__ . '/../../../..'));
include_once ROOT_PATH.'/constant.php';

include_once CONFIG_PATH.'functions.php';
include_once CONFIG_PATH.'database.php';
include_once RESOURCE_MASTERPATH.'evidence-type/evidence_type.php';
include_once RESOURCE_PATH.'logs/logs.php';
include_once RESOURCE_PATH.'auth/auth.php';

// Required headers
requireHeaders();
 
/*** get database connection ***/
$database 	= new Database();
$db 		= $database->getConnection();
$mdb 		= $database->getMongoConnection();
 
// instantiate EvidenceType object
$evidence  = new EvidenceType($db);
$logs  = new Logs($mdb);
$auth  = new Auth();

// Get Request Method 
$request_method		= get_request_method();

switch($request_method)
{
	
	case "POST":

		$bearerToken = getBearerToken();
		
		$access_token = isset($bearerToken) ? $bearerToken : "";

		// if jwt is not empty
		if($access_token){	

			// decode jwt
			$decoded = $auth->validateToken($access_token);
			
			$data 	= array();
			$getArr = array();
			$reqArr = array();
			
			// if decode succeed, show user details			
			if($decoded['status'] == true){	

				$user_id 		= $decoded['TokenData']->data->user_id; // Decode ID
				$username 		= $decoded['TokenData']->data->username; // Username
				$role_id 		= $decoded['TokenData']->data->role_id; // Role ID
				$tokenArr 		= array("user_id" => (int)$user_id, 
										"username" => $username, 
										"role_id" => (int)$role_id
										);	
		
						// get posted data
						$data 	= get_request_body();
						
						if($data['status'] == true){
							
							$error = array();
							
							// set evidence property
							$evidence->evidence_type_name = isset($data['requestData']['evidence_type_name']) ? $data['requestData']['evidence_type_name'] : "";
							
							if($evidence->evidence_type_name == ""){
								
								$error[] = "Please enter Evidence Type Name.";
								
							} 
							if (!preg_match('/^[\p{L} ]+$/u', $evidence->evidence_type_name)){
								
							  $error[] = 'Evidence Type Name must contain letters and spaces only!';
							  
							}
							
							
							if(count($error) > 0){
								
									$errorMsg = implode(", ", $error);
									
									// tell the user login failed
									$resArr = array("status" => "0", "message" => $errorMsg);
									
									 //Add log data
									 $logs->Module 		= "Evidence Type";
									 $logs->Action		= "create";
									 $logs->Request		= json_encode($data);
									 $logs->Response	= json_encode($resArr);
									 $logs->UserId		= (int)$user_id;
									 $logs->UserType    = ""; 
									 $logs->AddLog();
										 
									// tell the user login failed
									 response($resArr, 400);
								
							} else {
								
								$insertId = $evidence->create();
								
								if($insertId > 0){
									
									// generate jwt
									//$access_token = $auth->generateToken($tokenArr);
									
									$resArr = array("status" => "1", "message" => "Evidence Type created successfully.",
									"access_token" 	=> $access_token);
											
									 $logs->Module 		= "Evidence Type";
									 $logs->Action		= "create";
									 $logs->Request		= json_encode($data);
									 $logs->Response	= json_encode($resArr);
									 $logs->UserId		= (int)$user_id;
									 $logs->UserType    = ""; 
									 $logs->AddLog();
									
									// display message: Evidence was created
									 response($resArr, 200);
								 
								 } else { // message if unable to create Evidence
								 
								 // generate jwt
									//$access_token = $auth->generateToken($tokenArr);
									
										// display message: unable to create Evidence
										$resArr = array("status" => "0", 
													"message" => "Unable to create data.",
													"access_token" 	=> $access_token
													);
										
										 $logs->Module 		= "Evidence Type";
										 $logs->Action		= "create";
										 $logs->Request		= json_encode($data);
										 $logs->Response	= json_encode($resArr);
										 $logs->UserId		= (int)$user_id;
										 $logs->UserType    = ""; 
										 $logs->AddLog();
										
										 // display message: unable to create Evidence
										 response($resArr, 400);
										
									} // Insert ID Check
									
							}
						
						} else {
							
							// display message: unable to create Evidence
							$resArr = array("status" => "0", "message" => $data["requestData"]);
							
											
							 $logs->Module 		= "Evidence Type";
							 $logs->Action		= "create";
							 $logs->Request		= json_encode($data);
							 $logs->Response	= json_encode($resArr);
							 $logs->UserId		= (int)$user_id;
							 $logs->UserType    = ""; 
							 $logs->AddLog();
							 
							response($resArr, 400);
						}
		
		 } else {
				
					// show error message
					$resArr = array(
						"status" 	=> "0", 
						"message" 	=> "Invalid Authorization. Error : ". $decoded["TokenData"]
					);
					
					
					// Merge Array Post And GET Values
					$reqArr = array_merge($data, $getArr);
					
					//Add log data
					 $logs->Module 		= "Evidence Type";
					 $logs->Action		= "create";
					 $logs->Request		= json_encode($reqArr);
					 $logs->Response	= json_encode($resArr);
					 $logs->UserId		= "";
					 $logs->UserType    = "";	
					 $logs->AddLog();
					
					response($resArr, 401);
					
			} // Invalid Authorization error		
					
		
		}  else { // show error message if jwt is empty
		 
			// tell the user access denied
			$resArr = array("status" => "0", "message" => "Access denied.");
			
			 $logs->Module 		= "Evidence Type";
			 $logs->Action		= "create";
			 $logs->Request		= "";
			 $logs->Response	= json_encode($resArr);
			 $logs->UserId		= "";
			 $logs->UserType	= "";
			 $logs->AddLog();
			
			// display message: unable to create user
			response($resArr, 401);
		}	
		
		break;
		
	case "PUT":

		 $bearerToken = getBearerToken();
		
		$access_token = isset($bearerToken) ? $bearerToken : "";
	
		// if jwt is not empty
		if($access_token){	

			// decode jwt
			$decoded = $auth->validateToken($access_token);
			
			$data 	= array();
			$getArr = array();
			$reqArr = array();
			
			if($decoded['status'] == true){	

				$user_id 		= $decoded['TokenData']->data->user_id; // Decode ID
				$username 		= $decoded['TokenData']->data->username; // Username
				$role_id 		= $decoded['TokenData']->data->role_id; // Role ID
				$tokenArr 		= array("user_id" => (int)$user_id, 
										"username" => $username, 
										"role_id" => (int)$role_id
										);	
	    
						// Get URL Data	
						$getArr = get_url_data();
						
						$error = array();
						
						// Get ID From URL		
						$evidence->id = isset($getArr['id']) ? intval($getArr['id']) : "";
						
						if($evidence->id == ""){
										
							$error[] = "Evidence Type ID is required.";
							
						} 
						
						
						if(count($error) > 0){
								
							$errorMsg = implode(", ", $error);
							
							// tell the Evidence error occur
							$resArr = array("status" => "0", "message" => $errorMsg);
							
							// Merge Array Post And GET Values
								$reqArr = array_merge($data, $getArr);
							
							 //Add log data
							 $logs->Module 		= "Evidence Type";
							 $logs->Action		= "update";
							 $logs->Request		= json_encode($reqArr);
							 $logs->Response	= json_encode($resArr);
							 $logs->UserId		= (int)$user_id;
							 $logs->UserType    = ""; 
							 $logs->AddLog();
								 
							// tell the Evidence error occur
							 response($resArr, 400);
						
						}
						
						// get posted data
						$data = get_request_body();
						
						if($data['status'] == true){
							
							$error = array();
							
							// set evidence property
							$evidence->evidence_type_name = isset($data['requestData']['evidence_type_name']) ? $data['requestData']['evidence_type_name'] : "";
							
							$evidence->is_active = isset($data['requestData']['is_active']) ? $data['requestData']['is_active'] : "";
							
							if(isset($data['requestData']['is_active'])){
									
								$updateStatus 		= "UpdateStatus";
								$UpdateMsg			= "Evidence Type status successfully updated.";
								$evidence->is_active 	= $data['requestData']['is_active'];
								
								if($evidence->is_active == ""){
							
										$error[] = "Is Active is required.";
										
								} 
										
							} else {			
								
								$updateStatus 		= "UpdateField";
								$UpdateMsg			= "Evidence Type updated successfully.";
								
								if($evidence->evidence_type_name == ""){
									
									$error[] = "Please enter Evidence Type Name.";
									
								} 
								if (!preg_match('/^[\p{L} ]+$/u', $evidence->evidence_type_name)){
									
									$error[] = 'Evidence Type Name must contain letters and spaces only!';
								  
								}
								
							} // URL ID validation required			
							
							if(count($error) > 0){
								
									$errorMsg = implode(", ", $error);
									
									// tell the Evidence error occur
									$resArr = array("status" => "0", "message" => $errorMsg);
									
									// Merge Array Post And GET Values
										$reqArr = array_merge($data, $getArr);
									
									 //Add log data
									 $logs->Module 		= "Evidence Type";
									 $logs->Action		= "update";
									 $logs->Request		= json_encode($reqArr);
									 $logs->Response	= json_encode($resArr);
									 $logs->UserId		= (int)$user_id;
									 $logs->UserType    = ""; 
									 $logs->AddLog();
										 
									// tell the Evidence error occur
									 response($resArr, 400);
								
							} else {
								
								$evidence_type_id_exist = $evidence->evidence_type_id_exist($evidence->id);
									
								if($evidence_type_id_exist == true){
								
								$type_id = $evidence->update($updateStatus);
								
								if($type_id  == true){
									
									// generate jwt
									//	$access_token = $auth->generateToken($tokenArr);
									
									$resArr = array("status" => "1", 
									"message" => $UpdateMsg,
									"access_token" 	=> $access_token);
									
									// Merge Array Post And GET Values
										$reqArr = array_merge($data, $getArr);
											
									 $logs->Module 		= "Evidence Type";
									 $logs->Action		= "update";
									 $logs->Request		= json_encode($reqArr);
									 $logs->Response	= json_encode($resArr);
									 $logs->UserId		= (int)$user_id;
									 $logs->UserType    = ""; 
									 $logs->AddLog();
									
									// display message: Evidence was updated
									 response($resArr, 200);
								 
								 } else { // message if unable to Evidence
								 
								 // generate jwt
									//	$access_token = $auth->generateToken($tokenArr);
									
										// display message: unable to update Evidence
										$resArr = array("status" => "0", "message" => "Unable to update data.",
										"access_token" 	=> $access_token);
										
										// Merge Array Post And GET Values
											$reqArr = array_merge($data, $getArr);
										
										 $logs->Module 		= "Evidence Type";
										 $logs->Action		= "update";
										 $logs->Request		= json_encode($reqArr);
										 $logs->Response	= json_encode($resArr);
										 $logs->UserId		= (int)$user_id;
										 $logs->UserType    = ""; 
										 $logs->AddLog();
										
										 // display message: unable to update Evidence
										 response($resArr, 400);
										
									} // Insert ID Check
									
								} else {
									
									// generate jwt
								//$access_token = $auth->generateToken($tokenArr);
						
									$resArr = array(
											"status" 		=> "0", 
											"message" 		=> "Evidence Type ID does not exist.",
											"access_token" 	=> $access_token
										);
										
										// Merge Array Post And GET Values
								$reqArr = array_merge($data, $getArr);	
										
									 $logs->Module 		= "Evidence Type";
									 $logs->Action		= "update";
									 $logs->Request		= json_encode($reqArr);
									 $logs->Response	= json_encode($resArr);
									 $logs->UserId		= (int)$user_id;
									 $logs->UserType    = "";  
									 $logs->AddLog();	
										
									response($resArr, 200);
									
								}
									
							}
						
						} else {
							
							// display message: unable to update EvidenceType
							$resArr = array("status" => "0", "message" => $data["requestData"]);
							
							// Merge Array Post And GET Values
								$reqArr = array_merge($data, $getArr);
							
							 $logs->Module 		= "Evidence Type";
							 $logs->Action		= "update";
							 $logs->Request		= json_encode($reqArr);
							 $logs->Response	= json_encode($resArr);
							 $logs->UserId		= (int)$user_id;
							 $logs->UserType    = ""; 
							 $logs->AddLog();
							 
							response($resArr, 400);
						}
		
		} else {
				
						// show error message
						$resArr = array(
							"status" 	=> "0", 
							"message" 	=> "Invalid Authorization. Error : ". $decoded["TokenData"]
						);
						
						// Merge Array Post And GET Values
						$reqArr = array_merge($data, $getArr);
						
						//Add log data
						 $logs->Module 		= "Evidence Type";
						 $logs->Action		= "update";
						 $logs->Request		= json_encode($reqArr);
						 $logs->Response	= json_encode($resArr);
						 $logs->UserId		= "";
						 $logs->UserType    = "";	
						 $logs->AddLog();
						
						response($resArr, 401);
				}
		
			}  else { // show error message if jwt is empty
		 
			// tell the user access denied
			$resArr = array("status" => "0", "message" => "Access denied.");
			
			//Add log data
			 $logs->Module 		= "Evidence Type";
			 $logs->Action		= "update";
			 $logs->Request		= "";
			 $logs->Response	= json_encode($resArr);
			 $logs->UserId		= "";
			 $logs->UserType	= "";			 
			 $logs->AddLog();
			
			// display message: unable to update Category master
			response($resArr, 401);
		}		
						
		break;	
		
	case 'GET':
			
		$bearerToken = getBearerToken();
		
		$access_token = isset($bearerToken) ? $bearerToken : "";
			
		// if jwt is not empty
		if($access_token){
			
			// decode jwt		
			$decoded  = $auth->validateToken($access_token);	
			
			$getArr = array();
			
			 // if decode succeed, show user details
			if($decoded["status"] == true) {
				
				$user_id 		= $decoded['TokenData']->data->user_id; // Decode ID
				$username 		= $decoded['TokenData']->data->username; // Username
				$role_id 		= $decoded['TokenData']->data->role_id; // Role ID
				$tokenArr 		= array("user_id" => (int)$user_id, 
										"username" => $username, 
										"role_id" => (int)$role_id
										);	
			
				$getArr = get_url_data();					
				
				$id 	= isset($getArr['id']) ? intval($getArr['id']) : 0;
				
				$getList = false;
				
				if($id == ""){ 
				
					$getList = $evidence->getlisting();
				}
				
				if($id > 0){
					
				$evidence_type_id_exist = $evidence->evidence_type_id_exist($id);
				
					if($evidence_type_id_exist == true){
						
						$getList = $evidence->getlisting($id);
						
					} else {
						
						// generate jwt
						//	$access_token = $auth->generateToken($tokenArr);
						
						$resArr = array(
								"status" 		=> "0", 
								"message" 		=> "Evidence Type ID does not exist.",
								"access_token" 	=> $access_token
							);
							
						 $logs->Module 		= "Evidence Type";
						 $logs->Action		= "get";
						 $logs->Request		= json_encode($getArr);
						 $logs->Response	= json_encode($resArr);
						 $logs->UserId		= (int)$user_id;
						 $logs->UserType    = ""; 
						 $logs->AddLog();	
							
						response($resArr, 200);
					}
					
				}	
				
				if($getList){
					
					// generate jwt
					//$access_token = $auth->generateToken($tokenArr);
							
					$resArr = array(
						"status" 		=> "1", 
						"message" 		=> "Evidence Type data get successfully.",
						"access_token" 	=> $access_token,
						"data" 			=> $getList
					);

					 $logs->Module 		= "Evidence Type";
					 $logs->Action		= "get";
					 $logs->Request		= json_encode($getArr);
					 $logs->Response	= json_encode($resArr);
					 $logs->UserId		= (int)$user_id;
					 $logs->UserType    = ""; 	
					 $logs->AddLog();		
					
					response($resArr, 200);
					
				} else {
					
					$getList = array();
					
					// generate jwt
					//$access_token = $auth->generateToken($tokenArr);
					
					$resArr = array(
						"status" 		=> "0", 
						"message" 		=> "No data found.",
						"access_token" 	=> $access_token,
						"data" 			=> $getList
					);

					 $logs->Module 		= "Evidence Type";
					 $logs->Action		= "get";
					 $logs->Request		= json_encode($getArr);
					 $logs->Response	= json_encode($resArr);
					 $logs->UserId		= (int)$user_id;
					 $logs->UserType    = ""; 	
					 $logs->AddLog();		
					
					response($resArr, 200);
					
				}
				
						
		} else {			 
				
				// show error message
				$resArr =array(
					"status" 	=> "0", 
					"message" 	=> "Invalid Authorization. Error : ". $decoded["TokenData"]
				);
				
				//Add log data
				 $logs->Module 		= "Evidence Type";
				 $logs->Action		= "get";
				 $logs->Request		= "";
				 $logs->Response	= json_encode($resArr);
				 $logs->UserId		= "";
				 $logs->UserType	= "";	
				 $logs->AddLog();
				
				response($resArr, 401);
			}			
							
		}  else { // show error message if jwt is empty		 
			
			$resArr = array("status" => "0", "message" => "Access denied.");
			
			//Add log data
			 $logs->Module 		= "Evidence Type";
			 $logs->Action		= "get";
			 $logs->Request		= "";
			 $logs->Response	= json_encode($resArr);
			 $logs->UserId		= "";
			 $logs->UserType	= "";	
			 $logs->AddLog();
			
			response($resArr, 401);
		}
		
		break;
		
		
	case 'DELETE':	
	
		$bearerToken = getBearerToken();
		
		$access_token = isset($bearerToken) ? $bearerToken : "";
	
		// if jwt is not empty
		if($access_token)
		{
			
			// decode jwt
			$decoded = $auth->validateToken($access_token);
	
			$getArr = array();
			
			// if decode succeed, show user details
			if($decoded["status"] == true) {
					
				$user_id 		= $decoded['TokenData']->data->user_id; // Decode ID
				$username 		= $decoded['TokenData']->data->username; // Username
				$role_id 		= $decoded['TokenData']->data->role_id; // Role ID
				$tokenArr 		= array("user_id" => (int)$user_id, 
										"username" => $username, 
										"role_id" => (int)$role_id
										);	
			
							$getArr = get_url_data();
										
							// set EvidenceType property values	
							$evidence->id = isset($getArr['id']) ? intval($getArr['id']) : "";
							
							$error = array();
							
							if($evidence->id == ""){
											
								$error[] = "Evidence Type ID is required.";
								
							} 
							
							if(!is_numeric($evidence->id)){
								
								$error[] = "Evidence Type ID is must contain number.";
								
							}
							
							if(count($error) > 0){
									
							$errorMsg = implode(", ", $error);
								 
							// tell the user login failed
							$resArr = array("status" => "0", "message" => $errorMsg);						

							//Add log data
							 $logs->Module 		= "Evidence Type";
							 $logs->Action		= "delete";
							 $logs->Request		= json_encode($getArr);
							 $logs->Response	= json_encode($resArr);
							 $logs->UserId		= (int)$user_id;
							 $logs->UserType    = ""; 
							 $logs->AddLog();

							response($resArr, 200);
							 
						 } else {
							 
							 
							 $evidence_type_id_exist = $evidence->evidence_type_id_exist($evidence->id);
								
							if($evidence_type_id_exist == true){
							 
									 $delete_type = $evidence->delete();
														
										if($delete_type  == true){

												// generate jwt
										//$access_token = $auth->generateToken($tokenArr);
												 
											$resArr = array(
															"status" 		=> "1", 
															"message" 		=> "Evidence Type was successfully deleted.",
															"access_token" 	=> $access_token
														);			
											
											//Add log data
											 $logs->Module 		= "Evidence Type";
											 $logs->Action		= "delete";
											 $logs->Request		= json_encode($getArr);
											 $logs->Response	= json_encode($resArr);
											 $logs->UserId		= (int)$user_id;
											 $logs->UserType    = ""; 
											 $logs->AddLog();
								
											response($resArr, 200);
											
										 } else {
											 
											   // generate jwt
										 //$access_token = $auth->generateToken($tokenArr);
											 
											 $resArr = array(
															"status" 		=> "1", 
															"message" 		=> "Unable to delete evidence type.",
															"access_token" 	=> $access_token
														);
														
											//Add log data
											 $logs->Module 		= "Evidence Type";
											 $logs->Action		= "delete";
											 $logs->Request		= json_encode($getArr);
											 $logs->Response	= json_encode($resArr);
											 $logs->UserId		= (int)$user_id;
											 $logs->UserType    = ""; 
											 $logs->AddLog();		
												 
											response($resArr, 401);
											 
										 }	// delete condition end	
								 
								 } else {
									
									// generate jwt
								//$access_token = $auth->generateToken($tokenArr);	
											 
									$resArr = array(
										"status" 		=> "0", 
										"message" 		=> "Evidence Type ID does not exist.",
										"access_token" 	=> $access_token
									);
									
									 $logs->Module 		= "Evidence Type";
									 $logs->Action		= "delete";
									 $logs->Request		= json_encode($getArr);
									 $logs->Response	= json_encode($resArr);
									 $logs->UserId		= (int)$user_id;
									 $logs->UserType    = ""; 
									 $logs->AddLog();	
						
									response($resArr, 200);
									
								}		
							 
						 }
	 } else {
					 
						// set response code				
						$resArr = array(
									"status" 	=> "0", 
									"message" 	=> "Invalid Authorization. Error : ". $decoded["TokenData"]
								);
							
						//Add log data
						 $logs->Module 		= "Evidence Type";
						 $logs->Action		= "delete";
						 $logs->Request		= json_encode($getArr);
						 $logs->Response	= json_encode($resArr);
						 $logs->UserId		= "";
						 $logs->UserType	= "";
						 $logs->AddLog();			
										 
						response($resArr, 401);
					}
			 
		}  else { // show error message if jwt is empty
			 
				// tell the City access denied
				$resArr = array("status" => "0", "message" => "Access denied.");
				
				//Add log data
				 $logs->Module 		= "Evidence Type";
				 $logs->Action		= "delete";
				 $logs->Request		= "";
				 $logs->Response	= json_encode($resArr);
				 $logs->UserId		= "";
				 $logs->UserType	= "";
				 $logs->AddLog();
							 
				response($resArr, 401);
				
		}
	
	
		break;
		
	case "OPTIONS": //Handle Preflighted requests for CORS issue
		
		// set response code		
		header('Content-Type: text/plain');
		
		response("", 200);
		
		break;
		
	default:
	
		// Invalid Request Method
		$resArr = array("status" => "0", "message" => "Method Not Allowed.");						
		// display message: unable to create user
		response($resArr, 405);	
		
		break;
}

?>