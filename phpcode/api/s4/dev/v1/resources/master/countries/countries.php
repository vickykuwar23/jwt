<?php
/*** 'countries' object ***/
class Countries{
 
    /*** database connection and table name ***/
    private $conn;
    private $table_name 	= 'public."countries"';
	
    /*** object properties ***/ 
	public $id;
    public $country_code;
	public $country_name;
	public $phone_code;
	public $is_active;
 
    /*** constructor ***/
    public function __construct($db){
        $this->conn = $db;
		$this->createdOn = date("Y-m-d H:i:s");
    }
	
	/*** check if given Country ID exist in the database ***/
	function country_id_exist($id){
	 
		/*** query to check if Country exists ***/
		$query = 'SELECT "country_id"
					FROM '.$this->table_name.'
					WHERE "country_id" = ? AND "is_deleted" = ?
					LIMIT 1';
					
		/*** prepare the query ***/
		$stmt = $this->conn->prepare( $query );
	 
		/*** bind given Country ID value ***/
		$stmt->bindParam(1, $id);
		$stmt->bindValue(2, false, PDO::PARAM_BOOL);
		/*** execute the query ***/
		$stmt->execute();
	 
		/*** get number of rows ***/
		$num = $stmt->rowCount();
	 
		/*** if Country ID exists, assign values to object properties for easy access and use for php sessions ***/
		if($num > 0){
	 
			/*** return true because Country ID exists in the database ***/
			return true;
		}
	 
		/*** return false if Country ID does not exist in the database ***/
		return false;
	}
	
	function getlisting($id = 0){
		
		/*** query to check if country exists ***/
		if($id == 0){
			
			$id = 101;
			$query = 'SELECT "country_id", "country_code", "country_name", "phone_code", "is_active", "created_on" 
					FROM '.$this->table_name.' 
					WHERE "is_deleted" = ? AND "country_id" = ? ORDER BY "country_id" ASC';	
					
			/*** prepare the query ***/
			$stmt = $this->conn->prepare( $query );		
		 
			$stmt->bindValue(1, false, PDO::PARAM_BOOL);
			$stmt->bindValue(2, $id);
			
		} else {
			
			$query = 'SELECT "country_id", "country_code", "country_name", "phone_code", "is_active", "created_on" 
					FROM '.$this->table_name.' 
					WHERE "is_deleted" = ? AND "country_id" = ? ORDER BY "country_id" ASC';
			
			/*** prepare the query ***/
			$stmt = $this->conn->prepare( $query );		
		 
			$stmt->bindValue(1, false, PDO::PARAM_BOOL);
			$stmt->bindValue(2, $id);
						
		}
		
		/*** execute the query ***/
		$stmt->execute();
	 
		/*** get number of rows ***/
		$num = $stmt->rowCount();
	 
		/*** if country exists, assign values to object properties for easy access and use for php sessions ***/
		if($num>0){
	 
			/*** get record details / values ***/			
			while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
					$getData[] = $row;
			}
	 
			/*** return true because country exists in the database ***/
			return $getData;
		}
	 
		/*** return false if country does not exist in the database ***/
		return false;
		
	}
	
	function create(){
		
$query = 'INSERT INTO '.$this->table_name.' ("country_code", "country_name", "phone_code", "is_active", "is_deleted", "created_on") VALUES (:country_code, :country_name, :phone_code, :is_active, :is_deleted, :created_on) RETURNING country_id';		
		
		/*** prepare the query ***/
		$stmt = $this->conn->prepare($query);		
	 
		/*** sanitize ***/
		$this->is_active			=  '1';
		$this->is_deleted			=  '0';
		$this->created_on			=  $this->createdOn;
		
		$stmt->bindParam(':country_code', $this->country_code);
		$stmt->bindParam(':country_name', $this->country_name);
		$stmt->bindParam(':phone_code', $this->phone_code);
		$stmt->bindParam(':is_active', $this->is_active);
		$stmt->bindParam(':is_deleted', $this->is_deleted);
		$stmt->bindParam(':created_on', $this->created_on);
			 
		/*** execute the query, also check if query was successful ***/		
		if($stmt->execute()){
			
			$result = $stmt->fetch(PDO::FETCH_ASSOC);
						
			return $result["country_id"];
			
		} 
		
		return false;
		
	}
	
	
	/*** update record ***/
	public function update($updateStatus){
		
		/*** SQL QUERY ***/
		$query = 'UPDATE ' . $this->table_name . ' SET ';
		
		if($updateStatus == 'UpdateField'){
			
			$query .= '"country_code" = :country_code,  "country_name" = :country_name, "phone_code" = :phone_code, ';
			
		} else {
			
			$query .= '"is_active" = :is_active, ';
		}	
		
		
		$query .= '"updated_on" = :updated_on WHERE "country_id" = :country_id';
		
		/*** prepare the query ***/
		$stmt = $this->conn->prepare($query);
		
		$this->updated_on			=  $this->createdOn;
			
        if($this->country_code!= "" && $this->country_name!= "" && $this->phone_code!= ""){
			
			/*** bind the values from the form ***/		
			$stmt->bindParam(':country_code', $this->country_code);
			$stmt->bindParam(':country_name', $this->country_name);
			$stmt->bindParam(':phone_code', $this->phone_code);
		
		}	

		if($this->is_active!= ""){
			
			$stmt->bindParam(':is_active', $this->is_active);
		} 
		
		$stmt->bindParam(':updated_on', $this->updated_on);		
		
		/*** unique ID of record to be edited ***/
		$stmt->bindParam(':country_id', $this->id);
	 
		/*** execute the query ***/
		if($stmt->execute()){
			
			return true;
		}
	 
		return false;
		
	} // Function End
	
	
	function delete(){
		
		$query = 'UPDATE ' . $this->table_name . ' SET
						"deleted_on"	 	= :deleted_on,
						"is_deleted"	= :is_deleted
					  WHERE "country_id" 	= :country_id';
					  
		$stmt = $this->conn->prepare($query);	
		
		$this->is_deleted	=	'1';
		$this->deleted_on  	=	date("Y-m-d H:i:s");
		
		$stmt->bindParam(':is_deleted', $this->is_deleted);
		$stmt->bindParam(':deleted_on', $this->deleted_on);
		
		$stmt->bindParam(':country_id', $this->id);
		
		if($stmt->execute()){
			
			return true;
		}
	 
		return false;
			
	}
	
}