<?php
/*** 'city' object ***/
class City{
 
    /*** database connection and table name ***/
    private $conn;
    private $table_name = 'public."city"';
	private $table_state = 'public."states"';
	
    /*** object properties ***/ 
	public $id;
    public $state_id;
	public $city_name;
	public $is_active;
 
    /*** constructor ***/
    public function __construct($db){
        $this->conn = $db;
		$this->createdOn = date("Y-m-d H:i:s");
    }
	
	/*** check if given City ID exist in the database ***/
	function city_exist($id){
	 
		/*** query to check if City exists ***/
		$query = 'SELECT "city_id"
					FROM '.$this->table_name.'
					WHERE "city_id" = ? AND "is_deleted" = ?
					LIMIT 1';
					
		/*** prepare the query ***/
		$stmt = $this->conn->prepare( $query );
	 
		/*** bind given City ID value ***/
		$stmt->bindParam(1, $id);
		$stmt->bindValue(2, false, PDO::PARAM_BOOL);
		/*** execute the query ***/
		$stmt->execute();
	 
		/*** get number of rows ***/
		$num = $stmt->rowCount();
	 
		/*** if City ID exists, assign values to object properties for easy access and use for php sessions ***/
		if($num > 0){
	 
			/*** return true because City ID exists in the database ***/
			return true;
		}
	 
		/*** return false if City ID does not exist in the database ***/
		return false;
	}
	
	function getlisting($id = 0, $type = false){
		
		/*** query to check if city exists ***/
		if($id == 0){
			
			$country_id = 101;	// for India country only
			
			$query = 'SELECT S."state_id", S."state_name", C."city_id", 
						C."city_name", C."is_active", C."created_on" 
						FROM '.$this->table_name.' AS C JOIN 
						'. $this->table_state .' AS S ON
						C."state_id" = S."state_id"
					WHERE C."is_deleted" = ? AND S."country_id" = ? ORDER BY C."city_id" ASC';			
					
			/*** prepare the query ***/
			$stmt = $this->conn->prepare( $query );		
		 
			$stmt->bindValue(1, false, PDO::PARAM_BOOL);
			$stmt->bindValue(2, $country_id);
			
		} else {
			
			if($type == 'cities'){
				
				$query = 'SELECT S."state_id", S."state_name", C."city_id", 
						C."city_name", C."is_active", C."created_on" 
						FROM '.$this->table_name.' AS C JOIN 
						'. $this->table_state .' AS S ON
						C."state_id" = S."state_id"
					WHERE C."is_deleted" = ? AND S."state_id" = ? ORDER BY C."city_id" ASC';
				
				
			} else {
				
				$query = 'SELECT S."state_id", S."state_name", C."city_id", 
						C."city_name", C."is_active", C."created_on" 
						FROM '.$this->table_name.' AS C JOIN 
						'. $this->table_state .' AS S ON
						S.state_id = C."state_id"
					WHERE C."is_deleted" = ? AND C."city_id" = ?  ORDER BY C."city_id" ASC';
				
			}
			
			/*** prepare the query ***/
			$stmt = $this->conn->prepare( $query );		
		 
			$stmt->bindValue(1, false, PDO::PARAM_BOOL);
			$stmt->bindValue(2, $id);
						
		}
		
		/*** execute the query ***/
		$stmt->execute();
	 
		/*** get number of rows ***/
		$num = $stmt->rowCount();
	 
		/*** if city exists, assign values to object properties for easy access and use for php sessions ***/
		if($num>0){
	 
			/*** get record details / values ***/			
			while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
					$getData[] = $row;
			}
	 
			/*** return true because city exists in the database ***/
			return $getData;
		}
	 
		/*** return false if city does not exist in the database ***/
		return false;
		
	}
	
	function create(){
		
		$query = 'INSERT INTO '.$this->table_name.' ("city_name", "state_id", "is_active", "is_deleted", "created_on") VALUES (:city_name, :state_id, :is_active, :is_deleted, :created_on) RETURNING city_id';		
		
		/*** prepare the query ***/
		$stmt = $this->conn->prepare($query);		
	 
		/*** sanitize ***/
		$this->is_active			=  '1';
		$this->is_deleted			=  '0';
		$this->created_on			=  $this->createdOn;
		
		$stmt->bindParam(':city_name', $this->city_name);
		$stmt->bindParam(':state_id', $this->state_id);
		$stmt->bindParam(':is_active', $this->is_active);
		$stmt->bindParam(':is_deleted', $this->is_deleted);
		$stmt->bindParam(':created_on', $this->created_on);
		
	 
		/*** execute the query, also check if query was successful ***/		
		if($stmt->execute()){
			
			$result = $stmt->fetch(PDO::FETCH_ASSOC);
						
			return $result["city_id"];
			
		}  
		
		return false;
		
	}
	
	
	/*** update record ***/
	public function update($updateStatus){
		
		/*** SQL QUERY ***/
		$query = 'UPDATE ' . $this->table_name . ' SET ';
		
		if($updateStatus == 'UpdateField'){
			
			$query .= '"city_name" = :city_name,  "state_id" = :state_id, '; 	
			
		} else {
			
			$query .= '"is_active" = :is_active, ';
		}
		
		$query .= '"updated_on" = :updated_on WHERE "city_id" = :city_id';
		
		/*** prepare the query ***/
		$stmt = $this->conn->prepare($query);
		
		$this->updated_on			=  $this->createdOn;
		
		if($this->is_active!=""){
			
			/*** bind the values from the form ***/	
			$stmt->bindParam(':is_active', $this->is_active);
		} 
		
		if($this->city_name!= "" && $this->state_id!= ""){
			
			/*** bind the values from the form ***/		
			$stmt->bindParam(':city_name', $this->city_name);
			$stmt->bindParam(':state_id', $this->state_id);
			
		}				 
		
		$stmt->bindParam(':updated_on', $this->updated_on);		
		
		/*** unique ID of record to be edited ***/
		$stmt->bindParam(':city_id', $this->id);
	 
		/*** execute the query ***/
		if($stmt->execute()){
			
			return true;
		} 
	 
		return false;
		
	} // Function End
	
	
	// Delete Operations
	function delete(){
		
		$query = 'UPDATE ' . $this->table_name . ' SET
						"deleted_on"	 	= :deleted_on,
						"is_deleted"	= :is_deleted
					  WHERE "city_id" 	= :city_id';
					  
		$stmt = $this->conn->prepare($query);	
		
		$this->is_deleted	=	'1';
		$this->deleted_on  	=	date("Y-m-d H:i:s");
		
		$stmt->bindParam(':is_deleted', $this->is_deleted);
		$stmt->bindParam(':deleted_on', $this->deleted_on);
		
		$stmt->bindParam(':city_id', $this->id);
		
		if($stmt->execute()){
			
			return true;
			
		}
	 
		return false;
			
	}
	
}