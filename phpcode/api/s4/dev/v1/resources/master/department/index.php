<?php

/*** files needed to connect to database and class***/
define("ROOT_PATH", realpath(__DIR__ . '/../../../..'));
include_once ROOT_PATH.'/constant.php';

include_once CONFIG_PATH.'functions.php';
include_once CONFIG_PATH.'database.php';
include_once RESOURCE_MASTERPATH.'department/department.php';
include_once RESOURCE_PATH.'logs/logs.php';
include_once RESOURCE_PATH.'auth/auth.php';

// Required headers
requireHeaders();
 
/*** get database connection ***/
$database 	= new Database();
$db 		= $database->getConnection();
$mdb 		= $database->getMongoConnection();
 
// instantiate Department object
$department  = new Department($db);
$logs  = new Logs($mdb);
$auth  = new Auth();

// Get Request Method 
$request_method		= get_request_method();

switch($request_method)
{
	
	case "POST":
	
	$bearerToken = getBearerToken();
		
		$access_token = isset($bearerToken) ? $bearerToken : "";

		// if jwt is not empty
		if($access_token){	

			// decode jwt
			$decoded = $auth->validateToken($access_token);
			
			$data 	= array();
			$getArr = array();
			$reqArr = array();
			
			// if decode succeed, show user details			
			if($decoded['status'] == true){	

				$user_id 		= $decoded['TokenData']->data->user_id; // Decode ID
				$username 		= $decoded['TokenData']->data->username; // Username
				$role_id 		= $decoded['TokenData']->data->role_id; // Role ID
				$tokenArr 		= array("user_id" => (int)$user_id, 
										"username" => $username, 
										"role_id" => (int)$role_id
										);	

							// get posted data
							$data 	= get_request_body();
							
							if($data['status'] == true){
								
								$error = array();
								
								// set department property
								$department->department_name = isset($data['requestData']['department_name']) ? $data['requestData']['department_name'] : "";
								
								if($department->department_name == ""){
									
									$error[] = "Please enter Department Name.";
									
								} 
								if (!preg_match('/^[\p{L} ]+$/u', $department->department_name)){
									
								  $error[] = 'Department Name must contain letters and spaces only!';
								  
								}
								
								
								if(count($error) > 0){
									
										$errorMsg = implode(", ", $error);
										
										// tell the user login failed
										$resArr = array("status" => "0", "message" => $errorMsg);
										
										 //Add log data
										 $logs->Module 		= "Department";
										 $logs->Action		= "create";
										 $logs->Request		= json_encode($data);
										 $logs->Response	= json_encode($resArr);
										 $logs->UserId		= (int)$user_id;
										 $logs->UserType    = ""; 
										 $logs->AddLog();
											 
										// tell the user login failed
										 response($resArr, 400);
									
								} else {
									
									$dept_id = $department->create();
									
									if($dept_id > 0){
										
										// generate jwt
									//$access_token = $auth->generateToken($tokenArr);
										
										$resArr = array("status" => "1", 
										"message" => "Department created successfully.",
										"access_token" 	=> $access_token);
												
										 $logs->Module 		= "Department";
										 $logs->Action		= "create";
										 $logs->Request		= json_encode($data);
										 $logs->Response	= json_encode($resArr);
										 $logs->UserId		= (int)$user_id;
										 $logs->UserType    = ""; 
										 $logs->AddLog();
										
										// display message: Department was created
										 response($resArr, 200);
									 
									 } else { // message if unable to create Department
									 
									 // generate jwt
									//$access_token = $auth->generateToken($tokenArr);
										
											// display message: unable to create Department
											$resArr = array("status" => "0", 
											"message" => "Unable to create data.",
											"access_token" 	=> $access_token);
											
											 $logs->Module 		= "Department";
											 $logs->Action		= "create";
											 $logs->Request		= json_encode($data);
											 $logs->Response	= json_encode($resArr);
											 $logs->UserId		= (int)$user_id;
											 $logs->UserType    = ""; 
											 $logs->AddLog();
											
											 // display message: unable to create Department
											 response($resArr, 400);
											
										} // Insert ID Check
										
								}
							
							} else {
								
								// display message: unable to create Department
								$resArr = array("status" => "0", "message" => $data["requestData"]);
								
								// Merge Array Post And GET Values
							$reqArr = array_merge($data, $getArr);
								
								 $logs->Module 		= "Department";
								 $logs->Action		= "create";
								 $logs->Request		= json_encode($reqArr);
								 $logs->Response	= json_encode($resArr);
								 $logs->UserId		= (int)$user_id;
								 $logs->UserType    = ""; 
								 $logs->AddLog();
								 
								response($resArr, 400);
							}
				 } else {
				
					// show error message
					$resArr = array(
						"status" 	=> "0", 
						"message" 	=> "Invalid Authorization. Error : ". $decoded["TokenData"]
					);
					
					// Merge Array Post And GET Values
					$reqArr = array_merge($data, $getArr);
					
					//Add log data
					 $logs->Module 		= "Department";
					 $logs->Action		= "create";
					 $logs->Request		= json_encode($reqArr);
					 $logs->Response	= json_encode($resArr);
					 $logs->UserId		= "";
					 $logs->UserType    = "";	
					 $logs->AddLog();
					
					response($resArr, 401);
					
			} // Invalid Authorization error		
					
		
		}  else { // show error message if jwt is empty
		 
			// tell the user access denied
			$resArr = array("status" => "0", "message" => "Access denied.");
			
			 $logs->Module 		= "Department";
			 $logs->Action		= "create";
			 $logs->Request		= "";
			 $logs->Response	= json_encode($resArr);
			 $logs->UserId		= "";
			 $logs->UserType	= "";
			 $logs->AddLog();
			
			// display message: unable to create user
			response($resArr, 401);
		}	
		
		break;
		
	case "PUT":

	
		$bearerToken = getBearerToken();
		
		$access_token = isset($bearerToken) ? $bearerToken : "";
	
		// if jwt is not empty
		if($access_token){	

			// decode jwt
			$decoded = $auth->validateToken($access_token);
			
			$data 	= array();
			$getArr = array();
			$reqArr = array();
			
			if($decoded['status'] == true){	

				$user_id 		= $decoded['TokenData']->data->user_id; // Decode ID
				$username 		= $decoded['TokenData']->data->username; // Username
				$role_id 		= $decoded['TokenData']->data->role_id; // Role ID
				$tokenArr 		= array("user_id" => (int)$user_id, 
										"username" => $username, 
										"role_id" => (int)$role_id
										);	
	
					// Get URL Data	
					$getArr = get_url_data();
					
					// Get ID From URL		
					$department->id = isset($getArr['id']) ? intval($getArr['id']) : "";
					
					$error = array();
					
					if($department->id == ""){
									
						$error[] = "Department ID is required.";
						
					}
					
					if(count($error) > 0){
							
						$errorMsg = implode(", ", $error);
						
						// tell the Department error occur
						$resArr = array("status" => "0", "message" => $errorMsg);
						
						// Merge Array Post And GET Values
								$reqArr = array_merge($data, $getArr);
						
						 //Add log data
						 $logs->Module 		= "Department";
						 $logs->Action		= "update";
						 $logs->Request		= json_encode($reqArr);
						 $logs->Response	= json_encode($resArr);
						 $logs->UserId		= (int)$user_id;
						 $logs->UserType    = ""; 
						 $logs->AddLog();
							 
						// tell the Department error occur
						 response($resArr, 400);
							
					}
					
					// get posted data
					$data = get_request_body();
					
					if($data['status'] == true){					
						
						
						// set department property
						$department->department_name = isset($data['requestData']['department_name']) ? $data['requestData']['department_name'] : "";

						$department->is_active = isset($data['requestData']['is_active']) ? $data['requestData']['is_active'] : "";	

						if(isset($data['requestData']['is_active'])){
									
								$updateStatus 		= "UpdateStatus";
								$UpdateMsg			= "Department status successfully updated.";
								$department->is_active 	= $data['requestData']['is_active'];
								
								if($department->is_active == ""){
							
										$error[] = "Is Active is required.";
										
								} 
										
							} else {			
										
									
								$updateStatus 		= "UpdateField";
								$UpdateMsg			= "Department updated successfully.";
									
									if($department->department_name == ""){
								
										$error[] = "Please enter Department Name.";
										
									} 
									if (!preg_match('/^[\p{L} ]+$/u', $department->department_name)){
										
										$error[] = 'Department Name must contain letters and spaces only!';
									  
									}
									
							} // URL ID validation required			
						
						if(count($error) > 0){
							
								$errorMsg = implode(", ", $error);
								
								// tell the Department error occur
								$resArr = array("status" => "0", "message" => $errorMsg);
								
								// Merge Array Post And GET Values
										$reqArr = array_merge($data, $getArr);
								
								 //Add log data
								 $logs->Module 		= "Department";
								 $logs->Action		= "update";
								 $logs->Request		= json_encode($reqArr);
								 $logs->Response	= json_encode($resArr);
								 $logs->UserId		= (int)$user_id;
								 $logs->UserType    = ""; 
								 $logs->AddLog();
									 
								// tell the Department error occur
								 response($resArr, 400);
							
						} else {
							
							
							$department_id_exist = $department->department_id_exist($department->id);
								
							if($department_id_exist == true){
									
									$dept_id = $department->update($updateStatus);
									
									if($dept_id == true){
										
										// generate jwt
										//$access_token = $auth->generateToken($tokenArr);
										
										$resArr = array("status" => "1", 
										"message" => $UpdateMsg,
										"access_token" 	=> $access_token);
										
										// Merge Array Post And GET Values
										$reqArr = array_merge($data, $getArr);
												
										 $logs->Module 		= "Department";
										 $logs->Action		= "update";
										 $logs->Request		= json_encode($reqArr);
										 $logs->Response	= json_encode($resArr);
										 $logs->UserId		= (int)$user_id;
										 $logs->UserType    = ""; 
										 $logs->AddLog();
										
										// display message: Department was updated
										 response($resArr, 200);
									 
									 } else { // message if unable to Department
									 
									 // generate jwt
										//$access_token = $auth->generateToken($tokenArr);
										
											// display message: unable to update Department
											$resArr = array("status" => "0", 
											"message" => "Unable to update data.",
											"access_token" 	=> $access_token);
											
											// Merge Array Post And GET Values
											$reqArr = array_merge($data, $getArr);
											
											 $logs->Module 		= "Department";
											 $logs->Action		= "update";
											 $logs->Request		= json_encode($reqArr);
											 $logs->Response	= json_encode($resArr);
											 $logs->UserId		= (int)$user_id;
											 $logs->UserType    = ""; 
											 $logs->AddLog();
											
											 // display message: unable to update Department
											 response($resArr, 400);
											
									} // Insert ID Check
								
								
							} else {
								
								// generate jwt
								//$access_token = $auth->generateToken($tokenArr);
							
								$resArr = array(
										"status" 		=> "0", 
										"message" 		=> "Department ID does not exist.",
										"access_token" 	=> $access_token
									);
									
									// Merge Array Post And GET Values
								$reqArr = array_merge($data, $getArr);	
									
								 $logs->Module 		= "Department";
								 $logs->Action		= "update";
								 $logs->Request		= json_encode($reqArr);
								 $logs->Response	= json_encode($resArr);
								 $logs->UserId		= (int)$user_id;
								 $logs->UserType    = ""; 
								 $logs->AddLog();	
									
								response($resArr, 200);
								
							}
								
						}
					
					} else {
						
						// display message: unable to update Department
						$resArr = array("status" => "0", "message" => $data["requestData"]);
						
						// Merge Array Post And GET Values
								$reqArr = array_merge($data, $getArr);
						
						 $logs->Module 		= "Department";
						 $logs->Action		= "update";
						 $logs->Request		= json_encode($reqArr);
						 $logs->Response	= json_encode($resArr);
						 $logs->UserId		= (int)$user_id;
						 $logs->UserType    = ""; 
						 $logs->AddLog();
						 
						response($resArr, 400);
					}
		
		
		} else {
				
						// show error message
						$resArr = array(
							"status" 	=> "0", 
							"message" 	=> "Invalid Authorization. Error : ". $decoded["TokenData"]
						);
						
						// Merge Array Post And GET Values
						$reqArr = array_merge($data, $getArr);
						
						//Add log data
						 $logs->Module 		= "Department";
						 $logs->Action		= "update";
						 $logs->Request		= json_encode($reqArr);
						 $logs->Response	= json_encode($resArr);
						 $logs->UserId		= "";
						 $logs->UserType    = "";	
						 $logs->AddLog();
						
						response($resArr, 401);
				}
		
			}  else { // show error message if jwt is empty
		 
			// tell the user access denied
			$resArr = array("status" => "0", "message" => "Access denied.");
			
			//Add log data
			 $logs->Module 		= "Department";
			 $logs->Action		= "update";
			 $logs->Request		= "";
			 $logs->Response	= json_encode($resArr);
			 $logs->UserId		= "";
			 $logs->UserType	= "";			 
			 $logs->AddLog();
			
			// display message: unable to update Category master
			response($resArr, 401);
		}		
		
		break;	
		
	case 'GET':
			
			$bearerToken = getBearerToken();
		
		$access_token = isset($bearerToken) ? $bearerToken : "";
			
		// if jwt is not empty
		if($access_token){
			
			// decode jwt		
			$decoded  = $auth->validateToken($access_token);	
			
			$getArr = array();
			
			 // if decode succeed, show user details
			if($decoded["status"] == true) {
				
				$user_id 		= $decoded['TokenData']->data->user_id; // Decode ID
				$username 		= $decoded['TokenData']->data->username; // Username
				$role_id 		= $decoded['TokenData']->data->role_id; // Role ID
				$tokenArr 		= array("user_id" => (int)$user_id, 
										"username" => $username, 
										"role_id" => (int)$role_id
										);	
			
				$getArr = get_url_data();					
				
				$id 	= isset($getArr['id']) ? intval($getArr['id']) : 0;
				
				$getList = false;
				
				if($id == ""){ 
				
					$getList = $department->getlisting();
				}
				
				if($id > 0){
					
				$department_id_exist = $department->department_id_exist($id);
				
					if($department_id_exist == true){
						
						$getList = $department->getlisting($id);
						
					} else {
						
						// generate jwt
						//	$access_token = $auth->generateToken($tokenArr);
						
						$resArr = array(
								"status" 		=> "0", 
								"message" 		=> "Department ID does not exist.",
								"access_token" 	=> $access_token
							);
							
						 $logs->Module 		= "Department";
						 $logs->Action		= "get";
						 $logs->Request		= json_encode($getArr);
						 $logs->Response	= json_encode($resArr);
						 $logs->UserId		= (int)$user_id;
						 $logs->UserType    = ""; 
						 $logs->AddLog();	
							
						response($resArr, 200);
					}
					
				}				
				
				if($getList){
					
					// generate jwt
					//$access_token = $auth->generateToken($tokenArr);
							
					$resArr = array(
						"status" 		=> "1", 
						"message" 		=> "Department data get successfully.",
						"access_token" 	=> $access_token,
						"data" 			=> $getList
					);

					 $logs->Module 		= "Department";
					 $logs->Action		= "get";
					 $logs->Request		= json_encode($getArr);
					 $logs->Response	= json_encode($resArr);
					 $logs->UserId		= (int)$user_id;
					 $logs->UserType    = ""; 	
					 $logs->AddLog();		
					
					response($resArr, 200);
					
				} else {
					
					$getList = array();
					
					// generate jwt
					//$access_token = $auth->generateToken($tokenArr);
					
					$resArr = array(
						"status" 		=> "0", 
						"message" 		=> "No data found.",
						"access_token" 	=> $access_token,
						"data" 			=> $getList
					);

					 $logs->Module 		= "Department";
					 $logs->Action		= "get";
					 $logs->Request		= json_encode($getArr);
					 $logs->Response	= json_encode($resArr);
					 $logs->UserId		= (int)$user_id;
					 $logs->UserType    = "";  	
					 $logs->AddLog();		
					
					response($resArr, 200);
					
				}
				
				
		} else {			 
				
				// show error message
				$resArr =array(
					"status" 	=> "0", 
					"message" 	=> "Invalid Authorization. Error : ". $decoded["TokenData"]
				);
				
				//Add log data
				 $logs->Module 		= "Department";
				 $logs->Action		= "get";
				 $logs->Request		= "";
				 $logs->Response	= json_encode($resArr);
				 $logs->UserId		= "";
				 $logs->UserType	= "";	
				 $logs->AddLog();
				
				response($resArr, 401);
			}			
							
		}  else { // show error message if jwt is empty		 
			
			$resArr = array("status" => "0", "message" => "Access denied.");
			
			//Add log data
			 $logs->Module 		= "Department";
			 $logs->Action		= "get";
			 $logs->Request		= "";
			 $logs->Response	= json_encode($resArr);
			 $logs->UserId		= "";
			 $logs->UserType	= "";	
			 $logs->AddLog();
			
			response($resArr, 401);
		}	
		
		break;
		
		
	case 'DELETE':	
	
			$bearerToken = getBearerToken();
		
		$access_token = isset($bearerToken) ? $bearerToken : "";
	
		// if jwt is not empty
		if($access_token)
		{
			
			// decode jwt
			$decoded = $auth->validateToken($access_token);
	
			$getArr = array();
			
			// if decode succeed, show user details
			if($decoded["status"] == true) {
					
				$user_id 		= $decoded['TokenData']->data->user_id; // Decode ID
				$username 		= $decoded['TokenData']->data->username; // Username
				$role_id 		= $decoded['TokenData']->data->role_id; // Role ID
				$tokenArr 		= array("user_id" => (int)$user_id, 
										"username" => $username, 
										"role_id" => (int)$role_id
										);	
			
							$getArr = get_url_data();
										
							// set Department property values	
							$department->id = isset($getArr['id']) ? intval($getArr['id']) : "";
							
							$error = array();
							
							if($department->id == ""){
											
								$error[] = "Department ID is required.";
								
							} 
							
							if(!is_numeric($department->id)){
								
								$error[] = "Department ID is must contain number.";
								
							}
							
							if(count($error) > 0){
									
							$errorMsg = implode(", ", $error);
								 
							// tell the user login failed
							$resArr = array("status" => "0", "message" => $errorMsg);						

							//Add log data
							 $logs->Module 		= "Department";
							 $logs->Action		= "delete";
							 $logs->Request		= json_encode($getArr);
							 $logs->Response	= json_encode($resArr);
							 $logs->UserId		= (int)$user_id;
							 $logs->UserType    = ""; 
							 $logs->AddLog();

							response($resArr, 200);
							 
						 } else {
							 
							 $department_id_exist = $department->department_id_exist($department->id);
								
							if($department_id_exist == true){
							 
								 $delete_dept = $department->delete();
													
									if($delete_dept == true){	

									// generate jwt
								//$access_token = $auth->generateToken($tokenArr);	
											 
										$resArr = array(
														"status" 		=> "1", 
														"message" 		=> "Department was successfully deleted.",
														"access_token" 	=> $access_token
													);			
										
										//Add log data
										 $logs->Module 		= "Department";
										 $logs->Action		= "delete";
										 $logs->Request		= json_encode($getArr);
										 $logs->Response	= json_encode($resArr);
										 $logs->UserId		= (int)$user_id;
										 $logs->UserType    = ""; 
										 $logs->AddLog();
							
										response($resArr, 200);
										
									 } else {
										 
										 // generate jwt
								 //$access_token = $auth->generateToken($tokenArr);
										 
										 $resArr = array(
														"status" 		=> "1", 
														"message" 		=> "Unable to delete department.",
														"access_token" 	=> $access_token
													);
													
										//Add log data
										 $logs->Module 		= "Department";
										 $logs->Action		= "delete";
										 $logs->Request		= json_encode($getArr);
										 $logs->Response	= json_encode($resArr);
										 $logs->UserId		= (int)$user_id;
										 $logs->UserType    = ""; 
										 $logs->AddLog();		
											 
										response($resArr, 401);
										 
									 }	// delete condition end	
								 
								} else {
									 
								// generate jwt
								//$access_token = $auth->generateToken($tokenArr);
								
								$resArr = array(
									"status" 		=> "0", 
									"message" 		=> "Department ID does not exist.",
									"access_token" 	=> $access_token
								);
								
								 $logs->Module 		= "Department";
								 $logs->Action		= "get";
								 $logs->Request		= json_encode($getArr);
								 $logs->Response	= json_encode($resArr);
								 $logs->UserId		= (int)$user_id;
								 $logs->UserType    = ""; 
								 $logs->AddLog();	
					
								response($resArr, 200);
								
							}
							 
						 }
						 
		 } else {
					 
						// set response code				
						$resArr = array(
									"status" 	=> "0", 
									"message" 	=> "Invalid Authorization. Error : ". $decoded["TokenData"]
								);
							
						//Add log data
						 $logs->Module 		= "Department";
						 $logs->Action		= "delete";
						 $logs->Request		= json_encode($getArr);
						 $logs->Response	= json_encode($resArr);
						 $logs->UserId		= "";
						 $logs->UserType	= "";
						 $logs->AddLog();			
										 
						response($resArr, 401);
					}
			 
		}  else { // show error message if jwt is empty
			 
				// tell the City access denied
				$resArr = array("status" => "0", "message" => "Access denied.");
				
				//Add log data
				 $logs->Module 		= "Department";
				 $logs->Action		= "delete";
				 $logs->Request		= "";
				 $logs->Response	= json_encode($resArr);
				 $logs->UserId		= "";
				 $logs->UserType	= "";
				 $logs->AddLog();
							 
				response($resArr, 401);
				
		}
	
		break;
		
	case "OPTIONS": //Handle Preflighted requests for CORS issue
		
		// set response code		
		header('Content-Type: text/plain');
		
		response("", 200);
		
		break;
		
	default:
	
		// Invalid Request Method
		$resArr = array("status" => "0", "message" => "Method Not Allowed.");						
		// display message: unable to create user
		response($resArr, 405);	
		
		break;
}

?>