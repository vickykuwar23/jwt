<?php

/*** files needed to connect to database and class***/
define("ROOT_PATH", realpath(__DIR__ . '/../../../..'));
include_once ROOT_PATH.'/constant.php';

include_once CONFIG_PATH.'functions.php';
include_once CONFIG_PATH.'database.php';
include_once RESOURCE_MASTERPATH.'district/district.php';
include_once RESOURCE_PATH.'logs/logs.php';
//include_once RESOURCE_PATH.'auth/auth.php';

// Required headers
requireHeaders();
 
/*** get database connection ***/
$database 	= new Database();
$db 		= $database->getConnection();
$mdb 		= $database->getMongoConnection();
 
// instantiate District object
$district  = new District($db);
$logs  = new Logs($mdb);
//$auth  = new Auth();

// Get Request Method 
$request_method		= get_request_method();

switch($request_method)
{
	
	case "POST":

		// get posted data
		$data 	= get_request_body();
		
		if($data['status'] == true){
			
			$error = array();
			
			// set State property
			$district->district_name = isset($data['requestData']['district_name']) ? $data['requestData']['district_name'] : "";
			$district->city_id = isset($data['requestData']['city_id']) ? $data['requestData']['city_id'] : "";
			
			
			if($district->city_id == ""){
				
				$error[] = "Please select city id.";
				
			} 			
			if($district->district_name == ""){
				
				$error[] = "Please enter district name.";
				
			} else if (!preg_match('/^[\p{L} ]+$/u', $district->district_name)){
				
			  $error[] = 'District Name must contain letters and spaces only!';
			  
			}
			
			if(count($error) > 0){
				
					$errorMsg = implode(", ", $error);
					
					// tell the user login failed
					$resArr = array("status" => "0", "message" => $errorMsg);
					
					 //Add log data
					 $logs->Module 		= "District Master";
					 $logs->Action		= "create";
					 $logs->Request		= json_encode($data);
					 $logs->Response	= json_encode($resArr);
					 $logs->UserId		= "";
					 $logs->UserType	= "";
					 $logs->AddLog();
						 
					// tell the user login failed
					 response($resArr, 400);
				
			} else {
				
				$insertId = $district->create();
				
				if($insertId > 0){
					
					$resArr = array("status" => "1", "message" => "District created successfully.");
							
					 $logs->Module 		= "District Master";
					 $logs->Action		= "create";
					 $logs->Request		= json_encode($data);
					 $logs->Response	= json_encode($resArr);
					 $logs->UserId		= "";
					 $logs->UserType	= "";
					 $logs->AddLog();
					
					// display message: District Master was created
					 response($resArr, 200);
				 
				 } else { // message if unable to create District Master
					
						// display message: unable to create District 
						$resArr = array("status" => "0", "message" => "Unable to create data.");
						
						 $logs->Module 		= "State Master";
						 $logs->Action		= "create";
						 $logs->Request		= json_encode($data);
						 $logs->Response	= json_encode($resArr);
						 $logs->UserId		= "";
						 $logs->UserType	= "";
						 $logs->AddLog();
						
						 // display message: unable to create States Master
						 response($resArr, 400);
						
					} // Insert ID Check
					
			}
		
		} else {
			
			// display message: unable to create District Category
			$resArr = array("status" => "0", "message" => $data["requestData"]);
			
			 $logs->Module 		= "District Master";
			 $logs->Action		= "create";
			 $logs->Request		= "";
			 $logs->Response	= json_encode($resArr);
			 $logs->UserId		= "";
			 $logs->UserType	= "";
			 $logs->AddLog();
			 
			response($resArr, 400);
		}
		
		break;
		
	case "PUT":

	    
		// Get URL Data	
		$getArr = get_url_data();
		
		// Get ID From URL		
		$district->id = isset($getArr['id']) ? intval($getArr['id']) : "";
		
		// get posted data
		$data = get_request_body();
		
		if($data['status'] == true){
			
			$error = array();
			
			// set district property
			$district->district_name = isset($data['requestData']['district_name']) ? $data['requestData']['district_name'] : "";
			$district->city_id = isset($data['requestData']['city_id']) ? $data['requestData']['city_id'] : "";
			
			if($district->id == ""){
						
				$error[] = "District ID is required.";
				
			} else {			
				
				
				if($district->city_id == ""){
				
					$error[] = "Please select city id.";
					
				} 			
				if($district->district_name == ""){
					
					$error[] = "Please enter district name.";
					
				} else if (!preg_match('/^[\p{L} ]+$/u', $district->district_name)){
					
				  $error[] = 'District Name must contain letters and spaces only!';
				  
				}
				
				
			} // URL ID validation required			
			
			if(count($error) > 0){
				
					$errorMsg = implode(", ", $error);
					
					// tell the State Master error occur
					$resArr = array("status" => "0", "message" => $errorMsg);
					
					 //Add log data
					 $logs->Module 		= "District Master";
					 $logs->Action		= "update";
					 $logs->Request		= json_encode($data);
					 $logs->Response	= json_encode($resArr);
					 $logs->UserId		= "";
					 $logs->UserType	= "";
					 $logs->AddLog();
						 
					// tell the State Master error occur
					 response($resArr, 400);
				
			} else {
				
				$insertId = $district->update();
				
				if($insertId > 0){
					
					$resArr = array("status" => "1", "message" => "District updated successfully.");
							
					 $logs->Module 		= "District Master";
					 $logs->Action		= "update";
					 $logs->Request		= json_encode($data);
					 $logs->Response	= json_encode($resArr);
					 $logs->UserId		= "";
					 $logs->UserType	= "";
					 $logs->AddLog();
					
					// display message: District Master was updated
					 response($resArr, 200);
				 
				 } else { // message if unable to District Master
					
						// display message: unable to update District Master
						$resArr = array("status" => "0", "message" => "Unable to update data.");
						
						 $logs->Module 		= "District Master";
						 $logs->Action		= "update";
						 $logs->Request		= json_encode($data);
						 $logs->Response	= json_encode($resArr);
						 $logs->UserId		= "";
						 $logs->UserType	= "";
						 $logs->AddLog();
						
						 // display message: unable to update District Master
						 response($resArr, 400);
						
					} // Insert ID Check
					
			}
		
		} else {
			
			// display message: unable to update District
			$resArr = array("status" => "0", "message" => $data["requestData"]);
			
			 $logs->Module 		= "District Master";
			 $logs->Action		= "update";
			 $logs->Request		= "";
			 $logs->Response	= json_encode($resArr);
			 $logs->UserId		= "";
			 $logs->UserType	= "";
			 $logs->AddLog();
			 
			response($resArr, 400);
		}
		
		break;	
		
	case 'GET':
			
			$getArr = array();
			
				$getArr = get_url_data();					
				
				$id 	= isset($getArr['id']) ? intval($getArr['id']) : 0;
				//$type   = isset($getArr['type']) ? $getArr['type'] : "";
				
				if($id == "" || $id > 0 || $id == 0)
				{
					    $district->id = $id;
						
						$getList = $district->getlisting();
						
						if($getList){
							
							$resArr = array(
								"status" 		=> "1", 
								"message" 		=> "District data get successfully.",
								"data" 			=> $getList
							);

							 $logs->Module 		= "District Master";
							 $logs->Action		= "get";
							 $logs->Request		= json_encode($getArr);
							 $logs->Response	= json_encode($resArr);
							 $logs->UserId		= "";
							 $logs->UserType    = ""; 	
							 $logs->AddLog();		
							
							response($resArr, 200);
							
						} else {
							
							$getList = array();
							$resArr = array(
								"status" 		=> "0", 
								"message" 		=> "No data found.",
								"data" 			=> $getList
							);

							 $logs->Module 		= "District Master";
							 $logs->Action		= "get";
							 $logs->Request		= json_encode($getArr);
							 $logs->Response	= json_encode($resArr);
							 $logs->UserId		= "";
							 $logs->UserType    = ""; 	
							 $logs->AddLog();		
							
							response($resArr, 200);
							
						}
					
				
				} else {
					
					$resArr = array(
								"status" 		=> "0", 
								"message" 		=> "Master type parameter required."
							);

					 $logs->Module 		= "District Master";
					 $logs->Action		= "get";
					 $logs->Request		= json_encode($getArr);
					 $logs->Response	= json_encode($resArr);
					 $logs->UserId		= "";
					 $logs->UserType    = ""; 	
					 $logs->AddLog();		
					
					response($resArr, 200);
					
				}
		
		break;
		
		
	case 'DELETE':	
	
			$getArr = array();
			
			$getArr = get_url_data();
						
			// set District property values	
			$district->id = isset($getArr['id']) ? intval($getArr['id']) : "";
			
			$error = array();
			
			if($district->id == ""){
							
				$error[] = "District ID is required.";
				
			} 
			
			if(!is_numeric($district->id)){
				
				$error[] = "District ID is must contain number.";
				
			}
			
			if(count($error) > 0){
					
			$errorMsg = implode(", ", $error);
				 
			// tell the user login failed
			$resArr = array("status" => "0", "message" => $errorMsg);						

			//Add log data
			 $logs->Module 		= "District Master";
			 $logs->Action		= "delete";
			 $logs->Request		= json_encode($getArr);
			 $logs->Response	= json_encode($resArr);
			 $logs->UserId		= "";
			 $logs->UserType	= "";
			 $logs->AddLog();

			response($resArr, 200);
			 
		 } else {
			 
			 $deleteUser = $district->delete($district->id);
								
				if($deleteUser > 0){					
						 
					$resArr = array(
									"status" 		=> "1", 
									"message" 		=> "District was successfully deleted."
								);			
					
					//Add log data
					 $logs->Module 		= "District Master";
					 $logs->Action		= "delete";
					 $logs->Request		= json_encode($getArr);
					 $logs->Response	= json_encode($resArr);
					 $logs->UserId		= "";
					 $logs->UserType	= "";
					 $logs->AddLog();
		
					response($resArr, 200);
					
				 } else {
					 
					 $resArr = array(
									"status" 		=> "1", 
									"message" 		=> "Unable to delete district."
								);
								
					//Add log data
					 $logs->Module 		= "District Master";
					 $logs->Action		= "delete";
					 $logs->Request		= json_encode($getArr);
					 $logs->Response	= json_encode($resArr);
					 $logs->UserId		= "";
					 $logs->UserType	= "";
					 $logs->AddLog();		
						 
					response($resArr, 401);
					 
				 }	// delete condition end	
			 
		 }
	
		break;
		
	case "OPTIONS": //Handle Preflighted requests for CORS issue
		
		// set response code		
		header('Content-Type: text/plain');
		
		response("", 200);
		
		break;
		
	default:
	
		// Invalid Request Method
		$resArr = array("status" => "0", "message" => "Method Not Allowed.");						
		// display message: unable to create user
		response($resArr, 405);	
		
		break;
}

?>