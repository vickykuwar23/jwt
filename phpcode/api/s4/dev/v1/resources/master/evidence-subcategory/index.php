<?php

/*** files needed to connect to database and class***/
define("ROOT_PATH", realpath(__DIR__ . '/../../../..'));
include_once ROOT_PATH.'/constant.php';

include_once CONFIG_PATH.'functions.php';
include_once CONFIG_PATH.'database.php';
include_once RESOURCE_MASTERPATH.'evidence-subcategory/evidence_subcategory.php';
include_once RESOURCE_PATH.'logs/logs.php';
include_once RESOURCE_PATH.'auth/auth.php';

// Required headers
requireHeaders();
 
/*** get database connection ***/
$database 	= new Database();
$db 		= $database->getConnection();
$mdb 		= $database->getMongoConnection();


// instantiate EvidenceSubCategory object
$evidenceSubCat  = new Evidencesubcategory($db);
$logs  = new Logs($mdb);
$auth  = new Auth();

// Get Request Method 
$request_method		= get_request_method();

switch($request_method)
{
	
	case "POST":
	
	$bearerToken = getBearerToken();
		
		$access_token = isset($bearerToken) ? $bearerToken : "";

		// if jwt is not empty
		if($access_token){	

			// decode jwt
			$decoded = $auth->validateToken($access_token);
			
			$data 	= array();
			$getArr = array();
			$reqArr = array();
			
			// if decode succeed, show user details			
			if($decoded['status'] == true){	

				$user_id 		= $decoded['TokenData']->data->user_id; // Decode ID
				$username 		= $decoded['TokenData']->data->username; // Username
				$role_id 		= $decoded['TokenData']->data->role_id; // Role ID
				$tokenArr 		= array("user_id" => (int)$user_id, 
										"username" => $username, 
										"role_id" => (int)$role_id
										);	

				// get posted data
				$data 	= get_request_body();
				
				if($data['status'] == true){
					
					$error = array();
					
					// set EvidenceSubCategory property
					$evidenceSubCat->evidence_category_id = isset($data['requestData']['evidence_category_id']) ? $data['requestData']['evidence_category_id'] : "";
					
					$evidenceSubCat->evidence_subcategory_name = isset($data['requestData']['evidence_subcategory_name']) ? $data['requestData']['evidence_subcategory_name'] : "";
					
					if($evidenceSubCat->evidence_category_id == ""){
						
						$error[] = "Please select Evidence Category Name.";
						
					} 
					
					if($evidenceSubCat->evidence_subcategory_name == ""){
						
						$error[] = "Please enter Evidence Sub-Category Name.";
						
					} else if (!preg_match('/^[\p{L} ]+$/u', $evidenceSubCat->evidence_subcategory_name)){
						
					  $error[] = 'Evidence Sub-Category Name must contain letters and spaces only!';
					  
					}
					
					
					if(count($error) > 0){
						
							$errorMsg = implode(", ", $error);
							
							// tell the user login failed
							$resArr = array("status" => "0", "message" => $errorMsg);
							
							 //Add log data
							 $logs->Module 		= "Evidence Sub-Category";
							 $logs->Action		= "create";
							 $logs->Request		= json_encode($data);
							 $logs->Response	= json_encode($resArr);
							 $logs->UserId		= (int)$user_id;
							 $logs->UserType    = ""; 
							 $logs->AddLog();
								 
							// tell the user login failed
							 response($resArr, 400);
						
					} else {
						
						$insertId = $evidenceSubCat->create();
						
						if($insertId > 0){
							
							// generate jwt
							//$access_token = $auth->generateToken($tokenArr);
							
							$resArr = array("status" => "1", 
							"message" => "Evidence Sub-Category created successfully.",
							"access_token" 	=> $access_token);
									
							 $logs->Module 		= "Evidence Sub-Category";
							 $logs->Action		= "create";
							 $logs->Request		= json_encode($data);
							 $logs->Response	= json_encode($resArr);
							 $logs->UserId		= (int)$user_id;
							 $logs->UserType    = ""; 
							 $logs->AddLog();
							
							// display message: Evidence Sub-Category was created
							 response($resArr, 200);
						 
						 } else { // message if unable to create Evidence Sub-Category
						 
						  // generate jwt
								//$access_token = $auth->generateToken($tokenArr);
							
								// display message: unable to create Evidence Sub-Category
								$resArr = array("status" => "0", 
								"message" => "Unable to create data.",
								"access_token" 	=> $access_token);
								
								 $logs->Module 		= "Evidence Sub-Category";
								 $logs->Action		= "create";
								 $logs->Request		= json_encode($data);
								 $logs->Response	= json_encode($resArr);
								 $logs->UserId		= (int)$user_id;
								 $logs->UserType    = ""; 
								 $logs->AddLog();
								
								 // display message: unable to create Evidence Sub-Category
								 response($resArr, 400);
								
							} // Insert ID Check
							
					}
				
				} else {
					
					// display message: unable to create Evidence Sub-Category
					$resArr = array("status" => "0", "message" => $data["requestData"]);
					
					 $logs->Module 		= "Evidence Sub-Category";
					 $logs->Action		= "create";
					 $logs->Request		= json_encode($data);
					 $logs->Response	= json_encode($resArr);
					 $logs->UserId		= (int)$user_id;
					 $logs->UserType    = ""; 
					 $logs->AddLog();
					 
					response($resArr, 400);
				}
				
				
		 } else {
				
					// show error message
					$resArr = array(
						"status" 	=> "0", 
						"message" 	=> "Invalid Authorization. Error : ". $decoded["TokenData"]
					);
					
					
					// Merge Array Post And GET Values
					$reqArr = array_merge($data, $getArr);
					
					//Add log data
					 $logs->Module 		= "Evidence Sub-Category";
					 $logs->Action		= "create";
					 $logs->Request		= json_encode($reqArr);
					 $logs->Response	= json_encode($resArr);
					 $logs->UserId		= "";
					 $logs->UserType    = "";	
					 $logs->AddLog();
					
					response($resArr, 401);
					
			} // Invalid Authorization error		
					
		
		}  else { // show error message if jwt is empty
		 
			// tell the user access denied
			$resArr = array("status" => "0", "message" => "Access denied.");
			
			 $logs->Module 		= "Evidence Sub-Category";
			 $logs->Action		= "create";
			 $logs->Request		= "";
			 $logs->Response	= json_encode($resArr);
			 $logs->UserId		= "";
			 $logs->UserType	= "";
			 $logs->AddLog();
			
			// display message: unable to create user
			response($resArr, 401);
		}	
		
		break;
		
		
	case "PUT":

		$bearerToken = getBearerToken();
		
		$access_token = isset($bearerToken) ? $bearerToken : "";
	
		// if jwt is not empty
		if($access_token){	

			// decode jwt
			$decoded = $auth->validateToken($access_token);
			
			$data 	= array();
			$getArr = array();
			$reqArr = array();
			
			
			if($decoded['status'] == true){

				$user_id 		= $decoded['TokenData']->data->user_id; // Decode ID
				$username 		= $decoded['TokenData']->data->username; // Username
				$role_id 		= $decoded['TokenData']->data->role_id; // Role ID
				$tokenArr 		= array("user_id" => (int)$user_id, 
										"username" => $username, 
										"role_id" => (int)$role_id
										);	
	    
					// Get URL Data	
					$getArr = get_url_data();
					
					// Get ID From URL		
					$evidenceSubCat->id = isset($getArr['id']) ? intval($getArr['id']) : "";
					
					$error = array();
					
					if($evidenceSubCat->id == ""){
									
							$error[] = "Evidence Sub Category ID is required.";
							
						}
						
						
						if(count($error) > 0){
								
								$errorMsg = implode(", ", $error);
								
								// tell the Evidence Category error occur
								$resArr = array("status" => "0", "message" => $errorMsg);
								
								// Merge Array Post And GET Values
									$reqArr = array_merge($data, $getArr);
								
								 //Add log data
								 $logs->Module 		= "Evidence Sub-Category";
								 $logs->Action		= "update";
								 $logs->Request		= json_encode($reqArr);
								 $logs->Response	= json_encode($resArr);
								 $logs->UserId		= (int)$user_id;
								 $logs->UserType    = ""; 
								 $logs->AddLog();
									 
								// tell the Evidence Category error occur
								 response($resArr, 400);
							
						}
						
					// get posted data
					$data = get_request_body();
					
					if($data['status'] == true){						
						
							
						// set evidenceSubCat property
						$evidenceSubCat->evidence_category_id = isset($data['requestData']['evidence_category_id']) ? $data['requestData']['evidence_category_id'] : "";
						
						$evidenceSubCat->evidence_subcategory_name = isset($data['requestData']['evidence_subcategory_name']) ? $data['requestData']['evidence_subcategory_name'] : "";
						
						$evidenceSubCat->is_active = isset($data['requestData']['is_active']) ? $data['requestData']['is_active'] : "";
						
						if(isset($data['requestData']['is_active'])){
								
							$updateStatus 		= "UpdateStatus";
							$UpdateMsg			= "Evidence Sub-Category status successfully updated.";
							$evidenceSubCat->is_active 	= $data['requestData']['is_active'];
							
							if($evidenceSubCat->is_active == ""){
						
									$error[] = "Is Active is required.";
									
							} 
									
						} else {

							$updateStatus 		= "UpdateField";
							$UpdateMsg			= "Evidence Sub-Category updated successfully.";
							
							if($evidenceSubCat->evidence_category_id == ""){
							
								$error[] = "Please select Evidence Category Name.";
								
							} 
							
							if($evidenceSubCat->evidence_subcategory_name == ""){
								
								$error[] = "Please enter Evidence Sub-Category Name.";
								
							} else if (!preg_match('/^[\p{L} ]+$/u', $evidenceSubCat->evidence_subcategory_name)){
								
								$error[] = 'Evidence Sub-Category Name must contain letters and spaces only!';
							  
							}
							
						} // URL ID validation required	
						
						
						if(count($error) > 0){
								
									$errorMsg = implode(", ", $error);
									
									// tell the Evidence Category error occur
									$resArr = array("status" => "0", "message" => $errorMsg);
									
									// Merge Array Post And GET Values
										$reqArr = array_merge($data, $getArr);
									
									 //Add log data
									 $logs->Module 		= "Evidence Sub-Category";
									 $logs->Action		= "update";
									 $logs->Request		= json_encode($reqArr);
									 $logs->Response	= json_encode($resArr);
									 $logs->UserId		= (int)$user_id;
									 $logs->UserType    = ""; 
									 $logs->AddLog();
										 
									// tell the Evidence Category error occur
									 response($resArr, 400);
								
							} else {
								
		$evidence_sub_id_exist = $evidenceSubCat->evidence_sub_id_exist($evidenceSubCat->id);
									
								if($evidence_sub_id_exist == true){
								
									$sub_cat_id = $evidenceSubCat->update($updateStatus);
									
									if($sub_cat_id == true){
										
										// generate jwt
										//$access_token = $auth->generateToken($tokenArr);
										
										$resArr = array("status" => "1", 
										"message" => $UpdateMsg,
										"access_token" 	=> $access_token);
										
										// Merge Array Post And GET Values
										$reqArr = array_merge($data, $getArr);
												
										 $logs->Module 		= "Evidence Sub-Category";
										 $logs->Action		= "update";
										 $logs->Request		= json_encode($reqArr);
										 $logs->Response	= json_encode($resArr);
										 $logs->UserId		= (int)$user_id;
										 $logs->UserType    = ""; 
										 $logs->AddLog();
										
										// display message: Evidence Sub-Category was updated
										 response($resArr, 200);
									 
									 } else { // message if unable to Evidence Sub-Category
									 
									 // generate jwt
										//$access_token = $auth->generateToken($tokenArr);
										
											// display message: unable to update Evidence
											$resArr = array("status" => "0", "message" => "Unable to update data.",
											"access_token" 	=> $access_token);
											
											// Merge Array Post And GET Values
											$reqArr = array_merge($data, $getArr);
											
											 $logs->Module 		= "Evidence Sub-Category";
											 $logs->Action		= "update";
											 $logs->Request		= json_encode($reqArr);
											 $logs->Response	= json_encode($resArr);
											 $logs->UserId		= (int)$user_id;
											 $logs->UserType    = ""; 
											 $logs->AddLog();
											
											 // display message: unable to update Evidence Sub-Category
											 response($resArr, 400);
											
										} // Insert ID Check
										
								} else {
									
									// generate jwt
								//$access_token = $auth->generateToken($tokenArr);
								
									$resArr = array(
											"status" 		=> "0", 
											"message" 		=> "Evidence Sub-Category ID does not exist.",
											"access_token" 	=> $access_token
										);
										
										// Merge Array Post And GET Values
								$reqArr = array_merge($data, $getArr);	
										
									 $logs->Module 		= "Evidence Sub-Category";
									 $logs->Action		= "update";
									 $logs->Request		= json_encode($reqArr);
									 $logs->Response	= json_encode($resArr);
									 $logs->UserId		= (int)$user_id;
									 $logs->UserType    = "";  
									 $logs->AddLog();	
										
									response($resArr, 200);
									
								}		
									
							}
					
					
					} else {
						
						// display message: unable to update Crime Sub Type
						$resArr = array("status" => "0", "message" => $data["requestData"]);
						
						// Merge Array Post And GET Values
						$reqArr = array_merge($data, $getArr);
						
						 $logs->Module 		= "Crime Sub Type";
						 $logs->Action		= "update";
						 $logs->Request		= json_encode($reqArr);
						 $logs->Response	= json_encode($resArr);
						 $logs->UserId		= (int)$user_id;
						 $logs->UserType    = ""; 
						 $logs->AddLog();
						 
						response($resArr, 400);
					}
			
				} else {
				
					// show error message
					$resArr = array(
						"status" 	=> "0", 
						"message" 	=> "Invalid Authorization. Error : ". $decoded["TokenData"]
					);
					
					// Merge Array Post And GET Values
					$reqArr = array_merge($data, $getArr);
					
					//Add log data
					 $logs->Module 		= "Crime Sub Type";
					 $logs->Action		= "update";
					 $logs->Request		= json_encode($reqArr);
					 $logs->Response	= json_encode($resArr);
					 $logs->UserId		= "";
					 $logs->UserType    = "";	
					 $logs->AddLog();
					
					response($resArr, 401);
				} 
			
		
		}  else { // show error message if jwt is empty
		 
			// tell the user access denied
			$resArr = array("status" => "0", "message" => "Access denied.");
			
			//Add log data
			 $logs->Module 		= "Evidence Sub-Category";
			 $logs->Action		= "update";
			 $logs->Request		= "";
			 $logs->Response	= json_encode($resArr);
			 $logs->UserId		= "";
			 $logs->UserType	= "";			 
			 $logs->AddLog();
			
			// display message: unable to update Category master
			response($resArr, 401);
		}		
		
		
		break;	
		
	case 'GET':
			
		$bearerToken = getBearerToken();
		
		$access_token = isset($bearerToken) ? $bearerToken : "";
			
		// if jwt is not empty
		if($access_token){
			
			// decode jwt		
			$decoded  = $auth->validateToken($access_token);	
			
			$getArr = array();
			
			 // if decode succeed, show user details
			if($decoded["status"] == true) {
				
				$user_id 		= $decoded['TokenData']->data->user_id; // Decode ID
				$username 		= $decoded['TokenData']->data->username; // Username
				$role_id 		= $decoded['TokenData']->data->role_id; // Role ID
				$tokenArr 		= array("user_id" => (int)$user_id, 
										"username" => $username, 
										"role_id" => (int)$role_id
										);	
			
				$getArr = get_url_data();					
				
				$id 	= isset($getArr['id']) ? intval($getArr['id']) : 0;
				$type 	= isset($getArr['type']) ? $getArr['type'] : "";
				
				$getList = false;
				
				if($id > 0 && $type == 'evidenceSubcategory'){
					
					$getList = $evidenceSubCat->getlisting($id, $type);	
					
				}  else {
				
						if($id == ""){ 
						
							$getList = $evidenceSubCat->getlisting();
						}
						
						if($id > 0){
							
						$evidence_sub_id_exist = $evidenceSubCat->evidence_sub_id_exist($id);
						
							if($evidence_sub_id_exist == true){
								
								$getList = $evidenceSubCat->getlisting($id);
								
							} else {
								
								// generate jwt
									//$access_token = $auth->generateToken($tokenArr);
								
								$resArr = array(
										"status" 		=> "0", 
										"message" 		=> "Evidence Sub-Category ID does not exist.",
										"access_token" 	=> $access_token
									);
									
								 $logs->Module 		= "Evidence Sub-Category";
								 $logs->Action		= "get";
								 $logs->Request		= json_encode($getArr);
								 $logs->Response	= json_encode($resArr);
								 $logs->UserId		= (int)$user_id;
								 $logs->UserType    = "";  
								 $logs->AddLog();	
									
								response($resArr, 200);
							}
							
						}		
				}		
				
				if($getList){
					
					
					// generate jwt
					//$access_token = $auth->generateToken($tokenArr);
							
					$resArr = array(
						"status" 		=> "1", 
						"message" 		=> "Evidence Sub-Category data get successfully.",
						"access_token" 	=> $access_token,
						"data" 			=> $getList
					);

					 $logs->Module 		= "Evidence Sub-Category";
					 $logs->Action		= "get";
					 $logs->Request		= json_encode($getArr);
					 $logs->Response	= json_encode($resArr);
					 $logs->UserId		= (int)$user_id;
					 $logs->UserType    = ""; 	 	
					 $logs->AddLog();		
					
					response($resArr, 200);
					
				} else {
					
					$getList = array();
					
					// generate jwt
					//$access_token = $auth->generateToken($tokenArr);
					
					$resArr = array(
						"status" 		=> "0", 
						"message" 		=> "No data found.",
						"access_token" 	=> $access_token,
						"data" 			=> $getList
					);

					 $logs->Module 		= "Evidence Sub-Category";
					 $logs->Action		= "get";
					 $logs->Request		= json_encode($getArr);
					 $logs->Response	= json_encode($resArr);
					 $logs->UserId		= (int)$user_id;
					 $logs->UserType    = "";    	
					 $logs->AddLog();		
					
					response($resArr, 200);
					
				}
				
		} else {			 
				
				// show error message
				$resArr =array(
					"status" 	=> "0", 
					"message" 	=> "Invalid Authorization. Error : ". $decoded["TokenData"]
				);
				
				//Add log data
				 $logs->Module 		= "Evidence Sub-Category";
				 $logs->Action		= "get";
				 $logs->Request		= "";
				 $logs->Response	= json_encode($resArr);
				 $logs->UserId		= "";
				 $logs->UserType	= "";	
				 $logs->AddLog();
				
				response($resArr, 401);
			}			
							
		}  else { // show error message if jwt is empty		 
			
			$resArr = array("status" => "0", "message" => "Access denied.");
			
			//Add log data
			 $logs->Module 		= "Evidence Sub-Category";
			 $logs->Action		= "get";
			 $logs->Request		= "";
			 $logs->Response	= json_encode($resArr);
			 $logs->UserId		= "";
			 $logs->UserType	= "";	
			 $logs->AddLog();
			
			response($resArr, 401);
		}	
		
		break;	
	
	case 'DELETE':	
	
		$bearerToken = getBearerToken();
		
		$access_token = isset($bearerToken) ? $bearerToken : "";
	
		// if jwt is not empty
		if($access_token)
		{
			
			// decode jwt
			$decoded = $auth->validateToken($access_token);
	
			$getArr = array();
			
			// if decode succeed, show user details
			if($decoded["status"] == true) {
					
				$user_id 		= $decoded['TokenData']->data->user_id; // Decode ID
				$username 		= $decoded['TokenData']->data->username; // Username
				$role_id 		= $decoded['TokenData']->data->role_id; // Role ID
				$tokenArr 		= array("user_id" => (int)$user_id, 
										"username" => $username, 
										"role_id" => (int)$role_id
										);	
			
						$getArr = get_url_data();
									
						// set EvidenceSubCategory property values	
						$evidenceSubCat->id = isset($getArr['id']) ? intval($getArr['id']) : "";
						
						$error = array();
						
						if($evidenceSubCat->id == ""){
										
							$error[] = "Evidence Sub-Category ID is required.";
							
						} 
						
						if(!is_numeric($evidenceSubCat->id)){
							
							$error[] = "Evidence Sub-Category ID is must contain number.";
							
						}
						
						if(count($error) > 0){
								
						$errorMsg = implode(", ", $error);
							 
						// tell the user login failed
						$resArr = array("status" => "0", "message" => $errorMsg);						

						//Add log data
						 $logs->Module 		= "Evidence Sub-Category";
						 $logs->Action		= "delete";
						 $logs->Request		= json_encode($getArr);
						 $logs->Response	= json_encode($resArr);
						 $logs->UserId		= (int)$user_id;
						 $logs->UserType    = ""; 
						 $logs->AddLog();

						response($resArr, 200);
						 
					 } else {
						 
						 $evidence_sub_id_exist = $evidenceSubCat->evidence_sub_id_exist($evidenceSubCat->id);
							
						if($evidence_sub_id_exist == true){
						 
								 $deletesubcat = $evidenceSubCat->delete();
													
									if($deletesubcat == true){	

										// generate jwt
								//$access_token = $auth->generateToken($tokenArr);
											 
										$resArr = array(
														"status" 		=> "1", 
														"message" 		=> "Evidence Sub-Category was successfully deleted.",
														"access_token" 	=> $access_token
													);			
										
										//Add log data
										 $logs->Module 		= "Evidence Sub-Category";
										 $logs->Action		= "delete";
										 $logs->Request		= json_encode($getArr);
										 $logs->Response	= json_encode($resArr);
										 $logs->UserId		= (int)$user_id;
										 $logs->UserType    = ""; 
										 $logs->AddLog();
							
										response($resArr, 200);
										
									 } else {
										 
										  // generate jwt
								 //$access_token = $auth->generateToken($tokenArr);
										 
										 $resArr = array(
														"status" 		=> "1", 
														"message" 		=> "Unable to delete evidence sub-category.",
														"access_token" 	=> $access_token
													);
													
										//Add log data
										 $logs->Module 		= "Evidence Sub-Category";
										 $logs->Action		= "delete";
										 $logs->Request		= json_encode($getArr);
										 $logs->Response	= json_encode($resArr);
										 $logs->UserId		= (int)$user_id;
										 $logs->UserType    = ""; 
										 $logs->AddLog();		
											 
										response($resArr, 401);
										 
									 }	// delete condition end	
									 
							} else {
								 
								// generate jwt
								//$access_token = $auth->generateToken($tokenArr);
								
								$resArr = array(
									"status" 		=> "0", 
									"message" 		=> "Evidence Sub-Category ID does not exist.",
									"access_token" 	=> $access_token
								);
								
								 $logs->Module 		= "Evidence Sub-Category";
								 $logs->Action		= "get";
								 $logs->Request		= json_encode($getArr);
								 $logs->Response	= json_encode($resArr);
								 $logs->UserId		= (int)$user_id;
								 $logs->UserType    = "";  
								 $logs->AddLog();	
					
								response($resArr, 200);
								
							}		 
						 
					 }
					 
		 } else {
					 
						// set response code				
						$resArr = array(
									"status" 	=> "0", 
									"message" 	=> "Invalid Authorization. Error : ". $decoded["TokenData"]
								);
							
						//Add log data
						 $logs->Module 		= "Evidence Sub-Category";
						 $logs->Action		= "delete";
						 $logs->Request		= json_encode($getArr);
						 $logs->Response	= json_encode($resArr);
						 $logs->UserId		= "";
						 $logs->UserType	= "";
						 $logs->AddLog();			
										 
						response($resArr, 401);
					}
			 
		}  else { // show error message if jwt is empty
			 
				// tell the City access denied
				$resArr = array("status" => "0", "message" => "Access denied.");
				
				//Add log data
				 $logs->Module 		= "Evidence Sub-Category";
				 $logs->Action		= "delete";
				 $logs->Request		= "";
				 $logs->Response	= json_encode($resArr);
				 $logs->UserId		= "";
				 $logs->UserType	= "";
				 $logs->AddLog();
							 
				response($resArr, 401);
				
		}
		
	
		break;
		
	case "OPTIONS": //Handle Preflighted requests for CORS issue
		
		// set response code		
		header('Content-Type: text/plain');
		
		response("", 200);
		
		break;
		
	default:
	
	// Invalid Request Method
	$resArr = array("status" => "0", "message" => "Method Not Allowed.");						
	// display message: unable to create user
	response($resArr, 405);	
	
	break;
}
?>