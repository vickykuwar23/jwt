<?php
/*** 'EvidenceSubCategory' object ***/
class Evidencesubcategory{
 
    /*** database connection and table name ***/
    private $conn;
    private $table_name = 'public."evidence_sub_category"';
	private $table_category = 'public."evidence_category"';
	
    /*** object properties ***/ 
	public $id;
	public $evidence_category_id;
    public $evidence_subcategory_name;
	public $is_active;
 
    /*** constructor ***/
    public function __construct($db){
        $this->conn = $db;
		$this->createdOn = date("Y-m-d H:i:s");
    }
	
	/*** check if given Evidence Sub Category ID exist in the database ***/
	function evidence_sub_id_exist($id){
	 
		/*** query to check if Evidence Sub Category ID exists ***/
		$query = 'SELECT "evidence_subcategory_id"
					FROM '.$this->table_name.'
					WHERE "evidence_subcategory_id" = ? AND "is_deleted" = ?
					LIMIT 1';
					
		/*** prepare the query ***/
		$stmt = $this->conn->prepare( $query );
	 
		/*** bind given Evidence Sub Category ID  value ***/
		$stmt->bindParam(1, $id);
		$stmt->bindValue(2, false, PDO::PARAM_BOOL);
		/*** execute the query ***/
		$stmt->execute();
	 
		/*** get number of rows ***/
		$num = $stmt->rowCount();
	 
		/*** if Evidence Sub Category ID  exists, assign values to object properties for easy access and use for php sessions ***/
		if($num > 0){
	 
			/*** return true because Evidence Sub Category ID exists in the database ***/
			return true;
		}
	 
		/*** return false if Evidence Sub Category ID does not exist in the database ***/
		return false;
	}
	
	function getlisting($id = 0, $type = false){
		
		/*** query to check if data exists ***/
		if($id == 0){
			
			$query = 'SELECT C."evidence_subcategory_id", C."evidence_category_id", D."evidence_category_name",  C."evidence_subcategory_name", C."is_active", C."created_on" 
					FROM '.$this->table_name.' AS C JOIN
					'.$this->table_category.' AS D ON
					C.evidence_category_id = D.evidence_category_id
					WHERE C."is_deleted" = ? ORDER BY C."evidence_subcategory_id" ASC';	
					
			/*** prepare the query ***/
			$stmt = $this->conn->prepare( $query );		
		 
			$stmt->bindValue(1, false, PDO::PARAM_BOOL);
			
		} else {
			
			if($type == 'evidenceSubcategory'){
				
				$query = 'SELECT C."evidence_subcategory_id", C."evidence_category_id", D."evidence_category_name",  C."evidence_subcategory_name", C."is_active", C."created_on" 
					FROM '.$this->table_name.' AS C JOIN
					'.$this->table_category.' AS D ON
					C.evidence_category_id = D.evidence_category_id
					WHERE C."is_deleted" = ? AND C."evidence_category_id" = ? ORDER BY C."evidence_subcategory_id" ASC';
				
			} else {
				
				$query = 'SELECT C."evidence_subcategory_id", C."evidence_category_id", D."evidence_category_name",  C."evidence_subcategory_name", C."is_active", C."created_on" 
					FROM '.$this->table_name.' AS C JOIN
					'.$this->table_category.' AS D ON
					C.evidence_category_id = D.evidence_category_id
					WHERE C."is_deleted" = ? AND C."evidence_subcategory_id" = ? ORDER BY C."evidence_subcategory_id" ASC';
					
			}
			
			
			
			/*** prepare the query ***/
			$stmt = $this->conn->prepare( $query );		
		 
			$stmt->bindValue(1, false, PDO::PARAM_BOOL);
			$stmt->bindValue(2, $id);
						
		}
		
		/*** execute the query ***/
		$stmt->execute();
	 
		/*** get number of rows ***/
		$num = $stmt->rowCount();
	 
		/*** if data exists, assign values to object properties for easy access and use for php sessions ***/
		if($num>0){
	 
			/*** get record details / values ***/			
			while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
					$getData[] = $row;
			}
	 
			/*** return true because data exists in the database ***/
			return $getData;
		}
	 
		/*** return false if data does not exist in the database ***/
		return false;
		
	}
	
	function create(){
		
		$query = 'INSERT INTO '.$this->table_name.' ("evidence_category_id", "evidence_subcategory_name", "is_active", "is_deleted", "created_on") VALUES (:evidence_category_id, :evidence_subcategory_name, :is_active, :is_deleted, :created_on) RETURNING evidence_subcategory_id';		
		
		/*** prepare the query ***/
		$stmt = $this->conn->prepare($query);		
	 
		/*** sanitize ***/
		$this->is_active			=  '1';
		$this->is_deleted			=  '0';
		$this->created_on			=  $this->createdOn;
		
		$stmt->bindParam(':evidence_category_id', $this->evidence_category_id);
		$stmt->bindParam(':evidence_subcategory_name', $this->evidence_subcategory_name);
		$stmt->bindParam(':is_active', $this->is_active);
		$stmt->bindParam(':is_deleted', $this->is_deleted);
		$stmt->bindParam(':created_on', $this->created_on);		
	 
		/*** execute the query, also check if query was successful ***/		
		if($stmt->execute()){
			
			$result = $stmt->fetch(PDO::FETCH_ASSOC);
						
			return $result["evidence_subcategory_id"];
			
		} 
		
		return false;
		
	}
	
	
	/*** update record ***/
	public function update($updateStatus){
		
		/*** SQL QUERY ***/
		$query = 'UPDATE ' . $this->table_name . ' SET ';
		
		if($updateStatus == 'UpdateField'){
			
			$query .= '"evidence_category_id" = :evidence_category_id, "evidence_subcategory_name" = :evidence_subcategory_name, ';
			
		} else {
			
			$query .= '"is_active" = :is_active, ';
		}
		
		$query .='"updated_on" = :updated_on WHERE "evidence_subcategory_id" = :evidence_subcategory_id';
		
		/*** prepare the query ***/
		$stmt = $this->conn->prepare($query);
		
		$this->updated_on			=  $this->createdOn;
		
		if($this->evidence_category_id!= "" && $this->evidence_subcategory_name!= ""){
			
			/*** bind the values from the form ***/		
			$stmt->bindParam(':evidence_category_id', $this->evidence_category_id);	
			$stmt->bindParam(':evidence_subcategory_name', $this->evidence_subcategory_name);
		} 
		
		if($this->is_active!= ""){
			
			$stmt->bindParam(':is_active', $this->is_active);
		} 
		
		$stmt->bindParam(':updated_on', $this->updated_on);		
		
		/*** unique ID of record to be edited ***/
		$stmt->bindParam(':evidence_subcategory_id', $this->id);
	 
		/*** execute the query ***/
		if($stmt->execute()){
			
			return true;
		} 
	 
		return false;
		
	} // Function End
	
	
	function delete(){
		
		$query = 'UPDATE ' . $this->table_name . '
					  SET
						"deleted_on"	 	= :deleted_on,
						"is_deleted"	= :is_deleted
					  WHERE "evidence_subcategory_id" 	= :evidence_subcategory_id';
					  
		$stmt = $this->conn->prepare($query);	
		
		$this->is_deleted	=	'1';
		$this->deleted_on  	=	date("Y-m-d H:i:s");
		
		$stmt->bindParam(':is_deleted', $this->is_deleted);
		$stmt->bindParam(':deleted_on', $this->deleted_on);
		
		$stmt->bindParam(':evidence_subcategory_id', $this->id);
		
		//$stmt->execute();
		
		if($stmt->execute()){
			
			return true;
		}
	 
		return false;
			
	}
	
}