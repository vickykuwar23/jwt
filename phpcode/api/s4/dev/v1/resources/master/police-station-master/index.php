<?php

/*** files needed to connect to database and class***/
define("ROOT_PATH", realpath(__DIR__ . '/../../../..'));
include_once ROOT_PATH.'/constant.php';

include_once CONFIG_PATH.'functions.php';
include_once CONFIG_PATH.'database.php';
include_once RESOURCE_MASTERPATH.'police-station-master/police_station_master.php';
include_once RESOURCE_PATH.'logs/logs.php';
include_once RESOURCE_PATH.'auth/auth.php';

// Required headers
requireHeaders();
 
/*** get database connection ***/
$database 	= new Database();
$db 		= $database->getConnection();
$mdb 		= $database->getMongoConnection();
 
// instantiate PoliceStation object
$policeStation  = new PoliceStation($db);
$logs  = new Logs($mdb);
$auth  = new Auth();

// Get Request Method 
$request_method		= get_request_method();

switch($request_method)
{
	
	case "POST":
	
	   $bearerToken = getBearerToken();
		
		$access_token = isset($bearerToken) ? $bearerToken : "";

		// if jwt is not empty
		if($access_token){	

			// decode jwt
			$decoded = $auth->validateToken($access_token);
			
			$data 	= array();
			$getArr = array();
			$reqArr = array();
			
			// if decode succeed, show user details			
			if($decoded['status'] == true){	

				$user_id 		= $decoded['TokenData']->data->user_id; // Decode ID
				$username 		= $decoded['TokenData']->data->username; // Username
				$role_id 		= $decoded['TokenData']->data->role_id; // Role ID
				$tokenArr 		= array("user_id" => (int)$user_id, 
										"username" => $username, 
										"role_id" => (int)$role_id
										);	

							// get posted data
							$data 	= get_request_body();
							
							if($data['status'] == true){
								
								$error = array();
								
								// set policeStation property
								$policeStation->police_station_name = isset($data['requestData']['police_station_name']) ? $data['requestData']['police_station_name'] : "";
								$policeStation->police_station_address = isset($data['requestData']['police_station_address']) ? $data['requestData']['police_station_address'] : "";
								$policeStation->country_id = isset($data['requestData']['country_id']) ? $data['requestData']['country_id'] : "";
								$policeStation->state_id = isset($data['requestData']['state_id']) ? $data['requestData']['state_id'] : "";
								$policeStation->city_id = isset($data['requestData']['city_id']) ? $data['requestData']['city_id'] : "";
								$policeStation->town_name = isset($data['requestData']['town_name']) ? $data['requestData']['town_name'] : "";
								$policeStation->landmark = isset($data['requestData']['landmark']) ? $data['requestData']['landmark'] : "";
								$policeStation->pincode = isset($data['requestData']['pincode']) ? $data['requestData']['pincode'] : "";
								$policeStation->police_station_head = isset($data['requestData']['police_station_head']) ? $data['requestData']['police_station_head'] : "";
								
								if($policeStation->police_station_name == ""){
									
									$error[] = "Please enter Police Station Name.";
									
								} else if (!preg_match('/^[\p{L} ]+$/u', $policeStation->police_station_name)){
									
								  $error[] = 'Police Station Name must contain letters and spaces only!';
								  
								}
								if($policeStation->police_station_address == ""){
									
									$error[] = "Please enter Police Station Address.";
									
								}
								if($policeStation->country_id == ""){
									
									$error[] = "Please select country name.";
									
								} else if(!is_numeric($policeStation->country_id)){
									
									$error[] = "Please select country name is invalid.";
									
								}
								if($policeStation->state_id == ""){
									
									$error[] = "Please select state name.";
									
								} else if(!is_numeric($policeStation->state_id)){
									
									$error[] = "Please select state name is invalid.";
									
								}
								if($policeStation->city_id == ""){
									
									$error[] = "Please select city name.";
									
								} else if(!is_numeric($policeStation->city_id)){
									
									$error[] = "Please select city name is invalid.";
									
								}
								if($policeStation->town_name == ""){
									
									$error[] = "Please enter town name.";
									
								}
								if($policeStation->landmark == ""){
									
									$error[] = "Please enter landmark.";
									
								}
								if($policeStation->pincode == ""){
									
									$error[] = "Please enter pincode.";
									
								}
								if($policeStation->police_station_head == ""){
									
									$error[] = "Please enter police station head officer name.";
									
								} else if(!is_numeric($policeStation->police_station_head)){
									
									$error[] = "Sorry, your selected police station head officer name is invalid.";
									
								}
								
								
								if(count($error) > 0){
									
										$errorMsg = implode(", ", $error);
										
										// tell the user login failed
										$resArr = array("status" => "0", "message" => $errorMsg);
										
										 //Add log data
										 $logs->Module 		= "Police Station";
										 $logs->Action		= "create";
										 $logs->Request		= json_encode($data);
										 $logs->Response	= json_encode($resArr);
										 $logs->UserId		= (int)$user_id;
										 $logs->UserType    = ""; 
										 $logs->AddLog();
											 
										// tell the user login failed
										 response($resArr, 400);
									
								} else {
									
									$insertId = $policeStation->create();
									
									if($insertId > 0){
										
										
										// generate jwt
									//$access_token = $auth->generateToken($tokenArr);
										
										$resArr = array("status" => "1", 
										"message" => "Police Station created successfully.",
										"access_token" 	=> $access_token);
												
										 $logs->Module 		= "Police Station Master";
										 $logs->Action		= "create";
										 $logs->Request		= json_encode($data);
										 $logs->Response	= json_encode($resArr);
										 $logs->UserId		= (int)$user_id;
										 $logs->UserType    = ""; 
										 $logs->AddLog();
										
										// display message: Police Station Master was created
										 response($resArr, 200);
									 
									 } else { 
									 
									  // generate jwt
									//$access_token = $auth->generateToken($tokenArr);
										
											// display message: unable to create Police Station Master
											$resArr = array("status" => "0", "message" => "Unable to create data.",
											"access_token" 	=> $access_token);
											
											 $logs->Module 		= "Police Station Master";
											 $logs->Action		= "create";
											 $logs->Request		= json_encode($data);
											 $logs->Response	= json_encode($resArr);
											 $logs->UserId		= (int)$user_id;
											 $logs->UserType    = ""; 
											 $logs->AddLog();
											
											 // display message: unable to create Police Station Master
											 response($resArr, 400);
											
										} // Insert ID Check
										
								}
							
							} else {
								
								// display message: unable to create Police Station Master
								$resArr = array("status" => "0", "message" => $data["requestData"]);
								
								 $logs->Module 		= "Police Station Master";
								 $logs->Action		= "create";
								 $logs->Request		= json_encode($data);
								 $logs->Response	= json_encode($resArr);
								 $logs->UserId		= (int)$user_id;
								 $logs->UserType    = ""; 
								 $logs->AddLog();
								 
								response($resArr, 400);
							}
							
		 } else {
				
					// show error message
					$resArr = array(
						"status" 	=> "0", 
						"message" 	=> "Invalid Authorization. Error : ". $decoded["TokenData"]
					);
					
					
					// Merge Array Post And GET Values
					$reqArr = array_merge($data, $getArr);
					
					//Add log data
					 $logs->Module 		= "Police Station Master";
					 $logs->Action		= "create";
					 $logs->Request		= json_encode($reqArr);
					 $logs->Response	= json_encode($resArr);
					 $logs->UserId		= "";
					 $logs->UserType    = "";	
					 $logs->AddLog();
					
					response($resArr, 401);
					
			} // Invalid Authorization error		
					
		
		}  else { // show error message if jwt is empty
		 
			// tell the user access denied
			$resArr = array("status" => "0", "message" => "Access denied.");
			
			 $logs->Module 		= "Police Station Master";
			 $logs->Action		= "create";
			 $logs->Request		= "";
			 $logs->Response	= json_encode($resArr);
			 $logs->UserId		= "";
			 $logs->UserType	= "";
			 $logs->AddLog();
			
			// display message: unable to create user
			response($resArr, 401);
		}						
		
		break;
		
	case "PUT":

	    $bearerToken = getBearerToken();
		
		$access_token = isset($bearerToken) ? $bearerToken : "";
	
		// if jwt is not empty
		if($access_token){	

			// decode jwt
			$decoded = $auth->validateToken($access_token);
			
			$data 	= array();
			$getArr = array();
			$reqArr = array();
			
			if($decoded['status'] == true){	

				$user_id 		= $decoded['TokenData']->data->user_id; // Decode ID
				$username 		= $decoded['TokenData']->data->username; // Username
				$role_id 		= $decoded['TokenData']->data->role_id; // Role ID
				$tokenArr 		= array("user_id" => (int)$user_id, 
										"username" => $username, 
										"role_id" => (int)$role_id
										);	
				
						// Get URL Data	
						$getArr = get_url_data();
						
						// Get ID From URL		
						$policeStation->id = isset($getArr['id']) ? intval($getArr['id']) : "";
						
						$error = array();
						
						if($policeStation->id == ""){
										
							$error[] = "Police Station ID is required.";
							
						}

						if(count($error) > 0){
								
								$errorMsg = implode(", ", $error);
								
								// tell the PoliceStation error occur
								$resArr = array("status" => "0", "message" => $errorMsg);
								
								// Merge Array Post And GET Values
								$reqArr = array_merge($data, $getArr);
								
								 //Add log data
								 $logs->Module 		= "Police Station Master";
								 $logs->Action		= "update";
								 $logs->Request		= json_encode($reqArr);
								 $logs->Response	= json_encode($resArr);
								 $logs->UserId		= (int)$user_id;
								 $logs->UserType    = ""; 
								 $logs->AddLog();
									 
								// tell the PoliceStation error occur
								 response($resArr, 400);
							
						} 	
						
						// get posted data
						$data = get_request_body();
						
						if($data['status'] == true){
							
							
							
							// set policeStation property
							$policeStation->police_station_name = isset($data['requestData']['police_station_name']) ? $data['requestData']['police_station_name'] : "";
							$policeStation->police_station_address = isset($data['requestData']['police_station_address']) ? $data['requestData']['police_station_address'] : "";
							$policeStation->country_id = isset($data['requestData']['country_id']) ? $data['requestData']['country_id'] : "";
							$policeStation->state_id = isset($data['requestData']['state_id']) ? $data['requestData']['state_id'] : "";
							$policeStation->city_id = isset($data['requestData']['city_id']) ? $data['requestData']['city_id'] : "";
							$policeStation->town_name = isset($data['requestData']['town_name']) ? $data['requestData']['town_name'] : "";
							$policeStation->landmark = isset($data['requestData']['landmark']) ? $data['requestData']['landmark'] : "";
							$policeStation->pincode = isset($data['requestData']['pincode']) ? $data['requestData']['pincode'] : "";
							$policeStation->police_station_head = isset($data['requestData']['police_station_head']) ? $data['requestData']['police_station_head'] : "";
							
							$policeStation->is_active = isset($data['requestData']['is_active']) ? $data['requestData']['is_active'] : "";
							
							if(isset($data['requestData']['is_active'])){
									
								$updateStatus 		= "UpdateStatus";
								$UpdateMsg			= "Police Station status successfully updated.";
								$policeStation->is_active 	= $data['requestData']['is_active'];
								
								if($policeStation->is_active == ""){
							
										$error[] = "Is Active is required.";
										
								} 
										
							} else {			
								
								$updateStatus 		= "UpdateField";
								$UpdateMsg			= "Police Station updated successfully.";
								
								if($policeStation->police_station_name == ""){
								
								$error[] = "Please enter Police Station Name.";
									
								} else if (!preg_match('/^[\p{L} ]+$/u', $policeStation->police_station_name)){
									
								  $error[] = 'Police Station Name must contain letters and spaces only!';
								  
								}
								if($policeStation->police_station_address == ""){
									
									$error[] = "Please enter Police Station Address.";
									
								}
								if($policeStation->country_id == ""){
									
									$error[] = "Please select country name.";
									
								} else if(!is_numeric($policeStation->country_id)){
								
									$error[] = "Please select country name is invalid.";
									
								}
								if($policeStation->state_id == ""){
									
									$error[] = "Please select state name.";
									
								} else if(!is_numeric($policeStation->state_id)){
								
									$error[] = "Please select state name is invalid.";
									
								}
								if($policeStation->city_id == ""){
									
									$error[] = "Please select city name.";
									
								} else if(!is_numeric($policeStation->city_id)){
								
									$error[] = "Please select city name is invalid.";
									
								}
								if($policeStation->town_name == ""){
									
									$error[] = "Please enter town name.";
									
								}
								if($policeStation->landmark == ""){
									
									$error[] = "Please enter landmark.";
									
								}
								if($policeStation->pincode == ""){
									
									$error[] = "Please enter pincode.";
									
								}
								if($policeStation->police_station_head == ""){
									
									$error[] = "Please enter police station head officer name.";
									
								} else if(!is_numeric($policeStation->police_station_head)){
								
									$error[] = "Sorry, your selected police station head officer name is invalid.";
									
								}
								
							} // URL ID validation required			
							
							if(count($error) > 0){
								
									$errorMsg = implode(", ", $error);
									
									// tell the PoliceStation error occur
									$resArr = array("status" => "0", "message" => $errorMsg);
									
									// Merge Array Post And GET Values
									$reqArr = array_merge($data, $getArr);
									
									 //Add log data
									 $logs->Module 		= "Police Station Master";
									 $logs->Action		= "update";
									 $logs->Request		= json_encode($reqArr);
									 $logs->Response	= json_encode($resArr);
									 $logs->UserId		= (int)$user_id;
									 $logs->UserType    = ""; 
									 $logs->AddLog();
										 
									// tell the PoliceStation error occur
									 response($resArr, 400);
								
							} else {
								
								
								
								$ps_id_exist = $policeStation->police_station_id_exist($policeStation->id);
									
								if($ps_id_exist == true){									
									
										$ps_id = 
										$policeStation->update($updateStatus);
										
										if($ps_id == true){
											
											// generate jwt
											//$access_token = $auth->generateToken($tokenArr);
											
											$resArr = array("status" => "1", "message" => $UpdateMsg,
											"access_token" 	=> $access_token);
											
											// Merge Array Post And GET Values
											$reqArr = array_merge($data, $getArr);
													
											 $logs->Module 		= "Police Station Master";
											 $logs->Action		= "update";
											 $logs->Request		= json_encode($reqArr);
											 $logs->Response	= json_encode($resArr);
											 $logs->UserId		= (int)$user_id;
											 $logs->UserType    = ""; 
											 $logs->AddLog();
											
											// display message: Police Station Master was updated
											 response($resArr, 200);
										 
										 } else { 
										 
										 // generate jwt
										//$access_token = $auth->generateToken($tokenArr);
											
												// display message: unable to update Police Station Master
												$resArr = array("status" => "0", 
												"message" => "Unable to update data.",
												"access_token" 	=> $access_token);
												
												// Merge Array Post And GET Values
											$reqArr = array_merge($data, $getArr);
												
												 $logs->Module 		= "Police Station Master";
												 $logs->Action		= "update";
												 $logs->Request		= json_encode($reqArr);
												 $logs->Response	= json_encode($resArr);
												 $logs->UserId		= (int)$user_id;
												 $logs->UserType    = ""; 
												 $logs->AddLog();
												
												 // display message: unable to update Police Station Master
												 response($resArr, 400);
												
											} // Insert ID Check
											
									} else {
										
										// generate jwt
								//$access_token = $auth->generateToken($tokenArr);
						
									$resArr = array(
											"status" 		=> "0", 
											"message" 		=> "Police Station ID does not exist.",
											"access_token" 	=> $access_token
										);
										
										// Merge Array Post And GET Values
								$reqArr = array_merge($data, $getArr);	
										
									 $logs->Module 		= "Police Station Master";
									 $logs->Action		= "update";
									 $logs->Request		= json_encode($reqArr);
									 $logs->Response	= json_encode($resArr);
									 $logs->UserId		= (int)$user_id;
									 $logs->UserType    = "";  
									 $logs->AddLog();	
										
									response($resArr, 200);
									
								}
							}
						
						} else {
							
							// display message: unable to update Police Station Master
							$resArr = array("status" => "0", "message" => $data["requestData"]);
							
							// Merge Array Post And GET Values
								$reqArr = array_merge($data, $getArr);
							
							 $logs->Module 		= "Police Station Master";
							 $logs->Action		= "update";
							 $logs->Request		= json_encode($reqArr);
							 $logs->Response	= json_encode($resArr);
							 $logs->UserId		= (int)$user_id;
							 $logs->UserType    = ""; 
							 $logs->AddLog();
							 
							response($resArr, 400);
						}
		
		} else {
				
						// show error message
						$resArr = array(
							"status" 	=> "0", 
							"message" 	=> "Invalid Authorization. Error : ". $decoded["TokenData"]
						);
						
						// Merge Array Post And GET Values
						$reqArr = array_merge($data, $getArr);
						
						//Add log data
						 $logs->Module 		= "Police Station Master";
						 $logs->Action		= "update";
						 $logs->Request		= json_encode($reqArr);
						 $logs->Response	= json_encode($resArr);
						 $logs->UserId		= "";
						 $logs->UserType    = "";	
						 $logs->AddLog();
						
						response($resArr, 401);
				}
		
			}  else { // show error message if jwt is empty
		 
			// tell the user access denied
			$resArr = array("status" => "0", "message" => "Access denied.");
			
			//Add log data
			 $logs->Module 		= "Police Station Master";
			 $logs->Action		= "update";
			 $logs->Request		= "";
			 $logs->Response	= json_encode($resArr);
			 $logs->UserId		= "";
			 $logs->UserType	= "";			 
			 $logs->AddLog();
			
			// display message: unable to update Police Station Master
			response($resArr, 401);
		}		
		
		break;	
		
	case 'GET':
	
		$bearerToken = getBearerToken();
		
		$access_token = isset($bearerToken) ? $bearerToken : "";
			
		// if jwt is not empty
		if($access_token){
			
			// decode jwt		
			$decoded  = $auth->validateToken($access_token);	
			
			$getArr = array();
			
			 // if decode succeed, show user details
			if($decoded["status"] == true) {
				
				$user_id 		= $decoded['TokenData']->data->user_id; // Decode ID
				$username 		= $decoded['TokenData']->data->username; // Username
				$role_id 		= $decoded['TokenData']->data->role_id; // Role ID
				$tokenArr 		= array("user_id" => (int)$user_id, 
										"username" => $username, 
										"role_id" => (int)$role_id
										);	
			
				$getArr = get_url_data();					
				
				$id 	= isset($getArr['id']) ? intval($getArr['id']) : 0;
				
				
				$getList = false;
				
				if($id == ""){ 
				
					$getList = $policeStation->getlisting();
				}
				
				if($id > 0){
					
				$ps_id_exist = $policeStation->police_station_id_exist($id);
				
					if($ps_id_exist == true){
						
						$getList = $policeStation->getlisting($id);
						
					} else {
						
						// generate jwt
						//	$access_token = $auth->generateToken($tokenArr);
						
						$resArr = array(
								"status" 		=> "0", 
								"message" 		=> "Police Station ID does not exist.",
								"access_token" 	=> $access_token
							);
							
						 $logs->Module 		= "Police Station Master";
						 $logs->Action		= "get";
						 $logs->Request		= json_encode($getArr);
						 $logs->Response	= json_encode($resArr);
						 $logs->UserId		= (int)$user_id;
						 $logs->UserType    = "";  
						 $logs->AddLog();	
							
						response($resArr, 200);
					}
					
				}	
				
				if($getList){
					
					// generate jwt
					//$access_token = $auth->generateToken($tokenArr);
					
					$resArr = array(
						"status" 		=> "1", 
						"message" 		=> "Police Station data get successfully.",
						"access_token" 	=> $access_token,
						"data" 			=> $getList
					);

					 $logs->Module 		= "Police Station Master";
					 $logs->Action		= "get";
					 $logs->Request		= json_encode($getArr);
					 $logs->Response	= json_encode($resArr);
					 $logs->UserId		= (int)$user_id;
					 $logs->UserType    = ""; 		
					 $logs->AddLog();		
					
					response($resArr, 200);
					
				} else {
					
					$getList = array();
					
					// generate jwt
					//$access_token = $auth->generateToken($tokenArr);
					
					$resArr = array(
						"status" 		=> "0", 
						"message" 		=> "No data found.",
						"access_token" 	=> $access_token,
						"data" 			=> $getList
					);

					 $logs->Module 		= "Police Station Master";
					 $logs->Action		= "get";
					 $logs->Request		= json_encode($getArr);
					 $logs->Response	= json_encode($resArr);
					 $logs->UserId		= (int)$user_id;
					 $logs->UserType    = "";  	
					 $logs->AddLog();		
					
					response($resArr, 200);
					
				}
				
		} else {			 
				
				// show error message
				$resArr =array(
					"status" 	=> "0", 
					"message" 	=> "Invalid Authorization. Error : ". $decoded["TokenData"]
				);
				
				//Add log data
				 $logs->Module 		= "Police Station Master";
				 $logs->Action		= "get";
				 $logs->Request		= "";
				 $logs->Response	= json_encode($resArr);
				 $logs->UserId		= "";
				 $logs->UserType	= "";	
				 $logs->AddLog();
				
				response($resArr, 401);
			}			
							
		}  else { // show error message if jwt is empty		 
			
			$resArr = array("status" => "0", "message" => "Access denied.");
			
			//Add log data
			 $logs->Module 		= "Police Station Master";
			 $logs->Action		= "get";
			 $logs->Request		= "";
			 $logs->Response	= json_encode($resArr);
			 $logs->UserId		= "";
			 $logs->UserType	= "";	
			 $logs->AddLog();
			
			response($resArr, 401);
		}
		
		break;
		
		
	case 'DELETE':	
	
		$bearerToken = getBearerToken();
		
		$access_token = isset($bearerToken) ? $bearerToken : "";
	
		// if jwt is not empty
		if($access_token)
		{
			
			// decode jwt
			$decoded = $auth->validateToken($access_token);
	
			$getArr = array();
			
			// if decode succeed, show user details
			if($decoded["status"] == true) {
					
				$user_id 		= $decoded['TokenData']->data->user_id; // Decode ID
				$username 		= $decoded['TokenData']->data->username; // Username
				$role_id 		= $decoded['TokenData']->data->role_id; // Role ID
				$tokenArr 		= array("user_id" => (int)$user_id, 
										"username" => $username, 
										"role_id" => (int)$role_id
										);	
			
						$getArr = get_url_data();
									
						// set PoliceStation property values	
						$policeStation->id = isset($getArr['id']) ? intval($getArr['id']) : "";
						
						$error = array();
						
						if($policeStation->id == ""){
										
							$error[] = "Police station ID is required.";
							
						} 
						
						if(!is_numeric($policeStation->id)){
							
							$error[] = "Police station ID is must contain number.";
							
						}
						
						if(count($error) > 0){
								
						$errorMsg = implode(", ", $error);
							 
						// tell the user login failed
						$resArr = array("status" => "0", "message" => $errorMsg);						

						//Add log data
						 $logs->Module 		= "Police Station Master";
						 $logs->Action		= "delete";
						 $logs->Request		= json_encode($getArr);
						 $logs->Response	= json_encode($resArr);
						 $logs->UserId		= (int)$user_id;
						 $logs->UserType    = ""; 
						 $logs->AddLog();

						response($resArr, 200);
						 
					 } else {
						 
						 
						 $ps_id_exist = $policeStation->police_station_id_exist($policeStation->id);
							
						if($ps_id_exist == true){
						 
								 $delete_ps = $policeStation->delete();
													
									if($delete_ps == true){	

											// generate jwt
										//$access_token = $auth->generateToken($tokenArr);
											 
										$resArr = array(
														"status" 		=> "1", 
														"message" 		=> "Police Station was successfully deleted.",
														"access_token" 	=> $access_token
													);			
										
										//Add log data
										 $logs->Module 		= "Police Station Master";
										 $logs->Action		= "delete";
										 $logs->Request		= json_encode($getArr);
										 $logs->Response	= json_encode($resArr);
										 $logs->UserId		= (int)$user_id;
										 $logs->UserType    = ""; 
										 $logs->AddLog();

										response($resArr, 200);
										
									 } else {
										 
										 // generate jwt
										 //$access_token = $auth->generateToken($tokenArr);
										 
										 $resArr = array(
														"status" 		=> "1", 
														"message" 		=> "Unable to delete police station master.",
														"access_token" 	=> $access_token
													);
													
										//Add log data
										 $logs->Module 		= "Police Station Master";
										 $logs->Action		= "delete";
										 $logs->Request		= json_encode($getArr);
										 $logs->Response	= json_encode($resArr);
										 $logs->UserId		= (int)$user_id;
										 $logs->UserType    = ""; 
										 $logs->AddLog();		
											 
										response($resArr, 401);
										 
									 }	// delete condition end

							} else {
								
								// generate jwt
								//$access_token = $auth->generateToken($tokenArr);								
										 
								$resArr = array(
									"status" 		=> "0", 
									"message" 		=> "Police Station ID does not exist.",
									"access_token" 	=> $access_token
								);
								
								 $logs->Module 		= "Police Station Master";
								 $logs->Action		= "delete";
								 $logs->Request		= json_encode($getArr);
								 $logs->Response	= json_encode($resArr);
								 $logs->UserId		= (int)$user_id;
								 $logs->UserType    = "";   
								 $logs->AddLog();	
					
								response($resArr, 200);
								
							}						
						 
					 }
					 
		} else {
					 
						// set response code				
						$resArr = array(
									"status" 	=> "0", 
									"message" 	=> "Invalid Authorization. Error : ". $decoded["TokenData"]
								);
							
						//Add log data
						 $logs->Module 		= "Police Station Master";
						 $logs->Action		= "delete";
						 $logs->Request		= json_encode($getArr);
						 $logs->Response	= json_encode($resArr);
						 $logs->UserId		= "";
						 $logs->UserType	= "";
						 $logs->AddLog();			
										 
						response($resArr, 401);
					}
			 
		}  else { // show error message if jwt is empty
			 
				// tell the City access denied
				$resArr = array("status" => "0", "message" => "Access denied.");
				
				//Add log data
				 $logs->Module 		= "Police Station Master";
				 $logs->Action		= "delete";
				 $logs->Request		= "";
				 $logs->Response	= json_encode($resArr);
				 $logs->UserId		= "";
				 $logs->UserType	= "";
				 $logs->AddLog();
							 
				response($resArr, 401);
				
		}			 
	
		break;
		
	case "OPTIONS": //Handle Preflighted requests for CORS issue
		
		// set response code		
		header('Content-Type: text/plain');
		
		response("", 200);
		
		break;
		
	default:
	
		// Invalid Request Method
		$resArr = array("status" => "0", "message" => "Method Not Allowed.");						
		// display message: unable to create user
		response($resArr, 405);	
		
		break;
}

?>