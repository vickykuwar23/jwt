<?php
/*** 'PoliceStation' object ***/
class PoliceStation{
 
    /*** database connection and table name ***/
    private $conn;
    private $table_name 	= 'public."police_station_master"';
	private $table_user 	= 'public."users"';
	private $table_country 	= 'public."countries"';
	private $table_state 	= 'public."states"';
	private $table_city 	= 'public."city"';
	
    /*** object properties ***/ 
	public $id;
	public $police_station_name;
	public $police_station_address;
	public $country_id;
	public $state_id;
	public $city_id;
	public $town_name;
	public $landmark;
	public $pincode;
	public $police_station_head;
	public $is_active;
 
    /*** constructor ***/
    public function __construct($db){
        $this->conn = $db;
		$this->createdOn = date("Y-m-d H:i:s");
    }
	
	/*** check if given Police Station ID exist in the database ***/
	function police_station_id_exist($id){
	 
		/*** query to check if Police Station ID exists ***/
		$query = 'SELECT "police_station_id"
					FROM '.$this->table_name.'
					WHERE "police_station_id" = ? AND "is_deleted" = ?
					LIMIT 1';
					
		/*** prepare the query ***/
		$stmt = $this->conn->prepare( $query );
	 
		/*** bind given Police Station ID  value ***/
		$stmt->bindParam(1, $id);
		$stmt->bindValue(2, false, PDO::PARAM_BOOL);
		/*** execute the query ***/
		$stmt->execute();
	 
		/*** get number of rows ***/
		$num = $stmt->rowCount();
	 
		/*** if Police Station ID  exists, assign values to object properties for easy access and use for php sessions ***/
		if($num > 0){
	 
			/*** return true because Police Station ID exists in the database ***/
			return true;
		}
	 
		/*** return false if Police Station ID does not exist in the database ***/
		return false;
	}
	
	function getlisting($id =0){
		
		/*** query to check if email exists ***/
		if($id == 0){
					
			$query = 'SELECT P."police_station_id", P."police_station_name", P."police_station_address", P."country_id", C."country_name", P."state_id", S."state_name", P."city_id", T."city_name",  P."town_name",  P."landmark", P."pincode", P."police_station_head", U."display_name", U."username", P."is_active", P."created_on" 
					FROM '.$this->table_name.'  AS P 
					JOIN '.$this->table_user.'  AS U ON
					P."police_station_head" = U."users_id"
					JOIN '.$this->table_country.' AS C ON
					P."country_id" = C."country_id" 
					JOIN '.$this->table_state.' AS S ON
					P."state_id" = S."state_id"
					JOIN '.$this->table_city.' AS T ON
					P."city_id" = T."city_id" 
					WHERE P."is_deleted" = ? 
					ORDER BY P."police_station_id" ASC';		
					
			/*** prepare the query ***/
			$stmt = $this->conn->prepare( $query );		
		 
			$stmt->bindValue(1, false, PDO::PARAM_BOOL);
			
		} else {
					
			$query = 'SELECT P."police_station_id", P."police_station_name", P."police_station_address", P."country_id", C."country_name", P."state_id", S."state_name", P."city_id", T."city_name",  P."town_name",  P."landmark", P."pincode", P."police_station_head", U."display_name", U."username", P."is_active", P."created_on" 
					FROM '.$this->table_name.'  AS P 
					JOIN '.$this->table_user.'  AS U ON
					P."police_station_head" = U."users_id"
					JOIN '.$this->table_country.' AS C ON
					P."country_id" = C."country_id" 
					JOIN '.$this->table_state.' AS S ON
					P."state_id" = S."state_id"
					JOIN '.$this->table_city.' AS T ON
					P."city_id" = T."city_id" 
					WHERE P."is_deleted" = ? 
					AND P."police_station_id" = ? 
					ORDER BY P."police_station_id" ASC';		
			
			/*** prepare the query ***/
			$stmt = $this->conn->prepare( $query );		
		 
			$stmt->bindValue(1, false, PDO::PARAM_BOOL);
			$stmt->bindValue(2, $id);
						
		}
		
		/*** execute the query ***/
		$stmt->execute();
	 
		/*** get number of rows ***/
		$num = $stmt->rowCount();
	 
		/*** if PoliceStation exists, assign values to object properties for easy access and use for php sessions ***/
		if($num>0){
	 
			/*** get record details / values ***/			
			while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
					$getData[] = $row;
			}
	 
			/*** return true because PoliceStation exists in the database ***/
			return $getData;
		}
	 
		/*** return false if PoliceStation does not exist in the database ***/
		return false;
		
	}
	
	function create(){
		
		$query = 'INSERT INTO '.$this->table_name.' ("police_station_name", "police_station_address", "country_id", "state_id", "city_id", "town_name", "landmark", "pincode", "police_station_head", "is_active", "is_deleted", "created_on") VALUES (:police_station_name, :police_station_address, :country_id, :state_id, :city_id, :town_name, :landmark, :pincode, :police_station_head, :is_active, :is_deleted, :created_on) RETURNING police_station_id';		
		
		/*** prepare the query ***/
		$stmt = $this->conn->prepare($query);		
	 
		/*** sanitize ***/
		$this->is_active			=  '1';
		$this->is_deleted			=  '0';
		$this->created_on			=  $this->createdOn;
		
		$stmt->bindParam(':police_station_name', $this->police_station_name);
		$stmt->bindParam(':police_station_address', $this->police_station_address);
		$stmt->bindParam(':country_id', $this->country_id);
		$stmt->bindParam(':state_id', $this->state_id);
		$stmt->bindParam(':city_id', $this->city_id);
		$stmt->bindParam(':town_name', $this->town_name);
		$stmt->bindParam(':landmark', $this->landmark);
		$stmt->bindParam(':pincode', $this->pincode);
		$stmt->bindParam(':police_station_head', $this->police_station_head);
		$stmt->bindParam(':is_active', $this->is_active);
		$stmt->bindParam(':is_deleted', $this->is_deleted);
		$stmt->bindParam(':created_on', $this->created_on);
		
	 
		/*** execute the query, also check if query was successful ***/		
		if($stmt->execute()){
			
			$result = $stmt->fetch(PDO::FETCH_ASSOC);
						
			return $result["police_station_id"];
			
		} 
		
		return false;
		
	}
	
	
	/*** update record ***/
	public function update($updateStatus){
		
		/*** SQL QUERY ***/
		$query = 'UPDATE ' . $this->table_name . ' SET ';

		if($updateStatus == 'UpdateField'){
			
			$query .= '"police_station_name" = :police_station_name, 
					"police_station_address" = :police_station_address, 
					"country_id" = :country_id, 
					"state_id" = :state_id, 
					"city_id" = :city_id, 
					"town_name" = :town_name, 
					"landmark" = :landmark, 
					"pincode" = :pincode, 
					"police_station_head" = :police_station_head, ';
			
		} else {
			
			$query .= '"is_active" = :is_active, ';
		}	

		$query .='"updated_on" = :updated_on 
					WHERE "police_station_id" = :police_station_id';
					
		//echo $query;exit;				
		
		/*** prepare the query ***/
		$stmt = $this->conn->prepare($query);
		
		$this->updated_on			=  $this->createdOn;
		
		
		if($this->is_active!= ""){
			
			$stmt->bindParam(':is_active', $this->is_active);
			
		} else {
			
			/*** bind the values from the form ***/		
			$stmt->bindParam(':police_station_name', $this->police_station_name);
			$stmt->bindParam(':police_station_address', $this->police_station_address);
			$stmt->bindParam(':country_id', $this->country_id);
			$stmt->bindParam(':state_id', $this->state_id);
			$stmt->bindParam(':city_id', $this->city_id);
			$stmt->bindParam(':town_name', $this->town_name);
			$stmt->bindParam(':landmark', $this->landmark);
			$stmt->bindParam(':pincode', $this->pincode);
			$stmt->bindParam(':police_station_head', $this->police_station_head);
			
			
			
		}	
		
		$stmt->bindParam(':updated_on', $this->updated_on);
		
		/*** unique ID of record to be edited ***/
		$stmt->bindParam(':police_station_id', $this->id);
	 
		/*** execute the query ***/
		if($stmt->execute()){
			
			return true;
		}
	 
		return false;
		
	} // Function End
	
	
	function delete(){
		
		$query = 'UPDATE ' . $this->table_name . '
					  SET
						"deleted_on"	 	= :deleted_on,
						"is_deleted"	= :is_deleted
					  WHERE "police_station_id" 	= :police_station_id';
					  
		$stmt = $this->conn->prepare($query);	
		
		$this->is_deleted	=	'1';
		$this->deleted_on  	=	date("Y-m-d H:i:s");
		
		$stmt->bindParam(':is_deleted', $this->is_deleted);
		$stmt->bindParam(':deleted_on', $this->deleted_on);
		
		$stmt->bindParam(':police_station_id', $this->id);
		
		if($stmt->execute()){
			
			return true;
		}
	 
		return false;
			
	}
	
}