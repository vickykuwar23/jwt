<?php

/*** files needed to connect to database and class***/
define("ROOT_PATH", realpath(__DIR__ . '/../../../..'));
include_once ROOT_PATH.'/constant.php';

include_once CONFIG_PATH.'functions.php';
include_once CONFIG_PATH.'database.php';
include_once RESOURCE_MASTERPATH.'evidence-category/evidence_category.php';
include_once RESOURCE_PATH.'logs/logs.php';
include_once RESOURCE_PATH.'auth/auth.php';

// Required headers
requireHeaders();
 
/*** get database connection ***/
$database 	= new Database();
$db 		= $database->getConnection();
$mdb 		= $database->getMongoConnection();
 
// instantiate EvidenceCategory object
$evidenceCat  = new EvidenceCategory($db);
$logs  = new Logs($mdb);
$auth  = new Auth();

// Get Request Method 
$request_method		= get_request_method();

switch($request_method)
{
	
	case "POST":
	
	$bearerToken = getBearerToken();
		
		$access_token = isset($bearerToken) ? $bearerToken : "";

		// if jwt is not empty
		if($access_token){	

			// decode jwt
			$decoded = $auth->validateToken($access_token);
			
			$data 	= array();
			$getArr = array();
			$reqArr = array();
			
			// if decode succeed, show user details			
			if($decoded['status'] == true){	

				$user_id 		= $decoded['TokenData']->data->user_id; // Decode ID
				$username 		= $decoded['TokenData']->data->username; // Username
				$role_id 		= $decoded['TokenData']->data->role_id; // Role ID
				$tokenArr 		= array("user_id" => (int)$user_id, 
										"username" => $username, 
										"role_id" => (int)$role_id
										);	

						// get posted data
						$data 	= get_request_body();
						
						if($data['status'] == true){
							
							$error = array();
							
							// set EvidenceCategory property
							$evidenceCat->evidence_category_name = isset($data['requestData']['evidence_category_name']) ? $data['requestData']['evidence_category_name'] : "";
							
							if($evidenceCat->evidence_category_name == ""){
								
								$error[] = "Please enter Evidence Category Name.";
								
							} 
							if (!preg_match('/^[\p{L} ]+$/u', $evidenceCat->evidence_category_name)){
								
							  $error[] = 'Evidence Category Name must contain letters and spaces only!';
							  
							}
							
							
							if(count($error) > 0){
								
									$errorMsg = implode(", ", $error);
									
									// tell the user login failed
									$resArr = array("status" => "0", "message" => $errorMsg);
									
									 //Add log data
									 $logs->Module 		= "Evidence Category";
									 $logs->Action		= "create";
									 $logs->Request		= json_encode($data);
									 $logs->Response	= json_encode($resArr);
									 $logs->UserId		= (int)$user_id;
									 $logs->UserType    = ""; 
									 $logs->AddLog();
										 
									// tell the user login failed
									 response($resArr, 400);
								
							} else {
								
								$insertId = $evidenceCat->create();
								
								if($insertId > 0){
									
										// generate jwt
									//$access_token = $auth->generateToken($tokenArr);
									
									$resArr = array("status" => "1", 
									"message" => "Evidence Category created successfully.",
									"access_token" 	=> $access_token);
											
									 $logs->Module 		= "Evidence Category";
									 $logs->Action		= "create";
									 $logs->Request		= json_encode($data);
									 $logs->Response	= json_encode($resArr);
									 $logs->UserId		= (int)$user_id;
									 $logs->UserType    = ""; 
									 $logs->AddLog();
									
									// display message: Evidence Category was created
									 response($resArr, 200);
								 
								 } else { // message if unable to create Evidence Category
								 
								 // generate jwt
									//$access_token = $auth->generateToken($tokenArr);
									
										// display message: unable to create Evidence Category
										$resArr = array("status" => "0", 
										"message" => "Unable to create data.",
										"access_token" 	=> $access_token);
										
										 $logs->Module 		= "Evidence Category";
										 $logs->Action		= "create";
										 $logs->Request		= json_encode($data);
										 $logs->Response	= json_encode($resArr);
										 $logs->UserId		= (int)$user_id;
										 $logs->UserType    = ""; 
										 $logs->AddLog();
										
										 // display message: unable to create Evidence Category
										 response($resArr, 400);
										
									} // Insert ID Check
									
							}
						
						} else {
							
							// display message: unable to create Evidence
							$resArr = array("status" => "0", "message" => $data["requestData"]);
							
								// Merge Array Post And GET Values
							$reqArr = array_merge($data, $getArr);
							
							 $logs->Module 		= "Evidence Category";
							 $logs->Action		= "create";
							 $logs->Request		= json_encode($reqArr);
							 $logs->Response	= json_encode($resArr);
							 $logs->UserId		= (int)$user_id;
							 $logs->UserType    = ""; 
							 $logs->AddLog();
							 
							response($resArr, 400);
						}
						
			 } else {
				
					// show error message
					$resArr = array(
						"status" 	=> "0", 
						"message" 	=> "Invalid Authorization. Error : ". $decoded["TokenData"]
					);
					
					// Merge Array Post And GET Values
					$reqArr = array_merge($data, $getArr);
					
					//Add log data
					 $logs->Module 		= "Evidence Category";
					 $logs->Action		= "create";
					 $logs->Request		= json_encode($reqArr);
					 $logs->Response	= json_encode($resArr);
					 $logs->UserId		= "";
					 $logs->UserType    = "";	
					 $logs->AddLog();
					
					response($resArr, 401);
					
			} // Invalid Authorization error		
					
		
		}  else { // show error message if jwt is empty
		 
			// tell the user access denied
			$resArr = array("status" => "0", "message" => "Access denied.");
			
			 $logs->Module 		= "Evidence Category";
			 $logs->Action		= "create";
			 $logs->Request		= "";
			 $logs->Response	= json_encode($resArr);
			 $logs->UserId		= "";
			 $logs->UserType	= "";
			 $logs->AddLog();
			
			// display message: unable to create user
			response($resArr, 401);
		}	
		
		break;
		
	case "PUT":

	    $bearerToken = getBearerToken();
		
		$access_token = isset($bearerToken) ? $bearerToken : "";
	
		// if jwt is not empty
		if($access_token){	

			// decode jwt
			$decoded = $auth->validateToken($access_token);
			
			$data 	= array();
			$getArr = array();
			$reqArr = array();
			
			if($decoded['status'] == true){	

				$user_id 		= $decoded['TokenData']->data->user_id; // Decode ID
				$username 		= $decoded['TokenData']->data->username; // Username
				$role_id 		= $decoded['TokenData']->data->role_id; // Role ID
				$tokenArr 		= array("user_id" => (int)$user_id, 
										"username" => $username, 
										"role_id" => (int)$role_id
										);	
				
					// Get URL Data	
					$getArr = get_url_data();
					
					// Get ID From URL		
					$evidenceCat->id = isset($getArr['id']) ? intval($getArr['id']) : "";
					
					$error = array();
					
					if($evidenceCat->id == ""){
									
						$error[] = "Evidence Category ID is required.";
						
					} 
					
					
					if(count($error) > 0){
							
							$errorMsg = implode(", ", $error);
							
							// tell the Evidence Category error occur
							$resArr = array("status" => "0", "message" => $errorMsg);
							
							// Merge Array Post And GET Values
									$reqArr = array_merge($data, $getArr);
							
							 //Add log data
							 $logs->Module 		= "Evidence Category";
							 $logs->Action		= "update";
							 $logs->Request		= json_encode($reqArr);
							 $logs->Response	= json_encode($resArr);
							 $logs->UserId		= (int)$user_id;
							 $logs->UserType    = ""; 
							 $logs->AddLog();
								 
							// tell the Evidence Category error occur
							 response($resArr, 400);
						
					} 
					
					// get posted data
					$data = get_request_body();
					
					if($data['status'] == true){					
						
						
						// set EvidenceCategory property
						$evidenceCat->evidence_category_name = isset($data['requestData']['evidence_category_name']) ? $data['requestData']['evidence_category_name'] : "";
						
						$evidenceCat->is_active = isset($data['requestData']['is_active']) ? $data['requestData']['is_active'] : "";	
						
						if(isset($data['requestData']['is_active'])){
									
								$updateStatus 		= "UpdateStatus";
								$UpdateMsg			= "Evidence Category status successfully updated.";
								$evidenceCat->is_active 	= $data['requestData']['is_active'];
								
								if($evidenceCat->is_active == ""){
							
										$error[] = "Is Active is required.";
										
								} 
										
							} else {			
										
									
								$updateStatus 		= "UpdateField";
								$UpdateMsg			= "Evidence Category updated successfully.";
									
									if($evidenceCat->evidence_category_name == ""){
								
										$error[] = "Please enter Evidence Category Name.";
										
									} 
									if (!preg_match('/^[\p{L} ]+$/u', $evidenceCat->evidence_category_name)){
										
										$error[] = 'Evidence Category Name must contain letters and spaces only!';
									  
									}
									
							} // URL ID validation required					
								
						
						if(count($error) > 0){
							
								$errorMsg = implode(", ", $error);
								
								// tell the Evidence Category error occur
								$resArr = array("status" => "0", "message" => $errorMsg);
								
								// Merge Array Post And GET Values
										$reqArr = array_merge($data, $getArr);
								
								 //Add log data
								 $logs->Module 		= "Evidence Category";
								 $logs->Action		= "update";
								 $logs->Request		= json_encode($reqArr);
								 $logs->Response	= json_encode($resArr);
								 $logs->UserId		= (int)$user_id;
								 $logs->UserType    = ""; 
								 $logs->AddLog();
									 
								// tell the Evidence Category error occur
								 response($resArr, 400);
							
						} else {
							
							
							$evidence_id_exist = $evidenceCat->evidence_id_exist($evidenceCat->id);
								
							if($evidence_id_exist == true){
							
									$evidence_cat_id = $evidenceCat->update($updateStatus);
									
									if($evidence_cat_id == true){
										
										// generate jwt
										//$access_token = $auth->generateToken($tokenArr);
										
										$resArr = array("status" => "1", 
										"message" => $UpdateMsg,
										"access_token" 	=> $access_token);
										
										// Merge Array Post And GET Values
										$reqArr = array_merge($data, $getArr);
												
										 $logs->Module 		= "Evidence Category";
										 $logs->Action		= "update";
										 $logs->Request		= json_encode($reqArr);
										 $logs->Response	= json_encode($resArr);
										 $logs->UserId		= (int)$user_id;
										 $logs->UserType    = ""; 
										 $logs->AddLog();
										
										// display message: Evidence Category was updated
										 response($resArr, 200);
									 
									 } else { // message if unable to Evidence Category
									 
									 // generate jwt
										//$access_token = $auth->generateToken($tokenArr);
										
											// display message: unable to update Evidence
											$resArr = array("status" => "0", 
											"message" => "Unable to update data.",
											"access_token" 	=> $access_token);
											
											// Merge Array Post And GET Values
											$reqArr = array_merge($data, $getArr);
											
											 $logs->Module 		= "Evidence Category";
											 $logs->Action		= "update";
											 $logs->Request		= json_encode($reqArr);
											 $logs->Response	= json_encode($resArr);
											 $logs->UserId		= (int)$user_id;
											 $logs->UserType    = ""; 
											 $logs->AddLog();
											
											 // display message: unable to update Evidence Category
											 response($resArr, 400);
											
										} // Insert ID Check
										
							} else {
								
								
								// generate jwt
								//$access_token = $auth->generateToken($tokenArr);
							
								$resArr = array(
										"status" 		=> "0", 
										"message" 		=> "Evidence Category ID does not exist.",
										"access_token" 	=> $access_token
									);
									
									// Merge Array Post And GET Values
								$reqArr = array_merge($data, $getArr);	
									
								 $logs->Module 		= "Evidence Category";
								 $logs->Action		= "update";
								 $logs->Request		= json_encode($reqArr);
								 $logs->Response	= json_encode($resArr);
								 $logs->UserId		= (int)$user_id;
								 $logs->UserType    = ""; 
								 $logs->AddLog();	
									
								response($resArr, 200);
								
							}
								
						}
					
					} else {
						
						// display message: unable to update evidenceCat
						$resArr = array("status" => "0", "message" => $data["requestData"]);
						
						// Merge Array Post And GET Values
								$reqArr = array_merge($data, $getArr);
						
						 $logs->Module 		= "Evidence Category";
						 $logs->Action		= "update";
						 $logs->Request		= json_encode($reqArr);
						 $logs->Response	= json_encode($resArr);
						 $logs->UserId		= (int)$user_id;
						 $logs->UserType    = ""; 
						 $logs->AddLog();
						 
						response($resArr, 400);
					}
		
		} else {
				
						// show error message
						$resArr = array(
							"status" 	=> "0", 
							"message" 	=> "Invalid Authorization. Error : ". $decoded["TokenData"]
						);
						
						// Merge Array Post And GET Values
						$reqArr = array_merge($data, $getArr);
						
						//Add log data
						 $logs->Module 		= "Evidence Category";
						 $logs->Action		= "update";
						 $logs->Request		= json_encode($reqArr);
						 $logs->Response	= json_encode($resArr);
						 $logs->UserId		= "";
						 $logs->UserType    = "";	
						 $logs->AddLog();
						
						response($resArr, 401);
				}
		
			}  else { // show error message if jwt is empty
		 
			// tell the user access denied
			$resArr = array("status" => "0", "message" => "Access denied.");
			
			//Add log data
			 $logs->Module 		= "Evidence Category";
			 $logs->Action		= "update";
			 $logs->Request		= "";
			 $logs->Response	= json_encode($resArr);
			 $logs->UserId		= "";
			 $logs->UserType	= "";			 
			 $logs->AddLog();
			
			// display message: unable to update Category master
			response($resArr, 401);
		}		
		
		
		break;	
		
	case 'GET':
	
	$bearerToken = getBearerToken();
		
		$access_token = isset($bearerToken) ? $bearerToken : "";
			
		// if jwt is not empty
		if($access_token){
			
			// decode jwt		
			$decoded  = $auth->validateToken($access_token);	
			
			$getArr = array();
			
			 // if decode succeed, show user details
			if($decoded["status"] == true) {
				
				$user_id 		= $decoded['TokenData']->data->user_id; // Decode ID
				$username 		= $decoded['TokenData']->data->username; // Username
				$role_id 		= $decoded['TokenData']->data->role_id; // Role ID
				$tokenArr 		= array("user_id" => (int)$user_id, 
										"username" => $username, 
										"role_id" => (int)$role_id
										);	
			
				$getArr = get_url_data();					
				
				$id 	= isset($getArr['id']) ? intval($getArr['id']) : 0;
				$type 	= isset($getArr['type']) ? $getArr['type'] : "";
				
				$getList = false;
				
				if($id > 0 && $type == 'evidenceSubcategory'){
					
					$getList = $evidenceCat->getSublisting($id);					
					$resMsg = "Evidence Sub Category data get successfully.";
					
				}  else {
					
					if($id == ""){ 
				
						$getList = $evidenceCat->getlisting();
						$resMsg = "Evidence Category data get successfully.";
					}
					
					if($id > 0){
						
					$evidence_id_exist = $evidenceCat->evidence_id_exist($id);
					
						if($evidence_id_exist == true){
							
							$getList = $evidenceCat->getlisting($id);
							$resMsg = "Evidence Category data get successfully.";
							
						} else {
							
							// generate jwt
							//$access_token = $auth->generateToken($tokenArr);
							
							$resArr = array(
									"status" 		=> "0", 
									"message" 		=> "Evidence Category ID does not exist.",
									"access_token" 	=> $access_token
								);
								
							 $logs->Module 		= "Evidence Category";
							 $logs->Action		= "get";
							 $logs->Request		= json_encode($getArr);
							 $logs->Response	= json_encode($resArr);
							 $logs->UserId		= (int)$user_id;
							 $logs->UserType    = "";   
							 $logs->AddLog();	
								
							response($resArr, 200);
						}
						
					}	
					
				}
				
					
				
				if($getList){
					
					// generate jwt
					//$access_token = $auth->generateToken($tokenArr);
							
					$resArr = array(
						"status" 		=> "1", 
						"message" 		=> $resMsg,
						"access_token" 	=> $access_token,
						"data" 			=> $getList
					);

					 $logs->Module 		= "Evidence Category";
					 $logs->Action		= "get";
					 $logs->Request		= json_encode($getArr);
					 $logs->Response	= json_encode($resArr);
					 $logs->UserId		= (int)$user_id;
					 $logs->UserType    = ""; 		
					 $logs->AddLog();		
					
					response($resArr, 200);
					
				} else {
					
					$getList = array();
					
					// generate jwt
					//$access_token = $auth->generateToken($tokenArr);
					
					$resArr = array(
						"status" 		=> "0", 
						"message" 		=> "No data found.",
						"access_token" 	=> $access_token,
						"data" 			=> $getList
					);

					 $logs->Module 		= "Evidence Category";
					 $logs->Action		= "get";
					 $logs->Request		= json_encode($getArr);
					 $logs->Response	= json_encode($resArr);
					 $logs->UserId		= (int)$user_id;
					 $logs->UserType    = "";   	 	
					 $logs->AddLog();		
					
					response($resArr, 200);
					
				}
				
				
	} else {			 
				
				// show error message
				$resArr =array(
					"status" 	=> "0", 
					"message" 	=> "Invalid Authorization. Error : ". $decoded["TokenData"]
				);
				
				//Add log data
				 $logs->Module 		= "Evidence Category";
				 $logs->Action		= "get";
				 $logs->Request		= "";
				 $logs->Response	= json_encode($resArr);
				 $logs->UserId		= "";
				 $logs->UserType	= "";	
				 $logs->AddLog();
				
				response($resArr, 401);
			}			
							
		}  else { // show error message if jwt is empty		 
			
			$resArr = array("status" => "0", "message" => "Access denied.");
			
			//Add log data
			 $logs->Module 		= "Evidence Category";
			 $logs->Action		= "get";
			 $logs->Request		= "";
			 $logs->Response	= json_encode($resArr);
			 $logs->UserId		= "";
			 $logs->UserType	= "";	
			 $logs->AddLog();
			
			response($resArr, 401);
		}	
		
		break;
		
		
	case 'DELETE':	
	
		$bearerToken = getBearerToken();
		
		$access_token = isset($bearerToken) ? $bearerToken : "";
	
		// if jwt is not empty
		if($access_token)
		{
			
			// decode jwt
			$decoded = $auth->validateToken($access_token);
	
			$getArr = array();
			
			// if decode succeed, show user details
			if($decoded["status"] == true) {
					
				$user_id 		= $decoded['TokenData']->data->user_id; // Decode ID
				$username 		= $decoded['TokenData']->data->username; // Username
				$role_id 		= $decoded['TokenData']->data->role_id; // Role ID
				$tokenArr 		= array("user_id" => (int)$user_id, 
										"username" => $username, 
										"role_id" => (int)$role_id
										);	
			
						$getArr = get_url_data();
									
						// set EvidenceCategory property values	
						$evidenceCat->id = isset($getArr['id']) ? intval($getArr['id']) : "";
						
						$error = array();
						
						if($evidenceCat->id == ""){
										
							$error[] = "Evidence Category ID is required.";
							
						} 
						
						if(!is_numeric($evidenceCat->id)){
							
							$error[] = "Evidence Category ID is must contain number.";
							
						}
						
						if(count($error) > 0){
								
						$errorMsg = implode(", ", $error);
							 
						// tell the user login failed
						$resArr = array("status" => "0", "message" => $errorMsg);						

						//Add log data
						 $logs->Module 		= "Evidence Category";
						 $logs->Action		= "delete";
						 $logs->Request		= json_encode($getArr);
						 $logs->Response	= json_encode($resArr);
						 $logs->UserId		= (int)$user_id;
						 $logs->UserType    = ""; 
						 $logs->AddLog();

						response($resArr, 200);
						 
					 } else {
						 
						 
						 $evidence_id_exist = $evidenceCat->evidence_id_exist($evidenceCat->id);
							
						if($evidence_id_exist == true){
						 
						 $delete_cat = $evidenceCat->delete();
											
							if($delete_cat == true){

								// generate jwt
								//$access_token = $auth->generateToken($tokenArr);
									 
								$resArr = array(
												"status" 		=> "1", 
												"message" 		=> "Evidence Type was successfully deleted.",
												"access_token" 	=> $access_token
											);			
								
								//Add log data
								 $logs->Module 		= "Evidence Category";
								 $logs->Action		= "delete";
								 $logs->Request		= json_encode($getArr);
								 $logs->Response	= json_encode($resArr);
								 $logs->UserId		= (int)$user_id;
								 $logs->UserType    = ""; 
								 $logs->AddLog();
					
								response($resArr, 200);
								
							 } else {
								 
								  // generate jwt
								 //$access_token = $auth->generateToken($tokenArr);
								 
								 $resArr = array(
												"status" 		=> "1", 
												"message" 		=> "Unable to delete evidence category.",
												"access_token" 	=> $access_token
											);
											
								//Add log data
								 $logs->Module 		= "Evidence Category";
								 $logs->Action		= "delete";
								 $logs->Request		= json_encode($getArr);
								 $logs->Response	= json_encode($resArr);
								 $logs->UserId		= (int)$user_id;
								 $logs->UserType    = ""; 
								 $logs->AddLog();		
									 
								response($resArr, 401);
								 
							 }	// delete condition end	
							 
							 
							 } else {
								
								// generate jwt
								//$access_token = $auth->generateToken($tokenArr);								
										 
								$resArr = array(
									"status" 		=> "0", 
									"message" 		=> "Evidence Category ID does not exist.",
									"access_token" 	=> $access_token
								);
								
								 $logs->Module 		= "Evidence Category";
								 $logs->Action		= "get";
								 $logs->Request		= json_encode($getArr);
								 $logs->Response	= json_encode($resArr);
								 $logs->UserId		= (int)$user_id;
								 $logs->UserType    = "";   
								 $logs->AddLog();	
					
								response($resArr, 200);
								
							}
						 
					 }
	
		 } else {
					 
						// set response code				
						$resArr = array(
									"status" 	=> "0", 
									"message" 	=> "Invalid Authorization. Error : ". $decoded["TokenData"]
								);
							
						//Add log data
						 $logs->Module 		= "Evidence Category";
						 $logs->Action		= "delete";
						 $logs->Request		= json_encode($getArr);
						 $logs->Response	= json_encode($resArr);
						 $logs->UserId		= "";
						 $logs->UserType	= "";
						 $logs->AddLog();			
										 
						response($resArr, 401);
					}
			 
		}  else { // show error message if jwt is empty
			 
				// tell the City access denied
				$resArr = array("status" => "0", "message" => "Access denied.");
				
				//Add log data
				 $logs->Module 		= "Evidence Category";
				 $logs->Action		= "delete";
				 $logs->Request		= "";
				 $logs->Response	= json_encode($resArr);
				 $logs->UserId		= "";
				 $logs->UserType	= "";
				 $logs->AddLog();
							 
				response($resArr, 401);
				
		}
		
		
		break;
		
	case "OPTIONS": //Handle Preflighted requests for CORS issue
		
		// set response code		
		header('Content-Type: text/plain');
		
		response("", 200);
		
		break;
		
	default:
	
		// Invalid Request Method
		$resArr = array("status" => "0", "message" => "Method Not Allowed.");						
		// display message: unable to create user
		response($resArr, 405);	
		
		break;
}

?>