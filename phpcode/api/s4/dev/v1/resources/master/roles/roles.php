<?php
/*** 'Roles' object ***/
class Roles{
 
    /*** database connection and table name ***/
    private $aconn;
    private $table_name = 'public."AspNetRoles"';
	
    /*** object properties ***/
	public $id;
    public $role_name;
	public $is_active;
 
    /*** constructor ***/
    public function __construct($db){
        $this->aconn = $db;
		$this->createdOn = date("Y-m-d H:i:s");
    }
	
		/*** check if given Role ID exist in the database ***/
	public function role_id_exist($id){
	 
		/*** query to check if Role ID exists ***/
		$query = 'SELECT "Id"
					FROM '.$this->table_name.'
					WHERE "Id" = ? 
					LIMIT 1';
					
		/*** prepare the query ***/
		$stmt = $this->aconn->prepare( $query );
	 
		/*** bind given Role ID  value ***/
		$stmt->bindParam(1, $id);
		//$stmt->bindValue(2, false, PDO::PARAM_BOOL);
		/*** execute the query ***/
		$stmt->execute();
	 
		/*** get number of rows ***/
		$num = $stmt->rowCount();
	 
		/*** if Role ID  exists, assign values to object properties for easy access and use for php sessions ***/
		if($num > 0){
	 
			/*** return true because Role ID exists in the database ***/
			return true;
		}
	 
		/*** return false if Role ID does not exist in the database ***/
		return false;
	}
	
	public function getlisting($id = 0){
		
		/*** query to check if Roles exists ***/
		if($id == 0 ){
			
			$query = 'SELECT "Id", "Name", "NormalizedName"
					FROM '.$this->table_name.' 
					ORDER BY "Id" ASC';	
					
			/*** prepare the query ***/
			$stmt = $this->aconn->prepare( $query );
			
		} else {
			
			$query = 'SELECT "Id", "Name", "NormalizedName"
					FROM '.$this->table_name.' 
					WHERE "Id" = ? ORDER BY "Id" ASC';
			
			/*** prepare the query ***/
			$stmt = $this->aconn->prepare( $query );
			
			$stmt->bindValue(1, $id);
						
		}
		
		/*** execute the query ***/
		$stmt->execute();
	 
		/*** get number of rows ***/
		$num = $stmt->rowCount();
	 
		/*** if Roles exists, assign values to object properties for easy access and use for php sessions ***/
		if($num>0){
	 
			/*** get record details / values ***/			
			while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
					$getData[] = $row;
			}
	 
			/*** return true because Roles exists in the database ***/
			return $getData;
		}
	 
		/*** return false if Roles does not exist in the database ***/
		return false;
		
	}
	
	public function create(){
		
		 $query = 'INSERT INTO '.$this->table_name.' ("Name") VALUES (:Name)';		
		
		/*** prepare the query ***/
		$stmt = $this->aconn->prepare($query);
				
		$stmt->bindParam(':Name', $this->role_name);
	 
		/*** execute the query, also check if query was successful ***/		
		if($stmt->execute()){
			
			$result = $stmt->fetch(PDO::FETCH_ASSOC);
						
			//return $result["Id"];
			return true;
		} 
		
		//return false;
		
	}
	
	
	/*** update record ***/
	public function update($updateStatus){
		
		/*** SQL QUERY ***/
		$query = 'UPDATE ' . $this->table_name . ' SET ';
		
		if($updateStatus == "UpdateRole"){
			
			$query .='"Name" = :Name, "updated_on" = :updated_on WHERE "Id" = :Id';
			
		} else {
			
			$query .='"is_active" = :is_active, "updated_on" = :updated_on WHERE "Id" = :Id';
		}

		//echo $query;exit;
		
		/*** prepare the query ***/
		$stmt = $this->aconn->prepare($query);
		$this->updated_on		=	$this->createdOn;				 
		
		if($updateStatus == "UpdateRole"){
			
			/*** bind the values from the form ***/		
			$stmt->bindParam(':Name', $this->role_name);
			
		} else {
			
			$stmt->bindParam(':is_active', $this->is_active);
		} 
		
		$stmt->bindParam(':updated_on', $this->updated_on);
		
		/*** unique ID of record to be edited ***/
		$stmt->bindParam(':Id', $this->id);
	 
		/*** execute the query ***/
		if($stmt->execute()){
			
			return true;
		}
	 
		return false;
		
	} // Function End
	
	
	function delete(){
		
		$query = 'UPDATE ' . $this->table_name . '
					  SET
						"deleted_on"	 	= :deleted_on,
						"is_deleted"	= :is_deleted
					  WHERE "Id" 	= :Id';
					  
		$stmt = $this->aconn->prepare($query);	
		
		$this->is_deleted	=	'1';
		$this->deleted_on  	=	date("Y-m-d H:i:s");
		
		$stmt->bindParam(':is_deleted', $this->is_deleted);
		$stmt->bindParam(':deleted_on', $this->deleted_on);
		
		$stmt->bindParam(':Id', $this->id);
		
		if($stmt->execute()){
			
			return true;
		}
	 
		return false;
			
	}
	
}