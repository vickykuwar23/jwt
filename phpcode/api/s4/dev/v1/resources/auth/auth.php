<?php
// Required to encode json web token
include_once LIB_PATH.'php-jwt-master/src/BeforeValidException.php';
include_once LIB_PATH.'php-jwt-master/src/ExpiredException.php';
include_once LIB_PATH.'php-jwt-master/src/SignatureInvalidException.php';
include_once LIB_PATH.'php-jwt-master/src/JWT.php';
use \Firebase\JWT\JWT;
class Auth{
	
	// object properties   
    public $tokenArr;
	public $iss;
	public $aud;
	public $iat;
	public $nbf;
	public $exp;
	public $jti;
	public $key;
	public $token_duration;
	 /*** constructor ***/
    public function __construct(){
	   	
		include_once CONFIG_PATH.'core.php';	
		
	    $this->iss = $iss;
		$this->aud = $aud;
		$this->iat = $iat;
		$this->nbf = $nbf;
		$this->exp = $exp;
		$this->jti = $jti;
		$this->key = $key;
		$this->token_duration = $token_duration;
    }
	
	//generate
	public function generateToken($tokenArr){
		
		$tokenExp = $this->token_duration*60;
		
		$token = array(
				   "iss" => $this->iss,
				   "aud" => $this->aud,
				   "iat" => $this->iat,
				   "nbf" => $this->nbf,
				   "exp" => $this->exp,
				   "jti" => $this->jti,
				   "data" => $tokenArr
				);
				
		// generate jwt
		$jwt = JWT::encode($token, $this->key);		
				
		return 	$jwt;		
		
	}
	
	public function validateToken($jwt){
		
		$TokenData  = array();

		try {			
			// decode jwt
			$decoded = JWT::decode($jwt, $this->key, array('HS256'));			
			$TokenData  = array("status" => true, "TokenData" => $decoded);
		} 

		 // if decode fails, it means jwt is invalid
			catch (Exception $e){
			 
				// set response code
				http_response_code(401);			 
				
				$TokenData  = array("status" => false, "TokenData" => $e->getMessage());
			}	
		return $TokenData;	
	}
	
}
?>