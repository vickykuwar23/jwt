<?php
/*** 'user' object ***/
class Permission{
 
    /*** database connection and table name ***/
    private $aconn;
    private $table_name = 'public."AspNetRolePermissions"';
	
    /*** object properties ***/
    public $permission_id;
    public $apiname;
    public $controller_name;
    public $action_name;
	public $is_active;
 
    /*** constructor ***/
    public function __construct($db){
        $this->aconn = $db;
		$this->createdOn = date("Y-m-d H:i:s");
    }	
	
	function get(){
		
		/*** query to check if email exists ***/							
		
			
			$query = 'SELECT "permission_id", "apiname", "controller_name", "action_name", 	"is_active"
						FROM '.$this->table_name.' 
						WHERE "is_active" = ? ORDER BY "permission_id" ASC';
	 
		/*** prepare the query ***/
		$stmt = $this->aconn->prepare( $query );		
	 
		$stmt->bindValue(1, true, PDO::PARAM_BOOL);
		
		/*** execute the query ***/
		$stmt->execute();
	 
		/*** get number of rows ***/
		$num = $stmt->rowCount();
	 
		/*** if email exists, assign values to object properties for easy access and use for php sessions ***/
		if($num>0){
	 
			/*** get record details / values ***/			
			while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {					
					//$controller_name 	= $row['controller_name'];
					//$permissionData[$row['controller_name']][] = $row['action_name'];
					$controller_name 	= $row['controller_name'];
					$permissionData[$row['controller_name']][] = array($row['permission_id'] => $row['action_name']);
			}
	 
			/*** return true because email exists in the database ***/
			return $permissionData;
		}
	 
		/*** return false if email does not exist in the database ***/
		return false;
		
	}
	
}