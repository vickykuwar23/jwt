<?php
/*** files needed to connect to database and class***/
define("ROOT_PATH", realpath(__DIR__ . '/../../..'));
include_once ROOT_PATH.'/constant.php';

include_once CONFIG_PATH.'functions.php';
include_once CONFIG_PATH.'database.php';
include_once RESOURCE_PATH.'permission/permission.php';
include_once RESOURCE_PATH.'logs/logs.php';
include_once RESOURCE_PATH.'auth/auth.php';

//include_once LIB_PATH.'eCos/eCos.php';

// Required headers
requireHeaders();
 
/*** get database connection ***/
$database 	= new Database();
$db 		= $database->getAuthConnection();
$mdb 		= $database->getMongoConnection();
 
// instantiate product object
$permission  = new Permission($db);
$logs  = new Logs($mdb);
$auth  = new Auth();

// Get Request Method 
$request_method		= get_request_method();

switch($request_method)
{
		
	case 'GET':	
			
		$bearerToken = getBearerToken();
		
		$access_token = isset($bearerToken) ? $bearerToken : "";
		
		// if jwt is not empty
		if($access_token){
			
			// decode jwt		
			$decoded  = $auth->validateToken($access_token);
			
				$getArr = array();
				
				// if decode succeed, show user details
				if($decoded["status"] == true) {
					
				$user_id 		= $decoded['TokenData']->data->user_id; // Decode ID
				$username 		= $decoded['TokenData']->data->username; // Username
				$role_id 		= $decoded['TokenData']->data->role_id; // Role ID
				$tokenArr 		= array("user_id" => (int)$user_id, 
										"username" => $username, 
										"role_id" => (int)$role_id
										);		
				
				$userListing = $permission->get();
							
					
				 
				if($userListing){
					
					// generate jwt
					//$access_token = $auth->generateToken($tokenArr);
					
					// set response code
					$resArr = array(
								"status" 		=> "1", 
								"message" 		=> "Permission data found.",
								"access_token" 	=> $access_token,
								"data" 			=> $userListing
							);
						
							
						//Add log data
						 $logs->Module 		= "Permission";
						 $logs->Action		= "get";
						 $logs->Request		= json_encode($getArr);
						 $logs->Response	= json_encode($resArr);
						 $logs->UserId		= (int)$user_id;
						 $logs->UserType    = ""; 
						 $logs->AddLog();
							
						response($resArr, 200);
						
				} else { // message if unable to create user

					$userListing = array();	
					
					// generate jwt
					//$access_token = $auth->generateToken($tokenArr);	
					
					$resArr = array(
								"status" 		=> "0", 
								"message" 		=> "No data found.",
								"access_token" 	=> $access_token,
								"data"			=> $userListing
							);
								
					//Add log data
					 $logs->Module 		= "Permission";
					 $logs->Action		= "get";
					 $logs->Request		= json_encode($getArr);
					 $logs->Response	= json_encode($resArr);
					 $logs->UserId		= (int)$user_id;;
					 $logs->UserType    = ""; 					 
					 $logs->AddLog();	
							
					response($resArr, 200);
					
				}
				
					} else {			 
				
				// show error message
				$resArr =array(
					"status" 	=> "0", 
					"message" 	=> "Invalid Authorization. Error : ". $decoded["TokenData"]
				);
				
				//Add log data
				 $logs->Module 		= "Permission";
				 $logs->Action		= "get";
				 $logs->Request		= "";
				 $logs->Response	= json_encode($resArr);
				 $logs->UserId		= "";
				 $logs->UserType	= "";	
				 $logs->AddLog();
				
				response($resArr, 401);
			}			
				
				
		}  else { // show error message if jwt is empty		 
			
			$resArr = array("status" => "0", "message" => "Access denied.");
			
			//Add log data
			 $logs->Module 		= "Permission";
			 $logs->Action		= "get";
			 $logs->Request		= "";
			 $logs->Response	= json_encode($resArr);
			 $logs->UserId		= "";
			 $logs->UserType	= "";	
			 $logs->AddLog();
			
			response($resArr, 401);
		}
		
		break;
	
		
	case "OPTIONS": //Handle Preflighted requests for CORS issue
		
		// set response code		
		header('Content-Type: text/plain');
		
		response("", 200);
		
		break;
		
	default:
	
		// Invalid Request Method
		$resArr = array("status" => "0", "message" => "Method Not Allowed.");						
		// display message: unable to create user
		response($resArr, 405);	
		
		break;
}


?>