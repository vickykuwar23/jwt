<?php
// Required to encode json web token
include_once LIB_PATH.'RabbitMQ/RMQ.php';

class Logs{
 
    // database mconnection and table name
    private $mconn;
	private $DbName = "TAMS";
    private $TableName = "ActivityLogs";
	private $UserAgent;
	private $IP;
	private $CreatedOn;
 
    // object properties
    public $id;
    public $Module;
    public $Action;
    public $Response;
	public $Request;
	public $UserId;
	public $UserType;
 
    // constructor
    public function __construct($db){
        $this->mconn = $db;
    }
	
	// Get User Agent
	public function GetUserAgent(){
		$this->IP = $_SERVER['REMOTE_ADDR'];
	}
	
	// Get User Agent
	public function GetUserIP(){
		$this->UserAgent = $_SERVER['HTTP_USER_AGENT'];
	}
	
	// Get Created On
	public function GetCreatedOn(){
		$this->CreatedOn = date("Y-m-d H:i:s");
	}
	
	// Publish log message to RMQ
	public function AddLog(){
		
		$this->GetUserAgent();
		$this->GetUserIP();
		$this->GetCreatedOn();			
		
		$LogData = array('Module' => $this->Module, 'Action' => $this->Action, 'Request' => $this->Request, 'Response' => $this->Response, "UserId" => $this->UserId, "UserType" => $this->UserType, "CreatedOn" => $this->CreatedOn, "IP" => $this->IP, "UserAgent" => $this->UserAgent);
		
		//echo "#####".json_encode($LogData); die();
		
		$rmq = new RMQ();
		$rmq->publish(json_encode($LogData));
    }
	
	// Consume log message from RMQ
	public function ListenLogs(){
		
		$callback = function ($msg) {

			//echo "********".$msg->body; die();
				
			$data = json_decode($msg->body);
			$this->Add($data);			
		};
		
		$rmq = new RMQ();
		$rmq->consume($callback);
    }
 
	// Add new log to database
	public function Add($LogData){
		
		$rows = NULL;
		
		try {
			
			$query = new MongoDB\Driver\BulkWrite;
			$query->insert($LogData);
			$rows = $this->mconn->executeBulkWrite($this->DbName.".".$this->TableName, $query);
			
			/*foreach ($rows as $row) {
				print_r($row);
			}*/
			
		} catch (MongoDB\Driver\Exception\Exception $e) {
		
			//echo "It failed with the following exception:\n";			
			echo "Exception:", $e->getMessage(), "\n";
			//echo "In file:", $e->getFile(), "\n";
			//echo "On line:", $e->getLine(), "\n"; 
		}
 
        return $rows;
    }
	
	// Get Logs
    public function GetLogs($UserId = 0){
		
		$rows = NULL;
		
		$filter = array("UserType" => 'User');
		$options = array('sort' => array('_id' => -1));	// 1 >> ascending and -1 >> descending
		
		try {
			
			if($UserId > 0){
				
				$filter['UserId'] = (int)$UserId;
				
			}	
			
			$query = new MongoDB\Driver\Query($filter, $options); 
			 
			$result = $this->mconn->executeQuery($this->DbName.".".$this->TableName, $query);
			
			foreach ($result as $row) {
				
				$rows[] = $row;
			}
			
		} catch (MongoDB\Driver\Exception\Exception $e) {
		
			//echo "It failed with the following exception:\n";			
			echo "Exception:", $e->getMessage(), "\n";
			//echo "In file:", $e->getFile(), "\n";
			//echo "On line:", $e->getLine(), "\n"; 
		}
 
        return $rows;
    }
	
}
?>