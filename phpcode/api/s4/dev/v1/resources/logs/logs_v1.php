<?php
class Logs{
 
    // database mconnection and table name
    private $mconn;
	private $DbName = "pilotnijis";
    private $TableName = "ActivityLogs";
	private $UserAgent;
	private $IP;
	private $CreatedOn;
 
    // object properties
    public $id;
    public $Module;
    public $Action;
    public $Data;
	public $UserId;
	public $UserType;
 
    // constructor
    public function __construct($db){
        $this->mconn = $db;
    }
	
	// Get User Agent
	public function GetUserAgent(){
		$this->IP = $_SERVER['REMOTE_ADDR'];
	}
	
	// Get User Agent
	public function GetUserIP(){
		$this->UserAgent = $_SERVER['HTTP_USER_AGENT'];
	}
	
	// Get Created On
	public function GetCreatedOn(){
		$this->CreatedOn = date("Y-m-d H:i:s");
	}
 
	// Add new log
	public function AddLog(){
		
		$rows = NULL;
		
		try {
	
			$this->GetUserAgent();
			$this->GetUserIP();
			$this->GetCreatedOn();			
			
			$LogData = array('Module' => $this->Module, 'Action' => $this->Action, 'Data' => $this->Data, "UserId" => $this->UserId, "UserType" => $this->UserType, "CreatedOn" => $this->CreatedOn, "IP" => $this->IP, "UserAgent" => $this->UserAgent);
			
			$query = new MongoDB\Driver\BulkWrite;
			$query->insert($LogData);
			$rows = $this->mconn->executeBulkWrite($this->DbName.".".$this->TableName, $query);
			
			/*foreach ($rows as $row) {
				print_r($row);
			}*/
			
		} catch (MongoDB\Driver\Exception\Exception $e) {
		
			//echo "It failed with the following exception:\n";			
			echo "Exception:", $e->getMessage(), "\n";
			//echo "In file:", $e->getFile(), "\n";
			//echo "On line:", $e->getLine(), "\n"; 
		}
 
        return $rows;
    }
	
	// Get Logs
    public function GetLogs($UserId = 0){
		
		$rows = NULL;
		
		$filter = array("UserType" => 'User');
		$options = array();
		
		try {
			
			if($UserId > 0){
				
				$filter['UserId'] = (int)$UserId;
				
			}	
			
			$query = new MongoDB\Driver\Query($filter, $options); 
			 
			$result = $this->mconn->executeQuery($this->DbName.".".$this->TableName, $query);
			
			foreach ($result as $row) {
				
				$rows[] = $row;
			}
			
		} catch (MongoDB\Driver\Exception\Exception $e) {
		
			//echo "It failed with the following exception:\n";			
			echo "Exception:", $e->getMessage(), "\n";
			//echo "In file:", $e->getFile(), "\n";
			//echo "On line:", $e->getLine(), "\n"; 
		}
 
        return $rows;
    }
}
?>