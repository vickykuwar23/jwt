<?php
require_once __DIR__ . '/vendor/autoload.php';

use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

$connection = new AMQPStreamConnection('103.249.98.164', 5672, 'nijis', 'supp0rt#N1jIS');
$channel = $connection->channel();

$msg = new AMQPMessage('Hello World '.rand (0,1000000));
$channel->basic_publish($msg, '', 'nijisphp');

$channel->close();
$connection->close();
?>